//
//  CMChatManager.h
//  CMChatFramework
//
//  Created by Saurabh Verma on 30/12/10.
//  Copyright (c) 2013 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMStream.h"
#import "CMChatFrameworkConstants.h"
#import "CMMediaTransferHandler.h"
#import "CMChatFramework.h"

@class CMRoomMessageCoreDataStorageObject;
@class CMMessageArchiving_Message_CoreDataObject;

// Singelton Class
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Declaring a Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@protocol CMChatManagerDelegate;
@protocol CMChatManagerRosterDelegate;
@protocol CMChatManagerActiveChatDelegate;
@protocol CMChatManagerMessagesDelegate;

@interface CMChatManager : NSObject <CMStreamDelegate>
{
    __weak id <CMChatManagerDelegate> delegate;
    __weak id <CMChatManagerRosterDelegate> rosterDelegate;
    __weak id <CMChatManagerActiveChatDelegate> activeChatDelegate;
    __weak id <CMChatManagerMessagesDelegate> messagesDelegate;
}

@property (nonatomic,weak)id <CMChatManagerDelegate> delegate;
@property (nonatomic,weak)id <CMChatManagerRosterDelegate> rosterDelegate;
@property (nonatomic,weak)id <CMChatManagerActiveChatDelegate> activeChatDelegate;
@property (nonatomic,weak)id <CMChatManagerMessagesDelegate> messagesDelegate;

+ (CMChatManager*)sharedInstance;

- (CMStream *)getStreamForHost:(NSString *)hostName;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Add JID Account
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @details connect on the host with jid and access token.
 * @param hostName: string name of the targeted host.
 * @param JID: associated user jid
 * @param accessToken: access token for the host.
 **/
- (void)connectAccountWithHost:(NSString *)hostName forJID:(NSString*)JID accessToken:(NSString*)accessToken;

/**
 * @details connect on the host with jid and access token.
 * @param hostName: string name of the targeted host.
 * @param port: port number to connect
 * @param JID: associated user jid
 * @param accessToken: access token for the host.
 **/
- (void)connectAccountWithHost:(NSString *)hostName atPort:(NSString *)port forJID:(NSString*)JID accessToken:(NSString*)accessToken;

/**
 * @details disconnect the services on the host.
 * @param hostName: string name of the targeted host.
 **/
- (void)disconnectAccountWithHost:(NSString*)hostName;

/**
 * @details go online on the host.
 * @param hostName: string name of the targeted host.
 **/
- (void)goOnlineForHost:(NSString *)hostName;

/**
 * @details go offline on the host.
 * @param hostName: string name of the targeted host.
 **/
- (void)goOfflineForHost:(NSString *)hostName;

/**
 * @details go away on the host.
 * @param hostName: string name of the targeted host.
 **/
- (void)goAwayForHost:(NSString*)hostName;

/**
 * @details go busy on the host.
 * @param hostName: string name of the targeted host.
 **/
- (void)goBusyForHost:(NSString*)hostName;

/**
 * @details Register device token on server for APNS.
 * @param hostName: string name of the targeted host.
 * @param token: device token string to register.
 **/
- (void)token:(NSString *)token registerPushForHost:(NSString *)hostName;

/**
 * @details send media message of given type to a given jid on a host.
 * @param data: data of the media
 * @param type: type of the media (mov or png).
 * @param uniqueTag: tag to uniquely identify the message.
 * @param hostName: string name of the targeted host.
 * @param jid: JID of the targeted user.
 **/
- (void)media:(NSData *)data ofType:(NSString *)mediaType withTag:(NSInteger )uniqueTag transferMediaForHost:(NSString *)hostName toJID:(NSString *)toJID;

/**
  * @details send media message of given type to a given jid on a host.
  * @param data: data of the media
  * @param type: type of the media (mov or png).
  * @param uniqueTag: tag to uniquely identify the message.
  * @param hostName: string name of the targeted host.
  * @param jid: JID of the targeted user.
  * @param previewImage: small thumbnail to pass in base 64(Valid only for openfire host, not in use otherwise).
  **/
- (void)media:(NSData *)data ofType:(NSString *)mediaType withTag:(NSInteger )uniqueTag transferMediaForHost:(NSString *)hostName toJID:(NSString *)toJID previewImage:(UIImage *)previewImage;

/**
 * @details start downloading attached media to a message if it is not downloaded.
 * @param message: media message to download the media.
 **/
- (void) startDownloadingForMediaMessage:(CMMessageArchiving_Message_CoreDataObject *)message;

/**
 * @details returns the most recent messages associated with a given jid in the host.
 * @param hostName: string name of the targeted host.
 * @param jid: JID of the targeted user.
 * @param limit: limit the number of recent messages to fetch., 0 to get all the messages.
 * @return array of CMMessageArchiving_Message_CoreDataObject objects.
 **/
- (NSArray*)getMessagesWithHost:(NSString*)hostName forJID:(NSString*)JID limit:(NSInteger)limit;

/**
 * @details delete all messages history in given host associated with a given jid.
 * @param hostName: string name of the targeted host.
 * @param jid: JID of the targeted user.
 **/
- (void)deleteAllMessagesWithHost:(NSString*)hostName forJID:(NSString*)JID;

/**
 * @details delete messages history in given host.
 * @param hostName: string name of the targeted host.
 * @param messages: Array of CMMessageArchiving_Message_CoreDataObject or CMRoomMessageCoreDataStorageObject Message objects to delete from history.
 **/
- (void)deleteMessagesWithHost:(NSString*)hostName messages:(NSArray*)messages;

/**
 * @details Send message  to jid in given host.
 * @param hostName: string name of the targeted host.
 * @param jid: JID of the targeted user.
 * @param message: Message to send.
 **/
- (void)sendMessageWithHost:(NSString*)hostName forJID:(NSString*)JID message:(NSString*)message;
- (void)resendMessageWithHost:(NSString*)hostName failedMessage:(CMMessageArchiving_Message_CoreDataObject *)failedMessage;
//- (CMStream *)getStreamForHost:(NSString *)hostName;
-(void)setShouldStoreMessageblock:(BlockShouldStoreMessage)shouldStoreMessageblock forHost:(NSString *)hostName;

/**
 * @details Send message typing status to jid in given host.
 * @param hostName: string name of the targeted host.
 * @param jid: JID of the targeted user.
 * @param isTyping: Typing started or stopped.
 **/
- (void)sendMessageTypingStatusWithHost:(NSString*)hostName forJID:(NSString*)JID isTyping:(BOOL)isTyping;

/**
 * @details Reset the unread count for given jid
 * @param hostName: string name of the targeted host.
 * @param jid: JID of the targeted user.
 **/
- (void)resetUnreadMessageCountWithHost:(NSString*)hostName forJID:(NSString*)JID;

/**
 * @details Return the unread count for given jid
 * @param hostName: string name of the targeted host.
 * @param jid: JID of the targeted user.
 * @return retun the no. of unread messages
 **/
- (NSUInteger)unreadMessageCountWithHost:(NSString*)hostName forJID:(NSString*)JID;
- (NSUInteger)unreadMessageCountForAllBuddiesWithHost:(NSString*)hostName;
/**
 * @details return if given host is connected.
 * @param hostName: string name of the targeted host.
 * @return BOOL indicating the connectivity of the host.
 **/
- (BOOL)isConnectedHost:(NSString *)hostname;

/**
 * @details return linked JID with given host.
 * @param hostName: string name of the targeted host.
 * @return jid string if given host is connected nil otherwise.
 **/
- (NSString *)myJIDForHost:(NSString *)hostname;

- (void) setupMyVcardInHost:(NSString *)hostName firstName:(NSString *)firstName lastName:(NSString *)lastName phoneNumber:(NSString *)phoneNumber emailID:(NSString *)emailId profilePhoto:(NSData *)photoData;

- (void) setMyBubbleColor:(NSUInteger) bubbleColor forHost:(NSString *)hostName;

 //First Name = First_Name, Last Name = Last_Name, Email Id = Email_Id,Phone number = Phone_Number, User Image(UIImage) = User_Image,Bubble Color = Bubble_Color,
- (void)vcardInfoForHost:(NSString *)host JID:(NSString *)jid completionBlock:(VCardReceivedBlock) vcardReceiveBlock;
/**
 * @details Add new buddy in given host.
 * @param hostName: string name of the targeted host.
 * @param jid: JID of the targeted user.
 * @param nickName: nickname to show in list.
 **/
- (void)addBuddy:(NSString *)newJID inHost:(NSString *) hostName withNickName:(NSString *)nickName;

/**
 * @details Accept buddy request from given host.
 * @param hostName: string name of the targeted host.
 * @param jid: JID of the sender.
 **/
- (void)acceptBuddyRequestInHost:(NSString *)hostname From:(NSString *)fromBuddy;

/**
 * @details Reject buddy request from given host.
 * @param hostName: string name of the targeted host.
 * @param jid: JID of the sender.
 **/
- (void)rejectBuddyRequestInHost:(NSString *)hostname From:(NSString *)fromBuddy;

/**
* @details Remove buddy from given host.
* @param hostName: string name of the targeted host.
* @param jid: JID of the user to remove.
**/
- (void)removeBuddyInHost:(NSString *)hostname withJid:(NSString *)fromBuddy;

/**
 * @details Return user photo from given host.
 * @param hostName: string name of the targeted host.
 * @param jid: JID of the user to get the image
 * @return image if available nil otherwise.
 **/
- (UIImage *)fetchUserPhotoFrom:(NSString *)hostname forUser:(NSString *)jid;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Group Chat
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * @details Virtual group chat is used when user adds the participants from different streams.
 *          Default is NO, is set YES then ChatManager creates different groups for different streams.
 **/
@property (nonatomic, assign) BOOL isVirtualGroupChatEnabled;


/**
 * @details fetch the buddies of a given host.
 * @param hostName: targeted host name
 * @return An array of CMUserCoreDataStorageObject object.
 *
 **/
- (NSArray *)getBuddyListForHostName:(NSString *)hostName;

/**
 * @details fetch the user details for room participant jid.
 * @param jid: participant jid
 * @param host: host name to try
 * @return A CMUserCoreDataObject object attached to room participant jid.
 *
 **/
- (CMUserCoreDataStorageObject *)getUserDetails:(NSString *)host ForRoomParticipantJID:(NSString *)jid;


/**
 * @details fetch the message for active room in given host.
 * @param hostName: targeted host name
 * @param limit: number of messages to fetch.
 * @return An array of CMUserCoreDataObject object.
 *
 **/
- (NSArray*)getMessagesForGroup:(NSString*)roomJIDStr WithHost:(NSString*)hostName :(NSUInteger)limit;

/**
 * @details fetch the buddies in active group of a given host.
 * @param hostName: targeted host name
 * @return An array of participants JID in a group.
 *
 **/
- (NSArray*)getBuddyJIDsForGroup:(NSString*)roomJIDStr WithHost:(NSString*)hostName;

/**
 * @details refresh the active messages and invoke the active message delegate methods.
 *
 **/
- (void) forceRefreshActiveChatMessages;

/**
 * @details Send a message to the active group in given stream.
 * @param hostName: targeted host name
 * @param messageBody: message to send.
 *
 **/
- (void)sendMessageForRoom:(NSString*)roomJIDStr inHost:(NSString *)hostName body:(NSString *)messageBody;

/**
 * @details Creates and activate new room in the given host.
 * @param hostName: string name of the targeted host to create a group (not in use if 'isVirtualGroupChatEnabled' is YES).
 * @param roomName: required if hostname is openfire, for GTalk: not in use
 * @param myNickName: self nick name to set during room joining.
 * @param groupBuddies: array of buddies for given host, to send group join invitation, each object should contains a valid CMUserCoreDatastorageObject.
 * @param buddyInviteMessage: message to send other participants
 *
 * @return a room JID string if room can be created, nil otherwise
 *
 **/
- (NSString *)prepareRoomInHost:(NSString *)hostName withRoomName:(NSString *)roomName andNickName:(NSString *)myNickName withBuddies:(NSArray *)groupBuddies message:(NSString *) buddyInviteMessage;

/**
 * @details joins and activate new room in the given host.
 * @param hostName: string name of the targeted host to create a group (not in use if 'isVirtualGroupChatEnabled' is YES).
 * @param myNickName: self nick name to set during room joining.
 * @param roomJID: roomJIDStr to be sent
 *
 **/
- (void )joinRoomInHost:(NSString *)hostName andNickName:(NSString *)myNickName roomJID:(NSString*)roomJID;

/**
 * @details Invite buddies in the active room.
 * @param hostName: string name of the targeted host (not in use if 'isVirtualGroupChatEnabled' is YES).
 * @param buddies: array of buddies for given host, to send group join invitation, each object should contains a valid CMUserCoreDatastorageObject.
 * @param messagge: message to send other participants
 *
 **/
- (void)inviteBuddiesInRoom:(NSString *)roomJIDStr inHost:(NSString*)hostName buddies:(NSArray *)buddies withMessage:(NSString *)messagge;

/**
 * @details Leaves the room with given jid.
 * @param hostName:string name of the targeted host(not in use if 'isVirtualGroupChatEnabled' is YES).
 * @param roomJIDStr: room jid string.
 *
 **/
- (void)leaveRoomInHost:(NSString *)hostName roomJID:(NSString *) roomJIDStr;

@end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Declaring a Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@protocol CMChatManagerDelegate <NSObject>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


@optional
/**
 * @details This Delegate is called when hostName is successfully authenticated with given JID
 **/
- (void)forHost:(NSString *)hostName didAuthenticateJID:(NSString *)JID;

/**
 * @details This Delegate is called when hostName is failed to authenticate with given JID
 **/
- (void)forHost:(NSString *)hostName didNotAuthenticateJID:(NSString *)JID :(NSError *)error;

/**
 * @details This Delegate is called when stream identified with the hostname will Disconnect. After succesful disconnect, didDisConnect gets called.
 **/
- (void)forHost:(NSString *)hostName willDisConnect:(NSString *)JID;

/**
 * @details This Delegate is called when stream identified with the hostname will connect. After succesful connect, didAuthenticateJID gets called.
 **/
- (void)forHost:(NSString *)hostName willConnect:(NSString *)JID;

/**
 * @details This Delegate is called when stream identified with the hostname is disconnected.
 **/
- (void)forHost:(NSString *)hostName didDisConnect:(NSString *)JID :(NSError *)error;

/**
 * @details This Delegate is called when timout has occured.
 **/
- (void)forHost:(NSString *)hostName didConnectionTimout:(NSString *)JID :(NSError*)error;

/**
 * @details This Delegate is called user receives a new buddy request from hostname.
 **/
- (void)forHost:(NSString *)hostName didReceiveBuddyRequestFrom:(NSString *)from;

/**
 * @details This Delegate is called user receives a new room request from hostname.
 **/
- (void)forHost:(NSString *)hostName didReceiveRoomRequestFromUser:(NSString *)from roomJid:(NSString*)roomJID;

/**
 * @details This Delegate is called when occupant did Join Room from hostname.
 **/
- (void)forHost:(NSString *)hostName occupantDidJoinRoom:(NSString *)roomJID withJID:(NSString*)JID;

/**
 * @details This Delegate is called when occupant did leave Room from hostname.
 **/
- (void)forHost:(NSString *)hostName occupantDidLeaveRoom:(NSString *)roomJID withJID:(NSString*)JID;
@end

@protocol CMChatManagerRosterDelegate <NSObject>
/**
 * @details Gets called when there is a change in Roster Table. It sends back the list of all updated Rosters
 *          sends the array of NSFetchedResultsSectionInfo group by host names
 *          [NSFetchedResultsSectionInfo count] gives numberOfSectionsInTableView
 *          [NSFetchedResultsSectionInfo numberOfObjects] gives numberOfRowsInSection
 *          For, cellForRowAtIndexPath use the following:
 *          id <NSFetchedResultsSectionInfo> sectionInfo = [self.feedResults objectAtIndex:indexPath.section];
 *          CMUserCoreDataStorageObject *user = [sectionInfo.objects objectAtIndex:indexPath.row];
 *
 **/
- (void)didUpdateAllRoastersByHost:(NSArray *)rosters;

/**
 * @details sends the array of NSFetchedResultsSectionInfo group by Presense ie: Online, Offline, Away, Busy
 *          [NSFetchedResultsSectionInfo count] gives numberOfSectionsInTableView
 *          [NSFetchedResultsSectionInfo numberOfObjects] gives numberOfRowsInSection
 *          For, cellForRowAtIndexPath use the following:
 *          id <NSFetchedResultsSectionInfo> sectionInfo = [self.feedResults objectAtIndex:indexPath.section];
 *          CMUserCoreDataStorageObject *user = [sectionInfo.objects objectAtIndex:indexPath.row];
 *
 **/
- (void)didUpdateAllRoastersByPresence:(NSArray *)roaters;

- (void)didUpdateAllRoastersByUserName:(NSArray *)roasters;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@protocol CMChatManagerActiveChatDelegate <NSObject>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @details This Delegate is called when there is new message. It is used to refresh the Active chat list.
 * sends the array of XMPPMessageArchiving_Contact_CoreDataObject ordered by date in desc.
 **/
- (void)didUpdateActiveChatRecentMessages:(NSArray*)messages;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@protocol CMChatManagerMessagesDelegate <NSObject>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @details This Delegate is called when user has successfully sent the message.
 **/
@optional
- (void)forHost:(NSString *)hostName forJID:(NSString*)JID didSendMessage:(CMMessageArchiving_Message_CoreDataObject*)message;

/**
 * @details This Delegate is called when user has received a new message.
 **/
- (void)forHost:(NSString *)hostName forJID:(NSString*)JID didReceiveMessage:(CMMessageArchiving_Message_CoreDataObject*)message;

/**
 * @details This Delegate is called when other user has started/stopped typing.
 **/
- (void)forHost:(NSString *)hostName forJID:(NSString*)JID didReceiveMessageComposingStatus:(BOOL)isTyping;

/**
 * @details This Delegate is called when message was failed to sent. This method callback is used when user wants to show the failed message in the chat window with a retry button.
 **/
- (void)forHost:(NSString *)hostName forJID:(NSString*)JID didFailToSendMessage:(CMMessageArchiving_Message_CoreDataObject*)message;

/**
 * @details This Delegate is called until the media has uploaded on server to show the progress bar based on percent value.
 **/
- (void)forHost:(NSString *)hostName didProgressTransferMediaPercent:(float)percent withTag:(NSInteger)tag forJID:(NSString *)toJID;

/**
 * @details This Delegate is called when media transfer is completed but message is yet to be sent to receiver. Message is sent internally as soon as media transfer is completed. User doesn't need to send message.
 **/
- (void)forHost:(NSString *)hostName didCompleteTransferMedia:(NSString *)response withTag:(NSInteger)tag forJID:(NSString *)toJID;

/**
 * @details This Delegate is called when media transfer is failed and hence message is not sent to receiver.
 **/
- (void)forHost:(NSString *)hostName didFailTransferMedia:(NSError *)error withTag:(NSInteger)tag forJID:(NSString *)toJID;

/**
 * @details This Delegate is called when media file is downloaded from - (void) startDownloadingForMediaMessage:(CMMessageArchiving_Message_CoreDataObject *)message; method.
 **/
- (void)didDownloadMediaFileWithMessageForMessage:(CMMessageArchiving_Message_CoreDataObject*)message;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Group message delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @details This Delegate is called when user has received a new message in the active Room.
 **/
- (void)forHost:(NSString *)hostName didReceiveRoomMessage:(CMRoomMessageCoreDataStorageObject*)message;

/**
 * @details This Delegate is called when user has successfully sent the message in the active room.
 **/
- (void)forHost:(NSString *)hostName didSendRoomMessage:(CMRoomMessageCoreDataStorageObject*)message;

/**
 * @details This Delegate is called when message is failed to send.
 **/
- (void)forHost:(NSString *)hostName didFailedSendRoomMessage:(CMRoomMessageCoreDataStorageObject*)message;

@end

