//
//  CMRosterCoreDataStorage.h
//  iPhoneXMPP
//
//  Created by Saurabh Verma on 23/12/11.
//
//

#import "XMPPFramework.h"

@class CMUserCoreDataStorageObject;
@class CMResourceCoreDataStorageObject;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Declaring a Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@protocol CMRosterCoreDataStorageDelegate;

@interface CMRosterCoreDataStorage : XMPPRosterCoreDataStorage
{

}
@property (nonatomic, strong, readonly) NSFetchedResultsController *rosterFetchedResultsControllerByUsersAndAlphabaticIndexed;
@property (nonatomic, strong) id<CMRosterCoreDataStorageDelegate> delegate;
+ (CMRosterCoreDataStorage *)sharedInstance;
//- (void)handleCrossReferencing:(NSArray*)array withStream:(XMPPStream*)stream;
- (CMUserCoreDataStorageObject *)getUserForJID:(XMPPJID *)jid
                                    xmppStream:(XMPPStream *)stream
                          managedObjectContext:(NSManagedObjectContext *)moc;

- (CMUserCoreDataStorageObject *)myUserForXMPPStream:(XMPPStream *)stream
                                  managedObjectContext:(NSManagedObjectContext *)moc;

- (CMResourceCoreDataStorageObject *)myResourceForXMPPStream:(XMPPStream *)stream
                                          managedObjectContext:(NSManagedObjectContext *)moc;

- (CMUserCoreDataStorageObject *)userForJID:(XMPPJID *)jid
                                   xmppStream:(XMPPStream *)stream
                         managedObjectContext:(NSManagedObjectContext *)moc;

- (CMResourceCoreDataStorageObject *)resourceForJID:(XMPPJID *)jid
										   xmppStream:(XMPPStream *)stream
                                 managedObjectContext:(NSManagedObjectContext *)moc;
//- (void) updateProviderJIDForUserIfAny:(NSString *)myLifeJidStr forProvider:(NSString *)provider withStream: (XMPPStream *)stream;

//- (void)activateFetchedResultsControllerByUsers;
- (void)activateFetchedResultsControllerByHosts;
- (void)activateFetchedResultsControllerByPresence;
- (NSArray *)getBuddyListForHost:(NSString *)host;
@end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Declaring a Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@protocol CMRosterCoreDataStorageDelegate <NSObject>

@required
- (void)rosterCoreDataStorage:(CMRosterCoreDataStorage*)rosterStorage didUpdateAllRoastersByHost:(NSArray *)roaters;
- (void)rosterCoreDataStorage:(CMRosterCoreDataStorage*)rosterStorage didUpdateAllRoastersByUsers:(NSArray *)roaters;
- (void)rosterCoreDataStorage:(CMRosterCoreDataStorage*)rosterStorage didUpdateAllRoastersByPresence:(NSArray *)roaters;

@end




