//
//  Operation.h
//  Test
//
//  Created by Justin Paston-Cooper on 15/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OperationQueue.h"
@interface Operation : NSObject
{
    NSInteger duration;
}

@property NSInteger startTime;

- (Operation *) initWithStartTime:(NSInteger)t duration:(NSInteger)d;
- (BOOL) before:(Operation *)op;
- (NSInteger) duration;
- (NSInteger) completionTime;
@property (assign)   OperationQueue *ownerQueue;
@end
