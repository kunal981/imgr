//
//  CMChatFrameworkConstants.h
//  CMChatFramework
//
//  Created by Saurabh Verma on 31/12/10.
//  Copyright (c) 2013 Copper Mobile India Pvt Ltd. All rights reserved.
//

#ifndef CMChatFramework_CMChatFrameworkConstants_h
#define CMChatFramework_CMChatFrameworkConstants_h

#define RELEASE(obj)     if(obj) { [obj release]; obj = nil;}

// HOSTS SETTINGS9
//#define HOST_OPENFIRE       @"coppermobile.net"
//#define HOST_OPENFIRE       @"64.235.48.26"
#define HOST_OPENFIRE       @"imgrapp.com"
//#define HOST_OPENFIRE       @"coppermobile.mobi"
//#define HOST_OPENFIRE       @"coppermobile.mobi"

#define HOST_GTALK          @"talk.google.com"
#define HOST_FACEBOOK      @"chat.facebook.com"

// PORT SETTINGS
#define PORT_OPENFIRE       5222
#define PORT_GTALK          5222
#define PORT_FACEBOOK      5222

// PLUGINS SETTINGS
#define PATH_PLUGIN_MEDIATRANSFER   @"http://coppermobile.net:9090/plugins/ftpService/ftpservice"
//#define PATH_PLUGIN_MEDIATRANSFER   @"http://192.168.8.99:9090/plugins/userService/userservice"
#define MEDIA_TRANSFER_SECRET @"jBaG4U53"
//#define MEDIA_TRANSFER_SECRET @"3Yg1VeXM"

#define RESOURCE_CMCHAT     @"CMChat-iPhone"

#define FACEBOOK_APP_ID @"124242144347927"

#import "DDTTYLogger.h"
// Log levels: off, error, warn, info, verbose
#if DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_INFO;
#endif
// Configure logging framework

#define INIT_LOGGING() [DDLog addLogger:[DDTTYLogger sharedInstance]];
#define ENABLE_LOGGING_COLOR() [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
#define SET_ERROR_COLOR() [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:(255/255.0) green:(0.0) blue:(0.0) alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_ERROR];
#define SET_WARN_COLOR() [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:(255/255.0) green:(0.0) blue:(255/255.0) alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_WARN];
#define SET_INFO_COLOR() [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:(0.0) green:(255/255.0) blue:(255/255.0) alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_INFO];
#define SET_VERBOSE_COLOR() [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:(0) green:(255/255.0) blue:(0.0) alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_VERBOSE];

//Message constants
#define MESSAGE_BODY_TAG @"_message_body_tag"
#define PROMO_ID_TAG @"_promo_id_tag"

#endif
