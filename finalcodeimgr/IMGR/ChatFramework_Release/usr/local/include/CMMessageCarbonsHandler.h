//
//  CMMessageCarbonsHandler.h
//  CMChatFramework
//
//  Created by Tarun Khurana on 07/05/12.
//  Copyright (c) 2014 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"

@interface CMMessageCarbonsHandler : NSObject

+ (CMMessageCarbonsHandler*)sharedInstance;

- (void)getCarbonInfoFor:(XMPPStream *)stream;
@end
