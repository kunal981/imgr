//
//  CMMessageArchiving.h
//  CMChatFramework
//
//  Created by Saurabh Verma on 15/01/11.
//  Copyright (c) 2014 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPMessageArchiving.h"
#import "CMMessageArchivingCoreDataStorage.h"

typedef BOOL (^BlockShouldStoreMessage)(NSString *jid);
@class CMStream;
@interface CMMessageArchiving : XMPPMessageArchiving

@property (nonatomic, weak) id<CMMessageArchivingCoreDataStorageDelegate> messageCoreDataStorageDelegate;

@property (nonatomic,copy) BlockShouldStoreMessage shouldStoreMessageblock;

- (CMMessageArchiving *)initWithStream:(CMStream *)stream;
- (CMStream*)getStream;
- (void)deleteAllMessagesWithJid:(NSString*)jid;
- (void)deleteArchivedMessagesWithMessages:(NSArray*)messages;
- (void)resetUnreadCountWithJID:(NSString*)JID;
- (NSUInteger)unreadCountWithJID:(NSString*)JID;
- (NSArray*)getMessagesforJID:(NSString*)JID noOfMessages:(NSInteger)count;

@end
