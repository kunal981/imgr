//
//  CMStream.h
//  CMChatFramework
//
//  Created by Tarun Khurana on 31/12/10.
//  Copyright (c) 2013 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import "XMPPFramework.h"
#import "CMRoom.h"
#import "CMRoomInfoCoreDataStorageObject.h"
#import "CMUserCoreDataStorageObject.h"
#import "CMMessageArchiving.h"
#import "CMReconnect.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Declaring a Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef void (^VCardReceivedBlock)(NSDictionary *vCard, NSString *jid);

@protocol CMStreamDelegate;

@interface CMStream : XMPPStream
{
    __weak id <CMStreamDelegate> delegate;
}
@property BOOL isLogout;
@property (nonatomic,weak)id <CMStreamDelegate> delegate;
@property (nonatomic,strong)CMReconnect *cmReconnect;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Init/Connect
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (CMStream *)initWithHost:(NSString *)hostName atPort:(UInt16)port;
- (BOOL)connectWithJID:(NSString *)JID accessToken:(NSString *)accessToken;
- (void)teardownStream;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Utilities
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (CMStream *)getStreamForHost:(NSString *)hostName;

- (void)goOnline;
- (void)goOffline;
- (void)goAway;
- (void)goBusy;

- (void) setupMyVcardWithFirstName:(NSString *)firstName lastName:(NSString *)lastName phoneNumber:(NSString *)phoneNumber emailID:(NSString *)emailId profilePhoto:(NSData *)photoData;

//First Name = First_Name, Last Name = Last_Name, Email Id = Email_Id,Phone number = Phone_Number, User Image(UIImage) = User_Image,Bubble Color = Bubble_Color, 
- (void)vcardInfoForJID:(NSString *)jid completionBlock:(VCardReceivedBlock) vcardReceiveBlock;

- (void)setMyBubbleColor:(NSUInteger) bubbleColor;
- (void)sendMessageWithJID:(NSString*)JID message:(NSString*)message;
- (void)resendMessage:(CMMessageArchiving_Message_CoreDataObject *)failedMessage;
- (void)deleteAllMessagesWithJID:(NSString*)JID;
- (void)deleteArchivedMessages:(NSArray*)messages;
- (NSArray*)getMessagesforJID:(NSString*)JID count:(NSInteger)count;
- (void)sendMessageComposingStatusToJID:(NSString*)JID;
- (void)sendMessageActiveStatusToJID:(NSString*)JID;
- (void)resetUnreadCountForJID:(NSString*)JID;
- (NSUInteger)unreadCountForJID:(NSString*)JID;

//Group chat related methods
- (NSArray *)getBuddyList;
- (NSArray *)getBuddyListForRoom:(NSString*)roomJIDStr;

- (NSArray *)getMessagesforGroup:(NSString*)roomJIDStr WithLimit:(NSUInteger)count;

- (void)sendMessageForGroup:(NSString*)roomJIDStr message:(NSString *)messageBody;
-(void)setShouldStoreMessageblock:(BlockShouldStoreMessage)shouldStoreMessageblock;

- (void)prepareRoom:(NSString *)roomName roomJid:(NSString *)roomJID andNickName:(NSString *)myNickName withBuddies:(NSArray *)groupBuddies message:(NSString *) buddyInviteMessage andVirtualJID:(NSString *)virtualJID hostName:(NSString*)hostName isVirtualChatEnabled:(BOOL)isVirtualChatEnabled;

- (void)inviteBuddiesToRoom:(NSString*)roomJIDStr buddies:(NSArray *)buddies withMessage:(NSString *)messagge;

- (void)leaveRoom:(NSString *) roomJIDStr;

+ (NSArray *)groupOccupentsForRoom:(NSString *)roomJId errorObserved:(NSError *)error;

+ (CMRoomInfoCoreDataStorageObject *)roomInfoForRoomID:(NSString *)roomJid;

- (CMUserCoreDataStorageObject *)getUserDetailsForRoomParticipantJID:(NSString *)participantJid;

+ (BOOL) isGroupContact: (NSString *) jidString;
- (void) addUser:(NSString *)jid withNickName:(NSString *)nickName;
- (void)acceptPresenceSubscriptionRequestFrom:(NSString *)fromBuddy;
- (void)rejectPresenceSubscriptionRequestFrom:(NSString *)fromBuddy;
- (void) deleteBuddy:(NSString *)messageJid;
- (void)xmppRoster:(XMPPRoster *)sender didReceivePresenceSubscriptionRequest:(XMPPPresence *)presence;
- (UIImage *) photoForUser:(NSString *)jid;
+ (void) forceRefreshActiveChatMessages;
@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Declaring a Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@protocol CMStreamDelegate <NSObject>

@required
- (void)cmStreamDidAuthenticate:(CMStream *)sender;
- (void)cmStream:(CMStream *)sender didNotAuthenticate:(NSError *)error;
- (void)cmStreamWillDisConnect:(CMStream *)sender;
- (void)cmStreamWillConnect:(CMStream *)sender;
- (void)cmStream:(CMStream *)sender didDisConnect:(NSError *)error;
- (void)cmStream:(CMStream *)sender connectionDidTimeout:(NSError *)error;
- (void)cmStream:(CMStream *)sender roomDidReceiveMessage:(XMPPMessage *)message room:(CMRoom*)room;
- (void)cmStream:(CMStream *)sender didReceivePresenceSubscriptionRequestFrom:(NSString *)from;
- (void)cmStream:(CMStream *)sender didReceiveRoomRequestFrom:(NSString *)from roomJID:(NSString*)JID;
- (void)cmStream:(CMStream *)sender occupantDidJoinRoom:(NSString *)roomJID withJID:(NSString*)JID;
- (void)cmStream:(CMStream *)sender occupantDidLeaveRoom:(NSString *)roomJID withJID:(NSString*)JID;
@optional


@end
