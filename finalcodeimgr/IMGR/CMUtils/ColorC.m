//
//  ColorC.m
//  CustomColor
//
//  Created by Dev Team on 10/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ColorC.h"

@implementation UIColor ( ColorC )

+(UIColor *)colorFromRGBIntegers:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha
{
	CGFloat redF = red/255;
	CGFloat greenF = green/255;
	CGFloat blueF = blue/255;
	CGFloat alphaF = alpha/1.0;
	
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
	CGFloat components[4]={ redF, greenF, blueF, alphaF};
	CGColorRef colorRef = CGColorCreate(colorspace,components);
	CGColorSpaceRelease(colorspace);
	
	UIColor* color = [UIColor colorWithCGColor: colorRef];
	CGColorRelease(colorRef);
	return color;
}


@end
