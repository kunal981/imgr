//
//  OperationQueue.h
//  
//
//  Created by Admin on 09.04.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMOperationQueue : NSOperationQueue {
	BOOL didFailInQueueOperation;
	NSString *failOperationMessage;
	int numberExpectedOperations;
	id storedObject;
}

@property (nonatomic, assign) id storedObject;
@property (nonatomic, assign) BOOL didFailInQueueOperation;
@property (nonatomic, assign) int numberExpectedOperations;
@property (nonatomic, retain) NSString *failOperationMessage;

- (void)clearError;

@end
