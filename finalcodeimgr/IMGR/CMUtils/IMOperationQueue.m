//
//  OperationQueue.m
//  Pepsi
//
//  Created by Admin on 09.04.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "IMOperationQueue.h"


@implementation IMOperationQueue
@synthesize storedObject;
@synthesize didFailInQueueOperation;
@synthesize failOperationMessage;
@synthesize numberExpectedOperations;

- (void)clearError {
	didFailInQueueOperation = NO;
	self.failOperationMessage = @"";
}
-(void)cancelAllOperations{
//    DebugLog(@"Operations count %i", [self operationCount]);
//    for (DownloadPictureOperation* operation in [self operations]) {
//        [operation cancel];
//    }
//    DebugLog(@"Operations count %i", [self operationCount]);
    [super cancelAllOperations];
}
- (void)dealloc {
    
//    for (DownloadPictureOperation* operation in [self operations]) {
//        [operation finish];
//    }
	[failOperationMessage release];
	[super dealloc];
}

@end
