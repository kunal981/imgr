//
//  IMSponsoredAdsViewController.h
//  IMGR
//
//  Created by Satendra Singh on 2/4/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMSponsoredAdsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchDisplayDelegate, UISearchBarDelegate>
{
    IBOutlet UISearchBar*       promoSearchBar;
}

@property (nonatomic, readonly) NSFetchedResultsController* fectchedResultsController;
@property(strong, nonatomic) UISearchBar* promoSearchBar;

@end
