//
//  IMPromoSettingsOffView.h
//  IMGR
//
//  Created by Satendra Singh on 2/7/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMPromos.h"

@interface IMPromoSettingsOffView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *promoIcon;
@property (weak, nonatomic) IBOutlet UILabel *promoTitle;
@property (weak, nonatomic) IBOutlet UIImageView *promoOnImageView;
@property (weak, nonatomic) IBOutlet UIButton *removePromoButton;
@property (weak, nonatomic) IBOutlet UIButton *addPromoButton;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@property (strong,nonatomic,readonly) IMPromos *selectedPromo;


#pragma mark - Action Methods
- (IBAction)userDidTappedAddPromo:(id)sender;
- (IBAction)userDidTappedRemovePromo:(id)sender;

#pragma mark - Instance Methods
- (void) setSelectedPromo:(IMPromos *)selectedPromo;

@end
