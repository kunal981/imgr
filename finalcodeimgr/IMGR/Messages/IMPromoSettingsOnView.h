//
//  IMPromoSettingsOnView.h
//  IMGR
//
//  Created by Satendra Singh on 2/7/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMPromos.h"
#import "IMConstants.h"

@interface IMPromoSettingsOnView : UIView
@property (weak, nonatomic) IBOutlet UIButton *promoOnButton;
@property (weak, nonatomic) IBOutlet UIButton *addPromoButton;
@property (weak, nonatomic) IBOutlet UILabel *promoTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *promoIconImageView;
@property (strong,nonatomic,readonly) IMPromos *selectedPromo;
@property (strong,nonatomic,readonly) IMContacts *selectedContact;

@property (weak,nonatomic) UITableView *mainTableView;

@property (assign, nonatomic) BOOL isPromoEnabled;
- (void) setSelectedPromo:(IMPromos *)selectedPromo;
- (void) setSelectedContact:(IMContacts*)selectedContact;

- (IBAction)userDidTappedAddPromo:(id)sender;
- (IBAction)userDidSwipePromoLeft:(id)sender;
- (IBAction)userDidSwipePromoRight:(id)sender;
- (IBAction)userDidTappedOffOn:(id)sender;
- (void)controlllerWillAppear;
- (void)controlllerWillDisappear;

- (void) stepAheadRandom;

@property (assign, nonatomic) BOOL sponsoredAdsEnabled;
@property (assign, nonatomic) BOOL personalPromosEnabled;

@end
