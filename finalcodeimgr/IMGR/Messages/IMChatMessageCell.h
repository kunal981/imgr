//
//  CMChatMessageCell.h
//  DemochatApp
//
//  Created by Raiduk on 10/01/14.
//  Copyright (c) 2014 CopperMobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMPromos.h"
#import "IMChatViewController.h"

@interface IMChatMessageCell : UITableViewCell
{
   IBOutlet UIImageView *chatImageBubble;
    
   IBOutlet UILabel     *companyNamelbl;
   IBOutlet UILabel     *offerlbl;
    
    IBOutlet UIImageView *userImageView;
    IBOutlet UILabel     *topLbl;
    IBOutlet UILabel     *bottomLbl;
}

@property(nonatomic,retain)UIImageView *chatImageBubble;
@property(nonatomic,retain)UILabel     *companyNamelbl;
@property(nonatomic,retain)UILabel     *offerlbl;
@property (weak, nonatomic) IBOutlet UIImageView *messageDeliveredImage;

@property(nonatomic,retain)UIImageView *userImageView;
@property(nonatomic,retain)UILabel     *topLbl;
@property(nonatomic,retain)UILabel     *bottomLbl;
@property (nonatomic, strong) IMPromos *cellPromo;
@property (nonatomic, strong) CMMessageArchiving_Message_CoreDataObject *message;
@property(nonatomic,assign)IMChatViewController     *chatTableViewController;

@property (weak, nonatomic) IBOutlet UILabel *messageTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *resendButton;
@property (strong, nonatomic) NSIndexPath* lastSelectedIndexPath;

//- (void)userDidTappedOnMessageFooter:(UITapGestureRecognizer *)sender;
- (IBAction)userDidTappedOnResend:(UIButton *)sender;

@end
