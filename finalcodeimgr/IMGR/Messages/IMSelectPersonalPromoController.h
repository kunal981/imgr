//
//  IMSelectPersonalPromoController.h
//  IMGR
//
//  Created by Satendra Singh on 2/13/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMPersonalAdsViewController.h"
#import "IMPromos.h"

typedef void (^SelectionBlock)(IMPromos *selectedPromo);

@interface IMSelectPersonalPromoController : IMPersonalAdsViewController

@property (copy)  SelectionBlock selectionBlockObject;

@end
