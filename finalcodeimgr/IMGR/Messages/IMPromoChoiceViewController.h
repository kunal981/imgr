//
//  IMPromoChoiceViewController.h
//  IMGR
//
//  Created by Satendra Singh on 2/13/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//
#import "IMPromos.h"

typedef void (^SelectionBlock)(IMPromos *selectedPromo);

@interface IMPromoChoiceViewController : UIViewController

@property (copy)  SelectionBlock selectionBlockObject;

@end
