//
//  IMMessageListCell.h
//  IMGR
//
//  Created by Satendra Singh on 2/6/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMMessageListCell : UITableViewCell

+ (IMMessageListCell *)cellLoadedFromNibFile;

+ (NSString *) cellReusableIdentifier;

@property (weak, nonatomic) IBOutlet UILabel *userDisplayNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastMessageText;
@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;
@property (weak, nonatomic) IBOutlet UILabel *messageUnreadCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *messageCountBackgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *lastMessageReceivedTimeLabel;
@end
