//
//  IMPromoSettingsOnView.m
//  IMGR
//
//  Created by Satendra Singh on 2/7/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMPromoSettingsOnView.h"
#import "CMCoreDataHandler.h"
#import "IMPromos.h"

@interface IMPromoSettingsOnView()<NSFetchedResultsControllerDelegate>
{
    NSInteger selectedPromoIndex;
    BOOL isPromoEnable;
    NSFetchedResultsController *promosFetchResultController;
    IMPromos *_selectedPromo;
}

@end

@implementation IMPromoSettingsOnView

@synthesize selectedPromo = _selectedPromo;
@synthesize selectedContact = _selectedContact;
@synthesize isPromoEnabled = isPromoEnable;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
}

- (void) setLastSelectedPromo: (NSArray*)promoList {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"promo_id == %d", [self.selectedContact.last_promo_id intValue]];
    
    NSArray *filteredArray = [promoList filteredArrayUsingPredicate: predicate];
    if(filteredArray.count) {
        self.selectedPromo = [filteredArray lastObject];
    }
    else {
        selectedPromoIndex = ((NSUInteger)[[NSDate date] timeIntervalSince1970])%(promoList.count);
        self.selectedPromo = [promoList objectAtIndex:selectedPromoIndex];
    }
}

- (void) controlllerWillAppear
{
    selectedPromoIndex = 0;
    promosFetchResultController = nil;
    
    NSArray* promoList = [[self promosFetchResultController] fetchedObjects];
    if([promoList count])
    {
        if (![[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_ROTATE_ENABLED]) {
            [self setLastSelectedPromo: promoList];
        }
        else {
            if(!self.selectedContact.auto_rotation_enabled.boolValue) {
                [self setLastSelectedPromo: promoList];
            }
            else {
                selectedPromoIndex = ((NSUInteger)[[NSDate date] timeIntervalSince1970])%(promoList.count);
                self.selectedPromo = [promoList objectAtIndex:selectedPromoIndex];
            }
        }
    }
    else
    {
        _promoIconImageView.image = nil;
        _promoTitleLabel.text = @"";
        
    }
    isPromoEnable = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(personalPromoUpdated:) name:PERSONAL_PROMO_THIRDPARTY_UPDATED_NOTIFICATION object:nil];
}

- (void)controlllerWillDisappear
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) setIsPromoEnabled:(BOOL)isPromoEnabled
{
    if (isPromoEnable) {
        [_promoOnButton setImage:[UIImage imageNamed:@"icn_advt_off.png"] forState:UIControlStateNormal];
        _promoIconImageView.image = [UIImage imageNamed:@"icn_off.png"];
        _promoTitleLabel.text = @"Off";
        _promoTitleLabel.enabled = NO;
    }
    else
    {
        [_promoOnButton setImage:[UIImage imageNamed:@"icn_advt_on.png"] forState:UIControlStateNormal];
        if ([self promosFetchResultController].fetchedObjects.count) {
            if ([self promosFetchResultController].fetchedObjects.count > selectedPromoIndex) {
                self.selectedPromo = [[self promosFetchResultController].fetchedObjects objectAtIndex:selectedPromoIndex];
            }
            else
            {
                self.selectedPromo = [[self promosFetchResultController].fetchedObjects objectAtIndex:0];
            }
        }
        else
            self.selectedPromo = nil;
        _promoTitleLabel.enabled = YES;
        
    }
    isPromoEnable = isPromoEnabled;
}

- (void) setSelectedPromo:(IMPromos *)selectedPromo
{
    _selectedPromo = selectedPromo;
    ;
    NSString* promoImage = [[IMUtils getImageFolderPath] stringByAppendingPathComponent: [_selectedPromo.promo_image lastPathComponent]];
    _promoIconImageView.image = [UIImage imageWithContentsOfFile:promoImage];
    _promoTitleLabel.text = [_selectedPromo promo_name];
}

- (void) setSelectedContact:(IMContacts*)selectedContact {
    
    _selectedContact = selectedContact;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)userDidTappedAddPromo:(id)sender {
}

- (IBAction)userDidTappedOffOn:(UIButton *)sender
{
    [self setIsPromoEnabled:!isPromoEnable];
}

- (IBAction)userDidSwipePromoLeft:(id)sender
{
    if (isPromoEnable && [self promosFetchResultController].fetchedObjects.count) {
        selectedPromoIndex --;
        if (selectedPromoIndex < 0 || selectedPromoIndex >=[self promosFetchResultController].fetchedObjects.count) {
            selectedPromoIndex = [self promosFetchResultController].fetchedObjects.count - 1;
        }
        self.selectedPromo = [[self promosFetchResultController].fetchedObjects objectAtIndex:selectedPromoIndex];
    }
}

- (IBAction)userDidSwipePromoRight:(id)sender
{
    if (isPromoEnable && [self promosFetchResultController].fetchedObjects.count) {
        selectedPromoIndex ++;
        if (selectedPromoIndex >= [self promosFetchResultController].fetchedObjects.count) {
            selectedPromoIndex = 0;
        }
        self.selectedPromo = [[self promosFetchResultController].fetchedObjects objectAtIndex:selectedPromoIndex];
    }
}

- (void) stepAheadRandom
{//Seleact a random promo for next message
    if (isPromoEnable && [self promosFetchResultController].fetchedObjects.count) {
        NSUInteger numberOfObjects = [self promosFetchResultController].fetchedObjects.count;
        if (numberOfObjects) {
            selectedPromoIndex = arc4random() %numberOfObjects;
        }
        if (selectedPromoIndex >= [self promosFetchResultController].fetchedObjects.count) {
            selectedPromoIndex = 0;
        }
        self.selectedPromo = [[self promosFetchResultController].fetchedObjects objectAtIndex:selectedPromoIndex];
    }
}
#pragma mark-
#pragma mark Prom rotation loading related logic

- (NSFetchedResultsController*)promosFetchResultController
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    if(promosFetchResultController == nil)
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMPromos"
                                                  inManagedObjectContext:moc];
        
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"index_character" ascending:YES];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"promo_name" ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, sd2, nil];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:sortDescriptors];
        NSPredicate *predicate = nil;
        if (self.sponsoredAdsEnabled & self.personalPromosEnabled) {
            predicate = [NSPredicate predicateWithFormat:@"is_enabled == %d AND (promo_type == %d OR promo_type = %d) AND markedAsDeleted == %d",1,0,1,0];
        }
        else if(self.personalPromosEnabled)
        {
            predicate = [NSPredicate predicateWithFormat:@"is_enabled == %d AND promo_type == %d AND markedAsDeleted == %d",1,1,0];

        }
        else if(self.sponsoredAdsEnabled)
        {
            predicate = [NSPredicate predicateWithFormat:@"is_enabled == %d AND promo_type == %d AND markedAsDeleted == %d",1,0,0];
        }
        else
        {
            return nil;//Why we need extra job?
        }
        [fetchRequest setPredicate:predicate];

        promosFetchResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:moc
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
        [promosFetchResultController setDelegate:self];
        
        NSError *error = nil;
        if (![promosFetchResultController performFetch:&error])
        {
            DDLogError(@"Error performing fetch: %@", error);
        }
    }
    return promosFetchResultController;
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if ([promosFetchResultController isEqual:controller]) {
        
//        selectedPromoIndex = 0;
        [self.mainTableView reloadData];
        NSLog(@"reload data");
    }
}

- (void) personalPromoUpdated:(NSNotification *)notification
{
    [self.mainTableView reloadData];
}

@end
