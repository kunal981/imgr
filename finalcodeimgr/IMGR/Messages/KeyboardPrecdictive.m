//
//  KeyboardPrecdictive.m
//  IMGR
//
//  Created by brst on 2/21/15.
//  Copyright (c) 2015 Copper Mobile Inc. All rights reserved.
//

#import "KeyboardPrecdictive.h"

@implementation KeyboardPrecdictive

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (BOOL)isPredictiveTextEnabledForTextField:(UITextField *)textField
{
    if (textField.autocorrectionType == UITextSpellCheckingTypeNo) {
        return NO;
    }
    
    BOOL isFirstResponder = [textField isFirstResponder];
    BOOL autoCorrectionType = [textField autocorrectionType];
    
    [textField resignFirstResponder];
  
    
    // Get the frame with possibly including predictive text
    [textField becomeFirstResponder];
    CGRect predictiveKeyboardEndFrame = self.keyboardEndFrame;
    [textField resignFirstResponder];
    
    // Get the keyboard frame without predictive text
    textField.autocorrectionType = UITextSpellCheckingTypeNo;
    [textField becomeFirstResponder];
    CGRect defaultKeyboardEndFrame = self.keyboardEndFrame;
    [textField resignFirstResponder];
    
    // Restore state
    textField.autocorrectionType = autoCorrectionType;
    if (isFirstResponder) {
        [textField becomeFirstResponder];
    }
      textField.autocorrectionType=UITextAutocorrectionTypeYes;
    BOOL isPredictiveTextEnabled = !CGPointEqualToPoint(predictiveKeyboardEndFrame.origin, defaultKeyboardEndFrame.origin);
    return isPredictiveTextEnabled;
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.object;
    CGRect keyboardEndFrame;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    self.keyboardEndFrame = keyboardEndFrame;
    NSLog(@"Keyboard frame=%f",self.keyboardEndFrame.origin.y);
}


@end
