//
//  KeyboardPrecdictive.h
//  IMGR
//
//  Created by brst on 2/21/15.
//  Copyright (c) 2015 Copper Mobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyboardPrecdictive : NSObject
- (BOOL)isPredictiveTextEnabledForTextField:(UITextField *)textField;
@property (assign, nonatomic) CGRect keyboardEndFrame;
@end
