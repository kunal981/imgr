//
//  IMChatDateHeader.h
//  IMGR
//
//  Created by Satendra Singh on 2/25/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMChatDateHeader : UIView
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

+ (IMChatDateHeader *)cellLoadedFromNibFile;

+ (NSString *) cellReusableIdentifier;
@end
