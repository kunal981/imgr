//
//  IMSelectPersonalPromoController.m
//  IMGR
//
//  Created by Satendra Singh on 2/13/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMSelectPersonalPromoController.h"

@interface IMSelectPersonalPromoController ()

@end

@implementation IMSelectPersonalPromoController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    indexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section - 1];
    [self.navigationController popViewControllerAnimated:NO];
    _selectionBlockObject([[self fectchedResultsController] objectAtIndexPath:indexPath]);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section) {
        return 26;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

@end
