//
//  CMChatImage.m
//  DemochatApp
//
//  Created by Raiduk on 15/01/14.
//  Copyright (c) 2014 CopperMobile. All rights reserved.
//

#import "IMChatImage.h"

@implementation IMChatImage

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        imgVW = [[UIImageView alloc] init];
        imgVW.backgroundColor = [UIColor redColor];
        [self addSubview:imgVW];
    }
    return self;
}

- (void)setImageFrame:(CGRect)frame
{
    imgVW.frame = frame;
}

- (UIBezierPath *)rectPathForImage:(CGRect)imageframe{
    return [UIBezierPath bezierPathWithRect:imageframe];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
