//
//  CMViewController.h
//  DemochatApp
//
//  Created by Raiduk on 10/01/14.
//  Copyright (c) 2014 CopperMobile. All rights reserved.
//

#import <UIKit/UIKit.h>

#define IOS(x)                      (([[[UIDevice currentDevice] systemVersion] floatValue]>=x)?YES:NO)

@class IMChatMessageCell;
@interface IMChatViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate>
{
//    IBOutlet IMChatMessageCell      *chatMessageCell;
    BOOL keyboard;
    float bonds;
    BOOL perdicative;
    BOOL emoji;
    BOOL firstTime;
    CGSize imageSize;
     
}

//@property(nonatomic,retain)IMChatMessageCell  *chatMessageCell;
@property (nonatomic, retain) NSString *userJid;//SS TODO: need to set user object
@property (strong, nonatomic) IMContacts *userInfo;
@property (nonatomic)int unreadMessageCount;
@property (nonatomic,strong) NSString *countCheck;
- (void)refresh;
- (void)refreshViewForSwitchChat;
-(void)decreaseBadgeCount;
-(void)notificationCount;
- (NSString *)encodeToBase64String:(UIImage *)image;
@end
