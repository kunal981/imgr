//
//  IMContactsCell.h
//  IMGR
//
//  Created by akram on 10/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMContactsCell : UITableViewCell
{
    
}
@property (strong, nonatomic)IBOutlet UILabel* nameLabel;
@property (strong, nonatomic)IBOutlet UIButton* statusButton;
@property (weak, nonatomic) IBOutlet UIImageView *userImageViewHolder;
@property (strong, nonatomic)IBOutlet UIButton* infoButton;
@property (strong, nonatomic)IBOutlet UIImageView* userImageView;
@property (weak, nonatomic) IBOutlet UILabel *imgrLabel;

@property (assign, nonatomic)id delegate;


- (IBAction)onStatusButtonClick:(id)sender;
- (IBAction)onInfoButtonClick:(id)sender;

+ (IMContactsCell *)cellLoadedFromNibFile;
+ (NSString *)cellReusableIdentifier;

@end
