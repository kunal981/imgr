//
//  IMNewContactCell.m
//  IMGR
//
//  Created by akram on 11/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMNewContactCell.h"

@implementation IMNewContactCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:[IMNewContactCell cellReusableIdentifier]];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
//    if (selected) {
//        [self setAccessoryType:UITableViewCellAccessoryCheckmark];
//    }
//    else
//    {
//        [self setAccessoryType:UITableViewCellAccessoryNone];
//    }
}

+ (IMNewContactCell *)cellLoadedFromNibFile
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    for (NSObject *obj in objects)
    {
        if ([obj isKindOfClass:[self class]])
        {
            return (IMNewContactCell *)obj;
            break;
        }
    }
    return nil;
}

+ (NSString *) cellReusableIdentifier
{
    return @"IMNewContactCell";
}

@end
