//
//  IMEditContactViewController.h
//  IMGR
//
//  Created by akram on 05/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IMContacts;
@interface IMEditContactViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,UIAlertViewDelegate>
{
    IBOutlet UITableView*   editContactTableView;
    
    __weak IBOutlet UIImageView*        userImageView;
    IBOutlet NSLayoutConstraint* tableViewHeightConstraint;
}
@property(nonatomic)BOOL isIMGRUser;
@property (nonatomic, strong)IMContacts* contact;
@property(strong, nonatomic)NSLayoutConstraint* tableViewHeightConstraint;

- (IBAction)changePhoto:(id)sender;
- (IBAction)onDoneButtonClick:(id)sender;

@end
