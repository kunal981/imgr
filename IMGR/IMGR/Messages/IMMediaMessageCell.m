//
//  IMMediaMessageCell.m
//  IMGR
//
//  Created by Satendra Singh on 2/25/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMMediaMessageCell.h"

@implementation IMMediaMessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:[IMMediaMessageCell cellReusableIdentifier]];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (IMMediaMessageCell *)cellLoadedFromNibFile
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    for (NSObject *obj in objects)
    {
        if ([obj isKindOfClass:[self class]])
        {
            return (IMMediaMessageCell *)obj;
            break;
        }
    }
    return nil;
}

+ (NSString *) cellReusableIdentifier
{
    return @"IMMediaMessageCell";
}

- (IBAction)userDidTappedOnResend:(UIButton *)sender {
    [[CMChatManager sharedInstance] resendMessageWithHost:HOST_OPENFIRE
                                            failedMessage:self.message];
    [[CMChatManager sharedInstance] deleteMessagesWithHost:HOST_OPENFIRE messages:@[self.message]];
}

@end
