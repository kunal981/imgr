//
//  IMChatDateHeader.m
//  IMGR
//
//  Created by Satendra Singh on 2/25/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMChatDateHeader.h"

@implementation IMChatDateHeader

+ (IMChatDateHeader *)cellLoadedFromNibFile
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    for (NSObject *obj in objects)
    {
        if ([obj isKindOfClass:[self class]])
        {
            return (IMChatDateHeader *)obj;
            break;
        }
    }
    return nil;
}

+ (NSString *) cellReusableIdentifier
{
    return @"IMChatDateHeader";
}

@end
