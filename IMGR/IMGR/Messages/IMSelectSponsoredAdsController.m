//
//  IMSelectSponsoredAdsController.m
//  IMGR
//
//  Created by Satendra Singh on 2/13/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMSelectSponsoredAdsController.h"

@interface IMSelectSponsoredAdsController ()<UITableViewDelegate>

@end

@implementation IMSelectSponsoredAdsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.navigationController popViewControllerAnimated:NO];
    _selectionBlockObject([[self fectchedResultsController] objectAtIndexPath:indexPath]);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}


@end
