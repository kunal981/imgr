//
//  CMChatImage.h
//  DemochatApp
//
//  Created by Raiduk on 15/01/14.
//  Copyright (c) 2014 CopperMobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMChatImage : UIView
{
    UIImageView *imgVW;
}

- (UIBezierPath *)rectPathForImage:(CGRect)imageframe;
- (void)setImageFrame:(CGRect)frame;



@end
