//
//  IMMessagesListViewController.h
//  IMGR
//
//  Created by Satendra Singh on 2/6/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMMessagesListViewController : UITableViewController

- (IBAction)onSlideButtonClick:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activitiIndicator;
@property (strong, nonatomic) IBOutlet UIImageView *pullDownImg;

@end
