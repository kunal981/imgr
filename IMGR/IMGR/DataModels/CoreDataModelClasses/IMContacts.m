//
//  IMContacts.m
//  IMGR
//
//  Created by akram on 28/04/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMContacts.h"


@implementation IMContacts

@dynamic auto_rotation_enabled;
@dynamic contact_id;
@dynamic email;
@dynamic first_name;
@dynamic index_character;
@dynamic is_blocked;
@dynamic is_imgr_user;
@dynamic is_invited;
@dynamic isAvailabledInAddressBook;
@dynamic last_name;
@dynamic member_id;
@dynamic personal_promo_enabled;
@dynamic phone_number;
@dynamic phone_number_2;
@dynamic promo_enabled;
@dynamic sponsored_ads_enabled;
@dynamic user_id;
@dynamic last_promo_id;

@end
