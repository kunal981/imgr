#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#if !TARGET_OS_IPHONE
#import <Cocoa/Cocoa.h>
#endif

#import "XMPPUser.h"
#import "XMPP.h"


@class CMGroupCoreDataStorageObject;
@class CMResourceCoreDataStorageObject;


@interface CMUserCoreDataStorageObject : NSManagedObject <XMPPUser>
{
	NSInteger section;
}

@property (nonatomic, strong) XMPPJID *jid;
@property (nonatomic, strong) NSString * jidStr;
@property (nonatomic, strong) NSString * streamBareJidStr;

@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * displayName;
@property (nonatomic, strong) NSString * subscription;
@property (nonatomic, strong) NSString * ask;
@property (nonatomic, strong) NSNumber * unreadMessages;

#if TARGET_OS_IPHONE
@property (nonatomic, strong) UIImage *photo;
#else
@property (nonatomic, strong) NSImage *photo;
#endif

@property (nonatomic, assign) NSInteger section;
@property (nonatomic, strong) NSString * sectionName;
@property (nonatomic, strong) NSNumber * sectionNum;
@property (nonatomic, strong) NSString * hostName;
@property (nonatomic, strong) NSString * presenceStr;
@property (nonatomic, strong) NSString * alphabaticSection;

@property (nonatomic, strong) NSSet * groups;
@property (nonatomic, strong) CMResourceCoreDataStorageObject * primaryResource;
@property (nonatomic, strong) NSSet * resources;
@property (nonatomic, strong) CMUserCoreDataStorageObject* gmailUser;
@property (nonatomic, strong) CMUserCoreDataStorageObject* facebookUser;
@property (nonatomic, strong) CMUserCoreDataStorageObject* gmailInverse;
@property (nonatomic, strong) CMUserCoreDataStorageObject* facebookInverse;

+ (id)insertInManagedObjectContext:(NSManagedObjectContext *)moc
                           withJID:(XMPPJID *)jid
                  streamBareJidStr:(NSString *)streamBareJidStr;

+ (id)insertInManagedObjectContext:(NSManagedObjectContext *)moc
                           withJID:(XMPPJID *)jid
                  streamBareJidStr:(NSString *)streamBareJidStr
                       hostName:(NSString*)hostName;

+ (id)insertInManagedObjectContext:(NSManagedObjectContext *)moc
                          withItem:(NSXMLElement *)item
                  streamBareJidStr:(NSString *)streamBareJidStr;

+ (id)insertInManagedObjectContext:(NSManagedObjectContext *)moc
                          withItem:(NSXMLElement *)item
                  streamBareJidStr:(NSString *)streamBareJidStr
                       hostName:(NSString*)hostName;

- (void)updateWithItem:(NSXMLElement *)item;
- (void)updateWithPresence:(XMPPPresence *)presence streamBareJidStr:(NSString *)streamBareJidStr;

- (NSComparisonResult)compareByName:(CMUserCoreDataStorageObject *)another;
- (NSComparisonResult)compareByName:(CMUserCoreDataStorageObject *)another options:(NSStringCompareOptions)mask;

- (NSComparisonResult)compareByAvailabilityName:(CMUserCoreDataStorageObject *)another;
- (NSComparisonResult)compareByAvailabilityName:(CMUserCoreDataStorageObject *)another
                                        options:(NSStringCompareOptions)mask;

@end

@interface CMUserCoreDataStorageObject (CoreDataGeneratedAccessors)

- (void)addResourcesObject:(CMResourceCoreDataStorageObject *)value;
- (void)removeResourcesObject:(CMResourceCoreDataStorageObject *)value;
- (void)addResources:(NSSet *)value;
- (void)removeResources:(NSSet *)value;

- (void)addGroupsObject:(CMGroupCoreDataStorageObject *)value;
- (void)removeGroupsObject:(CMGroupCoreDataStorageObject *)value;
- (void)addGroups:(NSSet *)value;
- (void)removeGroups:(NSSet *)value;

@end
