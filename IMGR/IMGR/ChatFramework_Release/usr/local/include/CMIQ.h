//
//  CMIQ.h
//  CMChatFramework
//
//  Created by Tarun Khurana on 16/01/11.
//  Copyright (c) 2014 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import "XMPPIQ.h"

@class CMStream;
@interface CMIQ : XMPPIQ

-(CMIQ *)initWithStream:(CMStream *)stream;
@end
