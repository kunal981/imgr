//
//  CMRoomInfoCoreDataStorageObject.h
//  CMChatFramework
//
//  Created by Tarun Khurana on 16/01/14.
//  Copyright (c) 2014 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CMRoomInfoCoreDataStorageObject : NSManagedObject

@property (nonatomic, retain) NSData * roomIcon;
@property (nonatomic, retain) NSString * roomJIDStr;
@property (nonatomic, retain) NSString * roomName;
@property (nonatomic, retain) NSString * virtualGmailID;
@property (nonatomic, retain) NSString* hostName;
@end
