//
//  IMViewController.m
//  IMGR
//
//  Created by Satendra Singh on 2/3/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMSettingsViewController.h"
#import "IMMyProfileViewController.h"
#import "TWTSideMenuViewController.h"
#import "IMUtils.h"
#import "CMNetManager.h"
#import <MessageUI/MessageUI.h>

#define k_CONTROLLER_TITLE @"Settings"

@interface IMSettingsViewController ()<UIActionSheetDelegate, CMNetManagerDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
{
    BOOL isNotificationOn;
}
@property (weak, nonatomic) IBOutlet UIView *tableFooterCustomView;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
- (IBAction)onShareImgr:(id)sender;

@end

@implementation IMSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIView *tableFoot = self.tableFooterCustomView;
//    [self.tableFooterCustomView removeFromSuperview];
    self.tableView.backgroundView = self.backGroundImageView;
    self.tableView.tableFooterView = tableFoot;

    
//    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.navigationItem.title = k_CONTROLLER_TITLE;

    //[self getUserSettings];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    //if([IMUtils getOSVersion] >= 7.0)
    //    self.navigationController.navigationBar.frame = CGRectMake(0, 20, 320, 44);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (0 == section) {
        return 5;
    }
    else if (1 == section) {
        return 2;
    }
    else
    {
        return 1;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.section == 1) {
        if (nil == [cell.accessoryView viewWithTag:indexPath.row]) {
            UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
            cell.accessoryView = switchView;
            switchView.tag = indexPath.row;
            //[switchView setOn: isNotificationOn];
            [switchView addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
        }
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
  
    }
    [self configureCell:cell forIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (0==section) {
        return 20.0;
    }
    return 22;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (1 == section) {
        return 22;
    }
    return 0;
}

- (void) configureCell: (UITableViewCell *)cell forIndexPath:(NSIndexPath *) indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            cell.textLabel.font = [IMUtils appFontWithSize:17.0];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;

            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

            switch (indexPath.row) {
                case 0:
                {
                    cell.textLabel.text = @"My Profile";
                }
                    break;
                case 1:
                {
                    cell.textLabel.text = @"Promo Settings";
                }
                    break;
                case 2:
                {
                    cell.textLabel.text = @"Bubble Color";
                }
                    break;
                case 3:
                {
                    cell.textLabel.text = @"Blocked Contacts";
                }
                    break;
                case 4:
                {
                    cell.textLabel.text = @"Font Size";
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            UIFont *font = [IMUtils appBoldFontWithSize:17.0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.font = font;
            UISwitch *switchView = (UISwitch *)[cell.accessoryView viewWithTag:indexPath.row];
            
            switchView.on = YES;
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Promos";

                    switchView.on = [[[NSUserDefaults standardUserDefaults] objectForKey:k_DEFAULTS_KEY_PROMOS] boolValue];
                    break;
                    
                case 1:
                    cell.textLabel.text = @"Notifications";
                    [switchView setOn: [[[NSUserDefaults standardUserDefaults] objectForKey:k_DEFAULTS_KEY_NOTIFICATIONS]boolValue] animated: YES];

                    break;
                    
                default:
                    break;
            }
        }
            break;
//        case 2:
//        {
//            
//            switch (indexPath.row) {
//                case 0:
//                    cell.textLabel.text = @"Share IMGR with Friends";
//                    break;
//                    
//                default:
//                    break;
//            }
//        }
        default:
            break;
    }
    
}

- (void) switchChanged:(UISwitch *) sender
{
    NSString *key = nil;
    if (sender.tag) {
        key = k_DEFAULTS_KEY_NOTIFICATIONS;
        isNotificationOn = sender.isOn;
    }
    else
    {
        key = k_DEFAULTS_KEY_PROMOS;
    }
    
    
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:sender.isOn] forKey:key];
    NSLog(@"switchChanged %d",sender.tag);
    [self setUserSettings];
}

- (void)getUserSettings
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString* jidString = [[IMAppDelegate sharedDelegate] getUserID];
    DebugLog(@"jidString = %@", jidString);
    if(jidString != nil)
        [parameters setObject:[IMUtils phoneNumberForJid: jidString] forKey: @"username"];
    
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kGetUserSettings] parameters:parameters queueIdentifier:"settingsQueue" queueType:CONCURRENT httpMethod: GET];
    netManager.delegate = self;
    netManager.tag = E_GET_USER_SETTINGS_SERVICE;
    [netManager startRequest];
}

- (void)setUserSettings
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString* jidString = [[IMAppDelegate sharedDelegate] getUserID];
    DebugLog(@"jidString = %@", jidString);
    if(jidString != nil)
        [parameters setObject:jidString forKey: @"username"];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey: k_DEFAULTS_KEY_NOTIFICATIONS] boolValue])
        [parameters setObject:@"1" forKey: @"isNotificationOn"];
    else
        [parameters setObject:@"0" forKey: @"isNotificationOn"];
    
    [parameters setObject:@"0" forKey: @"isOnAutoRotate"];
    
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kSetUserSettings] parameters:parameters queueIdentifier:"settingsQueue" queueType:CONCURRENT httpMethod: GET];
    netManager.delegate = self;
    netManager.tag = E_SET_USER_SETTINGS_SERVICE;
    [netManager startRequest];
}

#pragma mark - CMNetManager Delegate methods
//This delegate gets called on success
-(void)netManager:(CMNetManager*)netManager didReceiveResponse:(NSDictionary*)dictionary
{
    DebugLog(@"Response = %@", dictionary);
    switch (netManager.tag)
    {
        case E_SET_USER_SETTINGS_SERVICE:
        {
           
            
        }break;
            
        case E_GET_USER_SETTINGS_SERVICE:
        {
            if([[dictionary objectForKey: @"data"]count] > 0)
                isNotificationOn = [[[[dictionary objectForKey: @"data"] objectAtIndex: 0] objectForKey: @"isNotificationOn"] boolValue];
            [self.tableView reloadData];
        }break;
            
        default:
            break;
    }
}

-(void)netManager:(CMNetManager*)netManager didReceiveResponseString:(NSString*)response
{
    
}

//This delegate gets called on failure
-(void)netManager:(CMNetManager*)netManager didFailWithError:(NSError*)error
{
    
}

-(void)netManager:(CMNetManager *)netManager didGetSuccessToConnectWithServer:(NSError *)error
{
    
}

//This delegate gets called if internet connection is not available
-(void)netManager:(CMNetManager *)netManager didFailToConnectWithServer:(NSError *)error
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    [self performSegueWithIdentifier:@"IMMyProfileViewController" sender:self];
                }
                    break;
                case 1:
                {
                    [self performSegueWithIdentifier:@"IMPromoSettingsViewController" sender:self];
                }
                    break;
                case 2:
                {
                    [self performSegueWithIdentifier:@"IMBubbleColorController" sender:self];
                }
                    break;
                case 3:
                {
                    [self performSegueWithIdentifier:@"IMBlockedContactsController" sender:self];
                }
                    break;
                case 4:
                {
                    [self performSegueWithIdentifier:@"IMFontSizeViewController" sender:self];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
//                    cell.textLabel.text = @"Promos";
//                    switchView.on = [[[NSUserDefaults standardUserDefaults] objectForKey:k_DEFAULTS_KEY_PROMOS] boolValue];
                    break;
                    
                case 1:
//                    cell.textLabel.text = @"Notifications";
//                    switchView.on = [[[NSUserDefaults standardUserDefaults] objectForKey:k_DEFAULTS_KEY_NOTIFICATIONS] boolValue];
                    
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            
            switch (indexPath.row) {
                case 0:
//                    cell.textLabel.text = @"Share IMGR with Friends";
                    break;
                    
                default:
                    break;
            }
        }
        default:
            break;
    }
   
}

- (IBAction)onSlideButtonClick:(id)sender
{
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}

- (IBAction)onShareImgr:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Tell a friend about IMGR via..." delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Mail",@"Message", nil];
    [actionSheet showInView:self.view];
}

- (void)inviteViaMessage
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    
    if([MFMessageComposeViewController canSendText]) {
        
        NSString* bodyOfMessage = NSLocalizedString(@"MESSAGE BODY", @"");
        controller.body = bodyOfMessage;
        //controller.recipients = [NSArray arrayWithObject: contact.first_name];
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated: YES completion: ^{}];
        
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated: YES completion: ^{}];
    
    if (result == MessageComposeResultCancelled) {
        
        NSLog(@"Message cancelled");
        
    } else if (result == MessageComposeResultSent) {
        
        NSLog(@"Message sent");
    }
    
}
- (void)sendInvitationToIMGR
{
    MFMailComposeViewController* mailCompose = [[MFMailComposeViewController alloc] init];
    if([MFMailComposeViewController canSendMail])
    {
        mailCompose.mailComposeDelegate = self;
        [mailCompose setSubject:NSLocalizedString(@"NEW MESSAGE", @"")];
        
        NSString* bodyStr = NSLocalizedString(@"MESSAGE BODY", @"");
        //bodyStr = [bodyStr stringByAppendingString: [NSString stringWithFormat: @"\n\nGet it now from\n%@", @"applinkfromweb.com"]];
        //[mailCompose setToRecipients: [NSArray arrayWithObject: contact.first_name]];
        [mailCompose setMessageBody: bodyStr isHTML: NO];
        
        [self.navigationController presentViewController:mailCompose animated:YES completion: ^{}];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            
            break;
            
        case MFMailComposeResultSaved:
            break;
            
        case MFMailComposeResultSent:
        {
            
        }break;
            
        case MFMailComposeResultFailed:
            break;
            
        default:
            break;
    }
    [controller dismissViewControllerAnimated: YES completion:^{}];
}

#pragma mark UIActionSheet Delegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self sendInvitationToIMGR];
            break;
            
        case 1:
            [self inviteViaMessage];
            break;
            
        case 2:
            
            break;
            
        default:
            break;
    }
}

@end
