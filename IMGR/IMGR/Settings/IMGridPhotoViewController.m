//
//  IMGridPhotoViewController.m
//  IMGR
//
//  Created by Satendra Singh on 2/6/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMGridPhotoViewController.h"
#import "IMUtils.h"
#import "CMCoreDataHandler.h"
#import "IMDataHandler.h"
#import "MBProgressHUD.h"

#define PROMO_ICON_TAG      10000
#define CHECK_ICON_TAG      10001

@interface IMGridPhotoViewController () <NSFetchedResultsControllerDelegate>
{
    NSArray* imageList;
    UICollectionViewController* c;
    
    NSFetchedResultsController* fectchedResultsController;
    
    MBProgressHUD*              progressView;
}
@property (weak, nonatomic)CMNetManager* netManager;
@end

@implementation IMGridPhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSFetchedResultsController*)fetchPromosResultController
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    if(fectchedResultsController == nil)
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMPromos"
                                                  inManagedObjectContext:moc];
        
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"index_character" ascending:YES];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"promo_name" ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects: sd1, sd2, nil];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSPredicate* predicate = [NSPredicate predicateWithFormat: @"promo_type == %d && markedAsDeleted == %d", 2, 0];
        [fetchRequest setPredicate: predicate];
        
        fectchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:moc
                                                                          sectionNameKeyPath:@"index_character"
                                                                                   cacheName:nil];
        [fectchedResultsController setDelegate:self];
        
        NSError *error = nil;
        if (![fectchedResultsController performFetch:&error])
        {
            DDLogError(@"Error performing fetch: %@", error);
        }
    }
    return fectchedResultsController;
}

- (void)reloadTable
{
    [progressView hide:YES];
    
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    [self fetchPromosResultController];
    
    [self.gridView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([IMUtils getOSVersion] >= 7.0) {
        [self.gridView setContentOffset: CGPointMake(0, 64)];
        [self.gridView setFrame: CGRectMake(0, 64, 320, self.gridView.frame.size.height - 64)];
    }
    
    //imageList = [[NSMutableArray alloc] init];
    progressView = [[MBProgressHUD alloc] initWithView: self.view];
    progressView.labelText = @"Please wait...";
    [app.window addSubview: progressView];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTable) name:PERSONAL_PROMO_THIRDPARTY_UPDATED_NOTIFICATION object:nil];

}

- (void)viewDidAppear:(BOOL)animated
{
    [progressView show: YES];
    
    self.netManager = [app getIMGRImages: self];
    
    [self performSelector:@selector(reloadTable) withObject:nil afterDelay:3];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name: PERSONAL_PROMO_THIRDPARTY_UPDATED_NOTIFICATION object:nil];
    self.netManager.failDelegate = nil;
}

#pragma mark - CMNetManager Delegate methods
//This delegate gets called on success
-(void)netManager:(CMNetManager*)netManager didReceiveResponse:(NSDictionary*)dictionary
{
    
}

-(void)netManager:(CMNetManager*)netManager didReceiveResponseString:(NSString*)response
{
    
}

//This delegate gets called on failure
-(void)netManager:(CMNetManager*)netManager didFailWithError:(NSError*)error
{
    [progressView hide: YES];
    [IMUtils noInternetAvailable];
}

-(void)netManager:(CMNetManager *)netManager didGetSuccessToConnectWithServer:(NSError *)error
{
    
}

//This delegate gets called if internet connection is not available
-(void)netManager:(CMNetManager *)netManager didFailToConnectWithServer:(NSError *)error
{
    [progressView hide: YES];
    [IMUtils noInternetAvailable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing:editing animated:animated];
	[self.gridView setEditing:editing animated:animated];
	
}

- (void)didTapAction:(id)sender
{
#if DEBUG
	NSLog(@"Selected item count : %i", [self.gridView.selectedIndexes count]);
#endif
	[[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%i items selected.", [self.gridView.selectedIndexes count]]
								 message:nil
								delegate:nil
					   cancelButtonTitle:@"Dismiss"
					   otherButtonTitles:nil]  show];
}

#pragma mark - VCGridViewDataSource

- (NSInteger)numberOfCellsInGridView:(VCGridView*)gridView
{
	return [fectchedResultsController.fetchedObjects count];
}

- (VCGridViewCell *)gridView:(VCGridView *)gridView cellAtIndex:(NSInteger)index
{
    IMPromos* promo = [fectchedResultsController.fetchedObjects objectAtIndex: index];

	VCGridViewCell *cell = [gridView dequeueReusableCell];
	if (!cell) {
		cell = [[VCGridViewCell alloc] initWithFrame:CGRectZero];
		
		CGRect contentFrame = CGRectInset(cell.bounds, 0, 0);
        
		UIImageView *imageView = [[UIImageView alloc] initWithFrame:contentFrame];
		imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		imageView.contentMode = UIViewContentModeCenter;
        
		imageView.image = [UIImage imageWithContentsOfFile: [NSString stringWithFormat: @"%@/%@", [IMUtils getImageFolderPath], [promo.promo_image lastPathComponent]]];//[UIImage imageNamed:@"cell"];
        imageView.tag = PROMO_ICON_TAG;
		[cell.contentView addSubview:imageView];
        imageView = nil;
        
		UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:contentFrame];
		imageView1.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		imageView1.contentMode = UIViewContentModeBottomRight;
		imageView1.image = [UIImage imageNamed:@"check.png"];
        imageView1.tag = CHECK_ICON_TAG;
		cell.editingSelectionOverlayView = imageView1;
		imageView1 = nil;
	}
    else{
        
        UIImageView *imageView = (UIImageView*)[cell.contentView viewWithTag: PROMO_ICON_TAG];
        imageView.image = [UIImage imageWithContentsOfFile: [NSString stringWithFormat: @"%@/%@", [IMUtils getImageFolderPath], [promo.promo_image lastPathComponent]]];
        
        UIImageView *imageView1 = (UIImageView*)[cell.contentView viewWithTag: CHECK_ICON_TAG];
        imageView1.image = [UIImage imageNamed:@"check.png"];
        cell.editingSelectionOverlayView = imageView1;
        
    }
    cell.highlightedBackgroundView.backgroundColor = [UIColor redColor];
    
	return cell;
}

- (BOOL)gridView:(VCGridView *)gridView canEditCellAtIndex:(NSInteger)index
{
	return YES;
}

#pragma mark - VCGridViewDelegate

- (void)gridView:(VCGridView*)gridView didSelectCellAtIndex:(NSInteger)index
{
#if DEBUG
	NSLog(@"Selected %i", index);
#endif
    IMPromos* promo = [fectchedResultsController.fetchedObjects objectAtIndex: index];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"SELECTED_PROMO" object: [NSString stringWithFormat: @"%@/%@", [IMUtils getImageFolderPath], [promo.promo_image lastPathComponent]]];
    [self.navigationController popViewControllerAnimated: YES];
}

- (CGSize)sizeForCellsInGridView:(VCGridView *)gridView
{
	return CGSizeMake(75.0f, 75.0f);
}

- (CGFloat)paddingForCellsInGridView:(VCGridView *)gridView
{
	return 4.0f;
}


@end
