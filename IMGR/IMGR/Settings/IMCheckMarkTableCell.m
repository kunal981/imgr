//
//  IMCheckMarkTableCell.m
//  IMGR
//
//  Created by Satendra Singh on 2/6/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMCheckMarkTableCell.h"
#import "IMUtils.h"

@implementation IMCheckMarkTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:[IMCheckMarkTableCell cellReusableIdentifier]];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.textLabel.textColor = [UIColor darkGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected) {
        [self setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else
    {
        [self setAccessoryType:UITableViewCellAccessoryNone];
    }
    // Configure the view for the selected state
}

+ (IMCheckMarkTableCell *)cellLoadedFromNibFile
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    for (NSObject *obj in objects)
    {
        if ([obj isKindOfClass:[self class]])
        {
            IMCheckMarkTableCell *object = (IMCheckMarkTableCell *)obj;
            object.textLabel.font = [IMUtils appFontWithSize:17.0];
            object.textLabel.textColor = [IMUtils appDarkGreyColor];
            return object;
            
            break;
        }
    }
    return nil;
}

+ (NSString *) cellReusableIdentifier
{
    return @"IMCheckMarkTableCell";
}
@end
