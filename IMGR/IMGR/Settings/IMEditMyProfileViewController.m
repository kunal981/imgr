//
//  IMEditMyProfileViewController.m
//  IMGR
//
//  Created by Satendra Singh on 2/3/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMEditMyProfileViewController.h"
#import "CMPhotoPickerController.h"
#import "IMUtils.h"
#import "CMCoreDataHandler.h"
#import "IMContacts.h"
#import "HMDiallingCode.h"
#import "IMConstants.h"
#import "IMDataHandler.h"

#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

@interface IMEditMyProfileViewController ()<UITextFieldDelegate, UIActionSheetDelegate,UIAlertViewDelegate,HMDiallingCodeDelegate>
{
    BOOL isTextFieldEditing;
}
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberField;
@property (weak, nonatomic) IBOutlet UITextField *emailIDField;
@property (weak, nonatomic) IBOutlet UIButton *addPhotoButton;

- (IBAction)onDeleteAccountAction:(id)sender;
- (IBAction)onCancelAction:(id)sender;
- (IBAction)onDoneAction:(id)sender;
- (IBAction)onAddPhotoAction:(id)sender;

@end

@implementation IMEditMyProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([IMUtils getOSVersion] < 7.0)
        [self.navigationController.navigationBar setBackgroundImage: [UIImage imageNamed: @"navbar_small.png"] forBarMetrics: UIBarMetricsDefault];
    
    _firstNameField.delegate = self;
    _lastNameField.delegate = self;
    _phoneNumberField.delegate = self;
    _emailIDField.delegate = self;
    _phoneNumberField.enabled = NO;
    [_addPhotoButton setImage:nil forState:UIControlStateNormal];
    //NSString *myJid = [IMAppDelegate jidForPhoneNumber:[[IMAppDelegate sharedDelegate] getUserID]];
    
    NSDictionary* myVcard = [[NSUserDefaults standardUserDefaults] objectForKey:k_DEFAULTS_KEY_MY_VCARD];
    NSString *firstName = [myVcard objectForKey:@"First_Name"];
    NSString *lastName = [myVcard objectForKey:@"Last_Name"];
    
    _firstNameField.text = firstName;
    _lastNameField.text = lastName;
    _phoneNumberField.text = [IMUtils myPhoneNumber];
    //        _phoneNumberField.text = [vCard objectForKey:@"Phone_Number"];
    _emailIDField.text = [myVcard objectForKey:@"Email_Id"];
    UIImage *image = [UIImage imageWithData: [myVcard objectForKey:@"User_Image"]];
    if (image) {
        [_addPhotoButton setImage:image forState:UIControlStateNormal];
        
    }
//    [[CMChatManager sharedInstance] vcardInfoForHost:HOST_OPENFIRE JID:myJid completionBlock:^(NSDictionary *vCard, NSString *jid) {
//        if (NO ==[myJid isEqualToString:jid]) {
//            return;
//        }
//        NSString *firstName = [vCard objectForKey:@"First_Name"];
//        NSString *lastName = [vCard objectForKey:@"Last_Name"];
//        
//        _firstNameField.text = firstName;
//        _lastNameField.text = lastName;
//        _phoneNumberField.text = [IMUtils myPhoneNumber];
////        _phoneNumberField.text = [vCard objectForKey:@"Phone_Number"];
//        _emailIDField.text = [vCard objectForKey:@"Email_Id"];
//        UIImage *image = [vCard objectForKey:@"User_Image"];
//        if (image) {
//            [_addPhotoButton setImage:image forState:UIControlStateNormal];
//            
//        }
//    }];

	// Do any additional setup after loading the view.
    
    HMDiallingCode* dialingCode = [[HMDiallingCode alloc] initWithDelegate:self];
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    // Get carrier name
    NSString *carrierName = [carrier carrierName];
    if (carrierName != nil)
        DebugLog(@"Carrier: %@", carrierName);
    
    // Get mobile country code
    NSString *mcc = [carrier isoCountryCode];
    if (mcc != nil)
        DebugLog(@"Mobile Country Code (MCC): %@", mcc);
    
    // Get mobile network code
    NSString *mnc = [carrier mobileNetworkCode];
    
    if (mnc != nil)
        DebugLog(@"Mobile Network Code (MNC): %@", mnc);
    
    [dialingCode getDiallingCodeForCountry: mcc];
}

#pragma mark - HMDialingCode Delegate methods

- (void)failedToGetDiallingCode
{
    
}
- (void)didGetDiallingCode:(NSString *)diallingCode forCountry:(NSString *)countryCode
{
    DebugLog(@"Country Dialing Code: %@", diallingCode);
    
    NSString* str = [@"+" stringByAppendingString: diallingCode];
    _phoneNumberField.text = [NSString stringWithFormat: @"%@ %@", str, _phoneNumberField.text];
}

- (void)didGetCountries:(NSArray *)countries forDiallingCode:(NSString *)diallingCode
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onDeleteAccountAction:(id)sender {
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"Delete Account" message: @"Are you sure you want to delete the account?" delegate:self cancelButtonTitle: @"NO" otherButtonTitles: @"YES", nil];
    alertView.tag = 87;
    [alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 87) {
        if (buttonIndex) {
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", KBASEURL, kDeleteAccount,[[IMAppDelegate sharedDelegate] getUserID]]];
            NSLog(@"url=%@",url);
            NSURLRequest *urlrequest = [NSURLRequest requestWithURL:url];
            [NSURLConnection sendAsynchronousRequest:urlrequest queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                NSString *stringData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"stringData = %@",stringData);
                if (NSNotFound !=[stringData rangeOfString:@":true"].location) {
                    
                    [self deleteAllMessages];
                    [self deleteAllContactsAndPersonalPromos];
                    [[CMChatManager sharedInstance] goOfflineForHost:HOST_OPENFIRE];
                    [[CMChatManager sharedInstance] disconnectAccountWithHost:HOST_OPENFIRE];
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: @"Account deleted successfully" delegate:self cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
                    [alertView show];
                    
                }
                else{
                    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: @"Error observed in delete account" delegate:nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
                    [alertView show];
                }
                NSLog(@"response = %@",response);
            }];
        }
    }
    else
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        app.isRegistering = YES;
        [defaults removeObjectForKey:k_DEFAULTS_KEY_USER_ID];
        
        [defaults setBool:NO forKey:K_ALREADY_REGISTERED];
        [defaults setBool:YES forKey:k_DEFAULTS_KEY_PROMOS];
        [defaults setBool:YES forKey:k_DEFAULTS_KEY_NOTIFICATIONS];
        [defaults setBool:YES forKey:k_DEFAULTS_KEY_ROTATE_PERSONAL];
        [defaults setBool:YES forKey:k_DEFAULTS_KEY_ROTATE_ENABLED];
        [defaults setBool:YES forKey:k_DEFAULTS_KEY_ROTATE_SPONSORED];
        [defaults setBool:NO forKey:k_DEFAULTS_KEY_OVERRIDE_FRIEND_BUBBLE_COLOR];
        [defaults synchronize];

        [IMAppDelegate sharedDelegate].window.rootViewController = [[[IMAppDelegate sharedDelegate] registrationStoryboard] instantiateInitialViewController];
    }
}

- (NSArray *)openfireContactFetched
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMContacts"
                                              inManagedObjectContext:moc];
    NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"first_name" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects: sd2, nil];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat: @"is_imgr_user == %d", YES];
    [fetchRequest setPredicate: predicate];
    
    return [moc executeFetchRequest:fetchRequest error:nil];
}



- (IBAction)onCancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)onDoneAction:(id)sender {
    
    if ([self.emailIDField text].length && ![IMUtils validEmail:[self.emailIDField text]])
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: @"Please enter valid email address" delegate:nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
        [alertView show];
        [self.emailIDField becomeFirstResponder];
        return;
    }
    
    NSString *firstName = self.firstNameField.text;
    NSString *lastName = self.lastNameField.text;
    NSString *emailID = self.emailIDField.text;
    //NSString *phoneNo = self.phoneNumberField.text;
    
    NSMutableDictionary *myVcard = [[[NSUserDefaults standardUserDefaults] objectForKey:k_DEFAULTS_KEY_MY_VCARD] mutableCopy];
    if (firstName == nil) {
        firstName = @"";
    }
    if (lastName == nil) {
        lastName = @"";
    }
    if (emailID == nil) {
        emailID = @"";
    }
    
    [myVcard setObject:firstName forKey:@"First_Name"];
    [myVcard setObject:lastName forKey:@"Last_Name"];
    [myVcard setObject:emailID forKey:@"Email_Id"];
    
    UIImage* image = [self.addPhotoButton imageForState:UIControlStateNormal];
    NSData* imageData = UIImageJPEGRepresentation(image, .5);//UIImagePNGRepresentation(image);
    if([imageData length] > (8*1024)) {
        
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"Please choose a low resolution image", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        return;
    }
    
    if(imageData) {
        [myVcard setObject:imageData forKey:@"User_Image"];
    }
    else {
        imageData = UIImagePNGRepresentation([UIImage imageNamed:@"img_reg_placeholder.png"]);
        [myVcard setObject:imageData forKey:@"User_Image"];
    }
    [[NSUserDefaults standardUserDefaults] setObject:myVcard forKey:k_DEFAULTS_KEY_MY_VCARD];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[CMChatManager sharedInstance] setupMyVcardInHost:HOST_OPENFIRE firstName:self.firstNameField.text lastName:self.lastNameField.text phoneNumber:self.phoneNumberField.text emailID:self.emailIDField.text profilePhoto: imageData];
    
    [self onCancelAction:sender];
}




#pragma UITextFieldDelegate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (!isTextFieldEditing) {
        UIScrollView *view = self.mainScrollView;
        CGRect frame  = view.frame;
        frame.size.height -= IOS_KEYBOARD_HEIGHT;
        view.frame =frame;
        [view setContentOffset: CGPointMake(view.contentOffset.x, 44*textField.tag) animated:YES];

        isTextFieldEditing = YES;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int charCount = [newString length];
    
    if([string isEqualToString:@" "] && charCount == 1) {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    isTextFieldEditing = NO;
    UIScrollView *view = self.mainScrollView;
    CGRect frame  = view.frame;
    frame.size.height += IOS_KEYBOARD_HEIGHT;
    view.frame =frame;
    [textField resignFirstResponder];
    [view setContentOffset:CGPointMake(view.contentOffset.x, 0) animated:YES];
    UITextField* tField = (UITextField*)[self.view viewWithTag: textField.tag + 1];
    if (textField.tag == 1) {//SS skip phone number , as we can not change the registered phone number
         tField = self.emailIDField;
    }
    [tField becomeFirstResponder];
    
    return YES;
}

- (IBAction)onAddPhotoAction:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Remove Photo" otherButtonTitles:@"Choose from existing",@"Take Photo", nil];
    [actionSheet showInView:self.view];

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:

            [_addPhotoButton setImage:[UIImage imageNamed:@"img_reg_placeholder.png"] forState:UIControlStateNormal];
            break;
        case 1:
            [[CMPhotoPickerController sharedInstance] displayImagePickerWithType:1 target:self];
            [self.navigationController presentViewController: [CMPhotoPickerController sharedInstance] animated: YES completion: nil];
            break;
            
        case 2:
            [[CMPhotoPickerController sharedInstance] displayImagePickerWithType:2 target:self];
            [self.navigationController presentViewController: [CMPhotoPickerController sharedInstance] animated: YES completion: nil];
            break;
            
        case 3:
            break;
            
        default:
            break;
    }
}

#pragma mark - UIImagePicker Delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    if([mediaType isEqualToString:@"public.image"])
    {
        UIImage* newImage = [info valueForKey: UIImagePickerControllerEditedImage];
        
        UIImage* image = [IMUtils circularScaleNCrop:newImage rect: CGRectMake(0, 0, 80, 80)];
        
        [_addPhotoButton setImage:image forState:UIControlStateNormal];
    }
    else if ( [ mediaType isEqualToString:@"public.movie" ])
    {
        
    }
    [picker dismissViewControllerAnimated:YES completion: nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion: nil];
}

- (void) deleteAllMessages
{
    NSManagedObjectContext *moc = [[CMMessageArchivingCoreDataStorage sharedInstance] mainThreadManagedObjectContext];
    if(moc){
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CMMessageArchiving_Contact_CoreDataObject"
                                              inManagedObjectContext:moc];
    
    NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"mostRecentMessageTimestamp" ascending:NO];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, nil];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:sortDescriptors];
        NSArray *AllContacts = [moc executeFetchRequest:fetchRequest error:nil];
        for (CMMessageArchiving_Contact_CoreDataObject *contact in AllContacts) {
            [[CMChatManager sharedInstance] deleteAllMessagesWithHost:HOST_OPENFIRE forJID:contact.bareJidStr];
        }
    }
}

- (void) deleteAllContactsAndPersonalPromos
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    NSArray *allContacts = [[IMDataHandler sharedInstance] fetchAllContacts];
    for (IMContacts *contact in allContacts) {
        [moc deleteObject:contact];
    }
    NSArray *personalPromos = [[IMDataHandler sharedInstance] fetchPersonalPromos];
    for (IMPromos *prom in personalPromos) {
        [moc deleteObject:prom];
    }
    [moc save:nil];
}

@end
