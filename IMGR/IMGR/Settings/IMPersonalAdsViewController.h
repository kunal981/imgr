//
//  IMPersonalAdsViewController.h
//  IMGR
//
//  Created by Satendra Singh on 2/5/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMPersonalAdsViewController : UIViewController <UISearchDisplayDelegate, UISearchBarDelegate>
{
    IBOutlet UISearchBar*       promoSearchBar;
}

@property (nonatomic, readonly)     NSFetchedResultsController* fectchedResultsController ;
@property(strong, nonatomic)UISearchBar* promoSearchBar;

- (IBAction)onPromoInfoButtonClick:(id)sender;
- (IBAction)onSelectAllTapped:(id)sender;

@end
