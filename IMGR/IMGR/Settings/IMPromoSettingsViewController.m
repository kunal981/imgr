//
//  IMPromoSettingsViewController.m
//  IMGR
//
//  Created by Satendra Singh on 2/4/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMPromoSettingsViewController.h"
#import "IMDataHandler.h"

@interface IMPromoSettingsViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSUInteger rowsInSectionOne;
    NSUInteger rowsInSectionTwo;
}

@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (strong, nonatomic) IBOutlet UIImageView *tableBackground;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellSponsoredAds;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellEditSponsoredAds;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellPersonalPromos;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellEditPersonalPromos;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellRotatePromos;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellCreatePromos;
@property (weak, nonatomic) IBOutlet UISwitch *sponsoredSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *rotateSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *personalSwitch;
- (IBAction)userDidSwitchSponsored:(id)sender;
- (IBAction)userDidSwitchPersonal:(id)sender;
- (IBAction)userDidSwitchRotate:(id)sender;
- (IBAction)userDidTappedCreateNewPromo:(id)sender;

@end

@implementation IMPromoSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mainTable.dataSource = self;
    self.mainTable.backgroundView = self.tableBackground;
    rowsInSectionOne = 2;
    rowsInSectionTwo = 2;

    self.personalSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_ROTATE_PERSONAL];
    if (!self.personalSwitch.on) {
        rowsInSectionTwo--;
    }
    self.sponsoredSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_ROTATE_SPONSORED];
    if (!self.sponsoredSwitch.on) {
        rowsInSectionOne--;
    }
    self.rotateSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_ROTATE_ENABLED];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    if (section == 0) {
        return 20.0;
    }
    return 22;
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (0 == section) {
        return rowsInSectionOne;
    }
    else if (1 == section) {
        return rowsInSectionTwo;
    }
    else
    {
        return 1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (3 == indexPath.section)
    {
        return 60;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
            
        case 0:
        {
            if (indexPath.row) {
                return _cellEditSponsoredAds;
            }
            else
            {
                return _cellSponsoredAds;
            }
        }
            break;
            
        case 1:
        {
            if (indexPath.row) {
                return _cellEditPersonalPromos;
            }
            else
            {
                return _cellPersonalPromos;
            }
        }
            break;
        
        case 2:
        {
            return _cellRotatePromos;
            
        }
            break;
            
        case 3:
        {
            return _cellCreatePromos;
        }
            break;
            
        default:
            break;
    }
    return nil;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 10.0;
//}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([(UIButton *)sender tag]) {
        [[segue destinationViewController] setTitle:@"Personal Promos"];
    }
    else
    {
       [[segue destinationViewController] setTitle:@"Sponsored Ads"];
    }
}
- (IBAction)userDidSwitchSponsored:(UISwitch *)sender {
   
    if (sender.isOn) {
        rowsInSectionOne ++;
        [self.mainTable insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else
    {
        rowsInSectionOne --;
        [self.mainTable deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:sender.isOn] forKey:k_DEFAULTS_KEY_ROTATE_SPONSORED];
}

- (IBAction)userDidSwitchPersonal:(UISwitch *)sender {
    if (sender.isOn) {
        rowsInSectionTwo ++;
        [self.mainTable insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:1 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];

    }
    else
    {
        rowsInSectionTwo --;
        [self.mainTable deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:1 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];

    }
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:sender.isOn] forKey:k_DEFAULTS_KEY_ROTATE_PERSONAL];
}

- (IBAction)userDidSwitchRotate:(UISwitch *)sender {
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:sender.isOn] forKey:k_DEFAULTS_KEY_ROTATE_ENABLED];

}

- (IBAction)userDidTappedCreateNewPromo:(id)sender {
    
    if ([[IMDataHandler sharedInstance] fetchPersonalPromos].count < 12) {
        [self performSegueWithIdentifier:@"CreateNewPromo" sender:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"You can not create more than 12 personal promos" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alertView show];
    }}


@end
