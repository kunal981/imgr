//
//  IMAddPersonalPromoController.m
//  IMGR
//
//  Created by Satendra Singh on 2/5/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMAddPersonalPromoController.h"
#import "IMTextFieldTableCell.h"
#import "VCGridViewController.h"
#import "CMPhotoPickerController.h"
#import "IMUtils.h"
#import "IMPromos.h"
#import "IMDataHandler.h"
#import "CMCoreDataHandler.h"
#import "MBProgressHUD.h"

@interface IMAddPersonalPromoController ()<UITextFieldDelegate, UIActionSheetDelegate>
{
    BOOL isTextFieldEditing;
    BOOL isPromoImageremovedOrChanged;
    BOOL        isIMGRImage;
    NSString*   promoImageName;
    BOOL hasPromoImage;
    UIImage *selectedImage;
    
    MBProgressHUD* progressView;
}
@property (weak, nonatomic) IBOutlet UIImageView *promoIcon;
@property (weak, nonatomic) IBOutlet UITableViewCell *deleteButtonCell;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
- (IBAction)userDidTappedOnImage:(id)sender;

- (IBAction)userDidCancel:(id)sender;
- (IBAction)userDidCreate:(id)sender;
- (IBAction)userDidTappedInfo:(id)sender;
@end

@implementation IMAddPersonalPromoController
@synthesize promo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    if([IMUtils getOSVersion] < 7.0)
        [self.navigationController.navigationBar setBackgroundImage: [UIImage imageNamed: @"navbar_small.png"] forBarMetrics: UIBarMetricsDefault];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedPromo:) name:@"SELECTED_PROMO" object:nil];
    _promoNameField.delegate = self;
    _promoMessageField.delegate = self;
    _promoLinkField.delegate = self;
    _promoLinkTextField.delegate = self;
    
    if (NO == self.isupdatingPromo) {
        self.deleteButtonCell.hidden = YES;
        CGSize contentSize = self.mainScrollView.contentSize;
        contentSize.height -= self.deleteButtonCell.frame.size.height;
        self.mainScrollView.contentSize = contentSize;
        self.promoLinkField.text = @"http://";//Added default text
        rightBarButton.title = NSLocalizedString(@"CREATE", @"");
        rightBarButton.enabled = NO;
    }
    else
    {
        rightBarButton.title = NSLocalizedString(@"DONE", @"Done");
        self.promoNameField.text = promo.promo_name;
        if(promo.promo_message.length)
           self.promoMessageField.text = promo.promo_message;
        if (promo.promo_link.length) {
            self.promoLinkField.text = promo.promo_link;
        }
        else
            self.promoLinkField.text = @"http://";//Added default tex
        self.promoLinkTextField.text = promo.promo_link_text;
        
        self.title = promo.promo_name;
        isIMGRImage = YES;
        NSString* filePath = [[IMUtils getImageFolderPath] stringByAppendingPathComponent: [promo.promo_image lastPathComponent]];
        BOOL isDirectory;
        BOOL fileExist = [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
        if (fileExist & !isDirectory) {
            hasPromoImage = YES;
            selectedImage = [UIImage imageWithContentsOfFile: filePath];
            [promoIconButton setImage:selectedImage forState:UIControlStateNormal];
        }
        else {
            [promoIconButton setImage: [UIImage imageNamed: @"thumb_add_promo_img.png"] forState: UIControlStateNormal];
        }
    }
    
    progressView = nil;//SS we do not need it
    progressView = [[MBProgressHUD alloc] initWithView: self.view];
    progressView.labelText = @"Please wait...";
    
    [app.window addSubview: progressView];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)selectedPromo: (NSNotification*)notif
{
    promoImageName = [notif object];
    
    isIMGRImage = YES;
    isPromoImageremovedOrChanged = YES;
    if([[NSFileManager defaultManager] fileExistsAtPath:promoImageName]){
        hasPromoImage = YES;
        [promoIconButton setImage: [UIImage imageWithContentsOfFile: promoImageName] forState: UIControlStateNormal];
    }
}

- (void)deletePromo
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString *jidString = [app getUserID];//;[[NSUserDefaults standardUserDefaults] valueForKey:k_DEFAULTS_KEY_USER_ID];
    DebugLog(@"jidString = %@", jidString);
    //    NSRange range = [jidString rangeOfString: @"@"];
    //    jidString = [jidString substringToIndex: range.location];
    if(jidString != nil)
        [parameters setObject:jidString forKey: @"username"];
    [parameters setObject:[NSString stringWithFormat: @"%d",[promo.promo_id intValue]] forKey: @"promo_id"];
    
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kDeletePromo] parameters:parameters queueIdentifier:"deletePromoQueue" queueType:CONCURRENT httpMethod: GET];
    netManager.delegate = self;
    netManager.tag = E_DELETE_PROMO_SERVICE;
    [netManager startRequest];
}

- (IBAction)onDeletePromoButtonClick:(id)sender
{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"DELETE PROMO", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"NO",@"") otherButtonTitles:NSLocalizedString(@"YES",@""), nil];
    [alertView show];
}
//-(void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showKeyboard:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
//}
#pragma mark - UIAlertView Delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 69) {
        [self dismissViewControllerAnimated: YES completion: ^{
        }];
    }
    switch (buttonIndex)
    {
        case 0:
            
            break;
            
        case 1:
        {
            [self deletePromo];
            
        }break;
            
        default:
            break;
    }
}

- (void)showLoader
{
    [progressView show: YES];
}

- (BOOL)validateFieldValues {
    
    if(!self.promoLinkField.text.length && self.promoLinkTextField.text.length)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: NSLocalizedString(@"LINK TEXT", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        return NO;
    }
    if ([self.promoLinkField.text isEqualToString: @"http://"] || [self.promoLinkField.text isEqualToString: @"https://"])
    {
        self.promoLinkField.text = @"";
    }
    
    if (0 == self.promoNameField.text.length)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message: NSLocalizedString(@"Please enter promo name.", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", @""), nil];
        [alertView show];
        return NO;
    }
   /* else if (0 == self.promoMessageField.text.length)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message: NSLocalizedString(@"Please enter promo message.", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", @""), nil];
        [alertView show];
        return NO;
    }*/
    if (!hasPromoImage)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message: NSLocalizedString(@"Please add some image for promo.", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", @""), nil];
        [alertView show];
        return NO;
    }
    return YES;
}

#pragma mark - WebService methods
- (void)createPersonalPromo
{
    if(![self validateFieldValues])
        return;
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    
    
    NSString *jidString = [app getUserID];
    DebugLog(@"jidString = %@", jidString);
    if(jidString != nil)
        [parameters setObject:jidString forKey: @"username"];
    
    [parameters setObject:self.promoNameField.text forKey: @"promo_name"];
    if(self.promoMessageField.text.length)
        [parameters setObject:self.promoMessageField.text forKey: @"promo_message_header"];

    
    
   // if(self.promoMessageField.text.length)
        
    if(self.promoLinkField.text.length)
    {
        NSString *urlStr = self.promoLinkField.text;
        if (NSNotFound == [urlStr rangeOfString:@"http"].location) {
        urlStr = [@"http://" stringByAppendingString:urlStr];
        }
        [parameters setObject:urlStr forKey: @"promo_link"];
        
        
        if(self.promoLinkTextField.text.length)
            [parameters setObject:self.promoLinkTextField.text forKey: @"promo_link_text"];
        else
            [parameters setObject:@"Tap here to learn more" forKey: @"promo_link_text"];
        
    }
    

    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    if (hasPromoImage) {
        if(isIMGRImage ) {
            [parameters setObject:[NSString stringWithFormat: @"uploads/promos/%@", [promoImageName lastPathComponent]] forKey: @"promo_image_url"];
        }
        else {
            NSData* data = UIImagePNGRepresentation(selectedImage);
            if(data != nil)
                [parameters setObject:data forKey: @"promo_image"];
        }
    }
    [self showLoader];
    DebugLog(@"Parameters = %@", parameters);
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kCreatePromo] parameters:parameters queueIdentifier:"createPromoQueue" queueType:CONCURRENT httpMethod: POST];
    netManager.delegate = self;
    netManager.tag = E_CREATE_PROMO_SERVICE;
    [netManager startRequest];
}

- (void)updatePersonalPromo
{
    if(![self validateFieldValues])
        return;

    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString *jidString = [app getUserID];
    
    DebugLog(@"jidString = %@", jidString);

    if(jidString != nil)
        [parameters setObject:jidString forKey: @"username"];
    [parameters setObject:[NSString stringWithFormat: @"%d", [promo.promo_id intValue]] forKey: @"promo_id"];
    [parameters setObject:self.promoNameField.text forKey: @"promo_name"];
   
    
    if(self.promoMessageField.text.length)
       [parameters setObject:self.promoMessageField.text forKey: @"promo_message_header"];
    
    
    NSLog(@"promoMsg=%@",self.promoMessageField.text);
    if(self.promoLinkField.text.length)
    {
        NSString *urlStr = self.promoLinkField.text;
        if (NSNotFound == [urlStr rangeOfString:@"http"].location) {
            urlStr = [@"http://" stringByAppendingString:urlStr];
        }
        [parameters setObject:urlStr forKey: @"promo_link"];
        
        if(self.promoLinkTextField.text.length)
            [parameters setObject:self.promoLinkTextField.text forKey: @"promo_link_text"];
        else
            [parameters setObject:@"Tap here to learn more" forKey: @"promo_link_text"];
    }
    
    if (hasPromoImage) {
        if(isIMGRImage && promoImageName) {
            [parameters setObject:[NSString stringWithFormat: @"uploads/promos/%@", [promoImageName lastPathComponent]] forKey: @"promo_image_url"];
        }
        else {
            NSData* data = UIImagePNGRepresentation(selectedImage);
            if (data) {
                [parameters setObject:data forKey: @"promo_image"];
            }
        }
    }
    [self showLoader];
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kUpdatePromo] parameters:parameters queueIdentifier:"updatePromoQueue" queueType:CONCURRENT httpMethod: POST];
    netManager.delegate = self;
    netManager.tag = E_UPDATE_PROMO_SERVICE;
    [netManager startRequest];
}
#pragma mark - CMNetManager Delegate methods
//This delegate gets called on success
-(void)netManager:(CMNetManager*)netManager didReceiveResponse:(NSDictionary*)dictionary
{
    [progressView hide: YES];
    DebugLog(@"Response = %@", dictionary);
    if ([dictionary objectForKey:@"success"] && [[dictionary objectForKey:@"success"] integerValue] == 0)
    {
        switch (netManager.tag)
        {
            case E_CREATE_PROMO_SERVICE:
            {
                int code = [[[dictionary objectForKey: @"error"] objectForKey: @"errorCode"] intValue];
                switch (code) {
                    case 1008:
                    {
                        UIAlertView* registrationAlertView = [[UIAlertView alloc] initWithTitle: @"" message:@"You cannot create more than 12 promos" delegate:nil cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
                        [registrationAlertView show];
                    }break;
                        
                    default:
                        break;
                }
            }break;
                
            default:
                break;
        }
    }
    else
    {
    switch (netManager.tag)
    {
        case E_CREATE_PROMO_SERVICE:
        {
            [[IMAppDelegate sharedDelegate] getPersonalAds];
            [self dismissViewControllerAnimated: YES completion: ^{}];
         
            
        }break;
            
        case E_UPDATE_PROMO_SERVICE:
        {
            promo.promo_name    = self.promoNameField.text;
            promo.promo_link    = self.promoLinkField.text;
            if(self.promoMessageField.text.length)
              promo.promo_message = self.promoMessageField.text;
            else
                promo.promo_message=@"";
            
            if(self.promoLinkField.text.length)
            {
                if(self.promoLinkTextField.text.length)
                    promo.promo_link_text = self.promoLinkTextField.text;
                else
                    promo.promo_link_text = @"Tap here to learn more";
            }
            
            if (isPromoImageremovedOrChanged) {
                NSString* filePath = [[IMUtils getImageFolderPath] stringByAppendingPathComponent: [promo.promo_image lastPathComponent]];
                if ([[NSFileManager defaultManager]fileExistsAtPath:filePath]) {
                    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
                }
                
                promo.promo_image = [[dictionary objectForKey: @"data"] objectForKey: @"promo_image"];
                
                if (!isIMGRImage) {
                    filePath = [[IMUtils getImageFolderPath] stringByAppendingPathComponent: [promo.promo_image lastPathComponent]];
                    NSData* data = UIImageJPEGRepresentation(selectedImage, 0.8);
                    
                    if (data) {
                        [data writeToFile:filePath atomically:YES];
                    }
                }
                else//Need to fillup for IMGR image
                {
                    [[IMAppDelegate sharedDelegate] downloadPromoImageForPromo:promo];
                }
            }
            [[IMDataHandler sharedInstance] saveDatabase];

            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: NSLocalizedString(@"PROMO UPDATED", @"") delegate: self cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
            alertView.tag = 69;
            [alertView show];
        }break;
            
        case E_DELETE_PROMO_SERVICE:
        {
            BOOL isDeleted = [[IMDataHandler sharedInstance] deletePromo: promo];
            DebugLog(@"Promo Deleted = %d", isDeleted);
            [self dismissViewControllerAnimated:YES completion:^{}];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"DELETE_PROMO" object: nil];
        }break;
            
        default:
            break;
    }
    }
}

-(void)netManager:(CMNetManager*)netManager didReceiveResponseString:(NSString*)response
{
    
}

//This delegate gets called on failure
-(void)netManager:(CMNetManager*)netManager didFailWithError:(NSError*)error
{
    [progressView hide: YES];
    
//    UIAlertView* registrationAlertView = [[UIAlertView alloc] initWithTitle: @"" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
//    [registrationAlertView show];
}

-(void)netManager:(CMNetManager *)netManager didGetSuccessToConnectWithServer:(NSError *)error
{
    
}

//This delegate gets called if internet connection is not available
-(void)netManager:(CMNetManager *)netManager didFailToConnectWithServer:(NSError *)error
{
    [progressView hide: YES];
    
    [IMUtils noInternetAvailable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)userDidTappedOnImage:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL",@"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"FROM IMGR",@""),NSLocalizedString(@"CHOOSE PHOTO", @""),NSLocalizedString(@"TAKE PHOTO",@""), nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex = %d",buttonIndex);
    switch (buttonIndex) {
        case -1://To delete promo image, not in use now
        {
            [promoIconButton setImage: [UIImage imageNamed: @"thumb_add_promo_img.png"] forState: UIControlStateNormal];
            isPromoImageremovedOrChanged = YES;
            hasPromoImage = NO;
        }
            break;
            
        case 0:
            if (isTextFieldEditing) {
                UIScrollView *view = self.mainScrollView;
                CGRect frame  = view.frame;
                frame.size.height += IOS_KEYBOARD_HEIGHT;
                view.frame =frame;
                [view setContentOffset:CGPointMake(view.contentOffset.x, 0) animated:YES];
                isTextFieldEditing = NO;
            }
            [_promoNameField resignFirstResponder];
            [_promoMessageField resignFirstResponder];
            [_promoLinkField resignFirstResponder];
            [_promoLinkTextField resignFirstResponder];
            [self performSegueWithIdentifier:@"IMGridPhotoViewController" sender:self];
            break;

        case 1:
            if (isTextFieldEditing) {
                UIScrollView *view = self.mainScrollView;
                CGRect frame  = view.frame;
                frame.size.height += IOS_KEYBOARD_HEIGHT;
                view.frame =frame;
                [view setContentOffset:CGPointMake(view.contentOffset.x, 0) animated:YES];
                isTextFieldEditing = NO;
            }
            [_promoNameField resignFirstResponder];
            [_promoMessageField resignFirstResponder];
            [_promoLinkField resignFirstResponder];
            [_promoLinkTextField resignFirstResponder];
            
            [[CMPhotoPickerController sharedInstance] displayImagePickerWithType:1 target:self];
            [self.navigationController presentViewController: [CMPhotoPickerController sharedInstance] animated: YES completion: nil];
            break;
            
            
        case 2:
            if (isTextFieldEditing) {
                UIScrollView *view = self.mainScrollView;
                CGRect frame  = view.frame;
                frame.size.height += IOS_KEYBOARD_HEIGHT;
                view.frame =frame;
                [view setContentOffset:CGPointMake(view.contentOffset.x, 0) animated:YES];
                isTextFieldEditing = NO;
            }
            [_promoNameField resignFirstResponder];
            [_promoMessageField resignFirstResponder];
            [_promoLinkField resignFirstResponder];
            [_promoLinkTextField resignFirstResponder];
            [[CMPhotoPickerController sharedInstance] displayImagePickerWithType:2 target:self];
            [self.navigationController presentViewController: [CMPhotoPickerController sharedInstance] animated: YES completion: nil];
            break;
            
        case 4:
            
            break;
            
            
        default:
            break;
    }
}


#pragma mark - UIImagePicker Delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    if([mediaType isEqualToString:@"public.image"])
    {
        UIImage* newImage = [info valueForKey: UIImagePickerControllerEditedImage];
        
        selectedImage  = [IMUtils resizeImage:newImage toSize:CGSizeMake(17, 17)];
        hasPromoImage = YES;
        //self.promoIcon.image = image;
        [promoIconButton setImage: [IMUtils resizeImage:newImage toSize:CGSizeMake(34, 34)] forState: UIControlStateNormal];
        isIMGRImage = NO;
        isPromoImageremovedOrChanged = YES;
    }
    else if ( [ mediaType isEqualToString:@"public.movie" ])
    {
        
    }
    [picker dismissViewControllerAnimated:YES completion: nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion: nil];
}

- (IBAction)userDidCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)userDidCreate:(id)sender {
    
    if([rightBarButton.title isEqualToString: @"Create"])
        [self createPersonalPromo];
    else
        [self updatePersonalPromo];
//    [self dismissViewControllerAnimated:YES completion:^{
//        
//    }];
}

- (IBAction)userDidTappedInfo:(id)sender {
    [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"PROMO IMAGE SIZE", @"") delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
}

#pragma UITextFieldDelegate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (!isTextFieldEditing) {
        UIScrollView *view = self.mainScrollView;
        CGRect frame  = view.frame;
        frame.size.height -= IOS_KEYBOARD_HEIGHT;
        view.frame =frame;
        [view setContentOffset:CGPointMake(view.contentOffset.x, 88*textField.tag) animated:YES];
        isTextFieldEditing = YES;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if(textField == self.promoLinkTextField && [self.promoLinkField.text isEqualToString: @""])
//    {
//        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: NSLocalizedString(@"LINK TEXT", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [alertView show];
//        return;
//    }
}

- (BOOL)backSpaceDetection: (NSString*)string fieldID: (int)index
{
    //Backspace Detection
    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char, "\b");
    
    if (isBackSpace == -8) {
        
        return YES;
    }
    return NO;
}

- (BOOL) validateLinkURL: (NSString *) url
{
    NSString *regex = @"((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?";
    
    NSPredicate *regextest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    if ([regextest evaluateWithObject: url] == YES) {
        return YES;
    } else {
        return NO;
    }
    
    return [regextest evaluateWithObject:url];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int charCount = [newString length];
    
    if([self backSpaceDetection: string fieldID: textField.tag])
        return YES;
    
    if(textField == self.promoNameField && charCount == 0)
        rightBarButton.enabled = NO;
    
    if(textField == self.promoLinkTextField && [self.promoLinkField.text isEqualToString: @""])
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: NSLocalizedString(@"LINK TEXT", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        return NO;
    }
    if(textField == self.promoNameField && charCount > 0)
    {
        rightBarButton.enabled = YES;
    }
    
    if(textField == self.promoNameField && charCount > 35)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: NSLocalizedString(@"Promo name should not exceed 35 characters", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", @""), nil];
        [alertView show];
        return NO;
    }
    if((textField == self.promoMessageField && charCount > 24) || (textField == self.promoLinkTextField && charCount > 24))
        return NO;
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    isTextFieldEditing = NO;
    UIScrollView *view = self.mainScrollView;
    CGRect frame  = view.frame;
    frame.size.height += IOS_KEYBOARD_HEIGHT;
    view.frame =frame;
    [textField resignFirstResponder];
    [view setContentOffset:CGPointMake(view.contentOffset.x, 0) animated:YES];
    UITextField* tField = (UITextField*)[self.mainScrollView viewWithTag: textField.tag + 1];
    [tField becomeFirstResponder];
    
    return YES;
}

@end
