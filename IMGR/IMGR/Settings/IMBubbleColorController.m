//
//  IMBubbleColorController.m
//  IMGR
//
//  Created by Satendra Singh on 2/4/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMBubbleColorController.h"
#import "IMCheckMarkTableCell.h"

@interface IMBubbleColorController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSInteger selectedRow;
}

@property (weak, nonatomic) IBOutlet UIImageView *bubbleImage;
@property (weak, nonatomic) IBOutlet UITableView *colorTable;

@property (weak, nonatomic) IBOutlet UITableViewCell *tableFooter;
@property (weak, nonatomic) IBOutlet UILabel *bubbleTextLabel;
@property (weak, nonatomic) IBOutlet UISwitch *overRideColorSwitch;
- (IBAction)userDidTappedOverrideFriendColor:(id)sender;
- (IBAction)userDidTappedDone:(id)sender;

@end

@implementation IMBubbleColorController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.colorTable.dataSource = self;
    self.colorTable.delegate = self;
   selectedRow = [[NSUserDefaults standardUserDefaults] integerForKey:k_DEFAULTS_KEY_BUBBLE_COLOR] - 1;
    if (selectedRow < 0) {
        selectedRow = 0;
    }
    NSIndexPath *selectedPath = [NSIndexPath indexPathForRow:selectedRow inSection:0];
    [self.colorTable selectRowAtIndexPath:selectedPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    [self tableView:self.colorTable didSelectRowAtIndexPath:selectedPath];
    [self.overRideColorSwitch setOn:[[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_OVERRIDE_FRIEND_BUBBLE_COLOR]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    if (0 == section) {
    // Return the number of rows in the section.
    return 3;
    }
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
        return 22;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (0 == indexPath.section) {
        // Return the number of rows in the section.
        return 44.0;
    }
    return 55.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section) {
        return _tableFooter;
    }
    
    IMCheckMarkTableCell *cell = [tableView dequeueReusableCellWithIdentifier:[IMCheckMarkTableCell cellReusableIdentifier]];
    if (nil == cell) {
        cell = [IMCheckMarkTableCell cellLoadedFromNibFile];
    }
    // Configure the cell...
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Blue";
            break;
            
        case 1:
            cell.textLabel.text = @"Green";
            break;
            
        case 2:
            cell.textLabel.text = @"Purple";
            break;
        default:
            break;
    }
    return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (0 == indexPath.section) {
        if (0 == indexPath.row) {
            self.bubbleImage.image = [UIImage imageNamed:@"bubble_blue_settings"];
            self.bubbleTextLabel.text = @"I am Blue bubble";
        }
        else if(1 == indexPath.row)
        {
            self.bubbleImage.image = [UIImage imageNamed:@"bubble_green_settings"];
            self.bubbleTextLabel.text = @"I am Green bubble";

        }
        else
        {
            self.bubbleImage.image = [UIImage imageNamed:@"bubble_purple_settings"];
            self.bubbleTextLabel.text = @"I am Purple bubble";
        }
        selectedRow = indexPath.row;
        [self userDidTappedDone:nil];

    }
}

//- (void) loadView
//{
//    self.tableView = _customTableView;
//    [super loadView];
//}
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section) {
        return [NSIndexPath indexPathForRow:selectedRow inSection:0];
    }
    return indexPath;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)userDidTappedOverrideFriendColor:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setBool:self.overRideColorSwitch.on forKey:k_DEFAULTS_KEY_OVERRIDE_FRIEND_BUBBLE_COLOR];
}

- (IBAction)userDidTappedDone:(id)sender
{
    [[CMChatManager sharedInstance] setMyBubbleColor:selectedRow + 1 forHost:HOST_OPENFIRE];
    [[NSUserDefaults standardUserDefaults] setInteger:selectedRow + 1 forKey:k_DEFAULTS_KEY_BUBBLE_COLOR];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
