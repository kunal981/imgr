//
//  IMSponsoredAdsViewController.m
//  IMGR
//
//  Created by Satendra Singh on 2/4/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMSponsoredAdsViewController.h"
#import "IMPromoHeaderView.h"
#import "IMDataHandler.h"
#import "CMCoreDataHandler.h"
#import "IMPromoSearchCell.h"

#define SELECTED_BG_VIEW_TAG 765

@interface IMSponsoredAdsViewController ()<NSFetchedResultsControllerDelegate, IMDownloadPictureOperationDelegate>
{
    UITableViewController *_tableViewController;
    
    NSFetchedResultsController* fectchedResultsController;
    
    IMOperationQueue* queueOperation;
    BOOL isSelectDeselectPressesd;
}

@property (weak, nonatomic) IBOutlet UITableView *sponsoredAdsTableView;
@property (weak, nonatomic) IBOutlet UIButton *selectAllButton;

- (IBAction)onEditTapped:(UIBarButtonItem *)sender;
- (IBAction)onSelectAllTapped:(id)sender;

@end

@implementation IMSponsoredAdsViewController
@synthesize promoSearchBar;
@synthesize fectchedResultsController = fectchedResultsController;

- (id)initWithStyle:(UITableViewStyle)style
{
//    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSFetchedResultsController*)fetchPromosResultController: (NSString*)searchString
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    if(fectchedResultsController == nil)
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMPromos"
                                                  inManagedObjectContext:moc];
        
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"index_character" ascending:YES];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"promo_name" ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, sd2, nil];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        if(searchString)
        {
            NSPredicate* predicate = [NSPredicate predicateWithFormat: @"promo_type == %d && promo_name contains[c] %@ && markedAsDeleted == %d", 0, searchString,0];
            [fetchRequest setPredicate: predicate];
        }
        else
        {
            NSPredicate* predicate = [NSPredicate predicateWithFormat: @"promo_type == %d && markedAsDeleted == %d", 0,0];
            [fetchRequest setPredicate: predicate];
        }
        fectchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                        managedObjectContext:moc
                                                                                                          sectionNameKeyPath:@"index_character"
                                                                                                                   cacheName:nil];
        [fectchedResultsController setDelegate:self];
        
        NSError *error = nil;
        if (![fectchedResultsController performFetch:&error])
        {
            DDLogError(@"Error performing fetch: %@", error);
        }
    }
    return fectchedResultsController;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (self.sponsoredAdsTableView.isEditing) {
        return;
    }
    //[self.sponsoredAdsTableView reloadData];
    [self performSelector:@selector(updateSponsoredAdsTable) withObject:nil afterDelay:.1];
}

- (void)reloadFetchedResults: (NSNotification*)notif
{
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    [self fetchPromosResultController: nil];
    [self.sponsoredAdsTableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectAllButton.hidden = YES;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadFetchedResults:)
                                                 name:@"SomethingChanged"
                                               object:nil];
    
//    queueOperation = [[IMOperationQueue alloc] init];
    fectchedResultsController = nil;
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    _tableViewController = [[UITableViewController alloc]initWithStyle:UITableViewStylePlain];
    [self addChildViewController:_tableViewController];
    
    _tableViewController.refreshControl = [UIRefreshControl new];
    [_tableViewController.refreshControl addTarget:self action:@selector(loadStream) forControlEvents:UIControlEventValueChanged];
    
    _tableViewController.tableView = _sponsoredAdsTableView;
    _sponsoredAdsTableView.contentOffset = CGPointMake(0, SEARCH_BAR_HEIGHT);

    _sponsoredAdsTableView.delegate = self;
    _sponsoredAdsTableView.dataSource = self;

    [self didMoveToParentViewController: _tableViewController];
	// Do any additional setup after loading the view.
    
    if([IMUtils getOSVersion] >= 7.0)
        self.sponsoredAdsTableView.sectionIndexBackgroundColor = [UIColor clearColor];
    
}

- (void)viewWillAppear:(BOOL)animated {
    isSelectDeselectPressesd=NO;
    [self fetchPromosResultController: nil];
    [self.sponsoredAdsTableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    [moc rollback];
}

#pragma mark - Webservice methods
- (void)getSponsoredAds
{
    [[IMAppDelegate sharedDelegate] getSponsoredAds];
}

- (void)setSponsoredAds:(NSNumber *)promo_id is_Enable:(NSNumber *)is_Enable
{
    [[IMAppDelegate sharedDelegate] setSponsoredAds:promo_id is_Enable: is_Enable];
}


- (void)loadStream
{
    //    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:self.ascending];
    //    NSArray *sortDescriptors = @[sortDescriptor];
    //
    //    NSArray* _objects = [_objects sortedArrayUsingDescriptors:sortDescriptors];
    
    //    _ascending = !_ascending;
    
    [self getSponsoredAds];
    [self performSelector:@selector(updateSponsoredAdsTable) withObject:nil afterDelay:1];
}

- (void)updateSponsoredAdsTable
{
    [self.sponsoredAdsTableView reloadData];
    
    [_tableViewController.refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [fectchedResultsController.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (fectchedResultsController.fetchedObjects.count) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }

    return [[fectchedResultsController.sections objectAtIndex: section] numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [IMPromoSearchCell cellLoadedFromNibFile];
    }
    if (cell.selectedBackgroundView.tag != SELECTED_BG_VIEW_TAG) {
        UIView *selectionColor = [[UIView alloc] initWithFrame:CGRectMake(0, 1, cell.frame.size.width, 42)];
        selectionColor.tag = SELECTED_BG_VIEW_TAG;
        selectionColor.backgroundColor = [UIColor whiteColor];
        cell.selectedBackgroundView = selectionColor;
    }
    
    //Added index checking to avoid crash related with index
    NSArray* array = nil;
    IMPromos* promo = nil;
    NSArray* sectionArray = fectchedResultsController.sections;
    if(sectionArray.count > indexPath.section) {
        array = [[sectionArray objectAtIndex: indexPath.section] objects];
    
    
        if([array count] > indexPath.row)
            promo = (IMPromos*)[array objectAtIndex: indexPath.row];
    }
    //IMPromos* promo = (IMPromos*)[[[fectchedResultsController.sections objectAtIndex: indexPath.section] objects] objectAtIndex: indexPath.row];
    
    cell.textLabel.text = promo.promo_name;
    // Configure the cell...
    if (tableView.isEditing) {
        cell.textLabel.textColor = [UIColor darkGrayColor];
        if (promo.is_enabled.boolValue) {
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition: UITableViewScrollPositionNone];
        }
        else
        {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
    else {
        if (promo.is_enabled.boolValue) {
            cell.textLabel.textColor = [UIColor darkGrayColor];
        }
        else {
            cell.textLabel.textColor = [UIColor redColor];
        }
    }
    if([promo.promo_name isEqualToString:@"Best Buy"])
    {
        NSLog(@"promo.is_enabled=%d",promo.is_enabled.boolValue);
    }

    NSLog(@"promo.is_enabled=%d",promo.is_enabled.boolValue);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.title isEqualToString: @"Enable/ Disable Ads"])
    {
    IMPromos* promo = (IMPromos*)[[[fectchedResultsController.sections objectAtIndex: indexPath.section] objects] objectAtIndex: indexPath.row];
    
    promo.is_enabled = [NSNumber numberWithBool: YES];
        [self setSponsoredAds:promo.promo_id is_Enable:promo.is_enabled];
        
        
    
//    [[IMDataHandler sharedInstance] saveDatabase];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.title isEqualToString: @"Enable/ Disable Ads"])
    {
        IMPromos* promo = (IMPromos*)[[[fectchedResultsController.sections objectAtIndex: indexPath.section] objects] objectAtIndex: indexPath.row];
        
        promo.is_enabled = [NSNumber numberWithBool: NO];
        [self setSponsoredAds:promo.promo_id is_Enable:promo.is_enabled];
        
//        [[IMDataHandler sharedInstance] saveDatabase];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 27;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return f
//}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    IMPromoHeaderView *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[IMPromoHeaderView cellReusableIdentifier]];
    if (nil == cell) {
        NSLog(@"[IMPromoHeaderView cellLoadedFromNibFile];");
        cell = [IMPromoHeaderView cellLoadedFromNibFile];
    }
//    cell.headerLabel.text = [NSString stringWithFormat:@"Header %d",section];
    cell.headerLabel.text = [[fectchedResultsController.sections objectAtIndex: section] indexTitle];

    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSMutableArray* sectionList = [[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles] mutableCopy];
    id lastObject = [sectionList lastObject];
    [sectionList removeLastObject];
    [sectionList insertObject:lastObject atIndex: 0];
    return sectionList;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (title == UITableViewIndexSearch)
    {
        CGRect searchBarFrame = self.searchDisplayController.searchBar.frame;
        [tableView scrollRectToVisible:searchBarFrame animated:YES];
        
        return -1;
    }
    else {
        
        int count = 0;
        
        for(count=0; count < fectchedResultsController.sections.count; count++)
        {
            id <NSFetchedResultsSectionInfo> sectionInfo =
            [[fectchedResultsController sections] objectAtIndex:count];
            NSString *sectionHeaderNames = [sectionInfo name];
            NSLog(@"Input title = %@,index = %d,section: %@,count: %i ",title,index, sectionHeaderNames,count);
            
            if([title isEqualToString:sectionHeaderNames] || [[title lowercaseString] isEqualToString:sectionHeaderNames])
            {
                NSLog(@"return %d",count);
                
                return count;
                break;
            }
            else
            {
                if (title.length && sectionHeaderNames.length) {
                    {
                    unichar first = [title characterAtIndex:0];
                    if ((first >= 'a' && first <= 'z') || (first >='A' && first <= 'Z')) {
                        unichar fectchedChar = [[sectionHeaderNames uppercaseString] characterAtIndex:0];
                        if (first < fectchedChar) {
                            
                            NSLog(@"return %d",count);
                            
                            return count;
                            break;
                        }
                    }
                }
            }
            }
        }
        NSLog(@"return %d",count);
        return count;
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

- (IBAction)onEditTapped:(UIBarButtonItem *)sender {
    
    [self.sponsoredAdsTableView setEditing:!self.sponsoredAdsTableView.editing animated:YES];
    
    if (self.sponsoredAdsTableView.editing) {
        if(![[[IMDataHandler sharedInstance] disabledPromos: 0] count])
            [_selectAllButton setTitle: @"Deselect All" forState: UIControlStateNormal];
        else
            [_selectAllButton setTitle: @"Select All" forState: UIControlStateNormal];
        
        [sender setTitle:@"Done"];
        self.title = @"Enable/ Disable Ads";
        self.selectAllButton.hidden = NO;
        CGRect tableFrame = self.sponsoredAdsTableView.frame;
        tableFrame.size.height -= self.selectAllButton.frame.size.height;
        self.sponsoredAdsTableView.frame = tableFrame;
    }
    else{
        [sender setTitle:@"Edit"];
        self.title = @"Sponsored Ads";
        [[IMDataHandler sharedInstance] saveDatabase];

        self.selectAllButton.hidden = YES;
        CGRect tableFrame = self.sponsoredAdsTableView.frame;
        tableFrame.size.height += self.selectAllButton.frame.size.height;
        self.sponsoredAdsTableView.frame = tableFrame;
        if(isSelectDeselectPressesd)
        {
            for(int i = 0; i < [fectchedResultsController.sections count]; ++i)
            {
                for(int j = 0; j < [[fectchedResultsController.sections objectAtIndex: i] numberOfObjects]; ++j)
                {
                    IMPromos* promo = (IMPromos*)[[[fectchedResultsController.sections objectAtIndex: i] objects] objectAtIndex: j];
                   [self setSponsoredAds:promo.promo_id is_Enable:promo.is_enabled];
                   
                }
            }

            
           
        }
    }
    isSelectDeselectPressesd=NO;
    [self.sponsoredAdsTableView reloadData];
}

- (IBAction)onSelectAllTapped:(id)sender{
    if (_selectAllButton.hidden) {
        return;
    }

    isSelectDeselectPressesd=YES;
    UIButton* button = (UIButton*)sender;
    
    if([button.titleLabel.text isEqualToString: @"Select All"])
    {
        for(int i = 0; i < [fectchedResultsController.sections count]; ++i)
        {
            for(int j = 0; j < [[fectchedResultsController.sections objectAtIndex: i] numberOfObjects]; ++j)
            {
                NSIndexPath* path = [NSIndexPath indexPathForRow: j inSection: i];
                IMPromos* promo = (IMPromos*)[[[fectchedResultsController.sections objectAtIndex: i] objects] objectAtIndex: j];
                //            if([promo.is_enabled boolValue])
                [self.sponsoredAdsTableView selectRowAtIndexPath:path animated:YES scrollPosition: UITableViewScrollPositionTop];
                promo.is_enabled = [NSNumber numberWithBool:YES];
            }
        }
        [button setTitle: @"Deselect All" forState: UIControlStateNormal];
    }
    else
    {
        for(int i = 0; i < [fectchedResultsController.sections count]; ++i)
        {
            for(int j = 0; j < [[fectchedResultsController.sections objectAtIndex: i] numberOfObjects]; ++j)
            {
                NSIndexPath* path = [NSIndexPath indexPathForRow: j inSection: i];
                IMPromos* promo = (IMPromos*)[[[fectchedResultsController.sections objectAtIndex: i] objects] objectAtIndex: j];
                //            if([promo.is_enabled boolValue])
                [self.sponsoredAdsTableView deselectRowAtIndexPath:path animated:YES];
                promo.is_enabled = [NSNumber numberWithBool:NO];
            }
        }
        [button setTitle: @"Select All" forState: UIControlStateNormal];
    }
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;                     // called when keyboard search button pressed
{
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    
    fectchedResultsController = [self fetchPromosResultController: searchBar.text];
    [self.sponsoredAdsTableView reloadData];
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    if([searchText isEqualToString: @""])
        fectchedResultsController = [self fetchPromosResultController: nil];
    else
        fectchedResultsController = [self fetchPromosResultController: searchText];
    [self.sponsoredAdsTableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    
    fectchedResultsController = [self fetchPromosResultController: nil];
    [self.sponsoredAdsTableView reloadData];
    
    //[nonImgrContactsTableView setContentOffset:CGPointMake(0, 44) animated:YES];
    [searchBar resignFirstResponder];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    
    //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    // NSArray* searchResults = [fetchedResultsController.fetchedObjects filteredArrayUsingPredicate:resultPredicate];
    if([searchText isEqualToString: @""])
        fectchedResultsController = [self fetchPromosResultController: nil];
    else
        fectchedResultsController = [self fetchPromosResultController:searchText];
    [self.sponsoredAdsTableView reloadData];
}

#pragma mark -
- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView
{
    tableView.editing = self.sponsoredAdsTableView.editing;
    tableView.allowsSelectionDuringEditing = self.sponsoredAdsTableView.allowsSelectionDuringEditing;
    tableView.allowsSelection = self.sponsoredAdsTableView.allowsSelection;
    tableView.allowsMultipleSelection = self.sponsoredAdsTableView.allowsMultipleSelection;
    tableView.allowsMultipleSelectionDuringEditing = self.sponsoredAdsTableView.allowsMultipleSelectionDuringEditing;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willUnloadSearchResultsTableView:(UITableView *)tableView;
{
    // search is done so get rid of the search FRC and reclaim memory
    //searchFetchedResultsController.delegate = nil;
    //searchFetchedResultsController = nil;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
