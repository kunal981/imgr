//
//  IMViewController.h
//  IMGR
//
//  Created by Satendra Singh on 2/3/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMSettingsViewController : UITableViewController

- (IBAction)onSlideButtonClick:(id)sender;
@end
