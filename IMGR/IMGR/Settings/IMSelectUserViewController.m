//
//  IMSelectUserViewController.m
//  IMGR
//
//  Created by Satendra Singh on 2/12/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMSelectUserViewController.h"
#import "IMContacts.h"
#import "IMContactDetailViewController.h"
#import "IMContactsCell.h"

@interface IMSelectUserViewController ()<UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *noUserView;

@end

@implementation IMSelectUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = nil;
    //[self onIMGRButtonClick:nil];
	// Do any additional setup after loading the view.
    
    isIMGRViewSelected = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem.title = @"Back";
    self.navigationItem.title = @"Select IMGR Contact";
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (0 == fetchedResultsController.fetchedObjects.count) {
        [self.view addSubview:self.noUserView];
        CGRect noViewFrame = self.noUserView.frame;
        noViewFrame.origin.y = self.view.frame.size.height/2 - noViewFrame.size.height/2 ;
        self.noUserView.frame = noViewFrame;
    }
    else
    {
        if ([self.noUserView superview]) {
            [self.noUserView removeFromSuperview];
        }
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *jid = [[[self fetchedResultsController] objectAtIndexPath:indexPath] user_id];
    [self.navigationController popViewControllerAnimated:NO];
    _selectionBlockObject(jid);
}

- (IBAction)onInfoButtonClick:(id)sender
{
    UIButton* button = (UIButton*)sender;
    
    IMContactsCell* cell = nil;//(IMContactsCell*)[[[button superview] superview] superview];
//    if([IMUtils getOSVersion] >= 7.0)
//        cell = (IMContactsCell*)[[[button superview] superview] superview];
//    else
        cell = (IMContactsCell*)[[button superview] superview];
   
    NSIndexPath* selectedPromoIndex = nil;

    if(isSearching)
        selectedPromoIndex = [self.searchDisplayController.searchResultsTableView indexPathForCell: cell];
    else
        selectedPromoIndex = [contactsTableView indexPathForCell: cell];

    
    IMContactDetailViewController* controller = [[[IMAppDelegate sharedDelegate] contactsStoryboard] instantiateViewControllerWithIdentifier:@"IMContactDetailViewController"];
    IMContacts* contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: selectedPromoIndex.section] objects] objectAtIndex: selectedPromoIndex.row];
    controller.contact = contactInfo;
    [self.navigationController pushViewController:controller animated:YES];
//    isIMGRViewSelected = YES;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender NS_AVAILABLE_IOS(6_0)
{
    if([identifier isEqualToString:@"IMContactDetailViewController"]){
        return NO;
    }
    return YES;
}
@end
