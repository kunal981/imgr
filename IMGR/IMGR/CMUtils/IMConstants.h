//
//  IMConstants.h
//  IMGR
//
//  Created by akram on 04/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#ifndef IMGR_IMConstants_h
#define IMGR_IMConstants_h

#define IOS(x)                      (([[[UIDevice currentDevice] systemVersion] floatValue]>=x)?YES:NO)

#define _RELEASE(x)                 if(x) [x release]; x = nil;

#ifdef DEBUG
#define DebugLog(s, ...) NSLog(s, ##__VA_ARGS__)
#else
#define DebugLog(s, ...)
#endif
#define IOS_KEYBOARD_HEIGHT 216

#define SEARCH_BAR_HEIGHT       44
#define MAX_PHONE_NUMBER 20

#define k_DEFAULTS_KEY_NOTIFICATIONS @"IMGR_Notifications"
#define k_DEFAULTS_KEY_PROMOS @"IMGR_PROMOS"
#define k_DEFAULTS_KEY_ROTATE_PERSONAL @"PERSONAL_PROMOS_ROTATION"
#define k_DEFAULTS_KEY_ROTATE_SPONSORED @"SPONSORED_ADS_ROTATION"
#define k_DEFAULTS_KEY_ROTATE_ENABLED @"AUTO_ROTATE_PROMOS"
#define k_DEFAULTS_KEY_BUBBLE_COLOR @"MY_MESSAGE_BUBBLE_COLOR"
#define k_DEFAULTS_KEY_FONT_SIZE @"MY_MESSAGE_FONT_SIZE"
#define k_DEFAULTS_KEY_USER_ID @"MY_LINKED_ACCOUNT"
#define k_DEFAULTS_KEY_OVERRIDE_FRIEND_BUBBLE_COLOR @"OVERRIDE_FRIEND_BUBBLE_COLOR"
#define k_DEFAULTS_KEY_MY_VCARD @"MY_VCARD_INFO"

#define K_DEFAULTS_DEVICE_UDID  @"MY_DEVICE_UDID"
#define k_DEFAULT_NUMBER_CHARACTERS_COUNT 10

#define k_IMAGE_IO_Height @"PixelHeight"
#define k_IMAGE_IO_Width @"PixelWidth"

#define K_DEVICE_TOKEN          @"K_DEVICE_TOKEN"
#define K_ALREADY_REGISTERED    @"K_ALREADY_REGISTERED"

#define k_NORMAL_FONT_SIZE 18
#define k_LARGE_FONT_SIZE 21



enum {
    E_REGISTER_SERVICE     = 100,
    E_VERIFICATION_SERVICE,
    E_CHECK_IMGR_SERVICE,
    E_GET_PROMO_LIST_SERVICE,
    E_SET_SPONSORE_PROMO_STATUS,
    E_CREATE_PROMO_SERVICE,
    E_UPDATE_PROMO_SERVICE,
    E_SEND_MESSAGE_SERVICE,
    E_SET_USER_SETTINGS_SERVICE,
    E_GET_USER_SETTINGS_SERVICE,
    E_DELETE_PROMO_SERVICE,
    E_DOWNLOAD_IMGR_IMAGES_SERVICE,
    E_GET_PERSONAL_PROM_OTHER_USER,
    E_DECREASE_BADGE_COUNT_SERVICE,
    E_UPDATE_DEVICE_TOKEN_SERVICE,
};

#define PERSONAL_PROMO_THIRDPARTY_UPDATED_NOTIFICATION @"PERSONAL PROMO UPDATED"
#define KAPIKEY         @"imgr"

#define KDeviceModel    @"2"
#define OS              [[UIDevice currentDevice] systemVersion]
#define KAppVersion     @"1.0"

#define IS_IPHONE5      (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

//#define KBASEURL        @"http://coppermobile.net/imgr/v1/webservices/index.php/ws/"
//#define KBASE_IMAGE_URL @"http://coppermobile.net/imgr/v1/webservices/"

//#define KBASEURL        @"http://64.235.48.26/release/index.php/ws/"
#define KBASEURL        @"https://imgrapp.com/release/index.php/ws/"

//#define KBASE_IMAGE_URL @"http://64.235.48.26/release/"
#define KBASE_IMAGE_URL @"https://imgrapp.com/release/"

#define kRegisterUser   @"webservice/registeration"
#define kVerifyUser     @"webservice/verifyRegistration"

#define kGetIMGRUsers   @"appuser/isImgrUser"
#define kGetPromoList   @"appuser/getPromoList"
#define kGetImgrImages  @"appuser/getImgrImages"
#define kSetPromoStatus @"appuser/setPromoStatus"
#define kCreatePromo    @"appuser/createSelfPromo"
#define kUpdatePromo    @"appuser/updateSelfPromo"
#define kDeleteAccount  @"appuser/deleteAccount?apiKey=imgr&username="

#define kPushNotification   @"appuser/sendPushNotification"
#define kSetUserSettings    @"appuser/setUserSetting"
#define kGetUserSettings    @"appuser/getUserSettings"
#define kGetPromWithID      @"appuser/getPromoDetails?apiKey=imgr&promo_id="
#define kDeletePromo        @"appuser/deletePersonalPromo"
#define kDecreaseBadgeCount @"appuser/decrementBadge"
#define kUpdateDeviceToken  @"appuser/updateDeviceToken"

#endif
