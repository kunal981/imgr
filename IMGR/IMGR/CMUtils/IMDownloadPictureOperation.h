//
//  DownloadPictureOperation.h
//

#import <Foundation/Foundation.h>
#import "IMOperation.h"

@protocol IMDownloadPictureOperationDelegate <NSObject>

@optional
-(void)didDownloadImage;

@end

@interface IMDownloadPictureOperation : Operation <IMDownloadPictureOperationDelegate> {
	id storedObject;
    
}
@property (nonatomic, retain) id storedObject;
@property (nonatomic, assign) id<IMDownloadPictureOperationDelegate> delegate;
- (id) initWithMode:(UIViewContentMode)contentMode picURL:(NSString*)url;
@end

@interface UIImage (imageWithShadowOffset)

- (UIImage *)imageWithShadow;
- (UIImage *)imageWithRoundedRect:(double)radius;

@end
