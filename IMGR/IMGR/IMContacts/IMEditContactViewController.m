//
//  IMEditContactViewController.m
//  IMGR
//
//  Created by akram on 05/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMEditContactViewController.h"
#import "IMNewContactCell.h"
#import "CMPhotoPickerController.h"
#import "IMUtils.h"
#import "IMContacts.h"
#import "IMDataHandler.h"
#import "CMCoreDataHandler.h"

#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface IMEditContactViewController ()
{
    BOOL        isPromoOn;
    BOOL        isAutoRotate;
    BOOL        isSponsoredAd;
    BOOL        isPersonalPromo;
}
@end

@implementation IMEditContactViewController
@synthesize isIMGRUser;
@synthesize contact;
@synthesize tableViewHeightConstraint;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIView*)getHeaderView
{
    UIView* headerView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 320, 138)];
    UIImageView* headerBackground = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"bg_contact_small.png"]];
    headerView.userInteractionEnabled = YES;
    headerBackground.userInteractionEnabled = YES;
    UIButton* photoBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    photoBtn.tag = 900;
    [photoBtn setFrame: CGRectMake(114, 20, 90, 90)];
    
    UIImage* image = [[CMChatManager sharedInstance] fetchUserPhotoFrom:HOST_OPENFIRE forUser:contact.user_id];
    
    if(image == nil)
        [photoBtn setBackgroundImage: [UIImage imageNamed: @"img_reg_placeholder@2x.png"] forState: UIControlStateNormal];
    else
        [photoBtn setBackgroundImage: image forState: UIControlStateNormal];
    photoBtn.userInteractionEnabled = NO;
    
    [headerView addSubview: photoBtn];
    [headerView addSubview: headerBackground];
    return headerView;
}

- (void)adjustHeightOfTableview
{
    [UIView animateWithDuration:0.25 animations:^{
        self.tableViewHeightConstraint.constant = 200;
        [self.view needsUpdateConstraints];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.navigationController.navigationItem.backBarButtonItem setTitle: @"Cancel"];
    
    
    int height = [[UIScreen mainScreen] bounds].size.height;
    if([IMUtils getOSVersion] >= 7.0)
        editContactTableView.frame = CGRectMake(0, 64, 320, height - 64);
    else
        editContactTableView.frame = CGRectMake(0, 0, 320, height - 20);
    
    if(isIMGRUser) {
        editContactTableView.tableHeaderView = [self getHeaderView];
    }
    else {
        [self adjustHeightOfTableview];
    }
    isPersonalPromo = [contact.personal_promo_enabled boolValue];
    isSponsoredAd   = [contact.sponsored_ads_enabled boolValue];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    IMNewContactCell* firstname = (IMNewContactCell*)[editContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:0]];
    
    IMNewContactCell* lastname = (IMNewContactCell*)[editContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:1 inSection:0]];
    NSString* first_name_text = firstname.editTextField.text;
    NSString* last_name_text = lastname.editTextField.text;
    
    [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"isEdited"];
    [[NSUserDefaults standardUserDefaults]setObject:first_name_text forKey:@"firstName"];
    [[NSUserDefaults standardUserDefaults]setObject:last_name_text forKey:@"lastName"];
    [[NSUserDefaults standardUserDefaults]synchronize];
   
}
- (void)viewDidDisappear:(BOOL)animated
{
   

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate/DataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return (isIMGRUser) ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (section == 0) ? 4 : ([contact.promo_enabled boolValue]) ? 4 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier = [NSString stringWithFormat:@"identifier%i", indexPath.row];//@"identifier";
    
    IMNewContactCell* cell = (IMNewContactCell*)[tableView dequeueReusableCellWithIdentifier: identifier];
    
    if(cell == nil)
    {
        cell = [IMNewContactCell cellLoadedFromNibFile];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.editTextField.delegate = self;
    cell.editTextField.tag = indexPath.row;
    
    switch (indexPath.section)
    {
        case 0:
        {
            cell.editTextField.enabled = YES;
            cell.editTextField.hidden = NO;
            switch (indexPath.row)
            {
                case 0:
                    cell.contactNameLabel.text      = NSLocalizedString(@"First Name", @"");
                    if(contact.first_name && ![contact.first_name isEqualToString: @""])
                        cell.editTextField.text     = [contact.first_name capitalizedString];
                    else
                        cell.editTextField.placeholder = NSLocalizedString(@"TAP TO ENTER", @"");
                    break;
                    
                case 1:
                    cell.contactNameLabel.text      = NSLocalizedString(@"Last Name", @"");
                    if(contact.last_name && ![contact.last_name isEqualToString: @""])
                        cell.editTextField.text     = [contact.last_name capitalizedString];
                    else
                        cell.editTextField.placeholder = NSLocalizedString(@"TAP TO ENTER", @"");
                    break;
                    
                case 2:
                    cell.contactNameLabel.text  = NSLocalizedString(@"Phone Number", @"");
                    if (isIMGRUser) {
                        cell.editTextField.text = contact.phone_number_2;
                    }
                    else
                        cell.editTextField.text     = contact.phone_number;
                    cell.editTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                    break;
                    
                case 3:
                    cell.contactNameLabel.text      = NSLocalizedString(@"Email", @"");
                    if(contact.email.length)
                        cell.editTextField.text     = contact.email;
                    else
                        cell.editTextField.placeholder = NSLocalizedString(@"TAP TO ENTER", @"");
                    cell.editTextField.keyboardType = UIKeyboardTypeEmailAddress;
                    break;
                    
                default:
                    break;
            }
        }break;
          
        case 1:
        {
            int xPos;
            if([IMUtils getOSVersion] >= 7.0)
                xPos = 250;
            else
                xPos = 230;
            cell.editTextField.hidden = YES;
            switch (indexPath.row)
            {
                case 0:
                {
                    cell.contactNameLabel.text = NSLocalizedString(@"Promos", @"");
                    
                    UISwitch* promoSwitch = [[UISwitch alloc] initWithFrame: CGRectMake(xPos, 7, 40, 20)];
                    promoSwitch.on = [contact.promo_enabled boolValue];
                    isPromoOn = [contact.promo_enabled boolValue];
                    [promoSwitch addTarget:self action:@selector(promoSwitchClicked:) forControlEvents: UIControlEventValueChanged];
                    [cell.contentView addSubview: promoSwitch];
                }break;
                    
                case 1:
                {
                    cell.contactNameLabel.text = NSLocalizedString(@"Auto Rotate Promos", @"");
                    cell.contactNameLabel.frame = CGRectMake(cell.contactNameLabel.frame.origin.x, cell.contactNameLabel.frame.origin.y, cell.contactNameLabel.frame.size.width + 100, cell.contactNameLabel.frame.size.height);
                    UISwitch* promoSwitch = [[UISwitch alloc] initWithFrame: CGRectMake(xPos, 7, 40, 20)];
                    promoSwitch.on = [contact.auto_rotation_enabled boolValue];
                    isAutoRotate = [contact.auto_rotation_enabled boolValue];
                    [promoSwitch addTarget:self action:@selector(autoRotateSwitchClicked:) forControlEvents: UIControlEventValueChanged];
                    [cell.contentView addSubview: promoSwitch];
                }break;
                    
                case 2:
                {
                    UILabel* nameLabel = (UILabel*)[cell.contentView viewWithTag: 100];
                    nameLabel.text = NSLocalizedString(@"Sponsored Ads", @"");
                    nameLabel.frame = CGRectMake(45, cell.contactNameLabel.frame.origin.y, cell.contactNameLabel.frame.size.width + 50, cell.contactNameLabel.frame.size.height);
                    
                    UIButton* button = [UIButton buttonWithType: UIButtonTypeCustom];
                    button.tag = 500;
                    button.backgroundColor = [UIColor clearColor];
                    button.selected = [contact.sponsored_ads_enabled boolValue];
                    isSponsoredAd = [contact.sponsored_ads_enabled boolValue];
                    
                    UIImage* image = nil;
                    if(isSponsoredAd)
                        image = [UIImage imageNamed: @"btn_chech.png"];
                    else
                        image = [UIImage imageNamed: @"btn_unchech.png"];
                    [button setBackgroundImage: image forState: UIControlStateNormal];
                    [button setFrame: CGRectMake(10, 10, 25, 25)];
                    [button addTarget:self action:@selector(onSponsoredAddButtonClick:) forControlEvents: UIControlEventTouchUpInside];
                    [cell.contentView addSubview: button];
                }break;
                    
                case 3:
                {
                    UILabel* nameLabel = (UILabel*)[cell.contentView viewWithTag: 100];
                    nameLabel.text = NSLocalizedString(@"Personal Promo", @"");
                    nameLabel.frame = CGRectMake(45, cell.contactNameLabel.frame.origin.y, cell.contactNameLabel.frame.size.width + 50, cell.contactNameLabel.frame.size.height);
                    
                    UIButton* button = [UIButton buttonWithType: UIButtonTypeCustom];
                    button.tag      = 501;
                    button.selected = [contact.personal_promo_enabled boolValue];
                    isPersonalPromo = [contact.personal_promo_enabled boolValue];
                    
                    UIImage* image = nil;
                    if(isPersonalPromo)
                        image = [UIImage imageNamed: @"btn_chech.png"];
                    else
                        image = [UIImage imageNamed: @"btn_unchech.png"];
                    
                    button.backgroundColor = [UIColor clearColor];
                    [button setBackgroundImage: image forState: UIControlStateNormal];
                    [button setFrame: CGRectMake(10, 10, 25, 25)];
                    [button addTarget:self action:@selector(onPersonalPromoButtonClick:) forControlEvents: UIControlEventTouchUpInside];
                    [cell.contentView addSubview: button];
                }break;
                    
                default:
                    break;
            }
            
            
        }break;
            
        default:
            break;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 1)
        return 20;
    else
        return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
        case 1:
        {
            switch (indexPath.row)
            {
                case 2:
                {
                    IMNewContactCell* cell = (IMNewContactCell*)[tableView cellForRowAtIndexPath: indexPath];
                    UIButton* button = (UIButton*)[cell.contentView viewWithTag: 500];
                    [self onSponsoredAddButtonClick: button];
                }break;
                    
                case 3:
                {
                    IMNewContactCell* cell = (IMNewContactCell*)[tableView cellForRowAtIndexPath: indexPath];
                    UIButton* button = (UIButton*)[cell.contentView viewWithTag: 501];
                    [self onPersonalPromoButtonClick: button];
                }break;
                    
                default:
                    break;
            }
        }break;
            
        default:
            break;
    }
}

- (void)promoSwitchClicked: (id)sender
{
    UISwitch* promoSwitch = (UISwitch*)sender;
    [editContactTableView beginUpdates];
    
    if([contact.promo_enabled boolValue] != promoSwitch.isOn) {
        if(promoSwitch.isOn)
        {
            isPromoOn = YES;
            contact.promo_enabled = [NSNumber numberWithBool:YES];
            [editContactTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:1 inSection:1],[NSIndexPath indexPathForItem:2 inSection:1], [NSIndexPath indexPathForItem:3 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else
        {
            isPromoOn = NO;
            contact.promo_enabled = [NSNumber numberWithBool:NO];

            [editContactTableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:3 inSection:1],[NSIndexPath indexPathForItem:2 inSection:1], [NSIndexPath indexPathForItem:1 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    [editContactTableView endUpdates];
    //[editContactTableView reloadData];
}

- (void)autoRotateSwitchClicked: (id)sender
{
    UISwitch* autoBtn = (UISwitch*)sender;
    if(autoBtn.isOn)
        isAutoRotate = YES;
    else
        isAutoRotate = NO;
    
    contact.auto_rotation_enabled = [NSNumber numberWithBool:isAutoRotate];
}

- (void)onSponsoredAddButtonClick: (id)sender
{
    UIButton* button = (UIButton*)sender;
    if(!button.selected) {
        [button setBackgroundImage: [UIImage imageNamed: @"btn_chech.png"] forState: UIControlStateNormal];
        button.selected = YES;
        isSponsoredAd = YES;
        contact.sponsored_ads_enabled = [NSNumber numberWithBool:YES];
    }
    else {
        [button setBackgroundImage: [UIImage imageNamed: @"btn_unchech.png"] forState: UIControlStateNormal];
        button.selected = NO;
        isSponsoredAd = NO;
        contact.sponsored_ads_enabled = [NSNumber numberWithBool:NO];
    }
}

- (void)onPersonalPromoButtonClick: (id)sender
{
    UIButton* button = (UIButton*)sender;
    if(!button.selected) {
        [button setBackgroundImage: [UIImage imageNamed: @"btn_chech.png"] forState: UIControlStateNormal];
        button.selected = YES;
        isPersonalPromo = YES;
        contact.personal_promo_enabled = [NSNumber numberWithBool:YES];
    }
    else {
        [button setBackgroundImage: [UIImage imageNamed: @"btn_unchech.png"] forState: UIControlStateNormal];
        button.selected = NO;
        isPersonalPromo = NO;
        contact.personal_promo_enabled = [NSNumber numberWithBool:NO];
    }
}
#pragma mark - UITextFieldDelegate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (isIMGRUser) {
        if (textField.tag == 2) {
            return NO;
        }
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(!isIMGRUser)
        return;
    [UIView animateWithDuration:0.3 animations:^{
        
        editContactTableView.contentSize = CGSizeMake(editContactTableView.frame.size.width, editContactTableView.frame.size.height + 200);
        
        switch (textField.tag)
        {
            case 1:
            case 2:
            case 3:
                editContactTableView.contentOffset = CGPointMake(editContactTableView.frame.origin.x, 44*textField.tag);
                break;
                
            default:
                break;
        }
        
    } completion:^(BOOL finished) {  }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int charCount = [newString length];
    
    switch (textField.tag)
    {
        case 0:
        case 1:
        case 3:
        {
            if([string isEqualToString:@" "] && textField.text.length == 1) {
                return NO;
            }
        }break;
            
        default:
            break;
    }
    
    if(charCount > MAX_PHONE_NUMBER && textField.tag == 2)
        return NO;
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(isIMGRUser)
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            editContactTableView.contentSize = CGSizeMake(editContactTableView.frame.size.width, editContactTableView.frame.size.height);
            
        } completion:^(BOOL finished) {  }];
    }
    [textField resignFirstResponder];
    
    
    UITextField* tField = (UITextField*)[editContactTableView viewWithTag: textField.tag + 1];
    [tField becomeFirstResponder];
    
    return YES;
}

#pragma mark - IBActions
- (IBAction)changePhoto:(id)sender
{
    UIActionSheet* changePhotoSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:(id)self
                                                         cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles: @"Choose From Existing", @"Take Photo", nil];
    
    [changePhotoSheet showFromRect:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 0) inView:self.view animated:YES];
    
    
}
-(BOOL) validEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


- (IBAction)onDoneButtonClick:(id)sender
{
    NSLog(@"DoneBtnPressed");
    IMNewContactCell* firstname = (IMNewContactCell*)[editContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:0]];
    
    IMNewContactCell* lastname = (IMNewContactCell*)[editContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:1 inSection:0]];
    
    IMNewContactCell* phoneNumber = (IMNewContactCell*)[editContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:2 inSection:0]];
    
    IMNewContactCell* email = (IMNewContactCell*)[editContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:3 inSection:0]];
    
    
    NSString* first_name_text = firstname.editTextField.text;
    NSString* last_name_text = lastname.editTextField.text;
    NSString* phone_number_text = phoneNumber.editTextField.text;
    NSString* email_text = email.editTextField.text;
    if(email_text.length)
    {
        if(![self validEmail:email_text])
        {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: @"Please enter valid email address" delegate:nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
            [alertView show];
            [email.editTextField becomeFirstResponder];
            return;
        }

    }
    
    
    IMContacts* existingContact = [[IMDataHandler sharedInstance] fetchContactWithPhoneNumber: contact.phone_number_2];
    
    //-------
    {
        CFErrorRef *error = nil;
        
        ABAddressBookRef lAddressBook = ABAddressBookCreateWithOptions(NULL, error);
        
        ABRecordID recordId = (ABRecordID)(contact.contact_id.integerValue);
        // Search for the desired in the address book
        ABRecordRef person = ABAddressBookGetPersonWithRecordID(lAddressBook, recordId);
        //if ((person != nil))
        {
            
            //ABRecordID recordId = ABRecordGetRecordID(person);
            
            //ABRecordRef person1 = ABAddressBookGetPersonWithRecordID(lAddressBook, recordId);
            
            ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
            
            CFIndex count = ABMultiValueGetCount(multiPhones);
            
            ABMutableMultiValueRef multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);

            for(int i=0;i<count;i++) {
                
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = (__bridge_transfer NSString *) phoneNumberRef;
                
                CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(multiPhones, i);
                
                //DebugLog(@"App Crash at phonenumber = %d and number = %@", i, phoneNumber);
                
                
                if(phoneNumber && [contact.phone_number_2 isEqualToString: [IMUtils phonenumberFromFormattedNumber: phoneNumber]])
                {
                    ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)phone_number_text, locLabel, NULL);
                }
                else
                {
                    ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)phoneNumber, locLabel, NULL);
                    
                }
            }
            ABRecordSetValue(person, kABPersonPhoneProperty, multiPhone, nil);

            
            
            //---------
            ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
            
            count = ABMultiValueGetCount(multiEmails);
            
            ABMutableMultiValueRef mutableMultiEmails = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            
            for(int i=0;i<count;i++) {
                
                CFStringRef emailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
                NSString *emailID = (__bridge_transfer NSString *) emailRef;
                
                CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(multiEmails, i);
                
                //DebugLog(@"App Crash at phonenumber = %d and number = %@", i, phoneNumber);
                
                
                if(emailID && [contact.email isEqualToString: emailID])
                {
                    ABMultiValueAddValueAndLabel(mutableMultiEmails, (__bridge CFTypeRef)email_text, locLabel, NULL);
                }
                else
                {
                    ABMultiValueAddValueAndLabel(mutableMultiEmails, (__bridge CFTypeRef)emailID, locLabel, NULL);
                    
                }
            }
            ABRecordSetValue(person, kABPersonEmailProperty, mutableMultiEmails, nil);
            
            
            ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFTypeRef)(first_name_text), nil);
            ABRecordSetValue(person, kABPersonLastNameProperty, (__bridge CFTypeRef)(last_name_text), nil);
            
            ABAddressBookSave(lAddressBook, error);
        }
        
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: @"Contact updated successfully" delegate:self cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
        [alertView show];
    }
    
    
    [existingContact setValue: firstname.editTextField.text forKey: @"first_name"];
    [existingContact setValue: lastname.editTextField.text forKey: @"last_name"];
    [existingContact setValue: phoneNumber.editTextField.text forKey: @"phone_number"];
    [existingContact setValue: [IMUtils phonenumberFromFormattedNumber: phoneNumber.editTextField.text] forKey: @"phone_number_2"];
    [existingContact setValue: email.editTextField.text forKey: @"email"];
    
    NSString *sectionName = @"#";
    NSString* first_name = firstname.editTextField.text;
    if (first_name.length) {
        
        unichar first = [first_name characterAtIndex:0];
        if ((first >= 'a' && first <= 'z') || (first >='A' && first <= 'Z')) {
            sectionName = [[NSString stringWithCharacters:&first length:1] lowercaseString];
        }
    }
    existingContact.index_character = sectionName;
    
    if(isIMGRUser)
    {
        [existingContact setValue: [NSNumber numberWithBool: isPersonalPromo] forKey: @"personal_promo_enabled"];
        [existingContact setValue: [NSNumber numberWithBool: isSponsoredAd] forKey: @"sponsored_ads_enabled"];
        [existingContact setValue: [NSNumber numberWithBool: isAutoRotate] forKey: @"auto_rotation_enabled"];
        [existingContact setValue: [NSNumber numberWithBool: isPromoOn] forKey: @"promo_enabled"];
    }
    
    [[IMDataHandler sharedInstance] setManagedObjectContext: [[CMCoreDataHandler sharedInstance] managedObjectContext]];
    
    [[IMDataHandler sharedInstance] saveDatabase];
    
    contact = existingContact;
   [self.navigationController popViewControllerAnimated: YES];
}

/*#pragma mark - UIAlertView Delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
 
    [self.navigationController popViewControllerAnimated: YES];
    
}*/

#pragma mark UIActionSheet Delegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [[CMPhotoPickerController sharedInstance] displayImagePickerWithType:1 target:self];
            [self.navigationController presentViewController: [CMPhotoPickerController sharedInstance] animated: YES completion: nil];
            break;
            
        case 1:
            [[CMPhotoPickerController sharedInstance] displayImagePickerWithType:2 target:self];
            [self.navigationController presentViewController: [CMPhotoPickerController sharedInstance] animated: YES completion: nil];
            break;
            
        case 2:
            break;
            
        default:
            break;
    }
}

#pragma mark - UIImagePicker Delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    if([mediaType isEqualToString:@"public.image"])
    {
        UIImage* newImage = [info valueForKey: UIImagePickerControllerEditedImage];
        
        UIImage* image = [IMUtils circularScaleNCrop:newImage rect: CGRectMake(0, 0, 80, 80)];
        
        UIButton* userPhotoButton = (UIButton*)[[editContactTableView tableHeaderView] viewWithTag: 900];
        userPhotoButton.imageView.image = nil;
        userPhotoButton.imageView.contentMode = UIViewContentModeScaleToFill;
        [userPhotoButton setImage: image forState: UIControlStateNormal];
        
        //[self performSelector: @selector(changeUserPhoto:) withObject: image afterDelay: .01];
    }
    else if ( [ mediaType isEqualToString:@"public.movie" ])
    {
        
    }
    self.navigationController.navigationBarHidden = YES;
    [picker dismissViewControllerAnimated:YES completion: nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    self.navigationController.navigationBarHidden = YES;
    [picker dismissViewControllerAnimated:YES completion: nil];
}

@end
