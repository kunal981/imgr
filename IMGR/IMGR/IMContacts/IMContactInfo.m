//
//  CMContactInfo.m
//  CMSmoothScrollDemo
//
//  Created by akram on 03/02/14.
//  Copyright (c) 2014 akram. All rights reserved.
//

#import "IMContactInfo.h"

@implementation IMContactInfo

@synthesize firstName;
@synthesize lastName;
@synthesize phoneNumber;

@end
