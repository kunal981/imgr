//
//  IMContactDetailViewController.m
//  IMGR
//
//  Created by akram on 05/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMContactDetailViewController.h"
#import "IMEditContactViewController.h"
#import "IMContacts.h"
#import "IMDataHandler.h"
#import "IMChatViewController.h"

@interface IMContactDetailViewController ()
{
    NSUserDefaults *contactDetail;
}

@end

@implementation IMContactDetailViewController
@synthesize contact;
@synthesize comingFromChatScreen;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    contactDetail=[NSUserDefaults standardUserDefaults];
    [contactDetail synchronize];
    [contactDetail setObject:contact.first_name forKey:@"firstName"];
    [contactDetail setObject:contact.last_name forKey:@"lastName"];
    
    
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    if (contact.first_name && contact.last_name) {
        nameLabel.text = [NSString stringWithFormat:@"%@ %@",[contact.first_name capitalizedString],[contact.last_name capitalizedString]];
        
    }
    else if(contact.first_name)
    {
        nameLabel.text = [contact.first_name capitalizedString];
    }
    else if(contact.last_name)
        nameLabel.text = [contact.last_name capitalizedString];
    else
        nameLabel.text = @"No Name";
    
    
    NSString *flag=[contactDetail objectForKey:@"isEdited"];
    if(flag.length>0)
    {
        if([flag isEqualToString:@"1"])
        {
           NSString *firstName=[NSString stringWithFormat:@"%@", [contactDetail objectForKey:@"firstName"]];
           NSString *lastName=[NSString stringWithFormat:@"%@", [contactDetail objectForKey:@"lastName"]];
            
            if (firstName && lastName) {
                nameLabel.text = [NSString stringWithFormat:@"%@ %@",[firstName capitalizedString],[lastName capitalizedString]];
                
            }
            else if(contact.first_name)
            {
                nameLabel.text = [firstName capitalizedString];
            }
            else if(contact.last_name)
                nameLabel.text = [lastName capitalizedString];
            else
                nameLabel.text = @"No Name";
           
          [contactDetail setObject:@"0" forKey:@"isEdited"];
        }
        
    }
    
    
    
    phoneNumberLabel.text = contact.phone_number_2;
    if(!contact.email.length)
        emailLabel.text = NSLocalizedString(@"NO EMAIL", @"");
    else
        emailLabel.text = contact.email;
    NSString *title = contact.is_blocked.boolValue ?@"Unblock Contact":@"Block Contact";
    [blockContactButton setTitle:title forState:UIControlStateNormal];
    
    
    UIImage* image = [[CMChatManager sharedInstance] fetchUserPhotoFrom:HOST_OPENFIRE forUser:contact.user_id];
    
    if(image == nil)
        userImageView.image  = [UIImage imageNamed: @"img_contact_placeholder@2x.png"];
    else
        userImageView.image = image;
}

- (void)viewDidAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions methods
- (IBAction)onEditButtonClick:(id)sender
{
    [self performSegueWithIdentifier:@"IMEditContactViewController" sender:self];
}

- (IBAction)onSendMessageButtonClick:(id)sender
{
    if (contact.is_blocked.boolValue) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"To send a message to blocked user, first unblock user." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        return;
    }
    
    if(comingFromChatScreen) {
        [self.navigationController popViewControllerAnimated: YES];
        return;
    }
    //start chat with selected user
    UIStoryboard *contactsStoryBoard = [[IMAppDelegate sharedDelegate] messagesStoryboard];
    __block IMChatViewController *chatController = [contactsStoryBoard instantiateViewControllerWithIdentifier:@"IMChatViewController"];
    chatController.userJid = contact.user_id;
    [self.navigationController pushViewController:chatController animated:YES];
   
}

- (IBAction)onBlockContactButtonClick:(id)sender
{
    if (NO == contact.is_blocked.boolValue) {
        
        [[IMAppDelegate sharedDelegate] blockContact:contact];
    }
    else
    {
        [[IMAppDelegate sharedDelegate] unBlockContact:contact];
    }
    
    NSString *title = contact.is_blocked.boolValue ?@"Unblock Contact":@"Block Contact";
    [blockContactButton setTitle:title forState:UIControlStateNormal];

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    IMEditContactViewController* controller = segue.destinationViewController;
    controller.contact = contact;
    controller.isIMGRUser = YES;
}




@end
