//
//  IMNonIMGRContactDetailViewController.h
//  IMGR
//
//  Created by akram on 05/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import	<MessageUI/MessageUI.h>

/*
 This class display Non IMGR user profile detail and provide the features like send invitation and edit profile.
 */

@class IMContacts;
@interface IMNonIMGRContactDetailViewController : UIViewController <MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
{
    IBOutlet UILabel*   nameLabel;
    IBOutlet UILabel*   phoneLabel;
    IBOutlet UILabel*   emailLabel;
    
    IBOutlet UIButton*  sendButton;
}
@property (nonatomic, strong)IMContacts* contact;

- (IBAction)onEditButtonClick:(id)sender;
- (IBAction)onInviteToIMGRButtonClick:(id)sender;

@end
