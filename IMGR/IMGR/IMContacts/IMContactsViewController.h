//
//  IMContactsViewController.h
//  IMGR
//
//  Created by akram on 05/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMNetManager.h"
#import <dispatch/dispatch.h>

/*
 This class displays IMGR and non IMGR contacts and provide the features like view user's profile, chat with selected user and send invitation to non imgr user.
 */

@interface IMContactsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate, CMNetManagerDelegate>
{
    IBOutlet UISearchBar*       contactSearchBar;
    
    IBOutlet UITableView*       contactsTableView;
    
    IBOutlet UIImageView*       emptyLogoImageView;
    
    IBOutlet UILabel*           accessContactsLabel;
    IBOutlet UILabel*           allowAccessLabel;
    
    IBOutlet UILabel*           noFriendLabel;
    IBOutlet UILabel*           inviteLabel;
    
    IBOutlet UIButton*          imgrButton;
    IBOutlet UIButton*          allButton;
    IBOutlet UIButton*          inviteButton;
    
    IBOutlet UIView*            segmentView;
    
    BOOL                        isIMGRViewSelected;

    BOOL                        isSearching;

    IBOutlet NSLayoutConstraint* tableViewHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *topSpaceTableView;
    
    __weak IBOutlet NSLayoutConstraint *topSpaceSegmentView;
    
    IBOutlet NSLayoutConstraint* inviteBtnYConstraint;
    __weak IBOutlet UIImageView *scopeBarBg;
    
    UITableViewController*      _tableViewController;
    
    NSFetchedResultsController* fetchedResultsController;
}

@property(strong, nonatomic)NSFetchedResultsController* fetchedResultsController;
@property(strong, nonatomic)NSLayoutConstraint* tableViewHeightConstraint;
@property(strong, nonatomic)UISearchBar* contactSearchBar;
@property(assign, nonatomic)BOOL ascending;

- (IBAction)onSlideButtonClick:(id)sender;
- (IBAction)onPlusButtonClick:(id)sender;

- (IBAction)onIMGRButtonClick:(id)sender;
- (IBAction)onAllButtonClick:(id)sender;

- (IBAction)onInviteFriendsButtonClick:(id)sender;

@end
