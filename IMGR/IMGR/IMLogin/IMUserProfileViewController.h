//
//  IMUserProfileViewController.h
//  IMGR
//
//  Created by akram on 04/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 This class is used to set the user profile information.
 */
@interface IMUserProfileViewController : UIViewController
{
    IBOutlet UITextField*       firstNameField;
    IBOutlet UITextField*       lastNameField;
    IBOutlet UITextField*       phoneNumberField;
    IBOutlet UITextField*       emailField;
    
    IBOutlet UIButton*          userPhotoButton;
    
    IBOutlet UILabel*           supportLabel;
    IBOutlet UILabel*           imgrLabel;
    
    IBOutlet UIScrollView*      userProfileScrollView;
    
    IBOutlet NSLayoutConstraint*    topConstraint;
}

@property (nonatomic, strong)NSLayoutConstraint*    topConstraint;
@property (nonatomic, strong)NSString* phoneNumber;
@property (nonatomic, strong)NSString* countryCode;

//It will display action sheet to choose photo from library or camera.
- (IBAction)changePhoto:(id)sender;

//Display the profile completion popup with instructions "you can complete remaining info in settings" if you
//missed anything.
- (IBAction)showProfileCompletionPopup:(id)sender;

@end
