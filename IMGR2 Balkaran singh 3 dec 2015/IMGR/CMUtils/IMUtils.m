//
//  IMUtils.m
//  IMGR
//
//  Created by akram on 04/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMUtils.h"
#import <ImageIO/ImageIO.h>

#define k_OpneFireExtension @"@imgrapp.com"
//#define k_OpneFireExtension @"@coppermobile.mobi"
//#define k_OpneFireExtension @"@64.235.48.26"

static UIColor *appBlueColor;
static UIColor *appRedColor;

@implementation IMUtils


+ (UIColor *) appDarkGreyColor
{
    return [UIColor darkGrayColor];
}

+ (void)load
{
    appBlueColor = [UIColor colorWithRed:57.0/255.0 green:155.0/255.0 blue:255.0 /255.0 alpha:1.0 ];
    appRedColor = [UIColor colorWithRed:233.0/255.0 green:38.0/255.0 blue:61.0/255.0 alpha:1.0 ];
}

+ (UIColor *) appBlueColor
{
    return appBlueColor;
}

+ (UIColor *) appRedColor
{
    return appRedColor;
}

+ (UIFont *) appFontWithSize:(CGFloat) fontSize
{
    return [UIFont fontWithName:@"Arial" size:fontSize];
}

+ (UIFont *) appBoldFontWithSize:(CGFloat) fontSize
{
    return [UIFont fontWithName:@"Arial-BoldMT" size:fontSize];
}

+ (float)getOSVersion
{
    static float version;
    static dispatch_once_t takenOnce;
    dispatch_once(&takenOnce, ^{
        version = [[[UIDevice currentDevice] systemVersion] floatValue];
    });
    return version;
}

+ (NSString*)getImageFolderPath
{
    static NSString* folderPath;
   // static dispatch_once_t takenOnce;
   // dispatch_once(&takenOnce, ^{
        NSArray* array = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        folderPath = [[array objectAtIndex: 0] stringByAppendingPathComponent: @"Images"];
   // });
    return folderPath;
}

+ (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage
{
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    UIImage *resultingImage = [UIImage imageWithCGImage:masked];
    CFRelease(masked);
    CFRelease(mask);
    return resultingImage;
    
}

+ (UIImage*)resizeImage:(UIImage*)image toSize:(CGSize) sacleSize
{
    UIGraphicsBeginImageContextWithOptions(sacleSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, sacleSize.width, sacleSize.height)];
    UIImage * resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}

+ (UIImage*)circularScaleNCrop:(UIImage*)image rect: (CGRect) rect
{
    // This function returns a newImage, based on image, that has been:
    // - scaled to fit in (CGRect) rect
    // - and cropped within a circle of radius: rectWidth/2
    
    //Create the bitmap graphics context
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(rect.size.width, rect.size.height), NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Get the width and heights
    CGFloat imageWidth = image.size.width;
    CGFloat imageHeight = image.size.height;
    CGFloat rectWidth = rect.size.width;
    CGFloat rectHeight = rect.size.height;
    
    //Calculate the scale factor
    CGFloat scaleFactorX = rectWidth/imageWidth;
    CGFloat scaleFactorY = rectHeight/imageHeight;
    
    //Calculate the centre of the circle
    CGFloat imageCentreX = rectWidth/2;
    CGFloat imageCentreY = rectHeight/2;
    
    // Create and CLIP to a CIRCULAR Path
    // (This could be replaced with any closed path if you want a different shaped clip)
    CGFloat radius = rectWidth/2;
    CGContextBeginPath (context);
    CGContextAddArc (context, imageCentreX, imageCentreY, radius, 0, 2*M_PI, 0);
    CGContextClosePath (context);
    CGContextClip (context);
    
    //Set the SCALE factor for the graphics context
    //All future draw calls will be scaled by this factor
    CGContextScaleCTM (context, scaleFactorX, scaleFactorY);
    
    // Draw the IMAGE
    CGRect myRect = CGRectMake(0, 0, imageWidth, imageHeight);
    [image drawInRect:myRect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (void)displayActionSheet: (UIViewController*)controller
{
    UIActionSheet* changePhotoSheet = [[UIActionSheet alloc] initWithTitle:@"Tell a friend about IMGR via" delegate:(id)controller
                                                         cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles: @"Mail", @"Message", nil];

    [changePhotoSheet showFromRect:CGRectMake(0, controller.view.frame.size.height, controller.view.frame.size.width, 0) inView:controller.view animated:YES];
}

+ (NSString *)phonenumberFromFormattedNumber:(NSString *) formatted
{
    NSCharacterSet *trim = [NSCharacterSet characterSetWithCharactersInString:@"()-."];
    formatted = [[formatted componentsSeparatedByCharactersInSet: trim] componentsJoinedByString: @""];
    
    NSString* noSpaces = [[formatted componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]
                          componentsJoinedByString:@""];
    return noSpaces;
}

+ (NSString *)phoneNumberForJid:(NSString *)jid
{
    return [jid stringByReplacingOccurrencesOfString:k_OpneFireExtension withString:@""];
}

+ (NSString *)myPhoneNumber
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:k_DEFAULTS_KEY_USER_ID];
}

/*+ (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}*/

+(BOOL) validEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (void)noInternetAvailable
{
//    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"NO INTERNET", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
//    [alertView show];
}

+ (CGSize) imageSizeAtFilePath:(NSString *) imagePath
{
    NSURL *imageURL = [NSURL fileURLWithPath:imagePath];
    NSLog(@"%@",imageURL);
    if (imageURL != nil)
    {
        CGImageSourceRef imageSourceRef = CGImageSourceCreateWithURL((__bridge CFURLRef)imageURL, NULL);
        if(imageSourceRef != NULL)
        {
            CFDictionaryRef props = CGImageSourceCopyPropertiesAtIndex(imageSourceRef, 0, NULL);
            CFRelease(imageSourceRef);
            NSDictionary *imageProperties = (__bridge NSDictionary *)props;
            CGSize imageSize = CGSizeMake([[imageProperties objectForKey:k_IMAGE_IO_Width] doubleValue], [[imageProperties objectForKey:k_IMAGE_IO_Height] doubleValue]);
            CFRelease(props);
            return imageSize;
        }
    }
    return CGSizeZero;
}

@end
