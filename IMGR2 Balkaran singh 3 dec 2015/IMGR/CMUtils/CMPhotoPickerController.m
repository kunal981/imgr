//
//  CMPhotoPickerController.m
//  Radus
//
//  Created by Akram on 7/26/13.
//  Copyright (c) 2013 test. All rights reserved.
//

#import "CMPhotoPickerController.h"
#import "IMUtils.h"

@interface CMPhotoPickerController ()

@end

@implementation CMPhotoPickerController

static CMPhotoPickerController *sharedInstance;

+ (CMPhotoPickerController *)sharedInstance
{
    
#ifndef __clang_analyzer__
    
    @synchronized(self)
    {
        if(!sharedInstance)
        {
          sharedInstance = [[CMPhotoPickerController alloc] init];
        }
    }
    
#endif
    
    return sharedInstance;
}

+(id)alloc
{
    @synchronized(self)
    {
        NSAssert(sharedInstance == nil, @"Attempted to allocate a second instance of a singleton LocationController.");
        sharedInstance = [super alloc];
    }
    return sharedInstance;
}


- (void)displayImagePickerWithType:(NSInteger)pickerType target:(id)target
{
    switch (pickerType)
    {
        case 1: //from gallary
        {
            self.delegate = target;
            self.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            self.allowsEditing = YES;
            
            if([IMUtils getOSVersion] >= 7.0)
            {
                self.navigationBar.translucent = NO;
            }
            else
            {
                [self.navigationBar setBackgroundImage: [UIImage imageNamed: @"navbar_small.png"] forBarMetrics: UIBarMetricsDefault];
            }
           // [self.navigationController presentViewController: self animated: YES completion: nil];
            
        }break;
            
        case 2: //from camera
        {
            if([CMPhotoPickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                self.delegate   = target;
                self.sourceType = UIImagePickerControllerSourceTypeCamera;
                self.allowsEditing = YES;
                //self.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.sourceType];
                
               // [self.navigationController presentModalViewController: self animated: YES];
            }
            else
            {
                UIAlertView* cameraNotFound=[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"CAMERA NOT FOUND", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil, nil];
                [cameraNotFound show];
            }
        }break;
            
        default:
            break;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //app.navController.delegate = self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
