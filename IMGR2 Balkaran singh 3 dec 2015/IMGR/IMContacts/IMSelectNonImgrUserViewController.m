//
//  IMSelectNonImgrUserViewController.m
//  IMGR
//
//  Created by akram on 24/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMSelectNonImgrUserViewController.h"
#import "IMContactsCell.h"
#import "CMCoreDataHandler.h"
#import "IMContacts.h"
#import "IMPromoHeaderView.h"
#import "IMDataHandler.h"

#define SELECTED_BG_VIEW_TAG    100000

@interface IMSelectNonImgrUserViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>
{
    NSFetchedResultsController* fetchedResultsController;
    
    NSMutableArray*             inviteeList;
}
@end

@implementation IMSelectNonImgrUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSFetchedResultsController*)fetchContactsResultController: (NSString*)searchString
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    if(fetchedResultsController == nil)
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMContacts"
                                                  inManagedObjectContext:moc];
        
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"index_character" ascending:YES];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"first_name" ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, sd2, nil];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        if(searchString)
        {
            NSPredicate* predicate = [NSPredicate predicateWithFormat: @"is_imgr_user == %d && first_name contains[c] %@", NO,searchString];
            [fetchRequest setPredicate: predicate];
        }
        else
        {
            NSPredicate* predicate = [NSPredicate predicateWithFormat: @"is_imgr_user == %d", NO];
            [fetchRequest setPredicate: predicate];
        }
        fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                       managedObjectContext:moc
                                                                         sectionNameKeyPath:@"index_character"
                                                                                  cacheName:nil];
        [fetchedResultsController setDelegate:self];
        
        NSError *error = nil;
        if (![fetchedResultsController performFetch:&error])
        {
            DDLogError(@"Error performing fetch: %@", error);
        }
    }
    return fetchedResultsController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    inviteeList = [[NSMutableArray alloc] init];
    fetchedResultsController = [self fetchContactsResultController: nil];
    [nonImgrContactsTableView setDataSource:self];
    [nonImgrContactsTableView reloadData];
    [nonImgrContactsTableView setEditing: YES animated:YES];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    fetchedResultsController.delegate = nil;
}

#pragma mark - IBActions
- (IBAction)onInviteButtonClick:(id)sender
{
    if([inviteeList count] > 1)
    {
        [self inviteViaMessage];
    }
    else if ([inviteeList count] == 1)
    {
        IMContacts* contact = [inviteeList objectAtIndex: 0];
        
        if(!contact.email.length)
            [self inviteViaMessage];
        else
            [IMUtils displayActionSheet: self];
    }
    else
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: @"Please select a contact to invite" delegate:nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
        [alertView show];
    }
}

- (void)inviteViaEmail
{
    MFMailComposeViewController* mailCompose = [[MFMailComposeViewController alloc] init];
    
    if([MFMailComposeViewController canSendMail])
    {
        mailCompose.mailComposeDelegate = self;
        [mailCompose setSubject:NSLocalizedString(@"NEW MESSAGE", @"")];
        
        NSString* bodyStr = NSLocalizedString(@"MESSAGE BODY", @"");
        //bodyStr = [bodyStr stringByAppendingString: [NSString stringWithFormat: @"\n\nGet it now from\n%@", @"applinkfromweb.com"]];
        
        NSMutableArray* tempArray = [[NSMutableArray alloc] init];
        for (int i = 0; i < [inviteeList count]; ++i)
        {
            [tempArray addObject: [[inviteeList objectAtIndex: i] email]];
        }
        [mailCompose setToRecipients: tempArray];
        [mailCompose setMessageBody: bodyStr isHTML: NO];
        
        [self.navigationController presentViewController:mailCompose animated:YES completion: ^{}];
    }
}

- (void)inviteViaMessage
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    
    if([MFMessageComposeViewController canSendText]) {
        
        NSString* bodyOfMessage = NSLocalizedString(@"MESSAGE BODY", @"");
        controller.body = bodyOfMessage;
        NSMutableArray* tempArray = [[NSMutableArray alloc] init];
        for (int i = 0; i < [inviteeList count]; ++i)
        {
            [tempArray addObject: [[inviteeList objectAtIndex: i] phone_number]];
        }
        controller.recipients = tempArray;
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated: YES completion: ^{}];
        
    }
}
#pragma mark UIActionSheet Delegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self inviteViaEmail];
            break;
            
        case 1:
            [self inviteViaMessage];
            break;
            
        case 2:
            
            break;
            
        default:
            break;
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated: YES completion:^{}];

    switch (result)
    {
        case MFMailComposeResultCancelled:
            
            break;
            
        case MFMailComposeResultSaved:
            break;
            
        case MFMailComposeResultSent:
        {
            for (int i = 0; i < [inviteeList count]; ++i)
            {
                IMContacts* contactInfo = (IMContacts*)[inviteeList objectAtIndex: i];
                
                [contactInfo setValue: [NSNumber numberWithBool: YES] forKey: @"is_invited"];
                [[IMDataHandler sharedInstance] saveDatabase];
            }
            
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message:@"Invitation sent successfully" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            
            //[self.navigationController popViewControllerAnimated: YES];
        }break;
            
        case MFMailComposeResultFailed:
            break;
            
        default:
            break;
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultSent:
        {
            for (int i = 0; i < [inviteeList count]; ++i)
            {
                IMContacts* contactInfo = (IMContacts*)[inviteeList objectAtIndex: i];
                
                [contactInfo setValue: [NSNumber numberWithBool: YES] forKey: @"is_invited"];
                [[IMDataHandler sharedInstance] saveDatabase];
            }
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"Invite" message: @"Invitation Sent Successfully" delegate:self cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
            [alertView show];
            
            //[self.navigationController popViewControllerAnimated: YES];

        }break;
            
        case MessageComposeResultFailed:
            break;
            
        default:
            break;
    }
    [controller dismissViewControllerAnimated: YES completion:^{}];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated: YES];
}
#pragma mark - IBActions
- (IBAction)onSelectAllButtonClick:(id)sender
{
    UIButton* button = (UIButton*)sender;
    [inviteeList removeAllObjects];
    
    if([button.titleLabel.text isEqualToString: @"Select All"])
    {
        for (int i = 0; i < [nonImgrContactsTableView numberOfSections]; i++) {
            for (int j = 0; j < [nonImgrContactsTableView numberOfRowsInSection:i]; j++) {
                
                [nonImgrContactsTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:j inSection:i] animated:YES scrollPosition:UITableViewScrollPositionNone];
            }
        }
        [button setTitle: @"Deselect All" forState: UIControlStateNormal];
        [inviteeList addObjectsFromArray: fetchedResultsController.fetchedObjects];
    }
    else
    {
        for (int i = 0; i < [nonImgrContactsTableView numberOfSections]; i++) {
            for (int j = 0; j < [nonImgrContactsTableView numberOfRowsInSection:i]; j++) {
                
                [nonImgrContactsTableView deselectRowAtIndexPath: [NSIndexPath indexPathForRow:j inSection:i] animated: YES];
            }
        }
        [button setTitle: @"Select All" forState: UIControlStateNormal];
    }
    
    if (inviteeList.count) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate/DataSource methods
#pragma mark - UITableView Delegate/DataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [fetchedResultsController.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[fetchedResultsController.sections objectAtIndex: section] numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier = @"identifier";
    
    IMContactsCell* cell = (IMContactsCell*)[tableView dequeueReusableCellWithIdentifier: identifier];
    
    if(cell == nil)
    {
        cell = [IMContactsCell cellLoadedFromNibFile];
        cell.imgrLabel.hidden = YES;
    }
    cell.delegate = self;
    cell.tag = indexPath.row;
    cell.infoButton.hidden = YES;
    cell.statusButton.hidden = YES;
    UILabel* nameLabel = (UILabel*)[cell.contentView viewWithTag: 100];

    IMContacts* contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: indexPath.section] objects] objectAtIndex: indexPath.row];
    
    NSString* fName = contactInfo.first_name;
    NSString* lName = contactInfo.last_name;
    
    nameLabel.text = @"No Name";
    if([fName length] > 0 && [lName length] > 0)
        nameLabel.text = [NSString stringWithFormat: @"%@ %@", fName, lName];
    else if([fName length] > 0)
        nameLabel.text = fName;
    else if([lName length] > 0)
        nameLabel.text = lName;
    
    //To avoid the row color background color when selected
    if (cell.selectedBackgroundView.tag != SELECTED_BG_VIEW_TAG) {
             UIView *selectionColor = [[UIView alloc] init];
        
        selectionColor.tag = SELECTED_BG_VIEW_TAG;
        
        UIView *rowSelectionColor = [[UIView alloc] initWithFrame:CGRectMake(0, 1, cell.frame.size.width, 42)];
        rowSelectionColor.tag = SELECTED_BG_VIEW_TAG;
        rowSelectionColor.backgroundColor = [UIColor whiteColor];
        cell.selectedBackgroundView = rowSelectionColor;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IMContacts* contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: indexPath.section] objects] objectAtIndex: indexPath.row];
    
    [inviteeList addObject: contactInfo];
    if (inviteeList.count) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IMContacts* contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: indexPath.section] objects] objectAtIndex: indexPath.row];
    [inviteeList removeObject: contactInfo];
    if (inviteeList.count) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    IMPromoHeaderView *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[IMPromoHeaderView cellReusableIdentifier]];
    if (nil == cell) {
        NSLog(@"[IMPromoHeaderView cellLoadedFromNibFile];");
        cell = [IMPromoHeaderView cellLoadedFromNibFile];
    }
    //    cell.headerLabel.text = [NSString stringWithFormat:@"Header %d",section];
    cell.headerLabel.text = [[fetchedResultsController.sections objectAtIndex: section] indexTitle];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 22.0;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:
            [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (title == UITableViewIndexSearch)
    {
        CGRect searchBarFrame = self.searchDisplayController.searchBar.frame;
        [tableView scrollRectToVisible:searchBarFrame animated:YES];
        
        return -1;
    }
    else {
        UILocalizedIndexedCollation *currentCollation = [UILocalizedIndexedCollation currentCollation];
        NSLog(@"selected charecter=%ld",(long)[currentCollation sectionForSectionIndexTitleAtIndex:index]);
        return [currentCollation sectionForSectionIndexTitleAtIndex:index-1];
    }
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    //searchResults = [recipes filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}


#pragma mark - UISearchBar Delegate methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;                     // called when keyboard search button pressed
{
    fetchedResultsController.delegate = nil;
    fetchedResultsController = nil;
    
    fetchedResultsController = [self fetchContactsResultController: searchBar.text];
    [nonImgrContactsTableView reloadData];
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    fetchedResultsController.delegate = nil;
    fetchedResultsController = nil;
    fetchedResultsController = [self fetchContactsResultController: searchText];
    [nonImgrContactsTableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    fetchedResultsController.delegate = nil;
    fetchedResultsController = nil;
    
    fetchedResultsController = [self fetchContactsResultController: nil];
    [nonImgrContactsTableView reloadData];
    
    [nonImgrContactsTableView setContentOffset:CGPointMake(0, 44) animated:YES];
    [searchBar resignFirstResponder];
}



@end
