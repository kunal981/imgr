//
//  IMAddNewContactViewController.h
//  IMGR
//
//  Created by akram on 07/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 This class gives the functionality to add new user in phone's address book.
 */
@class IMChatViewController;
@interface IMAddNewContactViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
{
    IBOutlet UITableView* newContactTableView;
    
    UILabel *labelPhone;
}
@property (strong, nonatomic)NSString* imgrPhoneNumber;
@property (weak, nonatomic)IMChatViewController* chatVC;

- (IBAction)onDoneButtonClick:(id)sender;

@end
