//
//  IMContactDetailViewController.h
//  IMGR
//
//  Created by akram on 05/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 This class display IMGR user profile detail and provide the features like send invitation, block contact and edit profile.
 */
@class IMContacts;
@interface IMContactDetailViewController : UIViewController
{
    IBOutlet UILabel* nameLabel;
    IBOutlet UILabel* phoneNumberLabel;
    IBOutlet UILabel* emailLabel;
    __weak IBOutlet UIButton *blockContactButton;
    
    __weak IBOutlet UIImageView* userImageView;
}
@property (nonatomic, strong)IMContacts* contact;
@property (nonatomic)BOOL comingFromChatScreen;

- (IBAction)onEditButtonClick:(id)sender;
- (IBAction)onSendMessageButtonClick:(id)sender;
- (IBAction)onBlockContactButtonClick:(id)sender;


@end
