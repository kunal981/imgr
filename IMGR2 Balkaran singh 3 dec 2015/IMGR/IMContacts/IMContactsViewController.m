//
//  IMContactsViewController.m
//  IMGR
//
//  Created by akram on 05/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMContactsViewController.h"
#import "IMMenuViewController.h"
#import "TWTSideMenuViewController.h"
#import "IMContactsCell.h"
#import "IMUtils.h"

#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "IMContactInfo.h"
#import "IMDataHandler.h"
#import "CMNetManager.h"
#import "CMCoreDataHandler.h"
#import "IMContacts.h"
#import "IMPromoHeaderView.h"
#import "MBProgressHUD.h"
#import "ColorC.h"
#import "IMNonIMGRContactDetailViewController.h"
#import "IMAddNewContactViewController.h"
#import "IMContactDetailViewController.h"
#import "IMSelectNonImgrUserViewController.h"
#import "IMChatViewController.h"


@interface IMContactsViewController ()<NSFetchedResultsControllerDelegate, UINavigationBarDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
   // NSFetchedResultsController* rosterFetchResultController;
    
    NSFetchedResultsController* searchFetchedResultsController;
    
    NSMutableString*            phoneNumberList;
    
    NSIndexPath*                selectedPromoIndex;
    
    NSString*                   newPhoneNumber;
    NSInteger                  maxLimit;
    
    MBProgressHUD*              progressView;
    
    BOOL                        isSynced;
    BOOL                        isEmailViewOpened;
    
    BOOL                        isAddressbookAccessible;
    BOOL                        isUpdatingContacts;
}
@property (weak, nonatomic)CMNetManager* netManager;

@end

@implementation IMContactsViewController
@synthesize contactSearchBar;
@synthesize ascending = _ascending;
@synthesize tableViewHeightConstraint;
@synthesize fetchedResultsController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (NSFetchedResultsController*)fetchContactsResultController: (BOOL)ImgrPredicate searchText: (NSString*)searchString
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    if(fetchedResultsController == nil)
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMContacts"
                                                  inManagedObjectContext:moc];
        
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"index_character" ascending:YES];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"first_name" ascending:YES];
      
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1,sd2, nil];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        if(ImgrPredicate)
        {
            if(searchString)
            {
                NSPredicate* predicate = [NSPredicate predicateWithFormat: @"is_imgr_user == %d && first_name contains[c] %@ && is_blocked == %d", ImgrPredicate, searchString, NO];
                [fetchRequest setPredicate: predicate];
            }
            else
            {
                NSPredicate* predicate = [NSPredicate predicateWithFormat: @"is_imgr_user == %d && is_blocked == %d", ImgrPredicate, NO];
                [fetchRequest setPredicate: predicate];
            }
        }
        else
        {
            if(searchString)
            {
                NSPredicate* predicate = [NSPredicate predicateWithFormat: @"first_name contains[c] %@ || last_name contains[c] %@ && is_blocked == %d", searchString,searchString, NO];
                [fetchRequest setPredicate: predicate];
            }
            else
            {
                NSPredicate* predicate = [NSPredicate predicateWithFormat: @"is_blocked == %d", NO];
                [fetchRequest setPredicate: predicate];
            }
        }
        if (searchString.length>0) {
            fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                           managedObjectContext:moc
                                                                             sectionNameKeyPath:nil
                                                                                      cacheName:nil];

        }
        else
        {
            fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                           managedObjectContext:moc
                                                                             sectionNameKeyPath:@"index_character"
                                                                                      cacheName:nil];

        }
        
            
        
        [fetchedResultsController setDelegate:self];
        
        NSError *error = nil;
        if (![fetchedResultsController performFetch:&error])
        {
            DDLogError(@"Error performing fetch: %@", error);
        }
    }
    return fetchedResultsController;
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    //if(!isUpdatingContacts)
    {
        //fetch imgr users from database
        NSArray* imgrContacts = [[IMDataHandler sharedInstance] fetchIMGRContacts];
        if([imgrContacts count] > 0 && isIMGRViewSelected) {
            NSLog(@"Found IMGR users");

            int height = [[UIScreen mainScreen] bounds].size.height;
            if(IS_IPHONE5)
                height = height - 114;
            else
                height = height - 50 - 20 - 44;
            
            [self adjustPositionOfButton: height];
            
            [self adjustHeightOfTableview];
            
            BOOL isRegistered = [[NSUserDefaults standardUserDefaults] boolForKey: K_ALREADY_REGISTERED];
            if(!isRegistered)
                [self showAddFriendsToIMGRPopup];
        }
        else if(isIMGRViewSelected) {
            NSLog(@"Not Found IMGR users");

            int height = [[UIScreen mainScreen] bounds].size.height;
            if(IS_IPHONE5)
                height = height - (height/3) - 90;
            else
                height = height - (height/3) - 30;
            
            [self adjustPositionOfButton: height];
        }
        [progressView hide: YES];
        
        phoneNumberList = [[NSMutableString alloc] init];
        isSynced = YES;
        
        fetchedResultsController.delegate = nil;
        fetchedResultsController = nil;
        fetchedResultsController = [self fetchContactsResultController: isIMGRViewSelected searchText: nil];
        
        [self performSelector:@selector(updateContactsTable) withObject:nil
                   afterDelay:.1];
        [contactsTableView reloadData];
    }
}

- (void)newContactAdded: (NSNotification*)notif
{
    NSString* phoneNumber = (NSString*)[notif object];
    newPhoneNumber = [[NSString alloc] initWithString: phoneNumber];
 
    [self performSegueWithIdentifier:@"IMNonIMGRContactDetailViewController" sender:self];
}

- (void)removeLoader
{
    if(isIMGRViewSelected)
    {
        [progressView hide: YES];

        DebugLog(@"Not Found IMGR users");
        contactsTableView.hidden = YES;
        inviteButton.hidden = NO;
        noFriendLabel.hidden    = NO;
        inviteLabel.hidden      = NO;
        int height = [[UIScreen mainScreen] bounds].size.height;
        if(IS_IPHONE5)
            height = height - (height/3) - 90;
        else
            height = height - (height/3) - 30;
        
        [self adjustPositionOfButton: height];
    }

    [self updateContactsTable];
}

- (void)dealloc
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"ADD_NEW_CONTACT" object: nil];
}

#pragma mark - Contacts View methods
//if Dont allow button clicked
- (void)userDidClickOnDontAllow
{
    [self hideAllViews];
    inviteButton.hidden         = YES;
    contactsTableView.hidden    = YES;
    
    emptyLogoImageView.hidden   = NO;
    accessContactsLabel.hidden  = NO;
    allowAccessLabel.hidden     = NO;
}

- (void)userDidClickOnAllow
{
    [self hideAllViews];
    
    [self fetchContactsAndSendToServer];
}

- (void)noContactFound
{
    if(isIMGRViewSelected)
    {
        [self displayNoIMGRContactView];
    }
    else
    {
        contactsTableView.hidden    = NO;
        noFriendLabel.hidden        = YES;
        inviteLabel.text            = NSLocalizedString(@"NO CONTACT", "@");
        inviteLabel.hidden          = NO;
        noFriendLabel.hidden        = YES;
        inviteButton.hidden         = YES;
        emptyLogoImageView.hidden   = YES;
    }
}

- (void)hideAllViews
{
    emptyLogoImageView.hidden   = YES;
    accessContactsLabel.hidden  = YES;
    allowAccessLabel.hidden     = YES;
    noFriendLabel.hidden        = YES;
    inviteLabel.hidden          = YES;
    inviteButton.hidden         = YES;
    
    if(isIMGRViewSelected) {
        inviteButton.hidden         = NO;
    
        int height = [[UIScreen mainScreen] bounds].size.height;
        if(IS_IPHONE5)
            height = height - 114;
        else
            height = height - 50 - 20 - 44;

        [self adjustPositionOfButton: height];
    }
    else
        inviteButton.hidden         = YES;
}

- (void)resetContactView
{
//    if(!isAddressbookAccessible  )
//    {
//        [self hideAllViews];
//        if (0==[fetchedResultsController.fetchedObjects count]) {
//            [self userDidClickOnDontAllow];
//        }
//    }
//    else
    {
        [self hideAllViews];
        contactsTableView.hidden = NO;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CMStream *stream = [[CMChatManager sharedInstance] getStreamForHost:HOST_OPENFIRE];
    NSString *IsContactsUploaded=[[NSUserDefaults standardUserDefaults]valueForKey:@"contactsUploaded"];
    if ([stream isConnected]|| [stream isConnecting]|| IsContactsUploaded.length>0)
    {
        // Do any additional setup after loading the view.
        phoneNumberList = [[NSMutableString alloc] init];
        
        contactsTableView.contentOffset = CGPointMake(0, SEARCH_BAR_HEIGHT);
        if([IMUtils getOSVersion] >= 7.0)
            contactsTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        _tableViewController = [[UITableViewController alloc]initWithStyle:UITableViewStylePlain];
        [self addChildViewController:_tableViewController];
        
        _tableViewController.refreshControl = [UIRefreshControl new];
        [_tableViewController.refreshControl addTarget:self action:@selector(pullDownToRefresh) forControlEvents:UIControlEventValueChanged];
        
        _tableViewController.tableView = contactsTableView;
        
        [self didMoveToParentViewController: _tableViewController];
        
        contactsTableView.hidden = YES;
        progressView = nil;//SS we do not need it
        progressView = [[MBProgressHUD alloc] initWithView: self.view];
        progressView.labelText = @"Please wait...";
        
        [app.window addSubview: progressView];
        
        imgrButton.highlighted = YES;
        allButton.highlighted = NO;
        
        inviteButton.hidden = YES;
        isIMGRViewSelected = YES;
        
        imgrButton.titleLabel.font = [UIFont boldSystemFontOfSize: 17.0];
        allButton.titleLabel.font = [UIFont systemFontOfSize: 17.0];
        
        noFriendLabel.hidden    = YES;
        inviteLabel.hidden      = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newContactAdded:) name:@"ADD_NEW_CONTACT" object: nil];
        
        if([IMUtils getOSVersion] >= 7.0)
            contactsTableView.sectionIndexBackgroundColor = [UIColor clearColor];
        
        BOOL isRegistered = [[NSUserDefaults standardUserDefaults] boolForKey: K_ALREADY_REGISTERED];
        
        fetchedResultsController.delegate = nil;
        fetchedResultsController = nil;
        fetchedResultsController = [self fetchContactsResultController: isIMGRViewSelected searchText: nil];
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"contactsUploaded"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        inviteLabel.text = @"";
        isAddressbookAccessible = YES;
        isUpdatingContacts = NO;
        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, nil);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error)
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         if (granted){
                                                             if(!isRegistered) {
                                                                 
                                                                 [self performSelector:@selector(fetchContactsFromAddressBook) withObject:nil afterDelay: .1];
                                                             }
                                                         }
                                                         
                                                         
                                                         
                                                     });
                                                 });

        
        
        [self adjustHeightOfTableview];

       

    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet Connection Error" message:NSLocalizedString(@"CHECK INTERNET CONNECTION", @"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}


- (void)viewWillAppear:(BOOL)animated
{
    
}

- (void)viewDidAppear:(BOOL)animated
{
    fetchedResultsController.delegate = self;
    //[contactsTableView reloadData];

    BOOL isRegistered = [[NSUserDefaults standardUserDefaults] boolForKey: K_ALREADY_REGISTERED];
    //isAddressbookAccessible = [self accessContactBook];

    if(isRegistered && isAddressbookAccessible)
    {
        if(isIMGRViewSelected)
        {
            [self onIMGRButtonClick:imgrButton];
        }
        else
        {
            [self onAllButtonClick: allButton];
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    self.netManager.failDelegate = nil;
}

- (void)pullDownToRefresh
{
    progressView.hidden = YES;
    
    isUpdatingContacts = YES;
    [self fetchContactsFromAddressBook];
}

#pragma mark - Fetch Contacts
- (void)fetchContacts
{
//    CFErrorRef *error = nil;
//    
//    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
//    
//    ABAddressBookRegisterExternalChangeCallback(addressBook, MyAddressBookExternalChangeCallback, (__bridge void *)(self));
//    __block BOOL accessGranted = NO;
//    if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
//        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
//        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
//            accessGranted = granted;
//            dispatch_semaphore_signal(sema);
//        });
//        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
//        
//    }
//    else { // we're on iOS 5 or older
//        accessGranted = YES;
//    }
    CFErrorRef *error = nil;
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        
#ifdef DEBUG
        NSLog(@"Fetching contact info ----> ");
#endif
       
        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonSortByFirstName);
        
        CFIndex nPeople = CFArrayGetCount(allPeople);
        
        NSString* myNumber = [[NSUserDefaults standardUserDefaults] objectForKey: k_DEFAULTS_KEY_USER_ID];

        [[CMCoreDataHandler sharedInstance] performAndSaveDataBaseTaskInBackground:^{
            
            for (int i = 0; i < nPeople; ++i)
            {
                ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
                
                //get First Name and Last Name
                
                ABRecordID contactID = ABRecordGetRecordID(person);
                
                NSString* firstName = (__bridge_transfer NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
                
                NSString* lastName =  (__bridge_transfer NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
                
            
                if (!firstName) {
                    firstName = @"";
                }
                if (!lastName) {
                    lastName = @"";
                }
            
                
                NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
                
                ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
                
                CFIndex count = ABMultiValueGetCount(multiPhones);
                for(int i=0;i<count;i++) {
                    
                    CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                    NSString *phoneNumber = (__bridge_transfer NSString *) phoneNumberRef;
                    
                    //DebugLog(@"App Crash at phonenumber = %d and number = %@", i, phoneNumber);
                    
                    if(phoneNumber)
                        [phoneNumbers addObject:phoneNumber];
                }
                
                NSMutableArray *contactEmails = [NSMutableArray new];
                ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
                
                CFIndex mailCount = ABMultiValueGetCount(multiEmails);
                for (CFIndex i=0; i<mailCount; i++) {
                    CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
                    NSString *contactEmail = (__bridge_transfer NSString *)contactEmailRef;
                    
                    if(contactEmail)
                        [contactEmails addObject:contactEmail];
                    
                }
                
                if([phoneNumbers count] > 0)
                {
                    for (int j = 0; j < [phoneNumbers count]; ++j)
                    {
                        //dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
                        [dict setObject: firstName forKey: @"first_name"];
                        [dict setObject: lastName forKey: @"last_name"];
                        [dict setObject: [phoneNumbers objectAtIndex: j] forKey: @"phone_number"];
                        
                        if([contactEmails count] > 0)
                            [dict setObject: [contactEmails objectAtIndex: 0] forKey: @"email"];
                        
                        NSString* str = [phoneNumbers objectAtIndex: j];
                        
                        if(str.length && [str characterAtIndex: 0] == '+')
                        {
                            NSLog(@"Character at index = %c", [str characterAtIndex:3]);
                            NSRange range;
//                            if([IMUtils getOSVersion] >= 7.0)
//                                range = [str rangeOfString: @"\u00a0" options:NSCaseInsensitiveSearch];
//                            else
                                range = [str rangeOfString: @" " options:NSCaseInsensitiveSearch];
                            
                            if(range.location != NSNotFound)
                                str = [str substringFromIndex: range.location + range.length];
                        }
                        
                        NSCharacterSet *trim = [NSCharacterSet characterSetWithCharactersInString:@"()-."];
                        str = [[str componentsSeparatedByCharactersInSet: trim] componentsJoinedByString: @""];
                        
                        NSString* noSpaces = [[str componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]
                                              componentsJoinedByString:@""];
                        
                        [dict setObject: [NSString stringWithFormat: @"%d", contactID] forKey: @"contactID"];
                        if (noSpaces.length>10) {
                            
                            
                            noSpaces=[noSpaces substringFromIndex:MAX((int)[noSpaces length]-10, 0)];
                            NSLog(@"%@",noSpaces);
                            
                        }else{
                            
                        }
                        NSLog(@"%@",noSpaces);
                        NSLog(@"%@",myNumber);
                        [dict setObject: noSpaces forKey: @"phone_number_2"];
                        
                        NSLog(@"IMGR NUMBER %@",noSpaces);
                        if(![noSpaces isEqualToString: myNumber])
                        {
                            [phoneNumberList appendString: noSpaces];
                            [phoneNumberList appendString: @","];
                            IMContacts* contact = [[IMDataHandler sharedInstance] parseContacts: dict];
                            NSLog(@" contact %@",contact);
                            contact.isAvailabledInAddressBook = [NSNumber numberWithBool: YES];
                        }
                        //});
                    }
                }
            }
            
            [[IMDataHandler sharedInstance] deleteDeadContacts];
            
            //if([self accessContactBook]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[[IMDataHandler sharedInstance] saveDatabase];
                    if(phoneNumberList.length == 0)
                    {
                        [self updateContactsTable];
                        [progressView hide: YES];
                        if(isIMGRViewSelected)
                            [self displayNoIMGRContactView];
                    }
                    self.netManager = [app checkIMGRUsers: phoneNumberList delegate: self];
                });
            //}
        }];
        
        
    } else {
#ifdef DEBUG
        NSLog(@"Cannot fetch Contacts :( ");        
#endif
    }
    
    
}

#pragma mark - CMNetManager Delegate methods
//This delegate gets called on success
-(void)netManager:(CMNetManager*)netManager didReceiveResponse:(NSDictionary*)dictionary
{
    //[self updateContactsTable];
    //[progressView hide: YES];
    [self removeLoader];
}

-(void)netManager:(CMNetManager*)netManager didReceiveResponseString:(NSString*)response
{
   
}

//This delegate gets called on failure
-(void)netManager:(CMNetManager*)netManager didFailWithError:(NSError*)error
{
    [self updateContactsTable];
    [progressView hide: YES];
    [IMUtils noInternetAvailable];
}

-(void)netManager:(CMNetManager *)netManager didGetSuccessToConnectWithServer:(NSError *)error
{
    
}

//This delegate gets called if internet connection is not available
-(void)netManager:(CMNetManager *)netManager didFailToConnectWithServer:(NSError *)error
{
    [self updateContactsTable];
    [progressView hide: YES];
    [IMUtils noInternetAvailable];
}

void MyAddressBookExternalChangeCallback (ABAddressBookRef ntificationaddressbook,CFDictionaryRef info,void *context)
{
    NSLog(@"Changed Detected......");
    
    //compare coredata and address book's contacts for deleted, modified or newly added contacts in order to minimize the server load.
    //Fetch all contacts from core data.
    //Fetch address book contacts.
    //Send filtered objects to server that tells us which object is IMGR object and which is not.
}

- (void)setButtonsVisibility
{
    BOOL isRegistered = [[NSUserDefaults standardUserDefaults] boolForKey: K_ALREADY_REGISTERED];
    if(!isRegistered) {
        
        contactsTableView.hidden    = YES;
        
        emptyLogoImageView.hidden   = NO;
        accessContactsLabel.hidden  = NO;
        allowAccessLabel.hidden     = NO;
    }
    else
    {
        emptyLogoImageView.hidden   = YES;
        accessContactsLabel.hidden  = YES;
        allowAccessLabel.hidden     = YES;
        contactsTableView.hidden    = NO;
    }
}

- (BOOL)accessContactBook
{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined)
    {
         ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
             if(granted){
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     isAddressbookAccessible = YES;
                     [self userDidClickOnAllow];
                 });
             
             }
             else {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if([fetchedResultsController.fetchedObjects count] == 0 ) {
                         [self userDidClickOnDontAllow];
                         isAddressbookAccessible = NO;
                     }
                 });
             }
         });
        
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        // The user has previously given access, add the contact
        isAddressbookAccessible = YES;
        return YES;
    }
    else
    {
         // The user has previously denied access
         // Send an alert telling user to change privacy setting in settings app
        dispatch_async(dispatch_get_main_queue(), ^{
            //[self setButtonsVisibility];
            [self userDidClickOnDontAllow];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"ACCESS DENIED", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
            [alertView show];
        });
        isAddressbookAccessible = NO;
        return NO;
        
    }
    if (addressBookRef)
    {
        CFRelease(addressBookRef);
    }
    isAddressbookAccessible = NO;
    return NO;
}
                
- (void)fetchContactsAndSendToServer
{
    [progressView show:YES];
    
    _ascending = !_ascending;
    
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    
    [self fetchContacts];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
    });
    
    //});
}

- (void)fetchContactsFromAddressBook
{
    [self accessContactBook];
    if (!isAddressbookAccessible){
        [self performSelector:@selector(updateContactsTable) withObject:nil
                   afterDelay:.1];
        return;
    }
    
    [self fetchContactsAndSendToServer];
}

- (void)updateContactsTable
{
    isUpdatingContacts = NO;

    [contactsTableView reloadData];
    
    [_tableViewController.refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAddFriendsToIMGRPopup
{
    UIAlertView* warningAlertView = [[UIAlertView alloc] initWithTitle: @"Add friends on \"IMGR\"" message:@"Some of your contacts may already be using IMGR. Tap on the \"invite\" to send our App Store download link to other friends and contacts." delegate:self cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
    warningAlertView.tag = 1010;
    [warningAlertView show];
}
#pragma mark - UIAlertView Delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1010)
        return;
}

- (NSFetchedResultsController *)fetchedResultsControllerForTableView:(UITableView *)tableView
{
    return tableView == contactsTableView ? fetchedResultsController : searchFetchedResultsController;
}

- (void)fetchedResultsController:(NSFetchedResultsController *)theFetchedResultsController configureCell:(IMContactsCell*)cell atIndexPath:(NSIndexPath *)indexPath
{
    // your cell guts here
    
}

#pragma mark - UITableView Delegate/DataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!isUpdatingContacts) {
        if([fetchedResultsController.fetchedObjects count] == 0 ) {
            if (isAddressbookAccessible) {
                [self noContactFound];
            }
            else
            {
                [self userDidClickOnDontAllow];
            }
        }
        else
        {
            [self resetContactView];
        }
    }
    
    return [fetchedResultsController.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [[fetchedResultsController.sections objectAtIndex: section] numberOfObjects];
  }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier = @"identifier";
    
    IMContactsCell* cell = (IMContactsCell*)[tableView dequeueReusableCellWithIdentifier: identifier];
    
    if(cell == nil)
    {
        cell = [IMContactsCell cellLoadedFromNibFile];
    }
    cell.delegate = self;
    cell.tag = indexPath.row;
    cell.infoButton.hidden = YES;
    cell.statusButton.hidden = YES;
    cell.userImageView.hidden = YES;
    cell.userImageViewHolder.hidden = YES;
    cell.userImageViewHolder.image = nil;
    
    UILabel* nameLabel = (UILabel*)[cell.contentView viewWithTag: 100];
    
    IMContacts* contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: indexPath.section] objects] objectAtIndex: indexPath.row];

    cell.imgrLabel.hidden = YES;
    nameLabel.hidden = NO;
    if(isIMGRViewSelected)
    {
        nameLabel.hidden = YES;
        cell.imgrLabel.hidden = NO;
        
        NSString* fName = [contactInfo.first_name capitalizedString];
        NSString* lName = [contactInfo.last_name capitalizedString];
        
        cell.imgrLabel.text = @"No Name";
        if([fName length] > 0 && [lName length] > 0)
            cell.imgrLabel.text = [NSString stringWithFormat: @"%@ %@", fName, lName];
        else if([fName length] > 0)
            cell.imgrLabel.text = fName;
        else if([lName length] > 0)
            cell.imgrLabel.text = lName;
        
        
        UIImage* image = [[CMChatManager sharedInstance] fetchUserPhotoFrom:HOST_OPENFIRE forUser:contactInfo.user_id];
        
        if(image == nil)
            cell.userImageView.image  = [UIImage imageNamed: @"img_contact_placeholder@2x.png"];
        else
            cell.userImageView.image = [IMUtils circularScaleNCrop: image rect: CGRectMake(0, 0, 34, 34)];

        
        cell.infoButton.hidden = NO;
        
        cell.userImageViewHolder.hidden = NO;
        cell.userImageView.hidden = NO;
    }
    else
    {
        NSString* fName = [contactInfo.first_name capitalizedString];
        NSString* lName = [contactInfo.last_name capitalizedString];
        
        nameLabel.text = @"No Name";
        if([fName length] > 0 && [lName length] > 0)
            nameLabel.text = [NSString stringWithFormat: @"%@ %@", fName, lName];
        else if([fName length] > 0)
            nameLabel.text = fName;
        else if([lName length] > 0)
            nameLabel.text = lName;
        
        nameLabel.frame = CGRectMake(10, cell.nameLabel.frame.origin.y, cell.nameLabel.frame.size.width, cell.nameLabel.frame.size.height);
        
        cell.statusButton.hidden = NO;
        
        if([contactInfo.is_imgr_user boolValue]) {
            [cell.statusButton setImage: [UIImage imageNamed: @"logo_imgr_contact.png"] forState: UIControlStateNormal];
            [cell.statusButton setTitle: nil forState: UIControlStateNormal];
            cell.statusButton.userInteractionEnabled = NO;
        }
        else {
            [cell.statusButton setImage: nil forState: UIControlStateNormal];
            
            if(![contactInfo.is_invited boolValue]) {
                [cell.statusButton setTitleColor: [UIColor colorFromRGBIntegers:24 green:162 blue:37 alpha:1.0] forState: UIControlStateNormal];
                [cell.statusButton setTitle: @"INVITE" forState: UIControlStateNormal];
                cell.statusButton.titleLabel.font = [IMUtils appFontWithSize:14.0];

                cell.statusButton.userInteractionEnabled = YES;
            }
            else {
                [cell.statusButton setTitleColor: [UIColor colorFromRGBIntegers:167 green:167 blue:167 alpha:1.0] forState: UIControlStateNormal];
                [cell.statusButton setTitle: @"Invited" forState: UIControlStateNormal];
                cell.statusButton.titleLabel.font = [UIFont italicSystemFontOfSize: 14.0];
                cell.statusButton.userInteractionEnabled = NO;
            }
        }
        
    }
    NSString *tag=[NSString stringWithFormat:@"%d00%ld",indexPath.section,(long)indexPath.row];
    cell.infoButton.tag=[tag intValue];
    return cell;

}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return [[fetchedResultsController.sections objectAtIndex: section] indexTitle];
//}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    IMPromoHeaderView *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[IMPromoHeaderView cellReusableIdentifier]];
    if (nil == cell) {
        NSLog(@"[IMPromoHeaderView cellLoadedFromNibFile];");
        cell = [IMPromoHeaderView cellLoadedFromNibFile];
    }
    //    cell.headerLabel.text = [NSString stringWithFormat:@"Header %d",section];
    cell.headerLabel.text = [[fetchedResultsController.sections objectAtIndex: section] indexTitle];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    NSInteger nRowCount = [[sectionList objectAtIndex:section] integerValue];
//    if ( nRowCount == 0 ) {
//        return 0.0f;
//    }
    
    return 22.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // NSIndexPath *selectedIndexPath = [tableView indexPathForSelectedRow];
    NSLog(@"section=%ld row=%ld",(long)indexPath.section ,(long)indexPath.row);
    selectedPromoIndex = indexPath;
    IMContacts* contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: indexPath.section] objects] objectAtIndex: indexPath.row];
    
    if(isIMGRViewSelected)
    {
        //start chat with selected user
        UIStoryboard *contactsStoryBoard = [[IMAppDelegate sharedDelegate] messagesStoryboard];
        __block IMChatViewController *chatController = [contactsStoryBoard instantiateViewControllerWithIdentifier:@"IMChatViewController"];
        chatController.userJid = contactInfo.user_id;
        [self.navigationController pushViewController:chatController animated:YES];
    }
    else
    {
        if([contactInfo.is_imgr_user boolValue])
        {
//            UIStoryboard *contactsStoryBoard = [[IMAppDelegate sharedDelegate] messagesStoryboard];
//            __block IMChatViewController *chatController = [contactsStoryBoard instantiateViewControllerWithIdentifier:@"IMChatViewController"];
//            chatController.userJid = contactInfo.phone_number;
//            [self.navigationController pushViewController:chatController animated:YES];
            [self performSegueWithIdentifier:@"IMContactDetailViewController" sender:self];
        }
        else
            [self performSegueWithIdentifier:@"IMNonIMGRContactDetailViewController" sender:self]; //uncomment if IMGR user
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.destinationViewController isKindOfClass: [IMAddNewContactViewController class]])
    {
        
    }
    else if ([segue.destinationViewController isKindOfClass: [IMContactDetailViewController class]])
    {
        IMContactDetailViewController* controller = segue.destinationViewController;
        IMContacts* contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: selectedPromoIndex.section] objects] objectAtIndex: selectedPromoIndex.row];
        controller.contact = contactInfo;
        [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"isEdited"];
        //isIMGRViewSelected = YES;
    }
    else if ([segue.destinationViewController isKindOfClass: [IMSelectNonImgrUserViewController class]])
    {
        //IMSelectNonImgrUserViewController* controller = segue.destinationViewController;
    }
    else
    {
        IMNonIMGRContactDetailViewController* controller = segue.destinationViewController;
        
        IMContacts* contactInfo = nil;
        if(!newPhoneNumber)
            contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: selectedPromoIndex.section] objects] objectAtIndex: selectedPromoIndex.row];
        else {
            contactInfo = [[IMDataHandler sharedInstance] fetchContactWithPhoneNumber: [IMUtils phonenumberFromFormattedNumber: newPhoneNumber]];
            newPhoneNumber = nil;
//            //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"ADD_NEW_CONTACT" object: nil];
        }
        controller.contact = contactInfo;
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSMutableArray* sectionList = [[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles] mutableCopy];
    id lastObject = [sectionList lastObject];
    [sectionList removeLastObject];
    [sectionList insertObject:lastObject atIndex: 0];
    return sectionList;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (title == UITableViewIndexSearch)
    {
        CGRect searchBarFrame = self.searchDisplayController.searchBar.frame;
        [tableView scrollRectToVisible:searchBarFrame animated:YES];
        
        return -1;
    }
    else {
        
        int count = 0;
        
        for(count=0; count < self.fetchedResultsController.sections.count; count++)
        {
            id <NSFetchedResultsSectionInfo> sectionInfo =
            [[self.fetchedResultsController sections] objectAtIndex:count];
           NSString *sectionHeaderNames = [sectionInfo name];
            NSLog(@"Input title = %@,index = %d,section: %@,count: %i ",title,index, sectionHeaderNames,count);
            if([sectionHeaderNames isEqualToString: @""])
                return -1;
            if([title isEqualToString:sectionHeaderNames] || [[title lowercaseString] isEqualToString:sectionHeaderNames])
            {
                NSLog(@"return %d",count);

                return count;
                break;
            }
            else
            {
                if (title.length && sectionHeaderNames.length) {
                    unichar first = [title characterAtIndex:0];
                    if ((first >= 'a' && first <= 'z') || (first >='A' && first <= 'Z')) {
                        unichar fectchedChar = [[sectionHeaderNames uppercaseString] characterAtIndex:0];
                        if (first < fectchedChar) {
                            
                            NSLog(@"return %d",count);
                            
                            return count;
                            break;
                        }
                    }
                }

           }
        }
        NSLog(@"return %d",count);
        return count;
//        UILocalizedIndexedCollation *currentCollation = [UILocalizedIndexedCollation currentCollation];
//        NSLog(@"selected charecter=%ld",(long)[currentCollation sectionForSectionIndexTitleAtIndex:index]);
//        return [currentCollation sectionForSectionIndexTitleAtIndex:index];
    }
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    fetchedResultsController.delegate = nil;
    fetchedResultsController = nil;
    
    //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
   // NSArray* searchResults = [fetchedResultsController.fetchedObjects filteredArrayUsingPredicate:resultPredicate];
    if([searchText isEqualToString: @""])
        fetchedResultsController = [self fetchContactsResultController:isIMGRViewSelected searchText: nil];
    else
        fetchedResultsController = [self fetchContactsResultController:isIMGRViewSelected searchText: searchText];
    [contactsTableView reloadData];
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willUnloadSearchResultsTableView:(UITableView *)tableView;
{
    // search is done so get rid of the search FRC and reclaim memory
    searchFetchedResultsController.delegate = nil;
    searchFetchedResultsController = nil;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

//- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
//{
//    [self filterContentForSearchText:[controller.searchBar text]
//                               scope:[controller.searchBar selectedScopeButtonIndex]];
//    
//    // Return YES to cause the search result table view to be reloaded.
//    return YES;
//}

//- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
//{
//    UITableView *tableView = controller == self.fetchedResultsController ? self.tableView : self.searchDisplayController.searchResultsTableView;
//    [tableView beginUpdates];
//}

#pragma mark - UISearchBar Delegate methods
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}

BOOL isShow = NO;
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar;          // return NO to not become first responder
{
    if (isShow) {
        return YES;
    }
    isSearching = YES;
    segmentView.hidden = YES;

    [UIView animateWithDuration:0.3 animations:^{
        topSpaceTableView.constant -=42;
        topSpaceSegmentView.constant -= 42;
        
        [self.view needsUpdateConstraints];
        
    } completion:^(BOOL finished) {
        if (finished) {
            isShow = YES;
            [searchBar becomeFirstResponder];
        }
    }];

    return NO;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar;             // called when text ends editing
{
//
//    [UIView animateWithDuration:0.1 animations:^{
//        
//        //[contactsTableView setContentOffset:CGPointMake(0, 0) animated:YES];
//        
//    }];
//    [UIView animateWithDuration:0.25 animations:^{
//        
//        
//        
//    }];
    //[contactsTableView setContentOffset:CGPointMake(0, 44) animated:YES];

}


-(void)animatetableViewDown{
    
    segmentView.hidden = NO;
    [contactsTableView reloadData];
    [UIView animateWithDuration:0.3 animations:^{
        topSpaceTableView.constant +=42;
        topSpaceSegmentView.constant += 42;
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        if (finished) {
            
//            scopeBarBg.hidden = NO;
//            imgrButton.hidden = NO;
//            allButton.hidden = NO;
            isSearching = NO;
        }
    }];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    fetchedResultsController.delegate = nil;
    fetchedResultsController = nil;
    [self.contactSearchBar resignFirstResponder];
    
    fetchedResultsController = [self fetchContactsResultController:isIMGRViewSelected searchText: searchBar.text];
    [contactsTableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    isShow = NO;
//    scopeBarBg.hidden = NO;
//    imgrButton.hidden = NO;
//    allButton.hidden = NO;
//    isSearching = NO;
//    topSpaceTableView.constant +=42;
//    [self.view needsUpdateConstraints];
    fetchedResultsController.delegate = nil;
    fetchedResultsController = nil;
    
//    [contactsTableView setContentOffset:CGPointMake(0, 44) animated:YES];
    [self.contactSearchBar resignFirstResponder];
    
    fetchedResultsController = [self fetchContactsResultController:isIMGRViewSelected searchText: nil];
    
    [self performSelector:@selector(animatetableViewDown) withObject:nil afterDelay:0.3];
//    [contactsTableView reloadData];
}

#pragma mark - Scrollview delegate methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    DebugLog(@"Content Offset = %f", scrollView.contentOffset.y);
}
                                           
#pragma mark - IBActions methods
- (IBAction)onSlideButtonClick:(id)sender
{
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}

- (IBAction)onPlusButtonClick:(id)sender
{
    if(!isAddressbookAccessible)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Access Denied" message:@"Please give access to AddressBook via settings." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    [self performSegueWithIdentifier:@"IMAddNewContactViewController" sender:self];
}

- (void)adjustHeightOfTableview
{
    CGFloat tableHeight = contactsTableView.contentSize.height;
    CGFloat maxHeight = contactsTableView.superview.frame.size.height - contactsTableView.frame.origin.y;
    
    // if the height of the content is greater than the maxHeight of
    // total space on the screen, limit the height to the size of the
    // superview.
    
    if (tableHeight > maxHeight)
        tableHeight = maxHeight;
    
    if(isIMGRViewSelected)
        tableHeight -= 50;
    // now set the height constraint accordingly
    int height = [[UIScreen mainScreen] bounds].size.height;
    if(IS_IPHONE5)
        height = height - 114;
    else
        height = height - 50 - 20 - 44;
    
    if(tableHeight < height)
        tableHeight = height - 44;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.tableViewHeightConstraint.constant = tableHeight;
        [self.view needsUpdateConstraints];
    }];
}

- (void)adjustPositionOfButton: (int)xPos
{
    [UIView animateWithDuration:0.0 animations:^{
        
        inviteBtnYConstraint.constant = xPos;
        [self.view needsUpdateConstraints];
    }];
}

- (void)displayNoIMGRContactView
{
    emptyLogoImageView.hidden   = NO;
    emptyLogoImageView.image = [UIImage imageNamed: @"img_nofriend.png"];
    
    accessContactsLabel.hidden  = YES;
    allowAccessLabel.hidden     = YES;
    
    noFriendLabel.hidden    = NO;
    inviteLabel.hidden      = NO;
    inviteButton.hidden     = NO;
    inviteLabel.text = NSLocalizedString(@"INVITE FRIEND", "@");
    
    contactsTableView.hidden    = YES;
    int height = [[UIScreen mainScreen] bounds].size.height;
    if(IS_IPHONE5)
        height = height - (height/3) - 90;
    else
        height = height - (height/3) - 30;
    [self adjustPositionOfButton: height];
}

- (IBAction)onIMGRButtonClick:(id)sender
{
    UIButton* button = (UIButton*)sender;
    if(button.selected) {
        return;
    }
    
    fetchedResultsController.delegate = nil;
    fetchedResultsController = nil;
    
    fetchedResultsController = [self fetchContactsResultController: YES searchText: nil];
    
    button.selected = YES;
    imgrButton.selected = YES;
    allButton.selected = NO;
    
    imgrButton.titleLabel.font = [UIFont boldSystemFontOfSize: 17.0];
    allButton.titleLabel.font = [UIFont systemFontOfSize: 17.0];
    
    isIMGRViewSelected = YES;
    
    [self adjustHeightOfTableview];

    [contactsTableView reloadData];
}

- (IBAction)onAllButtonClick:(id)sender
{
    UIButton* button = (UIButton*)sender;
    if(button.selected) {
        return;
    }
    
    button.selected = YES;
    allButton.selected = YES;
    imgrButton.selected = NO;
    
    allButton.titleLabel.font = [UIFont boldSystemFontOfSize: 17.0];
    imgrButton.titleLabel.font = [UIFont systemFontOfSize: 17.0];
    
    isIMGRViewSelected = NO;
    
    [self adjustHeightOfTableview];
    
    fetchedResultsController.delegate = nil;
    fetchedResultsController = nil;
    fetchedResultsController = [self fetchContactsResultController: NO searchText: nil];
    [contactsTableView reloadData];
}

- (IBAction)onInviteFriendsButtonClick:(id)sender
{
    [self performSegueWithIdentifier:@"IMSelectNonImgrUserViewController" sender:self];
}

- (void)inviteViaEmail: (IMContacts*)contactInfo
{
    MFMailComposeViewController* mailCompose = [[MFMailComposeViewController alloc] init];
    if([MFMailComposeViewController canSendMail])
    {
        mailCompose.mailComposeDelegate = self;
        [mailCompose setSubject:NSLocalizedString(@"NEW MESSAGE", @"")];
        
        NSString* bodyStr = NSLocalizedString(@"MESSAGE BODY", @"");
        //bodyStr = [bodyStr stringByAppendingString: [NSString stringWithFormat: @"\n\nGet it now from\n%@", @"applinkfromweb.com"]];
        
        [mailCompose setToRecipients: [NSArray arrayWithObject: contactInfo.email]];
        [mailCompose setMessageBody: bodyStr isHTML: NO];
        
        isEmailViewOpened = YES;
         [self.navigationController presentViewController:mailCompose animated:YES completion: ^{}];
    }
}

- (void)inviteViaMessage: (IMContacts*)contactInfo
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    
    if([MFMessageComposeViewController canSendText]) {
        
        NSString* bodyOfMessage = NSLocalizedString(@"MESSAGE BODY", @"");// @"Hey, Check out IMGR application for iPhone, it can do this and that and many things.\n Download it from \n applinkfromwebsite.com";
        controller.body = bodyOfMessage;
        
        controller.recipients = [NSArray arrayWithObject: contactInfo.phone_number];
        controller.messageComposeDelegate = self;
        isEmailViewOpened = YES;
        [self presentViewController:controller animated: YES completion: ^{}];
    }
}

#pragma mark UIActionSheet Delegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    IMContacts* contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: selectedPromoIndex.section] objects] objectAtIndex: selectedPromoIndex.row];
    
    switch (buttonIndex)
    {
        case 0:
            [self inviteViaEmail: contactInfo];
            break;
            
        case 1:
            [self inviteViaMessage: contactInfo];
            break;
            
        case 2:
            
            break;
            
        default:
            break;
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            isEmailViewOpened = NO;
            break;
            
        case MFMailComposeResultSaved:
            break;
            
        case MFMailComposeResultSent:
        {
            IMContactsCell* cell = (IMContactsCell*)[contactsTableView cellForRowAtIndexPath: selectedPromoIndex];
            [cell.statusButton setTitle: @"Invited" forState: UIControlStateNormal];
            [cell.statusButton setTitleColor: [UIColor colorFromRGBIntegers:167 green:167 blue:167 alpha:1.0] forState: UIControlStateNormal];
            cell.statusButton.titleLabel.font = [UIFont italicSystemFontOfSize: 14.0];
            IMContacts* contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: selectedPromoIndex.section] objects] objectAtIndex: selectedPromoIndex.row];
            
            [contactInfo setValue: [NSNumber numberWithBool: YES] forKey: @"is_invited"];
            [[IMDataHandler sharedInstance] saveDatabase];
            
            isEmailViewOpened = NO;
        }break;
            
        case MFMailComposeResultFailed:
            break;
            
        default:
            break;
    }
    [controller dismissViewControllerAnimated: YES completion:^{}];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
            isEmailViewOpened = NO;
            break;
            
        case MessageComposeResultSent:
        {
            IMContactsCell* cell = (IMContactsCell*)[contactsTableView cellForRowAtIndexPath: selectedPromoIndex];
            [cell.statusButton setTitle: @"Invited" forState: UIControlStateNormal];
            [cell.statusButton setTitleColor: [UIColor colorFromRGBIntegers:167 green:167 blue:167 alpha:1.0] forState: UIControlStateNormal];
            cell.statusButton.titleLabel.font = [UIFont italicSystemFontOfSize: 14.0];
            IMContacts* contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: selectedPromoIndex.section] objects] objectAtIndex: selectedPromoIndex.row];
            
            [contactInfo setValue: [NSNumber numberWithBool: YES] forKey: @"is_invited"];
            [[IMDataHandler sharedInstance] saveDatabase];
            
            isEmailViewOpened = NO;
        }break;
            
        case MessageComposeResultFailed:
            break;
            
        default:
            break;
    }
    [controller dismissViewControllerAnimated: YES completion:^{}];
}

- (IBAction)onStatusButtonClick:(id)sender
{
    //send invitation via native email
    UIButton* button = (UIButton*)sender;
    
    if([button.titleLabel.text isEqualToString: @"INVITE"])
    {
        
        IMContactsCell* cell = nil;
//        if([IMUtils getOSVersion] >= 7.0)
//            cell = (IMContactsCell*)[[[button superview] superview] superview];
//        else
            cell = (IMContactsCell*)[[button superview] superview];
                    
        
        
        selectedPromoIndex = [contactsTableView indexPathForCell: cell];
        
        IMContacts* contactInfo = (IMContacts*)[[[fetchedResultsController.sections objectAtIndex: selectedPromoIndex.section] objects] objectAtIndex: selectedPromoIndex.row];
        
        if(!contactInfo.email.length)
            [self inviteViaMessage: contactInfo];
        else
            [IMUtils displayActionSheet: self];
        
//        [contactInfo setValue: [NSNumber numberWithBool: YES] forKey: @"is_invited"];
//        [[IMDataHandler sharedInstance] saveDatabase];
//        
//        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Invitation sent successfully" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [alertView show];
    }
}

- (IBAction)onInfoButtonClick:(id)sender
{
    UIButton* button = (UIButton*)sender;
    NSLog(@"btntag=%d",button.tag);
    IMContactsCell* cell = nil;//(IMContactsCell*)[[[button superview] superview] superview];
//    if([IMUtils getOSVersion] >= 7.0)
//        cell = (IMContactsCell*)[[[button superview] superview] superview];
//    else
        cell = (IMContactsCell*)[[button superview] superview];
    
    if(isSearching)
        selectedPromoIndex = [self.searchDisplayController.searchResultsTableView indexPathForCell: cell];
    else
        selectedPromoIndex = [contactsTableView indexPathForCell: cell];
    
    [self performSegueWithIdentifier:@"IMContactDetailViewController" sender:self];
}


@end
