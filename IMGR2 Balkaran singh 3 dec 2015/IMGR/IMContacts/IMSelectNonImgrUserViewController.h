//
//  IMSelectNonImgrUserViewController.h
//  IMGR
//
//  Created by akram on 24/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import	<MessageUI/MessageUI.h>

@interface IMSelectNonImgrUserViewController : UIViewController <MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
{
    IBOutlet UITableView*       nonImgrContactsTableView;
    
    IBOutlet UISearchBar*       nonImgrContactSearchBar;
}

- (IBAction)onInviteButtonClick:(id)sender;
- (IBAction)onSelectAllButtonClick:(id)sender;

@end
