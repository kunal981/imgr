//
//  CMRoom.h
//  CMChatFramework
//
//  Created by Tarun Khurana on 16/01/11.
//  Copyright (c) 2014 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import "XMPPRoom.h"
#import "CMRoomInfoCoreDataStorageObject.h"

@interface CMRoom : XMPPRoom <XMPPRoomDelegate>

@property (nonatomic, assign) BOOL isVirtualGroupChatEnabled;
@property (nonatomic, retain) NSArray *roomBuddies;
@property (nonatomic, retain) NSString *inviteGroupMessage;

- (id)initWithJID:(NSString *)roomJid;
#pragma mark Room Lifecycle
- (void)joinRoom:(NSString *)aRoomJID name:(NSString *) aRoomName roomIcon:(NSData *)aRoomIcon withnickname: (NSString *) nickname andVirtualGmailGroupId:(NSString *)groupID hostName:(NSString*)hostName;
//- (void)joinRoom:(NSString *)roomJID name:(NSString *) roomName roomIcon:(NSData *)roomIcon withnickname: (NSString *) nickname;

//- (void) savePrivateDataToServer:(NSString *)aRoomJID name:(NSString *) aRoomName roomIcon: (NSData *)aRoomIcon;
- (NSArray *)getAvailableMessagesLimit:(NSUInteger)count;
+ (NSArray *)groupOccupentsForRoom:(NSString *)roomJId errorObserved:(NSError *)error;
- (NSArray *)getAvailableParticipants;
+ (CMRoomInfoCoreDataStorageObject *)roomInfoForRoomID:(NSString *)roomJid;
+ (NSString *)getUserJIDForRoomParticipantJID:(NSString *)participantJid;
+ (void)deleteRoomInfoForRoom:(NSString *)roomJID;

+ (void)deleteAllMessagesWithJid:(NSString*)jid;
+ (void)deleteArchivedMessagesWithMessages:(NSArray*)messages;

@end
