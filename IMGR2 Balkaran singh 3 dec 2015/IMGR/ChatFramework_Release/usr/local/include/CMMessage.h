//
//  CMMessage.h
//  CMChatFramework
//
//  Created by Tarun Khurana on 16/01/11.
//  Copyright (c) 2014 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import "XMPPMessage.h"

@class CMStream;
@interface CMMessage : XMPPMessage

-(CMMessage *)initWithStream:(CMStream *)stream;
@end
