//
//  Operation.m
//  Test
//
//  Created by Justin Paston-Cooper on 15/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Operation.h"

@implementation Operation
@synthesize startTime;

- (Operation *) initWithStartTime:(NSInteger)t duration:(NSInteger)d {
    self = [super init];
    if (self) {
        startTime = t;
        duration = d;
    }
    
    return self;
}

- (NSInteger) duration {
    return duration;
}

- (NSInteger) completionTime {
    return startTime + duration;
}

- (NSString *) description {
    return [NSString stringWithFormat:@"StartTime: %d, Duration: %d", startTime, duration];
}

- (BOOL) before:(Operation *)op {
    return self.completionTime <= op.startTime;
}
@end
