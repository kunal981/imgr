//
//  DFCoreDataHandler.h
//  Laser Liaison
//
//  Created by Akram on 4/4/13.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef void (^BackgroundTasks)();

@interface CMCoreDataHandler : NSObject
{
    
}

@property (nonatomic, retain) NSFetchedResultsController* fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, retain) NSPersistentStoreCoordinator* persistentStoreCoordinator;
@property (nonatomic, retain) NSManagedObjectModel* managedObjectModel;
@property (nonatomic, retain,readonly) NSManagedObjectContext* backgroundContext;
+ (CMCoreDataHandler *)sharedInstance;
- (void) performAndSaveDataBaseTaskInBackground:(BackgroundTasks) lengthyTasks;
- (void) performAndSaveDataBaseTaskInBackground:(BackgroundTasks) lengthyTasks completionHandler:(BackgroundTasks) performOnSaveData;
@end
