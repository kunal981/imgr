//
//  IMDataHandler.h
//  IMGR
//
//  Created by akram on 17/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "IMContacts.h"
#import "IMPromos.h"

@class IMContacts;
@class IMPromos;


@interface IMDataHandler : NSObject
{
}

@property (assign, nonatomic) NSManagedObjectContext *managedObjectContext;

+ (IMDataHandler*)sharedInstance;

- (IMContacts*)parseContacts: (NSDictionary*)contactInfo;
- (void)parsePromos: (NSDictionary*)promoInfo promoType: (int)promoType;

- (IMPromos*)fetchPromoWithId: (int)promoId;
- (IMContacts*)fetchContactWithPhoneNumber: (NSString*)phoneNumber;
- (IMContacts*)fetchContactInBackgroundWithPhoneNumber: (NSString*)phoneNumber;
- (NSArray*)fetchIMGRContacts;
- (NSArray*)fetchAllContacts;
- (NSArray *)fetchPersonalPromos;
- (NSArray *)fetchThirdPartyPersonalPromos;

- (BOOL)deletePromo: (IMPromos*)promo;
- (BOOL)deleteContact: (IMContacts*)contact;

- (BOOL)saveDatabase;

- (IMContacts*)fetchContactWithUserJid: (NSString*)userJid;

+ (IMPromos *) fetchPromoForPromoID:(NSString *)promo_ID;
- (IMPromos*)getPromoInfoStoredInBackground: (NSString*)promo_id;

- (NSArray*)disabledPromos: (int)promoType;

- (void)deleteDeadContacts;

@end
