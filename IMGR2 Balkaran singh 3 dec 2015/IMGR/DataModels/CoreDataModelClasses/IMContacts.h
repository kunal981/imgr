//
//  IMContacts.h
//  IMGR
//
//  Created by akram on 28/04/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface IMContacts : NSManagedObject

@property (nonatomic, retain) NSNumber * auto_rotation_enabled;
@property (nonatomic, retain) NSString * contact_id;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * first_name;
@property (nonatomic, retain) NSString * index_character;
@property (nonatomic, retain) NSNumber * is_blocked;
@property (nonatomic, retain) NSNumber * is_imgr_user;
@property (nonatomic, retain) NSNumber * is_invited;
@property (nonatomic, retain) NSNumber * isAvailabledInAddressBook;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) NSString * member_id;
@property (nonatomic, retain) NSNumber * personal_promo_enabled;
@property (nonatomic, retain) NSString * phone_number;
@property (nonatomic, retain) NSString * phone_number_2;
@property (nonatomic, retain) NSNumber * promo_enabled;
@property (nonatomic, retain) NSNumber * sponsored_ads_enabled;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSNumber * last_promo_id;

@end
