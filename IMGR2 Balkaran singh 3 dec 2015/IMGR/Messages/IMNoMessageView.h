//
//  IMNoMessageView.h
//  IMGR
//
//  Created by Satendra Singh on 2/11/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMNoMessageView : UIView

+ (IMNoMessageView *)viewLoadedFromNibFile;
+ (NSUInteger) noMessageTag;

@end
