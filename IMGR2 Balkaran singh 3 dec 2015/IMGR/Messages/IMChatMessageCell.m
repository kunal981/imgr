//
//  CMChatMessageCell.m
//  DemochatApp
//
//  Created by Raiduk on 10/01/14.
//  Copyright (c) 2014 CopperMobile. All rights reserved.
//

#import "IMChatMessageCell.h"

@implementation IMChatMessageCell

@synthesize chatImageBubble;
@synthesize companyNamelbl;
@synthesize offerlbl;

@synthesize userImageView,topLbl,bottomLbl,lastSelectedIndexPath;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
       
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)userDidTappedOnMessageFooter:(UITapGestureRecognizer *)sender
{
    NSLog(@"promo name = %@, link = %@",_cellPromo.promo_name,_cellPromo.promo_link);
        
    NSString *urlStr = _cellPromo.promo_link;
    if (urlStr.length) {
        if (0 == _cellPromo.promo_type.intValue && self.chatTableViewController.userInfo.member_id.length)
        {
            
            if ([urlStr  isEqual: @"http://microlinq.com/"])
            {
                
                urlStr = [urlStr stringByAppendingString:@"?u1="];
                urlStr = [urlStr stringByAppendingString:self.chatTableViewController.userInfo.member_id];
                
                NSLog(@"open link = %@",urlStr);
                
            }
            
            else
            {
                 urlStr = [urlStr stringByAppendingString:self.chatTableViewController.userInfo.member_id];
                
            }
            
            
           
            NSLog(@"memberID=%@",self.chatTableViewController.userInfo.member_id);
        }
        if (NSNotFound == [urlStr rangeOfString:@"http"].location)
        {
            urlStr = [@"http://" stringByAppendingString:urlStr];
        }
        
        //urlStr = [urlStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"open link = %@",urlStr);
        //urlStr = [urlStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    }
}

- (IBAction)userDidTappedOnResend:(UIButton *)sender {
    [[CMChatManager sharedInstance] resendMessageWithHost:HOST_OPENFIRE
                                            failedMessage:self.message];
    [[CMChatManager sharedInstance] deleteMessagesWithHost:HOST_OPENFIRE messages:@[self.message]];
}

@end
