//
//  IMSelectSponsoredAdsController.h
//  IMGR
//
//  Created by Satendra Singh on 2/13/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMSponsoredAdsViewController.h"
#import "IMPromos.h"

typedef void (^SelectionBlock)(IMPromos *selectedPromo);

@interface IMSelectSponsoredAdsController : IMSponsoredAdsViewController

@property (copy)  SelectionBlock selectionBlockObject;

@end
