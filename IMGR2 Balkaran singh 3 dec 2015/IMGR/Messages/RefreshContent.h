//
//  RefreshContent.h
//  IMGR
//
//  Created by brst on 17/08/15.
//  Copyright (c) 2015 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RefreshContent : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *pullDownImageView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *refreshIndicator;

@end
