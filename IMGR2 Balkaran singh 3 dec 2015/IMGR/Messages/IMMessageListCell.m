//
//  IMMessageListCell.m
//  IMGR
//
//  Created by Satendra Singh on 2/6/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMMessageListCell.h"

@implementation IMMessageListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:[[self class] cellReusableIdentifier]];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (IMMessageListCell *)cellLoadedFromNibFile
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    for (NSObject *obj in objects)
    {
        if ([obj isKindOfClass:[self class]])
        {
            return (IMMessageListCell *)obj;
            break;
        }
    }
    return nil;
}

+ (NSString *) cellReusableIdentifier
{
    return @"IMMessageListCell";
}

@end
