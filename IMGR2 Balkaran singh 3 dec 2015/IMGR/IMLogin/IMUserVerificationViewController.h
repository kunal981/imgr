//
//  IMUserVerificationViewController.h
//  IMGR
//
//  Created by akram on 04/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 This class is used to verify the user's phone number. Here you can edit the number and obtain the code again if 
 server doesn't return the code for previous request.
 */
@interface IMUserVerificationViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UILabel*       supportLabel;
    IBOutlet UILabel*       imgrLabel;
    
    IBOutlet UIScrollView*  verificationScrollView;
    
    IBOutlet UITextField*   pinCodeField;
    IBOutlet UITextField*   countryCodeField;
    IBOutlet UITextField*   phoneNumberField;
    
    IBOutlet NSLayoutConstraint*    topConstraint;
    IBOutlet NSLayoutConstraint*    xPosConstraint;
    
    IBOutlet UIButton*              editButton;
}

@property (nonatomic, strong)NSLayoutConstraint*    topConstraint;
@property (nonatomic, strong)NSString* phoneNumber;
@property (nonatomic, strong)NSString* countryCode;

//Shows registeration successfully popup if user's phone number verified successfully.
- (IBAction)showRegistrationPopup:(id)sender;

//Here you can change phone number if you entered wrong phone number in previous screen.
- (IBAction)onEditButtonClick:(id)sender;

//This method is used to obtain new verification code if server doesn't return the code for previous request.
- (IBAction)onSendAgainButtonClick:(id)sender;

@end
