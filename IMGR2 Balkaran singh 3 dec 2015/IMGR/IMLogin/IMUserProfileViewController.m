//
//  IMUserProfileViewController.m
//  IMGR
//
//  Created by akram on 04/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMUserProfileViewController.h"
#import "CMPhotoPickerController.h"
#import "IMAppDelegate.h"
#import "IMMenuViewController.h"
#import "IMUtils.h"

@interface IMUserProfileViewController ()
{
    BOOL isImageSelected;
}
@end

@implementation IMUserProfileViewController
@synthesize topConstraint;
@synthesize phoneNumber;
@synthesize countryCode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if ([self respondsToSelector:@selector(topLayoutGuide)]) {
        [self.view removeConstraint:self.topConstraint];
        
        self.topConstraint =
        [NSLayoutConstraint constraintWithItem:userProfileScrollView
                                     attribute:NSLayoutAttributeTop
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self.topLayoutGuide
                                     attribute:NSLayoutAttributeBottom
                                    multiplier:1
                                      constant:0];
        
        [self.view addConstraint:self.topConstraint];
        
        [self.view setNeedsUpdateConstraints];
        [self.view layoutIfNeeded];
    }
    firstNameField.autocapitalizationType   = UITextAutocapitalizationTypeSentences;
    lastNameField.autocapitalizationType    = UITextAutocapitalizationTypeSentences;
}

- (void)viewWillAppear:(BOOL)animated
{
    phoneNumberField.text = [NSString stringWithFormat: @"%@ %@",countryCode, phoneNumber];
}

- (void)viewDidAppear:(BOOL)animated
{
    
}
#pragma mark IBActions
- (IBAction)changePhoto:(id)sender
{
    [firstNameField resignFirstResponder];
    [lastNameField resignFirstResponder];
    [phoneNumberField resignFirstResponder];
    [emailField resignFirstResponder];
    
    UIActionSheet* changePhotoSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:(id)self
                                                         cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles: @"Choose From Existing", @"Take Photo", nil];
    
    [changePhotoSheet showFromRect:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 0) inView:self.view animated:YES];
    
    
}

-(UIImage *)changeWhiteColorTransparent: (UIImage *)image
{
    //convert to uncompressed jpg to remove any alpha channels
    //this is a necessary first step when processing images that already have transparency
    image = [UIImage imageWithData:UIImageJPEGRepresentation(image, 1.0)];
    CGImageRef rawImageRef=image.CGImage;
    //RGB color range to mask (make transparent)  R-Low, R-High, G-Low, G-High, B-Low, B-High
    CGFloat colorMasking[6] = {222, 255, 222, 255, 222, 255};
    
    UIGraphicsBeginImageContext(image.size);
    CGImageRef maskedImageRef=CGImageCreateWithMaskingColors(rawImageRef, colorMasking);
    
    //iPhone translation
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, image.size.height);
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, image.size.width, image.size.height), maskedImageRef);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(maskedImageRef);
    UIGraphicsEndImageContext();
    return result;
}

- (IBAction)showProfileCompletionPopup:(id)sender
{
    NSString *firstName = firstNameField.text;
    NSString *lastName = lastNameField.text;
    NSString *emailID = emailField.text;
    NSString *phoneNo = phoneNumberField.text;
    UIImage *image = isImageSelected?userPhotoButton.imageView.image: [UIImage imageNamed:@"img_reg_placeholder.png"];
    NSMutableDictionary *myVcard = [[NSMutableDictionary alloc] init];
    
    if (firstName == nil) {
        firstName = @"";
    }
    if (lastName == nil) {
        lastName = @"";
    }
    if (emailID == nil) {
        emailID = @"";
    }
    if (phoneNo == nil) {
        phoneNo = @"";
    }
    if (isImageSelected) {
        NSData* imageData = UIImageJPEGRepresentation(image, .5);//UIImagePNGRepresentation(image);
        if([imageData length] > (8*1024)) {
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"Please choose a low resolution image", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            return;
        }
        
        [myVcard setObject:imageData   forKey:@"User_Image"];
    }
    [myVcard setObject:firstName forKey:@"First_Name"];
    [myVcard setObject:lastName forKey:@"Last_Name"];
    [myVcard setObject:emailID forKey:@"Email_Id"];
    [myVcard setObject:phoneNo forKey:@"Phone_Number"];
    [[NSUserDefaults standardUserDefaults] setObject:myVcard forKey:k_DEFAULTS_KEY_MY_VCARD];
    [[NSUserDefaults standardUserDefaults] synchronize];

    if(0 == firstNameField.text.length && 0 == lastNameField.text.length && 0 == emailField.text.length && !isImageSelected)
    {
        UIAlertView* personalInfoAlert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"PROFILE HEADER", @"") message: NSLocalizedString(@"PROFILE COMPLETION", @"") delegate: self cancelButtonTitle: nil otherButtonTitles: @"OK, I will do it later", nil];
        [personalInfoAlert show];
    }
    else
    {
 //        [IMAppDelegate sharedDelegate].myVcard = myVcard;
        
        UIAlertView* personalInfoAlert = [[UIAlertView alloc] initWithTitle: @"" message: @"Profile created successfully" delegate: self cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
        [personalInfoAlert show];
    }
}

#pragma mark UIAlertView Delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString* phoneNo = [IMUtils phonenumberFromFormattedNumber:phoneNumber];
    
    [[NSUserDefaults standardUserDefaults] setObject: phoneNo forKey: k_DEFAULTS_KEY_USER_ID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[IMAppDelegate sharedDelegate] login];
    [[IMAppDelegate sharedDelegate] getPromos];
        
    UIStoryboard* contactSB = [UIStoryboard storyboardWithName: @"IMContacts" bundle: [NSBundle mainBundle]];
    
    UINavigationController* navigationController    = [contactSB instantiateInitialViewController];
    
    //app.window.rootViewController = navigationController;
    

    [[NSNotificationCenter defaultCenter] postNotificationName: @"REGISTRATION_COMPLETED" object: navigationController];
}

#pragma mark UIActionSheet Delegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [firstNameField resignFirstResponder];
            [lastNameField resignFirstResponder];
            [phoneNumberField resignFirstResponder];
            [emailField resignFirstResponder];
            [[CMPhotoPickerController sharedInstance] displayImagePickerWithType:1 target:self];
            [self.navigationController presentViewController: [CMPhotoPickerController sharedInstance] animated: YES completion: nil];
            break;
            
        case 1:
            [firstNameField resignFirstResponder];
            [lastNameField resignFirstResponder];
            [phoneNumberField resignFirstResponder];
            [emailField resignFirstResponder];
            [[CMPhotoPickerController sharedInstance] displayImagePickerWithType:2 target:self];
            [self.navigationController presentViewController: [CMPhotoPickerController sharedInstance] animated: YES completion: nil];
            break;
            
        case 2:
            break;
            
        default:
            break;
    }
}

#pragma mark - UITextField Delegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.3 animations:^{
        
        //userProfileScrollView.contentSize = CGSizeMake(userProfileScrollView.contentSize.width, userProfileScrollView.contentSize.height + 100);
        userProfileScrollView.contentSize = CGSizeMake(userProfileScrollView.frame.size.width, userProfileScrollView.frame.size.height + 200);
        switch (textField.tag)
        {
            case 302:
                userProfileScrollView.contentOffset = CGPointMake(userProfileScrollView.contentOffset.x, 44);
                textField.keyboardType = UIKeyboardTypeNamePhonePad;
                break;
                
            case 303:
                userProfileScrollView.contentOffset = CGPointMake(userProfileScrollView.contentOffset.x, 88);
                textField.keyboardType = UIKeyboardTypeEmailAddress;
                break;
                
            default:
                break;
        }
        
    } completion:^(BOOL finished) {  }];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    switch (textField.tag)
    {
        case 302:
        {
            NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
            
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                                   options:NSRegularExpressionCaseInsensitive
                                                                                     error:nil];
            NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                                options:0
                                                                  range:NSMakeRange(0, [newString length])];
            if (numberOfMatches == 0 || [newString length] > 15)
                return NO;
            else
                return YES;
        }break;
            
        case 300:
        case 301:
        case 303:
        {
            if([newString length] >= 140)
                return NO;
            else
                return YES;
        }break;
            
        default:
            break;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.3 animations:^{
        
        userProfileScrollView.contentSize = CGSizeMake(userProfileScrollView.frame.size.width, userProfileScrollView.frame.size.height);
        
        userProfileScrollView.contentOffset = CGPointMake(userProfileScrollView.contentOffset.x, 0);
        
    } completion:^(BOOL finished) {  }];
    
    [textField resignFirstResponder];
    
    switch (textField.tag)
    {
        case 300:
            [lastNameField becomeFirstResponder];
            break;
            
        case 301:
            [phoneNumberField becomeFirstResponder];
            break;
            
        case 302:
            [emailField becomeFirstResponder];
            break;
            
        case 303:
            break;
            
        default:
            break;
    }
    return YES;
}

#pragma mark - UIImagePicker Delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    if([mediaType isEqualToString:@"public.image"])
    {
        UIImage* newImage = [info valueForKey: UIImagePickerControllerEditedImage];
        
        UIImage* image = [IMUtils circularScaleNCrop:newImage rect: CGRectMake(0, 0, 64, 64)];
        
        userPhotoButton.imageView.image = nil;
        userPhotoButton.imageView.contentMode = UIViewContentModeScaleToFill;
        [userPhotoButton setImage: image forState: UIControlStateNormal];
        
        isImageSelected = YES;
        //[self performSelector: @selector(changeUserPhoto:) withObject: image afterDelay: .01];
    }
    else if ( [ mediaType isEqualToString:@"public.movie" ])
    {
        
    }
    self.navigationController.navigationBarHidden = YES;
    [picker dismissViewControllerAnimated:YES completion: nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    self.navigationController.navigationBarHidden = YES;
    [picker dismissViewControllerAnimated:YES completion: nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
