//
//  IMLoginViewController.h
//  IMGR
//
//  Created by akram on 04/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMNetManager.h"
#import "HMDiallingCode.h"

/*
 This class is used to register new phone number. Country code auto populated from SIM. You just
 */
@interface IMLoginViewController : UIViewController <UITextFieldDelegate, CMNetManagerDelegate, HMDiallingCodeDelegate>
{
    IBOutlet UILabel*       supportLabel;
    IBOutlet UILabel*       imgrLabel;
    
    IBOutlet UITextField*   countryCodeField;
    IBOutlet UITextField*   phoneNumberField;
    
    IBOutlet UIScrollView*  registerScrollView;
    
    IBOutlet NSLayoutConstraint*    topConstraint;    
}
@property (nonatomic, strong)NSLayoutConstraint*    topConstraint;

//Shows number confirmation popup with Change and Yes button.
//On Change button click, you can change the number.
//On Yes button click, registration process will proceed.
- (IBAction)showConfirmationPopup:(id)sender;


@end
