//
//  IMMyProfileViewController.m
//  IMGR
//
//  Created by Satendra Singh on 2/3/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMMyProfileViewController.h"
#import "MBProgressHUD.h"
#import "RefreshContents.h"

#define k_MYPROFILE_CONTROLLER_TITLE @"My Profile"

@interface IMMyProfileViewController ()
{
    //MBProgressHUD* progressView;
    RefreshContents * refreshContent;
     BOOL isLoadedFirstTime;
    
}
@property (weak, nonatomic) IBOutlet UILabel *displayNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *profilePictureImageview;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIView *profileView;

@end

@implementation IMMyProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(IS_IPHONE5) {
        if([IMUtils getOSVersion] >= 7.0)
            self.profileView.frame = CGRectMake(0, 200, 320, 214);
        else
            self.profileView.frame = CGRectMake(0, 150, 320, 214);
    }
    else {
        if([IMUtils getOSVersion] >= 7.0)
            self.profileView.frame = CGRectMake(0, 160, 320, 214);
        else
            self.profileView.frame = CGRectMake(0, 100, 320, 214);
    }
    
    
    
//    NSUserDefaults *sendReceiveCountSave = [NSUserDefaults standardUserDefaults];
//    NSInteger sendReceiveCount   = [sendReceiveCountSave integerForKey:@"sendReceiveCount"];
//    
//    _MTDPointTxtField.text = [@(sendReceiveCount) stringValue];
//    NSLog(@"%@",_MTDPointTxtField.text);
    	// Do any additional setup after loading the view.
//    progressView = nil;//SS we do not need it
//    progressView = [[MBProgressHUD alloc] initWithView: self.view];
//    progressView.labelText = @"Please wait...";
//    
//    [app.window addSubview: progressView];
}










- (UIImage*) processImage :(UIImage*) image
{
    CGFloat colorMasking[6]={222,255,222,255,222,255};
    CGImageRef imageRef = CGImageCreateWithMaskingColors(image.CGImage,colorMasking);
    UIImage* imageB = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return imageB;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //NSString *myJid = [IMAppDelegate jidForPhoneNumber:[[IMAppDelegate sharedDelegate] getUserID]];
    
    _displayNameLabel.text = nil;
    _phoneNumberLabel.text = [IMUtils myPhoneNumber];
    _emailLabel.text = nil;
    _profilePictureImageview.image = nil;
    _profilePictureImageview.layer.cornerRadius = _profilePictureImageview.frame.size.width/2;
    _profilePictureImageview.layer.masksToBounds = YES;
    _userImageView.layer.cornerRadius = _userImageView.frame.size.width/2;
    _userImageView.layer.masksToBounds = YES;
    
    
    //[progressView show: YES];
    
    NSDictionary* myVcard = [[NSUserDefaults standardUserDefaults] objectForKey:k_DEFAULTS_KEY_MY_VCARD];
    NSString *firstName = [myVcard objectForKey:@"First_Name"];
    NSString *lastName = [myVcard objectForKey:@"Last_Name"];
    
    if (firstName.length && lastName.length) {
        _displayNameLabel.text = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
    }
    else if(firstName.length)
    {
        _displayNameLabel.text = firstName;
    }
    else if(lastName.length)
        _displayNameLabel.text = lastName;
    else
        _displayNameLabel.text = @"No name";
    
    NSString *email = [myVcard objectForKey:@"Email_Id"];
    
    if(email.length)
    {
        _emailLabel.text = email;
    }
    else
    {
        _emailLabel.text = @"No email";
    }
    UIImage *image = [UIImage imageWithData: [myVcard objectForKey:@"User_Image"]];
    if (image) {
        NSData* data = UIImagePNGRepresentation(image);
        _profilePictureImageview.image = [UIImage imageWithData: data];
    }
    
//    [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(stopLoader) userInfo:nil repeats: NO];
//    
//    [[CMChatManager sharedInstance] vcardInfoForHost:HOST_OPENFIRE JID:myJid completionBlock:^(NSDictionary *vCard, NSString *jid) {
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//        if (NO ==[myJid isEqualToString:jid]) {
//            return;
//        }
//        NSString *firstName = [vCard objectForKey:@"First_Name"];
//        NSString *lastName = [vCard objectForKey:@"Last_Name"];
//        
//        if (firstName.length && lastName.length) {
//            _displayNameLabel.text = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
//        }
//        else if(firstName.length)
//        {
//            _displayNameLabel.text = firstName;
//        }
//        else if(lastName.length)
//            _displayNameLabel.text = lastName;
//        else
//            _displayNameLabel.text = @"No name";
//        
//
////        _phoneNumberLabel.text = [vCard objectForKey:@"Phone_Number"];
//        NSString *email = [vCard objectForKey:@"Email_Id"];
//        
//        if(email.length)
//        {
//            _emailLabel.text = email;
//        }
//        else
//        {
//            _emailLabel.text = @"No email";
//        }
//        UIImage *image = [vCard objectForKey:@"User_Image"];
//        if (image) {
//            _profilePictureImageview.image = image;
//
//        }
//        NSLog(@"%d",[[vCard objectForKey:@"Bubble_Color"] integerValue]);
//        
//            
//            [progressView hide: YES];
//        });
//        
//    }];
}

//- (void)stopLoader
//{
//    [progressView hide: YES];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pointFaqOpenTheLink:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://imgr.im/point-faqs"]];
}



@end
