//
//  IMAddPersonalPromoController.h
//  IMGR
//
//  Created by Satendra Singh on 2/5/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMNetManager.h"

@class IMPromos;
@interface IMAddPersonalPromoController : UIViewController <CMNetManagerDelegate, UITextFieldDelegate>
{
    IBOutlet UIButton*      promoIconButton;
    
    IBOutlet UIBarButtonItem*   rightBarButton;
}
@property (weak, nonatomic) IBOutlet UITextField *promoNameField;
@property (weak, nonatomic) IBOutlet UITextField *promoMessageField;
@property (weak, nonatomic) IBOutlet UITextField *promoLinkField;
@property (weak, nonatomic) IBOutlet UITextField *promoLinkTextField;

@property BOOL isupdatingPromo;
@property (nonatomic, strong)IMPromos* promo;


- (IBAction)onDeletePromoButtonClick:(id)sender;

@end
