//
//  IMPromoHeaderView.h
//  IMGR
//
//  Created by Satendra Singh on 2/11/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMPromoHeaderView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

+ (IMPromoHeaderView *)cellLoadedFromNibFile;

+ (NSString *) cellReusableIdentifier;

@end
