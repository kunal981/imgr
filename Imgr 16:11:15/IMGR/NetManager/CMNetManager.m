//
//  CMNetManager.m
//  Radus
//
//  Created by Copper Mobile on 5/28/13.
//  Copyright (c) 2013 test. All rights reserved.
//

#import "CMNetManager.h"
#import "Reachability.h"

#define NOTIFY_AND_LEAVE(X) {[self cleanup:X]; return;}
#define DATA(X)	[X dataUsingEncoding:NSUTF8StringEncoding]

#define IMAGE_CONTENT @"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n"
#define BOUNDARY @"------------0x0x0x0x0x0x0x0x"
/***************	Private Methods declaration		***************/

//static NSString* kImageNamePrefix	= @"File";
static NSInteger __LoadingObjectsCount = 0;

#pragma mark - Private Methods
@interface CMNetManager()
{
    NSDictionary* parameters;
    const char* queueIdentifier;
    NSString* url;
    NSMutableData* responseData;
    __block NSURLConnection* connection;
    dispatch_queue_t backgroundQueue;
   // Reachability *reachability;
    QueueType theQueueType;
    HttpMethod httpMethod;
    
    
    NSOutputStream *                _producerStream;
    NSInputStream *                 _consumerStream;
    
    const uint8_t *                 _buffer;
    uint8_t *                       _bufferOnHeap;
    size_t                          _bufferOffset;
    size_t                          _bufferLimit;
}

- (NSString*)encodeDictionary:(NSDictionary*)dictionary;
- (NSMutableURLRequest*)createRequest;
- (void) startA_SynchronousRequestWithSerialQueue;
- (void) startA_SynchronousRequestWithConcurrentQueue;
- (void) startSynchronousRequest;
- (NSMutableURLRequest*) createPostRequest;
- (NSMutableURLRequest*) createGetRequest;

@end

/***************	Base class implementation 		***************/

@implementation CMNetManager
@synthesize delegate, tag, attachmentName;
@synthesize storeObject;
@synthesize totalCount;

#pragma mark - Initialization
-(id)initWithURL: (NSString*)postUrl parameters: (NSDictionary*)postParameters queueIdentifier:(const char*)thequeueIdentifier queueType: (QueueType)queueType httpMethod:(HttpMethod)postMethod{
    
    self = [super init];
	if( nil == self ) {
		return nil;
	}
    theQueueType = queueType;
    parameters = [postParameters retain];
    url = [postUrl retain];
    queueIdentifier = thequeueIdentifier;
    httpMethod = postMethod;
    return self;
}
- (id)init {
	
	//[self enableNotifications];
	self = [super init];
	return self;
}
- (void)dealloc{
    if (responseData) { [responseData release]; responseData = nil;}
    if (connection) { [connection release]; connection = nil;}
    if (parameters) { [parameters release]; parameters = nil;}
    if (url) { [url release]; url = nil;}
   // if (reachability) { [reachability release]; reachability = nil;}
    if (backgroundQueue) { dispatch_release(backgroundQueue);}
    if (self.attachmentName) { [attachmentName release]; attachmentName = nil;}
    if (self.storeObject) { [storeObject release]; storeObject = nil;}
    [super dealloc];
}
-(void)startRequest{
    
    if(![Reachability connectedToNetwork])
    {
        if([delegate respondsToSelector: @selector(netManager:didFailToConnectWithServer:)])
            [delegate netManager:self didFailToConnectWithServer:nil];
        return;
    }
    
    if([delegate respondsToSelector: @selector(netManager:didGetSuccessToConnectWithServer:)])
        [delegate netManager:self didGetSuccessToConnectWithServer:nil];
    
    [self startLoad];
    
    if(theQueueType==SERIAL){
        [self startA_SynchronousRequestWithSerialQueue];
    }
    else{
        [self startA_SynchronousRequestWithConcurrentQueue];
    }
}
-(void)stopRequest{
    
    [self stopLoad];
    
    if(connection){
        DebugLog(@"Canceling request..................");
        [connection cancel];
    }
}

- (NSString *)_generateBoundaryString
{
    CFUUIDRef       uuid;
    CFStringRef     uuidStr;
    NSString *      result;
    
    uuid = CFUUIDCreate(NULL);
    assert(uuid != NULL);
    
    uuidStr = CFUUIDCreateString(NULL, uuid);
    assert(uuidStr != NULL);
    
    result = [NSString stringWithFormat:@"%@", uuidStr];
    
    CFRelease(uuidStr);
    CFRelease(uuid);
    
    return result;
}

- (void)createBoundInputStream:(NSInputStream **)inputStreamPtr outputStream:(NSOutputStream **)outputStreamPtr bufferSize:(NSUInteger)bufferSize
{
    CFReadStreamRef     readStream;
    CFWriteStreamRef    writeStream;
    
    assert( (inputStreamPtr != NULL) || (outputStreamPtr != NULL) );
    
    readStream = NULL;
    writeStream = NULL;
    
    CFStreamCreateBoundPair(
                            NULL,
                            ((inputStreamPtr  != nil) ? &readStream : NULL),
                            ((outputStreamPtr != nil) ? &writeStream : NULL),
                            (CFIndex) bufferSize
                            );
    
    if (inputStreamPtr != NULL) {
        *inputStreamPtr  = [NSMakeCollectable(readStream) autorelease];
    }
    if (outputStreamPtr != NULL) {
        *outputStreamPtr = [NSMakeCollectable(writeStream) autorelease];
    }
}

- (void)startLoad {
    [self updateLoadCountWithDelta:1];
}

- (void)stopLoad {
    [self updateLoadCountWithDelta:-1];
}
- (void)updateLoadCountWithDelta:(NSInteger)countDelta {
    @synchronized(self) {
        __LoadingObjectsCount += countDelta;
        __LoadingObjectsCount = (__LoadingObjectsCount < 0) ? 0 : __LoadingObjectsCount ;
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = __LoadingObjectsCount > 0;
    }
}
#pragma mark - Private Methods definition
//Call this method to make Asynchronous call on serial queue
- (void) startA_SynchronousRequestWithSerialQueue{
    
    NSMutableURLRequest *request = [[self createRequest] retain];
    backgroundQueue = dispatch_queue_create(queueIdentifier, NULL);
    connection  = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    dispatch_async(backgroundQueue, ^(void) {
        
        DebugLog(@"Start Asynchronous on Serial Queue...");
        [connection start];
        
    });
    if (request) { [request release]; request = nil;}
}
//Call this method to make Asynchronous call on concurrent queue
- (void) startA_SynchronousRequestWithConcurrentQueue{
    
    DebugLog(@"Start Asynchronous on Cuncurrent Queue...");
    NSMutableURLRequest *request = [[self createRequest] retain];
    backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    connection  = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    dispatch_async(backgroundQueue, ^(void) {
       [connection start];
        
    });
    if (request) { [request release]; request = nil;}
}
-(void) startSynchronousRequest{
    
    NSMutableURLRequest *request = [[self createRequest] retain];
    NSURLResponse* response;
    NSError* error = nil;
    //Capturing server response
    if (responseData) { [responseData release]; responseData = nil;}
    responseData = [(NSMutableData*)[NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error] retain];
    
    if (request) { [request release]; request = nil;}
    [self connectionDidFinishLoading:nil];
}
-(NSMutableURLRequest*)createRequest{
    
    switch (httpMethod) {
        case POST:
            return [self createPostRequest];
        case GET:
            return [self createGetRequest];
        default:
            return nil;
    }
}

- (NSString*)checkDictionaryForImageData
{
    for (NSString* key in parameters)
    {
        id value = [parameters objectForKey:key];
        if([value isKindOfClass:[NSData class]]){
            return key;
        }
    }
    return nil;
}

- (NSMutableURLRequest*) createPostRequest{
 
    DebugLog(@"Request URL = %@", url);
    //DebugLog(@"request parameters = %@", parameters);
    NSString* postString = [self encodeDictionary:parameters];
    //DebugLog(@"request parameters = %@", postString);
    NSData *postData = [postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    
    if([self checkDictionaryForImageData]) {
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        int numberOfMedia = 0;
        NSMutableData *body = [NSMutableData data];
        for(NSString* key in parameters) {
            id value = [parameters objectForKey:key];
            if([value isKindOfClass: [NSString class]]) {
                
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n", value] dataUsingEncoding:NSNonLossyASCIIStringEncoding]];

            }
            else if ([value isKindOfClass:[NSData class]]){
                
                // Now add Media
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", key, [self.attachmentName objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[NSData dataWithData: [parameters valueForKey: key]]];
                    
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                ++numberOfMedia;
            }
            value = nil;
        }
        [request setHTTPBody:body];
        [request setTimeoutInterval:numberOfMedia*TIMEOUT_INTERVAL];
    }
    else {
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        [request setTimeoutInterval:TIMEOUT_INTERVAL];
    }
    
    return request;
}

- (NSMutableURLRequest*) createGetRequest{
    
    NSString* postString = nil;
    NSMutableURLRequest *request= [[[NSMutableURLRequest alloc] init] autorelease];
    
    if(parameters){
        postString = [self encodeDictionary:parameters];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", url, postString]]];
    }
    else{
        postString = [url stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [request setURL:[NSURL URLWithString:postString]];
    }
    DebugLog(@"Request URL = %@", [NSString stringWithFormat:@"%@?%@", url, postString]);
    
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:TIMEOUT_INTERVAL];
    
    return request;
}
- (NSString*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[[NSMutableArray alloc] init] autorelease];
    for (NSString* key in dictionary) {
        id value = [dictionary objectForKey:key];
        if([value isKindOfClass:[NSString class]]){
            
            NSData *data = [value dataUsingEncoding:NSNonLossyASCIIStringEncoding];
            NSString *encodedValue = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
//            NSString *encodedValue = [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            // Neeraj :: Encode Special Character
            NSString *encodedSubject =
            (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                   (CFStringRef)encodedValue,
                                                                                   NULL,
                                                                                   (CFStringRef)@"!*'();:@&=+$,?%#[]₹",
                                                                                   kCFStringEncodingUTF8));
            NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedSubject];
            [parts addObject:part];
            _RELEASE(encodedValue);
        }
        else if([value isKindOfClass: [NSData class]]) {
            [parts addObject: [NSData dataWithData: value]];
        }
        else{
            [parts addObject:value];
        }
        
    }
    //DebugLog(@"%@", parts);
    return [parts componentsJoinedByString:@"&"];
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    [self stopLoad];
    
    DebugLog(@"Failed with error %@", [error localizedDescription]);
    
    //NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: SERVER_FAILED_TO_CONNECT, @"response", nil];
    //NSError* theerror = [[[NSError alloc] initWithDomain: @"Error" code: 1000 userInfo: dict] autorelease];
    
    if(delegate && [delegate respondsToSelector:@selector(netManager:didFailWithError:)]){
        [delegate netManager:self didFailWithError:error];
    }
}

#pragma mark - NSURLConnectionDataDelegate
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response{
    
    return request;
}
- (void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
    
    if(delegate && [delegate respondsToSelector:@selector(netManager:didSendBodyData:totalBytesWritten:totalBytesExpectedToWrite:)]){
        [delegate netManager:self didSendBodyData:bytesWritten totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];
    }
    
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
   // DebugLog(@"Received Response");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(!responseData){
        responseData = [[NSMutableData alloc] initWithData:data];
    }
    else
        [responseData appendData:data];
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
    DebugLog(@"Received Success");
    [self stopLoad];
    
    //NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
    //DebugLog(@"%@",decodedString);
    //NSDictionary *jsonDictD = [decodedString JSONValue];
    
    __autoreleasing NSError* error = nil;
    NSDictionary *jsonDictD = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    //DebugLog(@"%@",jsonDictD);
    
    if(jsonDictD){
//        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: SERVER_FAILED_TO_CONNECT, @"response", nil];
//        NSError* error = [[[NSError alloc] initWithDomain: @"Error" code: 1000 userInfo: dict] autorelease];
//        if(delegate && [delegate respondsToSelector:@selector(netManager:didFailWithError:)]){
//            [delegate netManager:self didFailWithError:error];
//        }
        if(delegate && [delegate respondsToSelector:@selector(netManager:didReceiveResponse:)]){
            [delegate netManager:self didReceiveResponse:jsonDictD];
        }
     
    }
    else{
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: @"Invalid Response", @"response", nil];
        NSError* error = [[[NSError alloc] initWithDomain: @"Error" code: 1000 userInfo: dict] autorelease];
        
        if(delegate && [delegate respondsToSelector:@selector(netManager:didFailWithError:)]){
            [delegate netManager:self didFailWithError:error];
        }
    }
}
#pragma mark - reachability
/**
 * Setup for reachability checks of the remote host
 *
 */
//- (void) enableNotifications {
//	if (!reachability) {
//		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
//		//reachability = [[Reachability reachabilityForInternetConnection] retain];
//		reachability = [[Reachability reachabilityWithHostName: @"www.apple.com"] retain];
//		[reachability startNotifier];
//	}
//}
//
//- (void)updateReachabilityStates {
//	NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
//	//NSLog(@"inet status = %d", remoteHostStatus);
//	if(remoteHostStatus == NotReachable) {
//		DebugLog(@"not reachable");
//
//	} else if (remoteHostStatus == ReachableViaWiFi) {
//		DebugLog(@"wifi");
//	} else if (remoteHostStatus == ReachableViaWWAN) {
//		DebugLog(@"cell");
//	}
//    
//    
//}
//
//- (BOOL)networkReachable {
//	NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
//	return (remoteHostStatus != NotReachable);
//}
//
//- (void)reachabilityChanged:(NSNotification *)note {
//    [self updateReachabilityStates];
//}
@end
