//
//  CMNoInternetView.h
//  Radus
//
//  Created by saurabh on 7/30/13.
//  Copyright (c) 2013 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMNoInternetView : CSBaseView

+ (CMNoInternetView*)sharedInstance;
-(void)showOnView:(UIView*)view;
-(void)showOnView:(UIView*)view withYPos:(CGFloat)yPos;
-(void)hideOnView:(UIView*)view;
@end

