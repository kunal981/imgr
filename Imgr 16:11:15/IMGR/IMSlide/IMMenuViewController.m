//
//  IMSlideMenuViewController.m
//  IMGR
//
//  Created by akram on 06/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMMenuViewController.h"
#import "TWTSideMenuViewController.h"
#import "IMAppDelegate.h"

#define SLIDE_BUTTON_TAG        100
#define MENU_TEXT_FONT_SIZE     21.0

@interface IMMenuViewController ()
{
    UIButton *previouslySelected;
}
@property (nonatomic, strong) UIImageView *backgroundImageView;

@end

@implementation IMMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg"]];
    
    CGRect imageViewRect = [[UIScreen mainScreen] bounds];
    imageViewRect.size.width += 568;
    self.backgroundImageView.frame = imageViewRect;
    
    [self.view addSubview:self.backgroundImageView];
    
    UIImage* logoImage = [UIImage imageNamed: @"logo_imgr_menu.png"];
    UIImageView* imgrLogoImageView = [[UIImageView alloc] initWithImage: logoImage];
    imgrLogoImageView.frame = CGRectMake(107, 44, logoImage.size.width, logoImage.size.height);
    [self.view addSubview: imgrLogoImageView];
    
    NSDictionary *viewDictionary = @{ @"imageView" : self.backgroundImageView };
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[imageView]" options:0 metrics:nil views:viewDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[imageView]" options:0 metrics:nil views:viewDictionary]];
    
    int yPos = ([[UIScreen mainScreen] bounds].size.height - 140) / 2;
    
    UIButton *contactsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    contactsButton.frame = CGRectMake(50.0f, yPos, 150, 40.0f);
    
    UIImageView* contactIcon = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"icn_contacts.png"]];
    contactIcon.frame = CGRectMake(10, yPos + 5, 30, 30);
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onContactsButtonClick:)];
    tapGesture.numberOfTapsRequired = 1;
    [contactIcon addGestureRecognizer: tapGesture];
    [self.view addSubview: contactIcon];
    
    contactsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    contactsButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:MENU_TEXT_FONT_SIZE];
    //previouslySelected = contactsButton;
    
    [contactsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [contactsButton setTitle:@"Contacts" forState:UIControlStateNormal];
    [contactsButton addTarget:self action:@selector(onContactsButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:contactsButton];
    
    UIButton *messagesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    messagesButton.frame = CGRectMake(50.0f, yPos + 50, 150, 40.0f);
    messagesButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    UIImageView* messageIcon = [[UIImageView alloc] initWithFrame: CGRectMake(10, yPos + 55, 30, 30)];
    messageIcon.image = [UIImage imageNamed: @"icn_messages.png"];
    UITapGestureRecognizer* messageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMessagesButtonClick:)];
    messageTapGesture.numberOfTapsRequired = 1;
    [messageIcon addGestureRecognizer: messageTapGesture];
    
    [self.view addSubview: messageIcon];
    
    messagesButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    messagesButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:MENU_TEXT_FONT_SIZE];

    [messagesButton setTitle:@"Messages" forState:UIControlStateNormal];
    [messagesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [messagesButton addTarget:self action:@selector(onMessagesButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:messagesButton];
    //previouslySelected = messagesButton;
    
    if(!app.isRegistering)
    {
        messagesButton.titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:MENU_TEXT_FONT_SIZE];
        previouslySelected = messagesButton;
    }
    else
    {
        contactsButton.titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:MENU_TEXT_FONT_SIZE];
        previouslySelected = contactsButton;
    }
    UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    settingsButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:MENU_TEXT_FONT_SIZE];
    settingsButton.frame = CGRectMake(50.0f, yPos + 100, 150, 40.0f);
    settingsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    UIImageView* settingIcon = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"icn_settings.png"]];
    settingIcon.frame = CGRectMake(10, yPos+105, 30, 30);
    UITapGestureRecognizer* settingsTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSettingsButtonClick:)];
    settingsTapGesture.numberOfTapsRequired = 1;
    [messageIcon addGestureRecognizer: settingsTapGesture];
    
    [self.view addSubview: settingIcon];
    
    settingsButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    [settingsButton setTitle:@"Settings" forState:UIControlStateNormal];
    [settingsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingsButton addTarget:self action:@selector(onSettingsButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingsButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions methods
- (void)animateView
{
    [self.sideMenuViewController closeMenuAnimated:YES completion:nil];
}

- (void)closeSettingsView
{
    [self.sideMenuViewController closeMenuAnimated:YES completion:nil];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (IBAction)onContactsButtonClick:(id)sender
{
    self.view.userInteractionEnabled = NO;
    if(previouslySelected == sender) {
        [self performSelector:@selector(closeContactsView) withObject:nil afterDelay:.5];
        return;
    }
    previouslySelected.titleLabel.font = [UIFont fontWithName:@"Arial" size:MENU_TEXT_FONT_SIZE];
    [(UIButton *)sender titleLabel].font = [UIFont fontWithName:@"Arial-BoldMT" size:MENU_TEXT_FONT_SIZE];
    previouslySelected = sender;

    UIStoryboard* contactSB = [UIStoryboard storyboardWithName: @"IMContacts" bundle: [NSBundle mainBundle]];
    
    UINavigationController* navigationController    = [contactSB instantiateInitialViewController];
    
    //app.window.rootViewController = navigationController;
    
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:navigationController.topViewController];
    
    //controller.navigationBar.frame = CGRectMake(0, 0, 320, 64);
    //[self.sideMenuViewController setMainViewController:controller animated:YES closeMenu:NO];
    //app.window.rootViewController = navigationController;
    if (app.navigationController.viewControllers.count) {
        [app.navigationController popViewControllerAnimated:NO];
    }
    [app.navigationController pushViewController: controller.topViewController animated: YES];
    [self performSelector:@selector(closeContactsView) withObject:nil afterDelay:.5];
}

- (IBAction)onMessagesButtonClick:(id)sender
{
    self.view.userInteractionEnabled = NO;
    if(previouslySelected == sender) {
        [self performSelector:@selector(closeContactsView) withObject:nil afterDelay:.5];
        return;
    }
    previouslySelected.titleLabel.font = [UIFont fontWithName:@"Arial" size:MENU_TEXT_FONT_SIZE];
    [(UIButton *)sender titleLabel].font = [UIFont fontWithName:@"Arial-BoldMT" size:MENU_TEXT_FONT_SIZE];
    previouslySelected = sender;

    UIStoryboard* messagesSB = [UIStoryboard storyboardWithName: @"MessagesControllers" bundle: [NSBundle mainBundle]];
    
    UINavigationController* navigationController    = [messagesSB instantiateInitialViewController];
    
    //app.window.rootViewController = navigationController;
    
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:navigationController.topViewController];
    //controller.navigationBar.frame = CGRectMake(0, 0, 320, 64);
    //[self.sideMenuViewController setMainViewController:controller animated:YES closeMenu:NO];
    //app.window.rootViewController = navigationController;
    if (app.navigationController.viewControllers.count) {
        [app.navigationController popViewControllerAnimated:NO];
    }
    [app.navigationController pushViewController: controller.topViewController animated: YES];
    
    [self performSelector:@selector(closeContactsView) withObject:nil afterDelay:.5];
}

//-vi
- (void)closeContactsView
{
    self.view.userInteractionEnabled = YES;
    [self.sideMenuViewController closeMenuAnimated:YES completion:nil];
}

- (IBAction)onSettingsButtonClick:(id)sender
{
    self.view.userInteractionEnabled = NO;
    if(previouslySelected == sender) {
        [self performSelector:@selector(closeContactsView) withObject:nil afterDelay:.5];
        return;
    }
    //app.window.rootViewController = nil;
    previouslySelected.titleLabel.font = [UIFont fontWithName:@"Arial" size:MENU_TEXT_FONT_SIZE];
    [(UIButton *)sender titleLabel].font = [UIFont fontWithName:@"Arial-BoldMT" size:MENU_TEXT_FONT_SIZE];
    previouslySelected = sender;
    UIStoryboard* contactSB = [UIStoryboard storyboardWithName: @"SettingsControllers" bundle: [NSBundle mainBundle]];
    
    UINavigationController* navigationController    = [contactSB instantiateInitialViewController];
    
    //app.window.rootViewController = navigationController;
    
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:navigationController.topViewController];
    
    //[self.sideMenuViewController setMainViewController:controller animated:YES closeMenu:NO];
    //app.window.rootViewController = navigationController;
    if (app.navigationController.viewControllers.count) {
        [app.navigationController popViewControllerAnimated:NO];
    }
    [app.navigationController pushViewController: controller.topViewController animated: YES];
    [self performSelector:@selector(closeContactsView) withObject:nil afterDelay:.5];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}


@end
