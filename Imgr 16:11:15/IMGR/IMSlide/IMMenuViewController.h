//
//  IMSlideMenuViewController.h
//  IMGR
//
//  Created by akram on 06/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMMenuViewController : UIViewController

- (IBAction)onContactsButtonClick:(id)sender;
- (IBAction)onMessagesButtonClick:(id)sender;
- (IBAction)onSettingsButtonClick:(id)sender;

@end
