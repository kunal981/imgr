//
//  IMPromoSettingsOffView.m
//  IMGR
//
//  Created by Satendra Singh on 2/7/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMPromoSettingsOffView.h"
#import "IMUtils.h"

@interface IMPromoSettingsOffView()
{
    IMPromos *_selectedPromo;
}
@end

@implementation IMPromoSettingsOffView

@synthesize selectedPromo = _selectedPromo;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    if ([IMUtils getOSVersion] < 7.0) {
        [self.removePromoButton setImage:[UIImage imageNamed:@"icn_delete_ios6.png"] forState:UIControlStateNormal];
    }
    self.removePromoButton.hidden = YES;
    self.promoIcon.hidden = YES;
    self.promoTitle.hidden = YES;
    self.bgImageView.image = nil;

    self.promoIcon.image = nil;
    self.promoTitle.text = @"";
    _bgImageView.image = [UIImage imageNamed:@"bg_bar_advt_off.png"];

}

- (void) setSelectedPromo:(IMPromos *)selectedPromo
{
    _selectedPromo = selectedPromo;
    
    NSString* promoImage = [[IMUtils getImageFolderPath] stringByAppendingPathComponent: [_selectedPromo.promo_image lastPathComponent]];
    _promoIcon.image = [UIImage imageWithContentsOfFile:promoImage];
    _promoTitle.text = [_selectedPromo promo_name];
    _promoTitle.hidden = NO;
    _promoIcon.hidden = NO;
    _removePromoButton.hidden = NO;
//    _bgImageView.image = [UIImage imageNamed:@"bg_bar_advt_off.png"];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (IBAction)userDidTappedAddPromo:(id)sender
{
    
}

- (IBAction)userDidTappedRemovePromo:(id)sender
{
    if (_selectedPromo) {
        
        CATransition *animation = [CATransition animation];
        [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setDuration:0.40];
        [animation setTimingFunction:
         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.layer addAnimation:animation forKey:kCATransition];

        self.removePromoButton.hidden = YES;
        self.promoIcon.hidden = YES;
        self.promoTitle.hidden = YES;
//        _bgImageView.image = nil;

        _selectedPromo = nil;
    }
}

@end
