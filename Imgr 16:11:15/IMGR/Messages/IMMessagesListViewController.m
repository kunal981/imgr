//
//  IMMessagesListViewController.m
//  IMGR
//
//  Created by Satendra Singh on 2/6/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMMessagesListViewController.h"
#import "IMMessageListCell.h"
#import "IMContactsViewController.h"
#import "IMNoMessageView.h"
#import "TWTSideMenuViewController.h"
#import "IMMenuViewController.h"
#import "IMUtils.h"
#import "IMChatViewController.h"
#import "IMSelectUserViewController.h"
#import "IMDataHandler.h"
#import "XMPPCoreDataStorage.h"
#import "XMPPCoreDataStorageProtected.h"
#import "MBProgressHUD.h"
#import "NSString+Emoticon.h"
#import "RefreshContents.h"

#define SINGLE_DELETE_ALERT_TAG 987
#define DELETE_ALL_ALERT_TAG 989
#define M_PI   1.57
@interface IMMessagesListViewController ()<UIAlertViewDelegate,  NSFetchedResultsControllerDelegate>
{
    IMNoMessageView *noMessageView;
    NSUInteger selectedCount;
    NSDateFormatter *timeFormatter;
    CMMessageArchiving_Contact_CoreDataObject *deleteContact;
    UIBarButtonItem *deleteButtonItem;
    NSFetchedResultsController *messagesContactFetchResultController;
    BOOL isLoadedFirstTime;
    MBProgressHUD *progressView;
    RefreshContents * refreshContent;
}

@property (nonatomic, assign) BOOL isDeleteEnabled;
@property (weak, nonatomic) IBOutlet UIImageView *tableBackgrImageView;
- (IMNoMessageView *)noMessageView;
@property (nonatomic, strong) NSString *selectedJid;
@property (nonatomic)int unreadMessageCount;
@property (nonatomic) dispatch_queue_t sessionQueue;
- (NSString *)dateSectionTitleForDate:(NSDate *)messageTimestamp;

@end

@implementation IMMessagesListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IMNoMessageView *)noMessageView
{
    if (nil == noMessageView) {
        noMessageView = [IMNoMessageView viewLoadedFromNibFile];
    }
    return noMessageView;
}

#pragma mark-
#pragma mark Fetchresultcontroller and delegates
#pragma mark-

- (NSFetchedResultsController *)messagesContactFetchResultController
{
    if (NO == [[CMMessageArchivingCoreDataStorage sharedInstance]isStoreSet]) {
        if (nil == progressView) {
            progressView = [[MBProgressHUD alloc] initWithView: self.view];
            progressView.labelText = @"Please wait ...";
            
            [app.window addSubview: progressView];
            [progressView show:YES];
        }

        NSLog(@"Please wait setting up iclod");
        return nil;
    }
	if (messagesContactFetchResultController == nil)
	{
        if (progressView) {
            [progressView hide:YES];
            progressView = nil;
        }
        
		NSManagedObjectContext *moc = [[CMMessageArchivingCoreDataStorage sharedInstance] mainThreadManagedObjectContext];
		if(!moc){
            return nil;
        }
        
		NSEntityDescription *entity = [NSEntityDescription entityForName:@"CMMessageArchiving_Contact_CoreDataObject"
		                                          inManagedObjectContext:moc];
		
		NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"mostRecentMessageTimestamp" ascending:NO];
		
		NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, nil];
		
		NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
		[fetchRequest setEntity:entity];
		[fetchRequest setSortDescriptors:sortDescriptors];
        
        if (nil == messagesContactFetchResultController) {
            messagesContactFetchResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                managedObjectContext:moc
                                                                                  sectionNameKeyPath:nil
                                                                                           cacheName:nil];
            messagesContactFetchResultController.delegate = self;
        }
        
		NSError *error = nil;
            if (![messagesContactFetchResultController performFetch:&error])
            {
                DDLogError(@"Error performing fetch: %@", error);
            }
 
//            [[[CMMessageArchivingCoreDataStorage sharedInstance] persistentStoreCoordinator] unlock];
//        }

        //mrk : hangs due to lock
//        if ([[[CMMessageArchivingCoreDataStorage sharedInstance] persistentStoreCoordinator] tryLock]) {
//            if (![messagesContactFetchResultController performFetch:&error])
//            {
//                DDLogError(@"Error performing fetch: %@", error);
//            }
//            [[[CMMessageArchivingCoreDataStorage sharedInstance] persistentStoreCoordinator] unlock];
//        }
		
        
	}
	
	return messagesContactFetchResultController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
    [self.tableView reloadData];
}


-(void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
   
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            //            [tableView scrollToRowAtIndexPath:newIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
                [self configureCell:(IMMessageListCell*)[tableView cellForRowAtIndexPath:indexPath] withMessage:[messagesContactFetchResultController objectAtIndexPath: indexPath]];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.activitiIndicator.hidden=YES;
    timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh:mm a"];
    self.tableView.allowsSelectionDuringEditing = NO;
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    self.tableView.tableHeaderView.layer.borderWidth=0.5;
    self.tableView.tableHeaderView.layer.borderColor=[UIColor grayColor].CGColor;
    [self setupNavigationBarForTableNormal];
    self.tableView.backgroundView = self.tableBackgrImageView;
    [self performSelector:@selector(reloadContent) withObject:nil afterDelay:2.0];
//    self.refreshControl = [UIRefreshControl new];
//    
//    [self.refreshControl addTarget:self action:@selector(loadStream) forControlEvents:UIControlEventValueChanged];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor= [UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0];
    refreshControl.tintColor = [UIColor clearColor];
   // refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull down to update promos"];
    [refreshControl addTarget:self action:@selector(loadStream) forControlEvents:UIControlEventValueChanged];
    
    
//    UIView *customView  = [[UIView alloc]initWithFrame:refreshControl.bounds];
//    customView.backgroundColor = [UIColor redColor];
//    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, customView.frame.size.width, customView.frame.size.height)];
//    lbl.text =  @"Test";
//    [customView addSubview:lbl];
//    [refreshControl addSubview:customView];
    
   // [self.tableView addSubview:refreshControl];
    [self.tableView insertSubview:refreshControl atIndex:0];
    [self loadNIBFile];
   // UIView *customView =  [[[NSBundle mainBundle] loadNibNamed:@"RefreshContents" owner:self options:nil] objectAtIndex:0];
   // [refreshControl addSubview:customView];
   // customView.frame = refreshControl.bounds;
//    [self messagesContactFetchResultController];
//    [self.tableView reloadData];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//    [self.tableView addSubview:[self noMessageView]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadContent)
                                                 name:@"SomethingChanged"
                                               object:nil];

    
   

}

-(void)loadNIBFile{
    refreshContent = [[[NSBundle mainBundle] loadNibNamed:@"RefreshContents" owner:self options:nil] objectAtIndex:0];
    UIView *customView = (UIView*)refreshContent;
    customView.frame = refreshControl.bounds;
   customView.backgroundColor = [UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0];
    [refreshControl addSubview:customView];
    
    refreshContent.refreshIndicator .hidden = YES;
    // get current date/time
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // display in 12HR/24HR (i.e. 11:25PM or 23:25) format according to User Settings
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm a"];
    currentTime = [dateFormatter stringFromDate:today];
  
    NSLog(@"User's current time in their preference format:%@",currentTime);
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"DateFirstLoading"];
    if(![[NSUserDefaults standardUserDefaults]boolForKey:@"DateFirstLoading"]){
        refreshContent.lblDate.text = currentTime;
    }

    
}

-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
    messagesContactFetchResultController = nil;
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
 
//    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"Refresh"]) {
//        self.refreshView.hidden = YES;
//        float y = -44;
//        self.tableView.frame = CGRectMake(0, y, self.tableView.frame.size.width, self.tableView.frame.size.height+44);
//    }

}

-(void)viewDidDisappear:(BOOL)animated{
   
   
 
}

- (void) reloadContent
{
    isLoadedFirstTime = YES;
    messagesContactFetchResultController = nil;
    [self.tableView reloadData];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSInteger recentContacts = [self messagesContactFetchResultController].fetchedObjects.count;

    if (isLoadedFirstTime) {
        if (0 ==recentContacts) {
            
            [self.view addSubview:self.noMessageView];
            [self setupNavigationBarForNoMessage];
        }
        else if( [self.noMessageView superview])
        {
            [self.noMessageView removeFromSuperview];
            [self setupNavigationBarForTableNormal];
            
        }
    }
    return recentContacts;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    IMMessageListCell *cell = [tableView dequeueReusableCellWithIdentifier:[IMMessageListCell cellReusableIdentifier]];
    
    if (nil == cell) {
        cell = [IMMessageListCell cellLoadedFromNibFile];
    }
    // Configure the cell...
    CMMessageArchiving_Contact_CoreDataObject *message = [[self messagesContactFetchResultController] objectAtIndexPath:indexPath];
    [self configureCell:cell withMessage:message];
    //    cell.textLabel.text = message.bareJid.user;
    //    cell.detailTextLabel.text = message.mostRecentMessageBody;
   
	return cell;
}

- (void) configureCell:(IMMessageListCell *)cell withMessage:(CMMessageArchiving_Contact_CoreDataObject *)messageContact
{
    NSLog(@"%@",messageContact);
    cell.lastMessageText.text = [messageContact.mostRecentMessageBody emoticonizedString];
    
    //    CMUserCoreDataStorageObject *user = [[CMChatManager sharedInstance] ];
    NSString *hostName = nil;
    hostName = HOST_OPENFIRE;//messageContact.hostName;//earlier we were using dynamic host name. now it is changed to imgrapp.com. Earlier app version were using dynamic host name due to which all messages coming from other users were bind by ip address, now changing it to imgrapp.com will now change all previous messages to imgrapp.com. So the issues which were found in earlier versions of deleting a messages will be resolved by doing so.
    
    UIImage *image = [[CMChatManager sharedInstance] fetchUserPhotoFrom:hostName forUser:messageContact.bareJidStr] ;
    NSString *displayName = nil;
    IMContacts *contact = [[IMDataHandler sharedInstance] fetchContactWithUserJid:messageContact.bareJidStr];
    if (contact.first_name.length) {
        
        displayName = [contact.first_name capitalizedString];
    }
    else if (contact.last_name.length) {
        displayName = [contact.last_name capitalizedString];
    }
    else
    {
        if (messageContact.bareJidStr.length > k_DEFAULT_NUMBER_CHARACTERS_COUNT) {
            displayName = [messageContact.bareJidStr substringToIndex:k_DEFAULT_NUMBER_CHARACTERS_COUNT];
        }
        else
        {
            displayName = NSLocalizedString(@"No Name", @"No Name");
        }
        
    }
    cell.userDisplayNameLabel.text = displayName;

    if (image == nil)
        image = [UIImage imageNamed: @"img_contact_placeholder@2x.png"];
    
        image  = [IMUtils circularScaleNCrop:image rect:CGRectMake(0, 0, cell.userPhoto.frame.size.width, cell.userPhoto.frame.size.height)];
        cell.userPhoto.image = image;

    if (messageContact.unreadCount.integerValue !=0) {
        cell.messageUnreadCountLabel.hidden = NO;
        cell.messageUnreadCountLabel.text = [NSString stringWithFormat:@"%ld",(long)messageContact.unreadCount.integerValue];
        cell.messageCountBackgroundImageView.hidden = NO;

    }
    else
    {
        cell.messageUnreadCountLabel.hidden = YES;
        cell.messageUnreadCountLabel.text = @"";
        cell.messageCountBackgroundImageView.hidden = YES;
    }
    
    cell.lastMessageReceivedTimeLabel.text = [self dateSectionTitleForDate:messageContact.mostRecentMessageTimestamp];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.tableView.isEditing) {
        
        CMMessageArchiving_Contact_CoreDataObject *message = [[self messagesContactFetchResultController] objectAtIndexPath:indexPath];;
        self.selectedJid = [message bareJidStr];
        // [[CMChatManager sharedInstance] resetUnreadMessageCountWithHost:HOST_OPENFIRE forJID:self.selectedJid];
        self.unreadMessageCount = [message.unreadCount intValue];
        [self performSegueWithIdentifier:@"IMChatViewController" sender:self];
    }
    else
    {
        selectedCount++;
        self.title = [NSString stringWithFormat:@"%lu selected",(unsigned long)selectedCount];

        NSLog(@"didSelectRowAtIndexPath");
    }
    if (selectedCount) {
        deleteButtonItem.enabled = YES;
    }
    else
    {
       deleteButtonItem.enabled = NO;
    }
}




-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.tableView.isEditing) {
//        [self performSegueWithIdentifier:@"IMChatViewController" sender:self];
    }
    else
    {
        selectedCount--;
        self.title = [NSString stringWithFormat:@"%lu selected",(unsigned long)selectedCount];

        NSLog(@"didDeselectRowAtIndexPath");
    }
    if (selectedCount) {
        deleteButtonItem.enabled = YES;
    }
    else
    {
        deleteButtonItem.enabled = NO;
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.isEditing) {
        [self setupNavigationBarForTableDeleteAll];
        return tableView.isEditing;
    }
    return YES;
    // Return NO if you do not want the specified item to be editable.
//    return YES;
}

- (void)tableView:(UITableView*)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setupNavigationBarForTableNormal];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        CMMessageArchiving_Contact_CoreDataObject *message = [[self messagesContactFetchResultController] objectAtIndexPath:indexPath];
        if (message) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"DELETE MESSAGE", @"") message:@"This action can't be undone." delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [alertView show];
            alertView.tag = SINGLE_DELETE_ALERT_TAG;
            deleteContact = message;
        }
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
//    tableViewCell.accessoryView.hidden = NO;
//    // if you don't use custom image tableViewCell.accessoryType = UITableViewCellAccessoryCheckmark;
//}
//
//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
//    tableViewCell.accessoryView.hidden = YES;
//    // if you don't use custom image tableViewCell.accessoryType = UITableViewCellAccessoryNone;
//}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return UITableViewCellEditingStyleNone;
//}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

- (void) setupNavigationBarForTableNormal
{
    self.title = @"Messages";
    self.isDeleteEnabled = NO;
    
    UIButton* editBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    editBtn.frame = CGRectMake(0, 0, 50, 30);
    [editBtn setTitle:@"Edit" forState: UIControlStateNormal];
    [editBtn setTitleColor: [IMUtils appBlueColor] forState: UIControlStateNormal];
    [editBtn addTarget:self action:@selector(onEditTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithCustomView: editBtn];
    //UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(onEditTapped:)];
    
    UIButton* messageBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    messageBtn.frame = CGRectMake(0, 0, 24, 24);
    [messageBtn setBackgroundImage: [UIImage imageNamed: @"btn_message.png"] forState: UIControlStateNormal];
    [messageBtn addTarget:self action:@selector(onNewMessageTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *newMessageButton = [[UIBarButtonItem alloc] initWithCustomView: messageBtn];
    //UIBarButtonItem *newMessageButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_message"] style:UIBarButtonItemStylePlain target:self action:@selector(onNewMessageTapped:)];
    
    self.navigationItem.rightBarButtonItems = @[newMessageButton,editButton];
    
    UIButton* menuBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    menuBtn.frame = CGRectMake(0, 0, 24, 24);
    [menuBtn setBackgroundImage: [UIImage imageNamed: @"btn_menu.png"] forState: UIControlStateNormal];
    [menuBtn addTarget:self action:@selector(onSlideButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView: menuBtn];
    //UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(onSlideButtonClick:)];
    self.navigationItem.leftBarButtonItem = doneButton;
//SS TODO:    self.navigationItem.leftBarButtonItem.action = @selector(onEditTapped:);

    selectedCount =0;
    
}

- (void) setupNavigationBarForNoMessage
{
    self.title = @"Messages";
    self.isDeleteEnabled = NO;
//    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(onEditTapped:)];
    
    UIButton* messageBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    messageBtn.frame = CGRectMake(0, 0, 24, 24);
    [messageBtn setBackgroundImage: [UIImage imageNamed: @"btn_message.png"] forState: UIControlStateNormal];
    [messageBtn addTarget:self action:@selector(onNewMessageTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    //UIBarButtonItem *newMessageButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_message"] style:UIBarButtonItemStylePlain target:self action:@selector(onNewMessageTapped:)];
    UIBarButtonItem *newMessageButton = [[UIBarButtonItem alloc] initWithCustomView: messageBtn];
    
    
    self.navigationItem.rightBarButtonItems = @[newMessageButton];
    
    UIButton* menuBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    menuBtn.frame = CGRectMake(0, 0, 24, 24);
    [menuBtn setBackgroundImage: [UIImage imageNamed: @"btn_menu.png"] forState: UIControlStateNormal];
    [menuBtn addTarget:self action:@selector(onSlideButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView: menuBtn];
    //UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(onSlideButtonClick:)];
    self.navigationItem.leftBarButtonItem = doneButton;
    //SS TODO:    self.navigationItem.leftBarButtonItem.action = @selector(onEditTapped:);
    
    
    
}

- (void) setupNavigationBarForTableDeleteAll
{
    if (self.isDeleteEnabled) {
        return;
    }
    self.isDeleteEnabled = YES;
    
    UIButton* deleteBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    deleteBtn.frame = CGRectMake(0, 0, 90, 30);
    [deleteBtn setTitle:@"Delete All" forState: UIControlStateNormal];
    [deleteBtn setTitleColor: [IMUtils appBlueColor] forState: UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(onDeleteAllTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *deleteAllButton = [[UIBarButtonItem alloc] initWithCustomView: deleteBtn];
    //UIBarButtonItem *deleteAllButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete All" style:UIBarButtonItemStylePlain target:self action:NULL];
    
    UIButton* doneBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(0, 0, 50, 30);
    [doneBtn setTitle:@"Done" forState: UIControlStateNormal];
    [doneBtn setTitleColor: [IMUtils appBlueColor] forState: UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(onDoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView: doneBtn];
    //UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(onDoneTapped:)];
    self.navigationItem.rightBarButtonItems = @[deleteAllButton];
    self.navigationItem.leftBarButtonItem = doneButton;
}

- (void) setupNavigationBarForTableSelectionDelete
{
    self.title = @"0 selected";
    self.isDeleteEnabled = YES;
    
    UIButton* deleteBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    deleteBtn.frame = CGRectMake(0, 0, 70, 30);
    [deleteBtn setTitle:@"Delete" forState: UIControlStateNormal];
    [deleteBtn setTitleColor: [IMUtils appBlueColor] forState: UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(onDeleteTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *deleteAllButton = [[UIBarButtonItem alloc] initWithCustomView: deleteBtn];
    //UIBarButtonItem *deleteAllButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(onDeleteTapped:)];
    deleteButtonItem = deleteAllButton;
    deleteButtonItem.enabled = NO;
    if ([IMUtils getOSVersion] >= 7.0) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [IMUtils appRedColor],UITextAttributeTextColor,
                                    nil];
        
        [deleteAllButton setTitleTextAttributes:attributes
                                       forState:UIControlStateNormal];
    }

    UIButton* doneBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(0, 0, 70, 30);
    [doneBtn setTitle:@"Cancel" forState: UIControlStateNormal];
    [doneBtn setTitleColor: [IMUtils appBlueColor] forState: UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(onDoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView: doneBtn];
    //UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(onDoneTapped:)];
    self.navigationItem.rightBarButtonItems = @[deleteAllButton];
    self.navigationItem.leftBarButtonItem = doneButton;
}

#pragma mark - IBAction methods
- (IBAction)onSlideButtonClick:(id)sender
{
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (IBAction)onEditTapped:(UIBarButtonItem *)sender {
    
    if (self.tableView.editing) {
        self.tableView.allowsSelectionDuringEditing = NO;
        self.tableView.allowsMultipleSelectionDuringEditing = NO;
        [self setupNavigationBarForTableNormal];
    }
    else
    {
        self.tableView.allowsSelectionDuringEditing = YES;
        self.tableView.allowsMultipleSelectionDuringEditing = YES;
        [self setupNavigationBarForTableSelectionDelete];
    }
    
    [self.tableView setEditing:!self.tableView.editing animated:YES];

//    if (self.tableView.editing) {
//        [self setupNavigationBarForTableSelectionDelete];
//    }
//    else{
//        
//        [self setupNavigationBarForTableNormal];
//    }
}

- (IBAction)onDeleteTapped:(UIBarButtonItem *)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DELETE MESSAGE", @"") message:@"This action can't be undone." delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alertView show];
}

- (IBAction)onDeleteAllTapped:(UIBarButtonItem *)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DELETE ALL MESSAGE", @"") message:@"This action can't be undone." delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alertView.tag = DELETE_ALL_ALERT_TAG;
    [alertView show];
}



- (void)decreaseBadgeCount: (int)badgeNumber unreadMessageCount: (int)unreadMessageCount
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString* deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:K_DEVICE_TOKEN];
    if(deviceToken != nil)
        [parameters setObject: deviceToken forKey: @"deviceToken"];
    
    [parameters setObject:[NSString stringWithFormat: @"%d", unreadMessageCount] forKey: @"unreadMessageCount"];
    
    [parameters setObject:[NSString stringWithFormat: @"%d", badgeNumber] forKey: @"badgeNumber"];
    
    NSString* username = [[NSUserDefaults standardUserDefaults] objectForKey: k_DEFAULTS_KEY_USER_ID];
    [parameters setObject:username forKey: @"username"];
    
    DebugLog(@"badgeNumber = %d, unreadMessageCount = %d",badgeNumber, unreadMessageCount);
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: badgeNumber];
    
    DebugLog(@"Badge parameters = %@, %@", parameters, [NSString stringWithFormat:@"%@%@", KBASEURL,kDecreaseBadgeCount]);
    
    CMNetManager* netManager1 = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kDecreaseBadgeCount] parameters:parameters queueIdentifier:"decreaseBadgeCountQueue" queueType:CONCURRENT httpMethod: GET];
    netManager1.delegate = self;
    netManager1.tag = E_DECREASE_BADGE_COUNT_SERVICE;
    [netManager1 startRequest];
}
#pragma mark - Webservice methods
- (void)getSponsoredAds
{
    [[IMAppDelegate sharedDelegate] getSponsoredAds];
}
- (void)loadStream
{
    [[NSUserDefaults standardUserDefaults]setValue:currentTime forKey:@"RefreshDate"];
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"DateFirstLoading"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    [refreshContent.refreshIndicator stopAnimating];
    refreshContent.refreshIndicator.hidden=YES;
    refreshContent.pullDownImageView.hidden=NO;

    
    //[refreshContent.refreshIndicator startAnimating];
    [self getSponsoredAds];
    [refreshControl endRefreshing];
    [self performSelector:@selector(updateSponsoredAdsTable) withObject:nil afterDelay:1];
}

-(void)updateSponsoredAdsTable
{
    //[[NSUserDefaults standardUserDefaults]setBool:true forKey:@"Refresh"];
    //[[NSUserDefaults standardUserDefaults]synchronize];
    //self.refreshView .hidden = YES;
    //float y = -44;
    //self.tableView.frame = CGRectMake(0, y, self.tableView.frame.size.width, self.tableView.frame.size.height);
    [self.tableView reloadData];
   
    
    
    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex) {
        
        if (alertView.tag == SINGLE_DELETE_ALERT_TAG) {
            NSLog(@"deleteAllMessagesWithHost jid = %@", deleteContact.bareJidStr);
            
            int unreadMessageCount = [[CMChatManager sharedInstance] unreadMessageCountWithHost:HOST_OPENFIRE forJID:deleteContact.bareJidStr];
            if(unreadMessageCount >= 0)
            {
                NSInteger badgeNumber = [[CMChatManager sharedInstance] unreadMessageCountForAllBuddiesWithHost:HOST_OPENFIRE] - unreadMessageCount;

                [self decreaseBadgeCount: badgeNumber unreadMessageCount: unreadMessageCount];
            }
            [[CMChatManager sharedInstance] deleteAllMessagesWithHost:HOST_OPENFIRE forJID:deleteContact.bareJidStr];
            [self setupNavigationBarForTableDeleteAll];
        }
        else if (alertView.tag == DELETE_ALL_ALERT_TAG)
        {
            [self decreaseBadgeCount: 0 unreadMessageCount: 0];
            
            for (CMMessageArchiving_Contact_CoreDataObject *message in [[self messagesContactFetchResultController] fetchedObjects]) {
                if (message) {
                    [[CMChatManager sharedInstance] deleteAllMessagesWithHost:HOST_OPENFIRE forJID:message.bareJidStr];
                    deleteContact = nil;
                }
            }
        }
        else
        {
            NSArray *selected = [self.tableView indexPathsForSelectedRows];
            for (NSIndexPath *indexPath in selected) {
                CMMessageArchiving_Contact_CoreDataObject *message = [[self messagesContactFetchResultController] objectAtIndexPath:indexPath];
                if (message) {
                    [[CMChatManager sharedInstance] deleteAllMessagesWithHost:HOST_OPENFIRE forJID:message.bareJidStr];
                    deleteContact = nil;
             }
            }
            
            [self.tableView setEditing:NO animated:YES];
            self.tableView.allowsSelectionDuringEditing = NO;
            self.tableView.allowsMultipleSelectionDuringEditing = NO;
            [self setupNavigationBarForTableNormal];
        }
    }
    else
    {
        if (alertView.tag != SINGLE_DELETE_ALERT_TAG) {

        [self.tableView setEditing:NO animated:YES];
        self.tableView.allowsSelectionDuringEditing = NO;
        self.tableView.allowsMultipleSelectionDuringEditing = NO;
        [self setupNavigationBarForTableNormal];
        }
    }
}

- (IBAction)onDoneTapped:(UIBarButtonItem *)sender {

    [self.tableView setEditing:NO animated:YES];
    self.tableView.allowsSelectionDuringEditing = NO;
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
        [self setupNavigationBarForTableNormal];
}

- (IBAction)onNewMessageTapped:(UIBarButtonItem *)sender
{
    UIStoryboard *contactsStoryBoard = [[IMAppDelegate sharedDelegate] settingsStoryboard];
    __block IMSelectUserViewController *contacts = [contactsStoryBoard instantiateViewControllerWithIdentifier:@"IMSelectUserViewController"];
    contacts.selectionBlockObject = ^(NSString *jid){
        [self setSelectedJid:jid];
        [self performSegueWithIdentifier:@"IMChatViewController" sender:self];
    };
    
    self.navigationItem.title = @"Back";
    //    contacts.navigationItem.leftBarButtonItem.title = @"Back";
    self.navigationItem.title = @"Messages";

    [self.navigationController pushViewController:contacts animated:YES];


}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"IMChatViewController"]) {
        IMChatViewController *chatController = [segue destinationViewController];
        chatController.userJid = self.selectedJid;
        chatController.unreadMessageCount = self.unreadMessageCount;
    }
    else{
        
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (NSString *)dateSectionTitleForDate:(NSDate *)messageTimestamp {
    
    // Date formatter
    static NSDateFormatter *formatter = nil;
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM dd"];
    }
    
    NSDate *today = [NSDate date];
    NSString *todayString = [formatter stringFromDate:today];
    NSString *messageDate = [formatter stringFromDate:messageTimestamp];
    // Return title calculated from entity's attributes
    // Or any other date calculations you need
    
    if ([todayString isEqualToString:messageDate]) {
        return [timeFormatter stringFromDate:messageTimestamp];
    }
    else
        return messageDate;
}
-(void)spinTimer{
    [NSTimer scheduledTimerWithTimeInterval:0.10 target:self selector:@selector(spinLayer:duration:direction:) userInfo:nil repeats:NO];
}

-(void)spinLayer:(CALayer *)inLayer duration:(CFTimeInterval)inDuration
        direction:(int)direction
{
    CABasicAnimation* rotationAnimation;
    direction=1;
    // Rotate about the z axis
    rotationAnimation =
    [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    // Rotate 360 degress, in direction specified
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 * direction];
    
    // Perform the rotation over this many seconds
    rotationAnimation.duration = inDuration;
    
    // Set the pacing of the animation
    rotationAnimation.timingFunction =
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    // Add animation to the layer and make it so
    [inLayer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    NSString * date =  [[NSUserDefaults standardUserDefaults]valueForKey:@"RefreshDate"];
    
    refreshContent.lblDate.text = [NSString stringWithFormat:@"%@",date];
        
}


-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    refreshContent.refreshIndicator.hidden=YES;
    refreshContent.pullDownImageView.hidden=NO;
    [self spinLayer:refreshContent.pullDownImageView.layer duration:0.5 direction:0.5];
    
    
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    //refreshContent.pullDownImageView.hidden=YES;
    
    
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    refreshContent.pullDownImageView.hidden=YES;
    refreshContent.refreshIndicator.hidden=NO;
    
    
    [refreshContent.refreshIndicator startAnimating];
        
    
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}
@end
