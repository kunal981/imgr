//
//  CMViewController.m
//  DemochatApp
//
//  Created by Raiduk on 10/01/14.
//  Copyright (c) 2014 CopperMobile. All rights reserved.
//


#define PADDLING_LINEHEIGHT        4
#define MINIMUM_FONT               18.0
#define MAXIMUM_FONT               31.0
#define SENDER_USER_IMAGE_WIDTH    50
#define SENDER_USER_IMAGE_HEIGHT   50
#define RECEIVER_USER_IMAGE_WIDTH  50
#define RECEIVER_USER_IMAGE_HEIGHT 50
#define PROMO_MAX_EDGE_SIZE     50.0
#define CHAT_IMAGE_BUBBLE_TOP_MARGIN 4
#define MAXIMUM_BUBBLE_WIDTH         295
#define USER_IMAGE_Y_POS             35
#define USER_IMAGE_Y_POS_WITHOUT_HEADER 25
#define FONT_NAME                   @"Arial"//@"Gotham-Light"
#define MESSAGE_TAG                 100000
#define CAMERA_ICON_NAME @"icn_camera.png"

#define MAX_ROW_HEIGHT              100000
#define NO_CONTACT_ALERT            200000

#define DUMMY_PROMO_IMAGE_SIZE      34

#import "IMChatViewController.h"
#import "IMChatMessageCell.h"
#import "IMChatImage.h"
#import "IMPromoSettingsOnView.h"
#import "IMPromoSettingsOffView.h"
#import "IMUtils.h"
#import "IMContactDetailViewController.h"
#import "IMPromoChoiceViewController.h"
#import "IMDataHandler.h"
#import "IMPromoHeaderView.h"
#import "CMPhotoPickerController.h"
#import "IMChatDateHeader.h"
#import "IMMediaMessageCell.h"
#import "CMNetManager.h"
#import "NSString+Emoticon.h"
#import "IMAddNewContactViewController.h"
#import "MBProgressHUD.h"
#import "NSData+Base64.h"
#import "XMPPStream.h"
#import "IMMessagesListViewController.h"


@interface IMChatViewController ()<CMChatManagerMessagesDelegate, NSFetchedResultsControllerDelegate, CMNetManagerDelegate,XMPPStreamDelegate>
{
    IBOutlet UITableView            *chatTableVW;
    IBOutlet UIButton               *sendBtn;
    IBOutlet UIButton               *chooseImageButton;
    IBOutlet UITextView             *messageTextField;
   // IBOutlet UITextField            *headerTextField;
 //   IBOutlet UITextField            *footerTextField;
    IBOutlet UIView                 *messageView;
    __weak IBOutlet UIButton *rightBarButton;
    
    UIButton                        *imgButton;
    float font_size;
    float font_sizeOthers;

    IBOutlet UILabel                *dummyTopLbl;
    IBOutlet UILabel                *dummyBottomLbl;

    UIImageView                     *selectedMediaImageView;
//    NSMutableArray *messagesArray;
    int lastSelectedRow;
    NSIndexPath* lastSelectedRowIndexPath;
    
    NSString *bubbleImageName_Promo;
    NSString *bubbleImageName_NO_Promo;
    NSString *bubbleImageName_Promo_NoLink;
    NSString *bubbleoutGoingImageName_Promo;
    NSString *bubbleoutGoingImageName_NO_Promo;
    NSString *bubbleoutImageName_NO_PromoHeader;
   
    
    NSFetchedResultsController *messagesFetchResultController;
    NSDateFormatter *timeFormatter;
    UIAlertView *mediaTransferAlertView;
    BOOL isSelectingPromo;
    BOOL isPresentedSheet;
    UIImage *capturedImage;
    
    MBProgressHUD*  progressView;
    
    CMNetManager* netManager;
    CGPoint touchStart;
    CGRect keyboradFrame;
    NSUInteger bubbleItemOrigin;
    
    XMPPStream *xmppStream;
    
    dispatch_queue_t moduleQueue;
    void *moduleQueueTag;
}

@property (strong, nonatomic) IBOutlet UIView *headerViewCustom;
@property (weak, nonatomic) IBOutlet UIImageView *inputbackgroundImageView;

@property (strong, nonatomic) IBOutlet IMPromoSettingsOnView *promoSettingsOnView;
@property (strong, nonatomic) IBOutlet IMPromoSettingsOffView *promoSettingsOffView;

-(IBAction)sendButtonAction:(id)sender;
-(IBAction)sendMediaMessageAction:(id)sender;
- (IBAction)userDidTappedOnShowProfile:(id)sender;


@end

@implementation IMChatViewController
//@synthesize chatMessageCell;
@synthesize unreadMessageCount;
// Shrink a View
-(void)onSwipeLeftGesture: (UIGestureRecognizer*)gesture
{
    [UIView animateWithDuration:0.5 delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:
     ^{
         self.view.transform = CGAffineTransformMakeScale(0.2f, 0.2f); // shrink a View.
         
         CGRect rect = self.view.frame;
         rect.origin.x = 250;
         self.view.frame = rect;
         imgButton.hidden = NO;
     }
    completion:nil
     ];
}

//- (XMPPStream *)xmppStream
//{
//    XMPPStream
//    if (dispatch_get_specific(moduleQueueTag))
//    {
//        return xmppStream;
//    }
//    else
//    {
//        __block XMPPStream *result;
//        
////        dispatch_sync(moduleQueue, ^{
////            result = xmppStream;
////        });
//        
//        return result;
//    }
//}


- (NSFetchedResultsController *)messagesFetchResultController
{
    if (NO == [[CMMessageArchivingCoreDataStorage sharedInstance]isStoreSet]) {
        if (nil == progressView) {
            progressView = [[MBProgressHUD alloc] initWithView: self.view];
            progressView.labelText = @"Please wait ...";
            
            [app.window addSubview: progressView];
            [progressView show:YES];
        }
        
        NSLog(@"Please wait setting up iclod");
        return nil;
    }
	if (messagesFetchResultController == nil)
	{
        if (progressView) {
            [progressView hide:YES];
            progressView = nil;
        }
		NSManagedObjectContext *moc = [[CMMessageArchivingCoreDataStorage sharedInstance] mainThreadManagedObjectContext];
		if(!moc){
            return nil;
        }
		NSEntityDescription *entity = [NSEntityDescription entityForName:@"CMMessageArchiving_Message_CoreDataObject"
		                                          inManagedObjectContext:moc];
		
		NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES];
		
		NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, nil];
		
		NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
		[fetchRequest setEntity:entity];
		[fetchRequest setSortDescriptors:sortDescriptors];

        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bareJidStr = %@ AND hostName = %@ ",self.userJid, HOST_OPENFIRE];

		[fetchRequest setPredicate:predicate];
        if (nil == messagesFetchResultController) {
            messagesFetchResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                managedObjectContext:moc
                                                                                  sectionNameKeyPath:@"dateSectionTitle"
                                                                                           cacheName:nil];
            messagesFetchResultController.delegate = self;
        }

		NSError *error = nil;
		if (![messagesFetchResultController performFetch:&error])
		{
			DDLogError(@"Error performing fetch: %@", error);
		}
        
	}
   
	
	return messagesFetchResultController;
}

-(void)imgBtnClick:(id)sender
{
    [UIView animateWithDuration:.5
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:
     ^{
         self.view.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
         self.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
         imgButton.hidden = YES;
     }
                     completion:nil
     ];
}


- (void)reloadFetchedResults: (NSNotification*)notif
{
    messagesFetchResultController.delegate = nil;
    messagesFetchResultController = nil;
    [chatTableVW reloadData];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}



-(UIImage *)makeRoundedImage:(UIImage *) image radius: (float) radius;
{
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}



- (void)viewDidLoad
{
       [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
     [self notificationCount];
    
    firstTime = false;
    CGRect frame = messageView.frame;
    NSLog(@"%@",NSStringFromCGRect(frame));
    keyboard = true;
    [super viewDidLoad];
    self.title = @"";
    font_sizeOthers = 18.0;
    bonds = 216;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadFetchedResults:)
                                                 name:@"SomethingChanged"
                                               object:nil];
   
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(appDidEnterForeground) name:@"appDidEnterForeground" object:nil];
    
//    NSNotificationCenter *center1 = [NSNotificationCenter defaultCenter];
//    [center1 addObserver:self selector:@selector(appDidEnterTerminate) name:@"appWillTerminate" object:nil];
    
    messagesFetchResultController = nil;
    self.promoSettingsOnView.mainTableView = chatTableVW;
    [CMChatManager sharedInstance].messagesDelegate = self;

    font_size = [[NSUserDefaults standardUserDefaults] integerForKey:k_DEFAULTS_KEY_FONT_SIZE];
    
    if (font_size < k_NORMAL_FONT_SIZE) {
        font_size = k_NORMAL_FONT_SIZE;
    }
    
    //SS TODO get firend's bubble color
     bubbleoutGoingImageName_Promo = @"bubble_white_no_footer.png";
     bubbleoutGoingImageName_NO_Promo = @"bubble_white_no_ad.png";
     bubbleImageName_Promo = @"bubble_blue_ad.png";
    bubbleoutImageName_NO_PromoHeader=@"bubble_blue_ad_noHeader";
    bubbleImageName_Promo_NoLink = @"bubble_blue_ad_noLink.png";
     bubbleImageName_NO_Promo = @"bubble_blue_no_ad.png";
    
    __block int bubbleColor = 1;
    
   
    
   
    NSLog(@"%@",self.userJid);
    NSString *jid = self.userJid;
    jid = [jid stringByReplacingOccurrencesOfString:@"@imgrapp.com" withString:@""];
    NSString *post = [NSString stringWithFormat:@"apiKey=imgr&jid=%@",jid];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    
    NSString *urlString=[NSString stringWithFormat:@"http://imgrapp.com/release/index.php/ws/appuser/getbubble"];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
   
    
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data == nil) {
            
        }else  {
            id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&connectionError];
            
            NSLog(@"%@",result);
            
            
            
            bubbleColor = [[result objectForKey:@"color"] integerValue];
        }
        NSLog(@"received vcard color = %d",bubbleColor);
        bubbleColor -= 1;
        if (bubbleColor < 0) {
            bubbleColor = 0;
        }
        //        UIImage *image = [vCard objectForKey:@"User_Image"];
        //        if(image == nil)
        //           image  = [IMUtils circularScaleNCrop:[UIImage imageNamed: @"img_contact_placeholder@2x.png"] rect:CGRectMake(0, 0, 40 , 40)];
        //
        //        [rightBarButton setImage:image forState:UIControlStateNormal];
        NSLog(@"bubbleColor = %d",bubbleColor);
        if ([[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_OVERRIDE_FRIEND_BUBBLE_COLOR]) {
            bubbleImageName_Promo = @"bubble_blue_ad.png";
            bubbleImageName_NO_Promo = @"bubble_blue_no_ad.png";
        }
        else{
            if (0 == bubbleColor) {
                bubbleImageName_Promo = @"bubble_blue_ad.png";
                bubbleImageName_NO_Promo = @"bubble_blue_no_ad.png";
                bubbleImageName_Promo_NoLink = @"bubble_blue_ad_noLink.png";
                bubbleoutImageName_NO_PromoHeader=@"bubble_blue_ad_noHeader";
            }
            else if (1 == bubbleColor)
            {
                bubbleImageName_Promo = @"bubble_green_ad.png";
                bubbleImageName_NO_Promo = @"bubble_green_no_ad.png";
                bubbleImageName_Promo_NoLink = @"bubble_green_ad_noLink.png";
                bubbleoutImageName_NO_PromoHeader=@"bubble_green_ad_noHeader";
            }
            else
            {
                bubbleImageName_Promo = @"bubble_purple_ad.png";
                bubbleImageName_NO_Promo = @"bubble_purple_no_ad.png";
                bubbleImageName_Promo_NoLink = @"bubble_purple_ad_noLink.png";
                bubbleoutImageName_NO_PromoHeader=@"bubble_purple_ad_noHeader";
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [chatTableVW reloadData];
        });
        
    }];
    
    
    
   
    
    //************************ Code for shrink a View *************************
    
//    UISwipeGestureRecognizer* swipeLeftGR = [[UISwipeGestureRecognizer alloc] initWithTarget: self action: @selector(onSwipeLeftGesture:)];
//    swipeLeftGR.direction = UISwipeGestureRecognizerDirectionRight;
//    [self.view addGestureRecognizer: swipeLeftGR];
   
    //SS don't know why this button was added as full screen child view to self view.
//    imgButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    imgButton.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    [imgButton addTarget:self action:@selector(imgBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:imgButton];

    //imgButton.hidden = YES;
    //*************************************************************************
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardFrameWillChange:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
   
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardOnScreen:) name:UIKeyboardDidShowNotification object:nil];
    if([IMUtils getOSVersion] >= 7.0)
    {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            [self setEdgesForExtendedLayout:UIRectEdgeRight];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    UIImage* image = [[CMChatManager sharedInstance] fetchUserPhotoFrom:HOST_OPENFIRE forUser:self.userJid];
    
    if(image == nil)
        [rightBarButton setImage:[UIImage imageNamed: @"img_contact_placeholder@2x.png"] forState: UIControlStateNormal];
    else {
        
        [rightBarButton setImage: [self makeRoundedImage:image radius:image.size.width/2] forState: UIControlStateNormal];
    }
    
    [self refreshTitleView];

//    font_size = MINIMUM_FONT;
    chatTableVW.backgroundColor = [UIColor clearColor];
    chatTableVW.backgroundView = nil;
    chatTableVW.delegate = self;
    chatTableVW.dataSource = self;
    timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh:mm a"];

//    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
//    [messageTextField addGestureRecognizer:swipe];
    
  

//    chatTableVW.tableHeaderView = self.promoSettingsOnView;
    
    
}
//- (void)resignOnSwipe:(id)sender {
//    [messageTextField resignFirstResponder];
//}
/*-(void)keyboardOnScreen:(NSNotification *)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    //CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    CGRect rawFrame      = [keyboardFrameBegin CGRectValue];
    //CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    keyboradFrame=rawFrame;
    NSLog(@"keyboardFrame rawframe=%@",NSStringFromCGRect(rawFrame));
}*/

-(void)appDidEnterForeground{
    [self notificationCount];
    NSLog(@"back");
}
-(void)appDidEnterTerminate{
    [self notificationCount];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
     touchStart = [touch locationInView:self.view];
    NSLog(@"touchStart=%f touchStart=%f",touchStart.x,touchStart.y);
    }


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint touchLocation = [[touches anyObject] locationInView:self.view];
     NSLog(@"touchLocation=%f touchLocation=%f",touchLocation.x,touchLocation.y);
    if (CGRectContainsPoint(messageView.frame, touchLocation)) {
        if(touchLocation.y>touchStart.y)
        {
         [messageTextField resignFirstResponder];
            keyboard=true;
            bonds=216;
        }
        
        return;
        
    }
}


-(void)refreshViewForSwitchChat
{
    //SS TODO get firend's bubble color
    bubbleoutGoingImageName_Promo = @"bubble_white_no_footer.png";
    bubbleoutGoingImageName_NO_Promo = @"bubble_white_no_ad.png";
    bubbleImageName_Promo = @"bubble_blue_ad.png";
    bubbleImageName_Promo_NoLink = @"bubble_blue_ad_noLink.png";
    bubbleImageName_NO_Promo = @"bubble_blue_no_ad.png";
    __block int bubbleColor = 1;
    
    UIImage* image = [[CMChatManager sharedInstance] fetchUserPhotoFrom:HOST_OPENFIRE forUser:self.userJid];
    
    if(image == nil)
        [rightBarButton setImage:[UIImage imageNamed: @"img_contact_placeholder@2x.png"] forState: UIControlStateNormal];
    else
        [rightBarButton setImage:[self makeRoundedImage:image radius:40] forState: UIControlStateNormal];
    
    
   
    IMContacts *contact = [[IMDataHandler sharedInstance] fetchContactWithUserJid:self.userJid];
    self.userInfo = contact;
    NSString *displayName = nil;
    if (contact.first_name) {
        
        displayName = contact.first_name;
    }
    else
    {
        if (self.userJid.length > k_DEFAULT_NUMBER_CHARACTERS_COUNT) {
            displayName = [self.userJid substringToIndex:k_DEFAULT_NUMBER_CHARACTERS_COUNT];
        }
        else
        {
            displayName = NSLocalizedString(@"No Name", @"No Name");
        }
        
    }
    self.title = displayName;
    [self refresh];
}

- (void)refreshTitleView {
    
    IMContacts *contact = [[IMDataHandler sharedInstance] fetchContactWithUserJid:self.userJid];
    self.userInfo = contact;
    
    NSString *displayName = nil;
    if (self.userInfo.first_name.length) {
        
        displayName = self.userInfo.first_name;
    }
    else if (self.userInfo.last_name.length) {
        displayName = self.userInfo.last_name;
    }
    else
    {
        if (self.userJid.length > k_DEFAULT_NUMBER_CHARACTERS_COUNT) {
            displayName = [self.userJid substringToIndex:k_DEFAULT_NUMBER_CHARACTERS_COUNT];
        }
        else
        {
            displayName = NSLocalizedString(@"No Name", @"No Name");
        }
        
    }
    self.title = displayName;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshTitleView];
     NSLog(@" messageTextField.autocorrectionType=%ld",(long)messageTextField.autocorrectionType);
//    messageTextField.autocorrectionType=UITextAutocorrectionTypeNo;
//    messageTextField.spellCheckingType=UITextSpellCheckingTypeYes;
    
    
    
   // messageTextField.autocorrectionType=UITextAutocorrectionTypeNo;
  /*  if(messageTextField.autocorrectionType)
    {
        NSLog(@"YES messageTextField.autocorrectionType=%d",messageTextField.autocorrectionType);
    }
    else
    {
        NSLog(@"NO messageTextField.autocorrectionType=%d",messageTextField.autocorrectionType);
    }*/

    messagesFetchResultController.delegate = self;

    messageTextField.font = [IMUtils appFontWithSize:font_size];
    if (NO == [[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_PROMOS] || NO == self.userInfo.promo_enabled.boolValue) {
        
        [self.view addSubview:self.promoSettingsOffView];
        self.promoSettingsOffView.frame = self.promoSettingsOnView.frame;
        [self.promoSettingsOnView removeFromSuperview];
    }
    else
    {
        [self.view addSubview:self.promoSettingsOnView];
        [self.promoSettingsOffView removeFromSuperview];
        
        self.promoSettingsOnView.personalPromosEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_ROTATE_PERSONAL] & self.userInfo.personal_promo_enabled.boolValue;
        self.promoSettingsOnView.sponsoredAdsEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_ROTATE_SPONSORED] & self.userInfo.sponsored_ads_enabled.boolValue;
    }
    if (NO == isSelectingPromo) {
        [self.promoSettingsOnView setSelectedContact:self.userInfo];
        [self.promoSettingsOnView controlllerWillAppear];// Setup fetch result controller
    }
    isSelectingPromo = NO;
}

- (void)stopLoader {
    
    [progressView hide:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [chatTableVW reloadData];
//    if (nil == progressView) {
//        if([messagesFetchResultController.fetchedObjects count] > 15)
//        {
//            progressView = [[MBProgressHUD alloc] initWithView: self.view];
//            progressView.labelText = @"Please wait ...";
//            
//            [app.window addSubview: progressView];
//            [progressView show:YES];
//            
//            [self performSelector:@selector(stopLoader) withObject:nil afterDelay:.75];
//        }
   // }
    
    [self scrollToBottom];
    
    if([IMUtils getOSVersion] >= 7.0) {
        messageTextField.layoutManager.allowsNonContiguousLayout = NO;
        messageTextField.contentInset = UIEdgeInsetsMake(0.0, 0.0, 1.0, 0.0);
    }
    else
        messageTextField.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    keyboard=true;
    bonds=216;
    self.userInfo.last_promo_id = self.promoSettingsOnView.selectedPromo.promo_id;
    [[IMDataHandler sharedInstance] saveDatabase];
    //Call web service to decrease badge count
    messagesFetchResultController.delegate = nil;
    unreadMessageCount = [[CMChatManager sharedInstance] unreadMessageCountWithHost:HOST_OPENFIRE forJID:self.userJid];
   if(unreadMessageCount >=0)
    {
       [self decreaseBadgeCount];
        [[CMChatManager sharedInstance] resetUnreadMessageCountWithHost:HOST_OPENFIRE forJID:self.userJid];
   }
    [self.promoSettingsOnView controlllerWillDisappear];// teardown
    
    netManager.delegate = nil;
}

-(void)notificationCount{
    [[CMChatManager sharedInstance] resetUnreadMessageCountWithHost:HOST_OPENFIRE forJID:self.userJid];
    unreadMessageCount = [[CMChatManager sharedInstance] unreadMessageCountWithHost:HOST_OPENFIRE forJID:self.userJid];
    if(unreadMessageCount >=0)
    {
        [self decreaseBadgeCount];
        [[CMChatManager sharedInstance] resetUnreadMessageCountWithHost:HOST_OPENFIRE forJID:self.userJid];
    }
//    IMMessagesListViewController *messVC = [[IMMessagesListViewController alloc]init];
//    [messVC messagesContactFetchResultController];
    
}


-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Webservice methods
- (void)sendMessageOnServer: (NSString*)messageStr
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString* jidString = [[IMAppDelegate sharedDelegate] getUserID];
    DebugLog(@"jidString = %@", jidString);
    
    [parameters setObject:jidString forKey: @"sender_id"];
    DebugLog(@"Receiver jidString = %@", self.userJid);
    [parameters setObject:[IMUtils phoneNumberForJid: self.userJid] forKey: @"receiver_id"];
    [parameters setObject:@"1" forKey: @"type"];
    [parameters setObject:@"1.2" forKey:@"version"];
//    [parameters setObject:@"" forKey:@"message_id"];
//    [parameters setObject:@"" forKey:@"message"];
//    [parameters setObject:@"" forKey:@"messagePayload"];
    
    
    NSString* messageString = nil;
    if(messageStr.length > 80)
        messageString = [messageStr substringToIndex: 80];
    else
        messageString = messageStr;
    
    [parameters setObject:messageString forKey: @"message"];
    
    netManager.delegate = nil;
    DebugLog(@"parameters = %@, %@", parameters, [NSString stringWithFormat:@"%@%@", KBASEURL, kPushNotification]);
    netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kPushNotification] parameters:parameters queueIdentifier:"sendMessageQueue" queueType:CONCURRENT httpMethod: GET];
    netManager.delegate = self;
    netManager.tag = E_SEND_MESSAGE_SERVICE;
    [netManager startRequest];
}

- (void)decreaseBadgeCount 
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString* deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:K_DEVICE_TOKEN];
    if(deviceToken != nil)
        [parameters setObject: deviceToken forKey: @"deviceToken"];
    
   self.unreadMessageCount = [[CMChatManager sharedInstance] unreadMessageCountWithHost:HOST_OPENFIRE forJID:self.userJid];

    [parameters setObject:[NSString stringWithFormat: @"%d", unreadMessageCount] forKey: @"unreadMessageCount"];
    
    NSInteger badgeNumber = [[CMChatManager sharedInstance] unreadMessageCountForAllBuddiesWithHost:HOST_OPENFIRE] - unreadMessageCount;
    
    [parameters setObject:[NSString stringWithFormat: @"%ld", (long)badgeNumber] forKey: @"badgeNumber"];
    
    NSString* username = [[NSUserDefaults standardUserDefaults] objectForKey: k_DEFAULTS_KEY_USER_ID];
    [parameters setObject:username forKey: @"username"];
    
//    NSInteger badgeCount = [[UIApplication sharedApplication] applicationIconBadgeNumber];
   // DebugLog(@"badgeNumber = %ld, unreadMessageCount = %d",(long)badgeNumber,unreadMessageCount);

//    badgeCount =badgeNumber - unreadMessageCount;
//    DebugLog(@"Unread badge count = %d",badgeCount);

  //  [[UIApplication sharedApplication] setApplicationIconBadgeNumber: badgeNumber];
    
   // DebugLog(@"Badge parameters = %@, %@", parameters, [NSString stringWithFormat:@"%@%@", KBASEURL,kDecreaseBadgeCount]);
    
    CMNetManager* netManager1 = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kDecreaseBadgeCount] parameters:parameters queueIdentifier:"decreaseBadgeCountQueue" queueType:CONCURRENT httpMethod: GET];
    //netManager1.delegate = self;
    netManager1.tag = E_DECREASE_BADGE_COUNT_SERVICE;
    [netManager1 startRequest];
}

- (void)refresh
{
    messagesFetchResultController.delegate = nil;
    messagesFetchResultController = nil;
    [CMChatManager sharedInstance].messagesDelegate = self;
    [self scrollToBottom];
    
    [chatTableVW reloadData];
}
#pragma mark - CMNetManager Delegate methods
//This delegate gets called on success
-(void)netManager:(CMNetManager*)inputManager didReceiveResponse:(NSDictionary*)dictionary
{
    DebugLog(@"Response = %@", dictionary);
    
    switch (inputManager.tag)
    {
        case E_SEND_MESSAGE_SERVICE:
        {
            if ([dictionary objectForKey:@"success"] && [[dictionary objectForKey:@"success"] integerValue] == 0) {
                
                int code = [[[dictionary objectForKey: @"error"] objectForKey: @"errorCode"] intValue];
                if(code == 1006) {
                    UIAlertView* invalidUserAlertView = [[UIAlertView alloc] initWithTitle: @"" message:NSLocalizedString(@"INVALID USER", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
                    [invalidUserAlertView show];
                }
            }
           
            
        }break;
            
        case E_DECREASE_BADGE_COUNT_SERVICE:
        {
            
        }break;
          
             case E_CHECK_IMGR_SERVICE:
        {
            NSLog(@"%@",[dictionary objectForKey:@"sucess"]);
//            if ([dictionary objectForKey:@"success"] && [[dictionary objectForKey:@"sucess"]integerValue] == 1) {
            
                
                  NSInteger  bubbleColor = [[dictionary objectForKey:@"color"] integerValue];
                    NSLog(@"received vcard color = %ld",(long)bubbleColor);
                    bubbleColor -= 1;
                    if (bubbleColor < 0) {
                        bubbleColor = 0;
                    }
                    //        UIImage *image = [vCard objectForKey:@"User_Image"];
                    //        if(image == nil)
                    //            image  = [IMUtils circularScaleNCrop:[UIImage imageNamed: @"img_contact_placeholder@2x.png"] rect:CGRectMake(0, 0, 40 , 40)];
                    //
                    //        [rightBarButton setImage:image forState:UIControlStateNormal];
                    NSLog(@"bubbleColor = %ld",(long)bubbleColor);
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_OVERRIDE_FRIEND_BUBBLE_COLOR]) {
                        bubbleImageName_Promo = @"bubble_blue_ad.png";
                        bubbleImageName_NO_Promo = @"bubble_blue_no_ad.png";
                    }
                    else{
                        if (0 == bubbleColor) {
                            bubbleImageName_Promo = @"bubble_blue_ad.png";
                            bubbleImageName_NO_Promo = @"bubble_blue_no_ad.png";
                            bubbleImageName_Promo_NoLink = @"bubble_blue_ad_noLink.png";
                        }
                        else if (1 == bubbleColor)
                        {
                            bubbleImageName_Promo = @"bubble_green_ad.png";
                            bubbleImageName_NO_Promo = @"bubble_green_no_ad.png";
                            bubbleImageName_Promo_NoLink = @"bubble_green_ad_noLink.png";
                        }
                        else
                        {
                            bubbleImageName_Promo = @"bubble_purple_ad.png";
                            bubbleImageName_NO_Promo = @"bubble_purple_no_ad.png";
                            bubbleImageName_Promo_NoLink = @"bubble_purple_ad_noLink.png";
                        }
                    //}
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [chatTableVW reloadData];
                    });
                
                
            }
        }
        default:
            break;
    }
}

-(void)netManager:(CMNetManager*)inputManager didReceiveResponseString:(NSString*)response
{
    DebugLog(@"didReceiveResponseString=%@",response);
}

//This delegate gets called on failure
-(void)netManager:(CMNetManager*)inputManager didFailWithError:(NSError*)error
{
    DebugLog(@"didFailWithError");
}

-(void)netManager:(CMNetManager *)inputManager didGetSuccessToConnectWithServer:(NSError *)error
{
    DebugLog(@"didGetSuccessToConnectWithServer");
}

//This delegate gets called if internet connection is not available
-(void)netManager:(CMNetManager *)inputManager didFailToConnectWithServer:(NSError *)error
{
    DebugLog(@"didFailToConnectWithServer");
}

-(IBAction)sendButtonAction:(id)sender
{
    CMStream *stream = [[CMChatManager sharedInstance] getStreamForHost:HOST_OPENFIRE];
    
    if ([stream isConnected])
    {
        
       
    //Send image message
    if (capturedImage) {
            [[CMChatManager sharedInstance] media:UIImagePNGRepresentation(capturedImage) ofType:@"png" withTag:100 transferMediaForHost:HOST_OPENFIRE toJID:self.userJid previewImage:[IMChatViewController imageWithImage:capturedImage scaledToSize:CGSizeMake(250, 250)]];
        
        [chooseImageButton setImage:[UIImage imageNamed:CAMERA_ICON_NAME] forState:UIControlStateNormal];
    }
    
    //send text message
    if ([messageTextField.text length]) {
        NSString *messageText = messageTextField.text;
        
        messageText = [NSString emoticonizedString:messageText];
       
        IMPromos *selectedPromo = nil;
        if (NO == [[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_PROMOS] || NO == self.userInfo.promo_enabled.boolValue)
        {
            selectedPromo = [self.promoSettingsOffView selectedPromo];
            [self.promoSettingsOffView userDidTappedRemovePromo:nil];
        }
        else
        {
            if (self.promoSettingsOnView.isPromoEnabled) {
                selectedPromo = [self.promoSettingsOnView selectedPromo];
                if ([[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_ROTATE_ENABLED] && self.userInfo.auto_rotation_enabled.boolValue) {
                    [self.promoSettingsOnView stepAheadRandom];//Added random promo change service
                }
            }
            else
            {
                self.promoSettingsOnView.isPromoEnabled = YES;
            }
            
        }
        
        NSString* pushString = nil;
        if(capturedImage)
            pushString = @"image";
        else
            pushString = messageTextField.text;
        //    NSData *imagedata = UIImageJPEGRepresentation(image,0.75);
        
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:@[messageText, [NSString stringWithFormat:@"%ld",(long)selectedPromo.promo_id.integerValue], pushString] forKeys:@[MESSAGE_BODY_TAG,PROMO_ID_TAG, @"pushkey"]];
       
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
        
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@",self.userJid);
        
        NSLog(@"%@ %@ %@",HOST_OPENFIRE,self.userJid,jsonString);
        NSLog(@"%@",dictionary);
        
        xmppStream =   [[CMChatManager sharedInstance]getStreamForHost:HOST_OPENFIRE];
        NSLog(@"%@",xmppStream.myJID);
       
        
        NSXMLElement *presence = [NSXMLElement elementWithName:@"presence"];
//        [presence addAttributeWithName:@"to" stringValue:self.userJid ];
//        [presence addAttributeWithName:@"type" stringValue:@"subscribe"];
        
        [xmppStream sendElement:presence];
        
        [[CMChatManager sharedInstance] sendMessageWithHost:HOST_OPENFIRE forJID:self.userJid message:jsonString];//SS TODO
        
        //[[CMChatManager sharedInstance]goOfflineForHost:HOST_OPENFIRE];
        
        messageTextField.text = @"";
        
        [self adjustFrames:messageTextField];
    }
        
        capturedImage = nil;//Make it empty
        
    }

    else
    {
    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"WAIT FOR NETWORK",@"") message:NSLocalizedString(@"CHECK INTERNET CONNECTION", @"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
       
    
    }
    
}




- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    // This method is invoked on the moduleQueue.
    
    // XEP-0115 presence:
    //
    // <presence from="romeo@montague.lit/orchard">
    //   <c xmlns="http://jabber.org/protocol/caps"
    //       hash="sha-1"
    //       node="http://code.google.com/p/exodus"
    //       ver="QgayPKawpkPSDYmwT/WM94uA1u0="/>
    // </presence>
    
    NSString *type = [presence type];
    
    XMPPJID *myJID = xmppStream.myJID;
    if ([myJID isEqual:[presence from]])
    {
        // Our own presence is being reflected back to us.
        return;
    }
    
    if ([type isEqualToString:@"unavailable"])
    {
       
    }
    else if ([type isEqualToString:@"available"])
    {
    }
}


#pragma mark - CMChatmanager Delegate methods
- (void)forHost:(NSString *)hostName forJID:(NSString*)JID didSendMessage:(CMMessageArchiving_Message_CoreDataObject*)message
{
//    if (message.mediaThumbnailPath)
//        [self sendMessageOnServer: NSLocalizedString(@"MEDIA MESSAGE", @"")];
//    else
//     [self sendMessageOnServer: message.body];
}

-(IBAction)sendMediaMessageAction:(id)sender
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:(nil == capturedImage)?nil:NSLocalizedString(@"Remove Image", @"Remove Image") otherButtonTitles:
                            @"Take Photo",
                            @"Choose Existing",
                            nil];
    isPresentedSheet = YES;
    [popup showInView:chatTableVW];
}

- (IBAction)userDidTappedOnShowProfile:(id)sender {
    if (nil == self.userInfo) {
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"NO CONTACT FOUND", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", @"") otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil];
        alertView.tag = NO_CONTACT_ALERT;
        [alertView show];
        return;
    }
    IMContactDetailViewController *contactDetails = [[[IMAppDelegate sharedDelegate] contactsStoryboard] instantiateViewControllerWithIdentifier:@"IMContactDetailViewController"];
    contactDetails.contact = self.userInfo;
    contactDetails.comingFromChatScreen = YES;
    [self.navigationController pushViewController:contactDetails animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    switch (buttonIndex)
//    {
//        case 1:
//            [messageTextField resignFirstResponder];
//
//            break;
//            
//        case 0:
//            [messageTextField resignFirstResponder];
// 
//            break;
//            
//        default:
//            break;
//    }

 }

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex;  // after animation
{
    isPresentedSheet = NO;
    NSUInteger deltaValue = (nil != capturedImage);
    buttonIndex -= deltaValue;
    
    switch (buttonIndex)
    {
        case 1:
             [messageTextField resignFirstResponder];
             keyboard=true;
            bonds=216;
            [[CMPhotoPickerController sharedInstance] displayImagePickerWithType:1 target:self];
            [self.navigationController presentViewController: [CMPhotoPickerController sharedInstance] animated: YES completion: nil];
            break;
            
        case 0:
            [messageTextField resignFirstResponder];
             keyboard=true;
            bonds=216;
            [[CMPhotoPickerController sharedInstance] displayImagePickerWithType:2 target:self];
            [self.navigationController presentViewController: [CMPhotoPickerController sharedInstance] animated: YES completion: nil];
            break;
        case -1:
        {
            capturedImage = nil;//Make it empty
            [chooseImageButton setImage:[UIImage imageNamed:CAMERA_ICON_NAME] forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }

}


#pragma mark - UIImagePicker Delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    if([mediaType isEqualToString:@"public.image"])
    {
        capturedImage = [info valueForKey: UIImagePickerControllerEditedImage];
        [chooseImageButton setImage:capturedImage forState:UIControlStateNormal];

//SS we have planned to send image on send button click, instead of sending on choose image
//        mediaTransferAlertView = [[UIAlertView alloc] initWithTitle:@"Sending Image" message:@"Please wait sending image." delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
//        [mediaTransferAlertView show];
//        [[CMChatManager sharedInstance] media:UIImagePNGRepresentation(capturedImage) ofType:@"png" withTag:100 transferMediaForHost:HOST_OPENFIRE toJID:self.userJid previewImage:[IMChatViewController imageWithImage:capturedImage scaledToSize:CGSizeMake(150, 150)]];
//        UIImage* image = [IMUtils circularScaleNCrop:newImage rect: CGRectMake(0, 0, 80, 80)];
        
        //[self sendMessageOnServer: NSLocalizedString(@"MEDIA MESSAGE", @"")];
//        self.userProfileImageView.image = image;
    }
    else if ( [ mediaType isEqualToString:@"public.movie" ])
    {
        
    }
    [picker dismissViewControllerAnimated:YES completion: nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion: nil];
}

+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


//Find number of characters from a string that fits in a label
- (int)getSplitIndexWithString:(NSString *)str frame:(CGRect)frame andFont:(UIFont *)font
{
    str = [str stringByAppendingString:@" "];
    int length = 1;
    int lastSpace = 1;
    NSString *cutText = [str substringToIndex:length];
    
    CGSize textSize;
    if([IMUtils getOSVersion] >= 7.0)
    {
        CGRect textRect = [cutText boundingRectWithSize:CGSizeMake(frame.size.width, frame.size.height + MAX_ROW_HEIGHT)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:font}
                                             context:nil];
        textSize = textRect.size;
    }
    else
    {
        CGSize rect1 = [cutText sizeWithFont:font constrainedToSize:CGSizeMake(frame.size.width, frame.size.height + MAX_ROW_HEIGHT) lineBreakMode:NSLineBreakByWordWrapping];
        textSize = CGSizeMake(ceilf(rect1.width), ceilf(rect1.height));
    }
    
    while (textSize.height <= frame.size.height)
    {
        if(length < [str length])
        {
            NSRange range = NSMakeRange (length, 1);
            if ([[str substringWithRange:range] isEqualToString:@" "])
            {
                lastSpace = length;
            }
            length++;
            cutText = [str substringToIndex:length];
            if([IMUtils getOSVersion] >= 7.0)
            {
                CGRect textRect = [cutText boundingRectWithSize:CGSizeMake(frame.size.width, frame.size.height + MAX_ROW_HEIGHT)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                     attributes:@{NSFontAttributeName:font}
                                                        context:nil];
                textSize = textRect.size;
            }
            else
            {
                CGSize rect1 = [cutText sizeWithFont:font constrainedToSize:CGSizeMake(frame.size.width, frame.size.height + 1000) lineBreakMode:NSLineBreakByWordWrapping];
                textSize = CGSizeMake(ceilf(rect1.width), ceilf(rect1.height));
            }
        }
        else
            break;
    }
    return lastSpace;
}


#pragma mark - table view data source methods

static CGFloat padding = 20.0;

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [chatTableVW beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [chatTableVW endUpdates];
    [self scrollToBottom];
    
}

- (void) scrollToBottom
{
    NSUInteger sections  = [[[self messagesFetchResultController] sections] count];
    if (sections) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self messagesFetchResultController] sections][sections - 1];
        NSUInteger lastCell = [sectionInfo numberOfObjects];
        if (lastCell) {
            NSIndexPath* ipath = [NSIndexPath indexPathForRow: lastCell-1 inSection: sections-1];
            [chatTableVW scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
        }
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [chatTableVW insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [chatTableVW deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
      [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    if(unreadMessageCount >=0)
    {
        [self decreaseBadgeCount];
        [[CMChatManager sharedInstance] resetUnreadMessageCountWithHost:HOST_OPENFIRE forJID:self.userJid];
    }
    
    UITableView *tableView = chatTableVW;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            //[tableView scrollToRowAtIndexPath:newIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];

            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [chatTableVW reloadData];
//            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    id <NSFetchedResultsSectionInfo> sectionInfo = [[self messagesFetchResultController] sections][section];
//    return [sectionInfo name];
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 17;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return f
//}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    IMChatDateHeader *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[IMChatDateHeader cellReusableIdentifier]];
    if (nil == cell) {
        cell = [IMChatDateHeader cellLoadedFromNibFile];
    }
    //    cell.headerLabel.text = [NSString stringWithFormat:@"Header %d",section];
    cell.dateLabel.text = [[[self messagesFetchResultController].sections objectAtIndex: section] name];
    
    return cell;
}

- (IMMediaMessageCell*)mediaMessageCell:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath messageInfo: (CMMessageArchiving_Message_CoreDataObject*)msgInfo
{
        IMMediaMessageCell *mediaCell = (IMMediaMessageCell*)[tableView dequeueReusableCellWithIdentifier:[IMMediaMessageCell cellReusableIdentifier]];
        if (mediaCell == nil) {
            mediaCell = [IMMediaMessageCell cellLoadedFromNibFile];
            UITapGestureRecognizer* singleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(saveImageInPhotoGallery:)];
            mediaCell.userInteractionEnabled = YES;
            mediaCell.mediaImageView.userInteractionEnabled = YES;
            mediaCell.mediaImageView.contentMode = UIViewContentModeScaleAspectFit;
            singleTapGesture.numberOfTapsRequired = 1;
            [mediaCell addGestureRecognizer: singleTapGesture];
        }
  
        mediaCell.message = msgInfo;
        mediaCell.messageDeliveredImage.image = nil;

        //mediaCell.tag = indexPath.row;
        mediaCell.lastSelectedRowIndexPath = indexPath;
        
        mediaCell.messageTimeLabel.text = [timeFormatter stringFromDate:msgInfo.timestamp];
        mediaCell.mediaImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    
        UIImage* image = [UIImage imageWithContentsOfFile:msgInfo.mediaThumbnailPath];
        if(image == nil) {
            
            NSString* messageBody = msgInfo.body;
            
            if ([messageBody rangeOfString:@"server_URL"].location != NSNotFound && [messageBody rangeOfString:@"preview_Icon"].location != NSNotFound) {//now We are sure that it is a media file
                
                NSData *jsonData = [messageBody dataUsingEncoding:NSUTF8StringEncoding];
                NSError *error = nil;
                
                NSDictionary *jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
                NSString *imageString = [jsonObjects valueForKey:@"preview_Icon"];
                NSString *serverURL = [jsonObjects valueForKey:@"server_URL"];
                
                NSArray *link = [serverURL componentsSeparatedByString:@"="];
                if([link count] > 1)
                {
                    DDLogInfo(@"Encodedlink: %@",[link objectAtIndex:1]);
                    NSString *decoded = [[link objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSData *base64DecodedData = [NSData dataFromBase64String:decoded];
                    NSString *base64DecodedString = [[NSString alloc] initWithData:base64DecodedData encoding:NSUTF8StringEncoding];
                    DDLogInfo(@"DecodeLink: %@", base64DecodedString);
                }
                
                NSData *imageData = [NSData dataFromBase64String:imageString];
                [imageData writeToFile:msgInfo.mediaThumbnailPath atomically:NO];
                
                image = [UIImage imageWithData:imageData];
            }
        }
        mediaCell.mediaImageView.image = image;//[UIImage imageWithContentsOfFile:msgInfo.mediaThumbnailPath];
        mediaCell.resendButton.enabled = NO;
        mediaCell.resendButton.hidden = YES;
        mediaCell.messageTimeLabel.textColor = [UIColor blackColor];
        
        if (YES == msgInfo.isOutgoing) {
            
            CGRect mediaVIewFrame = mediaCell.mediaImageView.frame;
            mediaVIewFrame.origin.x = mediaCell.frame.size.width - mediaCell.mediaImageView.frame.size.width - 15;
            mediaCell.mediaImageView.frame = mediaVIewFrame;
            if (NO == msgInfo.isDelivered.boolValue) {
                mediaCell.resendButton.enabled = YES;
                mediaCell.resendButton.hidden = NO;
                mediaCell.messageTimeLabel.textColor = [IMUtils appRedColor];
                mediaCell.messageTimeLabel.text = NSLocalizedString(@"Message not sent", @"Message not sent");

            }
            else
            {
                if (msgInfo.isReceiptReceived.boolValue) {
                    mediaCell.messageDeliveredImage.image = [UIImage imageNamed:@"check_single_green.png"];
                }
                else
                {
                    mediaCell.messageDeliveredImage.image = [UIImage imageNamed:@"check_single.png"];
                }
            }
            mediaCell.messageTimeLabel.textAlignment = NSTextAlignmentRight;
        }
        else
        {
            CGRect mediaVIewFrame = mediaCell.mediaImageView.frame;
            mediaVIewFrame.origin.x = 15;
            mediaCell.mediaImageView.frame = mediaVIewFrame;
            mediaCell.messageTimeLabel.textAlignment = NSTextAlignmentLeft;
            
        }
    
    return mediaCell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView              // Default is 1 if not implemented
{
    return [[[self messagesFetchResultController] sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self messagesFetchResultController] sections][section];
    
    return [sectionInfo numberOfObjects];}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    static NSString *cellIdentifier = @"IMChatMessageCell";

    CMMessageArchiving_Message_CoreDataObject *msgInfo = (CMMessageArchiving_Message_CoreDataObject *)[[self messagesFetchResultController] objectAtIndexPath:indexPath];
    
    NSLog(@"message body= %@",msgInfo.body);
    
    
    IMPromos *promo = nil;
    if (msgInfo.promo_Object) {
        promo = msgInfo.promo_Object;
    }
    else
    {
        promo = [IMDataHandler fetchPromoForPromoID:msgInfo.promo_id];
        msgInfo.promo_Object = promo;
    }
//    NSString *sender = [msgInfo bareJidStr];

    if (msgInfo.mediaThumbnailPath)
        return [self mediaMessageCell:tableView indexPath:indexPath messageInfo: msgInfo];
    
   

    IMChatMessageCell *chatMessageCell = (IMChatMessageCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (chatMessageCell == nil) {
        NSArray *objects = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:nil options:nil];
        for (NSObject *obj in objects)
        {
            if ([obj isKindOfClass:[IMChatMessageCell class]])
            {
                chatMessageCell = (IMChatMessageCell *)obj;
                break;
            }
        }
        [chatMessageCell.offerlbl addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:chatMessageCell action:@selector(userDidTappedOnMessageFooter:)]];
        chatMessageCell.userImageView.contentMode = UIViewContentModeCenter;
        chatMessageCell.chatTableViewController = self;//weak reference

    }
    chatMessageCell.userImageView.image = nil;
    chatMessageCell.messageDeliveredImage.image = nil;
    chatMessageCell.resendButton.enabled = NO;
    chatMessageCell.resendButton.hidden = YES;
    chatMessageCell.cellPromo = promo;
    chatMessageCell.message = msgInfo;
    NSLog(@"message=%@",msgInfo);
    chatMessageCell.messageTimeLabel.text = [timeFormatter stringFromDate:msgInfo.timestamp];
    chatMessageCell.topLbl.font         = [IMUtils appFontWithSize:font_size];
    chatMessageCell.bottomLbl.font      = [IMUtils appFontWithSize:font_size];//Fixed issue 8 from imgr-alpha-bug sheet
    chatMessageCell.companyNamelbl.font = [IMUtils appFontWithSize:font_sizeOthers];
    chatMessageCell.offerlbl.font       = [IMUtils appFontWithSize:font_sizeOthers];
    chatMessageCell.topLbl.tag          = indexPath.row;
    chatMessageCell.bottomLbl.tag       = indexPath.row+MESSAGE_TAG;
    
    chatMessageCell.accessoryType = UITableViewCellAccessoryNone;
	chatMessageCell.userInteractionEnabled = NO;
    chatMessageCell.chatImageBubble.autoresizesSubviews = NO;
    chatMessageCell.chatImageBubble.autoresizingMask = NO;
	chatMessageCell.messageTimeLabel.textColor = [UIColor blackColor];
    
    CGSize size;
    CGSize headerSize;
    CGSize footerSize;
    CGSize stringSize;
    int imageWith;
    
    NSString *message = [[msgInfo body] emoticonizedString];
    NSLog(@"%@",message);
    headerSize = [self findSizeChangeForString:[chatMessageCell.cellPromo promo_message] WithOriginalWidth:MAXIMUM_BUBBLE_WIDTH];
    NSLog(@"headerSize=%f",headerSize.height);
    
    stringSize = [self findSizeChangeForString:message WithOriginalWidth:MAXIMUM_BUBBLE_WIDTH];
    
    if (NO == msgInfo.isOutgoing)// left aligned
    {
        imageWith = RECEIVER_USER_IMAGE_WIDTH;
        footerSize = [self findSizeChangeForString:[chatMessageCell.cellPromo promo_link_text] WithOriginalWidth:MAXIMUM_BUBBLE_WIDTH];
        footerSize.height += 5.0;
        
        size = (headerSize.width>footerSize.width)? headerSize: footerSize;
        if(stringSize.width>size.width)
            size = stringSize;
        
        if(size.width<imageWith)
            size.width = imageWith;
        
        int leftBubbleWidht = size.width+padding+(SENDER_USER_IMAGE_WIDTH*2);
        CGFloat leftBubbleHeight;
        
        chatMessageCell.companyNamelbl.textAlignment = NSTextAlignmentCenter;
        chatMessageCell.offerlbl.textAlignment       = NSTextAlignmentCenter;
        chatMessageCell.offerlbl.hidden = NO;
        if (chatMessageCell.cellPromo)
        {
            int footerMultiplier;
            int marginMultiplier;
            if ([(IMPromos *)msgInfo.promo_Object promo_link].length)
            {
                footerMultiplier = 2;
                marginMultiplier = CHAT_IMAGE_BUBBLE_TOP_MARGIN;
                chatMessageCell.offerlbl.hidden = NO;
            }
            else
            {
                footerMultiplier = 1;
                marginMultiplier = -CHAT_IMAGE_BUBBLE_TOP_MARGIN;
                chatMessageCell.offerlbl.hidden = YES;
            }
        if(leftBubbleWidht > MAXIMUM_BUBBLE_WIDTH)
            leftBubbleWidht = MAXIMUM_BUBBLE_WIDTH;
        
        NSString* promoImage = [[IMUtils getImageFolderPath] stringByAppendingPathComponent: [chatMessageCell.cellPromo.promo_image lastPathComponent]];
            
            UIImage *image = [UIImage imageWithContentsOfFile:promoImage];
            if (image) {
                               chatMessageCell.userImageView.image = image;
                
                if(!promo.promo_message.length)
                {
                    chatMessageCell.userImageView.frame = CGRectIntegral(CGRectMake(padding, 15, image.size.width, image.size.height));
                    bubbleItemOrigin=USER_IMAGE_Y_POS_WITHOUT_HEADER;
                    if(!promo.promo_link.length)
                        bubbleItemOrigin-=5;
                }
                
                else
                {
                    chatMessageCell.userImageView.frame = CGRectIntegral(CGRectMake(padding, USER_IMAGE_Y_POS, image.size.width, image.size.height));
                    bubbleItemOrigin=USER_IMAGE_Y_POS;
                }
                
                
            }
            else {
                chatMessageCell.userImageView.frame = CGRectIntegral(CGRectMake(padding, USER_IMAGE_Y_POS, DUMMY_PROMO_IMAGE_SIZE, DUMMY_PROMO_IMAGE_SIZE));
                chatMessageCell.userImageView.image = [UIImage imageNamed: @"promo_placeholder.png"];
            }
           
            
            
        int labelWidth = leftBubbleWidht - (chatMessageCell.userImageView.frame.size.width + padding) - 15;
        
        
        chatMessageCell.topLbl.numberOfLines = 0;
        chatMessageCell.topLbl.lineBreakMode = NSLineBreakByWordWrapping;
            
            if(!promo.promo_message.length)
                 chatMessageCell.topLbl.frame =CGRectIntegral(CGRectMake(chatMessageCell.userImageView.frame.size.width+padding+5,12, labelWidth, chatMessageCell.userImageView.frame.size.height+font_size));
            else
                chatMessageCell.topLbl.frame = CGRectIntegral(CGRectMake(chatMessageCell.userImageView.frame.size.width+padding+5, USER_IMAGE_Y_POS-3, labelWidth, chatMessageCell.userImageView.frame.size.height+font_size));
        
        NSString* str1 = nil;
        NSString* str2 = nil;
       
        if([message length] != 0)
        {
            int len = [self getSplitIndexWithString:message frame:chatMessageCell.topLbl.frame andFont:chatMessageCell.topLbl.font];
            str1 = [message substringToIndex:len];
            if([message length]>[str1 length])
            {
                str2 = [message substringFromIndex:len+1];
                
                chatMessageCell.topLbl.text = str1;
                [chatMessageCell.topLbl sizeToFit];
                chatMessageCell.topLbl.hidden = NO;
                chatMessageCell.bottomLbl.hidden = NO;
                CGSize size2 = [self findSizeChangeForString:str2 WithOriginalWidth:size.width];
                
                int bottomY;
                if(chatMessageCell.userImageView.frame.size.height>chatMessageCell.topLbl.frame.size.height)
                    bottomY = chatMessageCell.userImageView.frame.origin.y+ chatMessageCell.userImageView.frame.size.height ;
                else
                    bottomY = chatMessageCell.topLbl.frame.origin.y + chatMessageCell.topLbl.frame.size.height;
                
                
                chatMessageCell.bottomLbl.frame = CGRectIntegral(CGRectMake(padding,bottomY, leftBubbleWidht-30, size2.height));
                chatMessageCell.bottomLbl.numberOfLines = 0;
                chatMessageCell.bottomLbl.text = str2;
                [chatMessageCell.bottomLbl sizeToFit];
                
                if(chatMessageCell.userImageView.frame.size.height>chatMessageCell.topLbl.frame.size.height)
                    leftBubbleHeight = chatMessageCell.userImageView.frame.size.height + chatMessageCell.bottomLbl.frame.size.height + ((bubbleItemOrigin-marginMultiplier)*footerMultiplier);
                else
                    leftBubbleHeight = chatMessageCell.topLbl.frame.size.height + chatMessageCell.bottomLbl.frame.size.height + ((bubbleItemOrigin-marginMultiplier)*footerMultiplier);
            }
            else
            {
                
                
                chatMessageCell.topLbl.hidden = NO;
                chatMessageCell.bottomLbl.hidden = YES;
                chatMessageCell.topLbl.text = message;
                [chatMessageCell.topLbl sizeToFit];
                if ([(IMPromos *)msgInfo.promo_Object promo_link].length)
                    marginMultiplier = CHAT_IMAGE_BUBBLE_TOP_MARGIN;
                else
                    marginMultiplier = marginMultiplier-CHAT_IMAGE_BUBBLE_TOP_MARGIN;
                
                if(chatMessageCell.userImageView.frame.size.height>chatMessageCell.topLbl.frame.size.height)
                    leftBubbleHeight = chatMessageCell.userImageView.frame.size.height + ((bubbleItemOrigin-marginMultiplier)*footerMultiplier);
                else
                    leftBubbleHeight = chatMessageCell.topLbl.frame.size.height + ((bubbleItemOrigin-marginMultiplier)*footerMultiplier);
            }
        }
        else
        {
            chatMessageCell.topLbl.hidden = YES;
            chatMessageCell.bottomLbl.hidden = YES;
            leftBubbleHeight = chatMessageCell.userImageView.frame.size.height + ((bubbleItemOrigin-marginMultiplier)*footerMultiplier);
        }
    }
    else //Not Promo
    {
        chatMessageCell.topLbl.hidden = YES;
        chatMessageCell.bottomLbl.hidden = NO;
        CGSize noPromoStringSize = [self findSizeChangeForString:message WithOriginalWidth:MAXIMUM_BUBBLE_WIDTH];
        int widht = noPromoStringSize.width+(padding*2);
        if(widht > MAXIMUM_BUBBLE_WIDTH)
            widht = MAXIMUM_BUBBLE_WIDTH;
        
        CGSize size2 = [self findSizeChangeForString:message WithOriginalWidth:size.width];
        chatMessageCell.bottomLbl.frame = CGRectIntegral(CGRectMake(padding,padding/2, widht-30, size2.height));
        chatMessageCell.bottomLbl.numberOfLines = 0;
        chatMessageCell.bottomLbl.text = message;
        [chatMessageCell.bottomLbl sizeToFit];
        
        leftBubbleWidht  = chatMessageCell.bottomLbl.frame.size.width+padding+padding/2;
        leftBubbleHeight = chatMessageCell.bottomLbl.frame.size.height+5 + padding/2;
        
    }
        [chatMessageCell.chatImageBubble setFrame:CGRectIntegral(CGRectMake(0,
                                                                            CHAT_IMAGE_BUBBLE_TOP_MARGIN,
                                                                            leftBubbleWidht,
                                                                            leftBubbleHeight))];
        [chatMessageCell.companyNamelbl setFrame:CGRectIntegral(CGRectMake(chatMessageCell.chatImageBubble.frame.origin.x+22, ([IMUtils getOSVersion] >= 7.0) ? 2 : 2, chatMessageCell.chatImageBubble.frame.size.width - 35, 30))];
        [chatMessageCell.offerlbl setFrame:CGRectIntegral(CGRectMake(chatMessageCell.chatImageBubble.frame.origin.x+22, chatMessageCell.chatImageBubble.frame.size.height-29, chatMessageCell.chatImageBubble.frame.size.width - 35, 30))];
        chatMessageCell.messageTimeLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        imageWith = SENDER_USER_IMAGE_WIDTH;
        size = headerSize;
        if(stringSize.width>size.width)
            size = stringSize;
        
        if(size.width<imageWith)
            size.width = imageWith;
        
        int rightBubbleWidht = size.width+padding+(RECEIVER_USER_IMAGE_WIDTH*2);
        CGFloat rightBubbleHeight;
        int rightBubbleXPos;

		chatMessageCell.offerlbl.hidden = YES;
        if (chatMessageCell.cellPromo)
        {
        CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
        rightBubbleXPos = screenWidth - size.width - padding - (RECEIVER_USER_IMAGE_WIDTH*2);
        if(rightBubbleWidht > MAXIMUM_BUBBLE_WIDTH)
        {
            rightBubbleXPos = screenWidth-MAXIMUM_BUBBLE_WIDTH;
            rightBubbleWidht = MAXIMUM_BUBBLE_WIDTH;
        }
        
        chatMessageCell.companyNamelbl.textAlignment = NSTextAlignmentCenter;
        chatMessageCell.offerlbl.textAlignment       = NSTextAlignmentCenter;
        
        NSString* promoImage = [[IMUtils getImageFolderPath] stringByAppendingPathComponent: [chatMessageCell.cellPromo.promo_image lastPathComponent]];
        UIImage *image = [UIImage imageWithContentsOfFile:promoImage];
        if (image) {
            
            if(!promo.promo_message.length)
            {
                chatMessageCell.userImageView.frame = CGRectIntegral(CGRectMake(rightBubbleXPos + padding/2, 15, image.size.width, image.size.height));
                bubbleItemOrigin=20;
            }
            
            else{
                 chatMessageCell.userImageView.frame = CGRectIntegral(CGRectMake(rightBubbleXPos + padding/2, USER_IMAGE_Y_POS, image.size.width, image.size.height));
             
                bubbleItemOrigin=USER_IMAGE_Y_POS;
            }
            chatMessageCell.userImageView.image = image;
        }
        else {
            chatMessageCell.userImageView.frame = CGRectIntegral(CGRectMake(rightBubbleXPos + padding/2, USER_IMAGE_Y_POS, DUMMY_PROMO_IMAGE_SIZE, DUMMY_PROMO_IMAGE_SIZE));
            chatMessageCell.userImageView.image = [UIImage imageNamed: @"promo_placeholder.png"];
            bubbleItemOrigin=USER_IMAGE_Y_POS;
        }
        int labelWidth1 = rightBubbleWidht - (chatMessageCell.userImageView.frame.size.width + padding) - 15;
  
                chatMessageCell.topLbl.numberOfLines = 0;
        chatMessageCell.topLbl.lineBreakMode = NSLineBreakByWordWrapping;
            
            if(!promo.promo_message.length)
                chatMessageCell.topLbl.frame = CGRectIntegral(CGRectMake(rightBubbleXPos + chatMessageCell.userImageView.frame.size.width+padding/2+5, 12, labelWidth1, chatMessageCell.userImageView.frame.size.height+font_size));
            else
                chatMessageCell.topLbl.frame = CGRectIntegral(CGRectMake(rightBubbleXPos + chatMessageCell.userImageView.frame.size.width+padding/2+5, USER_IMAGE_Y_POS-3, labelWidth1, chatMessageCell.userImageView.frame.size.height+font_size));

                
                
            
        NSString* str1 = nil;
        NSString* str2 = nil;
        
        if([message length] != 0)
        {
            int len = [self getSplitIndexWithString:message frame:chatMessageCell.topLbl.frame andFont:chatMessageCell.topLbl.font];
                str1 = [message substringToIndex:len];
            if([message length]>[str1 length])
            {
                str2 = [message substringFromIndex:len+1];
                
                chatMessageCell.topLbl.text = str1;
                [chatMessageCell.topLbl sizeToFit];
                chatMessageCell.topLbl.hidden = NO;
                chatMessageCell.bottomLbl.hidden = NO;
                CGSize size1 = [self findSizeChangeForString:str2 WithOriginalWidth:size.width];
                
                int bottomY;
                if(chatMessageCell.userImageView.frame.size.height>chatMessageCell.topLbl.frame.size.height)
                    bottomY = chatMessageCell.userImageView.frame.origin.y + chatMessageCell.userImageView.frame.size.height;
                else
                    bottomY = chatMessageCell.topLbl.frame.origin.y + chatMessageCell.topLbl.frame.size.height;
                
                chatMessageCell.bottomLbl.frame = CGRectIntegral(CGRectMake(rightBubbleXPos + padding/2, bottomY, rightBubbleWidht-30, size1.height));
                chatMessageCell.bottomLbl.numberOfLines = 0;
                chatMessageCell.bottomLbl.text = str2;
                [chatMessageCell.bottomLbl sizeToFit];
                
                if(chatMessageCell.userImageView.frame.size.height>chatMessageCell.topLbl.frame.size.height)
                    rightBubbleHeight = chatMessageCell.userImageView.frame.size.height + chatMessageCell.bottomLbl.frame.size.height + ((bubbleItemOrigin+CHAT_IMAGE_BUBBLE_TOP_MARGIN));
                else
                    rightBubbleHeight = chatMessageCell.topLbl.frame.size.height + chatMessageCell.bottomLbl.frame.size.height + ((bubbleItemOrigin+CHAT_IMAGE_BUBBLE_TOP_MARGIN));
            }
            else
            {
                chatMessageCell.topLbl.hidden = NO;
                chatMessageCell.bottomLbl.hidden = YES;
                chatMessageCell.topLbl.text = message;
                [chatMessageCell.topLbl sizeToFit];
                
                if(chatMessageCell.userImageView.frame.size.height>chatMessageCell.topLbl.frame.size.height)
                    rightBubbleHeight = chatMessageCell.userImageView.frame.size.height + ((bubbleItemOrigin+CHAT_IMAGE_BUBBLE_TOP_MARGIN*2));
                else
                    rightBubbleHeight = chatMessageCell.topLbl.frame.size.height + ((bubbleItemOrigin+CHAT_IMAGE_BUBBLE_TOP_MARGIN*2));
            }
        }
        else
        {
            chatMessageCell.topLbl.hidden = YES;
            chatMessageCell.bottomLbl.hidden = YES;
            
            rightBubbleHeight = chatMessageCell.userImageView.frame.size.height + ((bubbleItemOrigin+CHAT_IMAGE_BUBBLE_TOP_MARGIN*2));
            
        }
    }
        else //Not Promo
        {
            chatMessageCell.topLbl.hidden = YES;
            chatMessageCell.bottomLbl.hidden = NO;
            CGSize noPromoStringSize = [self findSizeChangeForString:message WithOriginalWidth:MAXIMUM_BUBBLE_WIDTH];
            
            CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
            int widht1 = noPromoStringSize.width+(padding*2);
            rightBubbleXPos = screenWidth - noPromoStringSize.width - (padding*2);
            if(widht1 > MAXIMUM_BUBBLE_WIDTH)
            {
                rightBubbleXPos = screenWidth-MAXIMUM_BUBBLE_WIDTH;
                widht1 = MAXIMUM_BUBBLE_WIDTH;
            }
            
            CGSize size1 = [self findSizeChangeForString:message WithOriginalWidth:size.width];
            chatMessageCell.bottomLbl.frame = CGRectIntegral(CGRectMake(rightBubbleXPos + padding/2, padding/2, widht1-30, size1.height));
            chatMessageCell.bottomLbl.numberOfLines = 0;
            chatMessageCell.bottomLbl.text = message;
            [chatMessageCell.bottomLbl sizeToFit];
            
            rightBubbleWidht    = chatMessageCell.bottomLbl.frame.size.width+padding+padding/2;
            rightBubbleHeight   = chatMessageCell.bottomLbl.frame.size.height+5 + padding/2;
            }
            [chatMessageCell.chatImageBubble setFrame:CGRectIntegral(CGRectMake(rightBubbleXPos,
                                                                                CHAT_IMAGE_BUBBLE_TOP_MARGIN,
                                                                                rightBubbleWidht,
                                                                                rightBubbleHeight))];
            [chatMessageCell.companyNamelbl setFrame:CGRectIntegral(CGRectMake(chatMessageCell.chatImageBubble.frame.origin.x + 12, (([IMUtils getOSVersion] >= 7.0)?2:3), chatMessageCell.chatImageBubble.frame.size.width - 35, 30))];
            [chatMessageCell.offerlbl setFrame:CGRectIntegral(CGRectMake(chatMessageCell.chatImageBubble.frame.origin.x + 12, chatMessageCell.chatImageBubble.frame.size.height-26, chatMessageCell.chatImageBubble.frame.size.width - 35, 30))];
            if (NO == msgInfo.isDelivered.boolValue) {
                chatMessageCell.resendButton.enabled = YES;
                chatMessageCell.resendButton.hidden = NO;
                chatMessageCell.messageTimeLabel.textColor = [IMUtils appRedColor];
                chatMessageCell.messageTimeLabel.text = NSLocalizedString(@"Message not sent", @"Message not sent");
            }
            else
            {
                if (msgInfo.isReceiptReceived.boolValue) {
                    chatMessageCell.messageDeliveredImage.image = [UIImage imageNamed:@"check_single_green.png"];
                }
                else
                {
                    chatMessageCell.messageDeliveredImage.image = [UIImage imageNamed:@"check_single.png"];
                }
            }
            chatMessageCell.messageTimeLabel.textAlignment = NSTextAlignmentRight;

    }
 
    UIImage* msgBubbleImg = [self getChatBubbleImage: msgInfo];
    chatMessageCell.chatImageBubble.image = msgBubbleImg;
   
    chatMessageCell.companyNamelbl.numberOfLines = 1;
    [chatMessageCell.companyNamelbl setAdjustsFontSizeToFitWidth:YES];
  // chatMessageCell.companyNamelbl.text = [chatMessageCell.cellPromo promo_message];
   

    
    if(font_size == MAXIMUM_FONT)
        chatMessageCell.companyNamelbl.minimumScaleFactor = MINIMUM_FONT/MAXIMUM_FONT;
    else
        chatMessageCell.companyNamelbl.minimumScaleFactor = 1.0;
    
    chatMessageCell.offerlbl.numberOfLines = 1;
    [chatMessageCell.offerlbl setAdjustsFontSizeToFitWidth:YES];
    chatMessageCell.offerlbl.text = [chatMessageCell.cellPromo promo_link_text];
    if(font_size == MAXIMUM_FONT)
        chatMessageCell.offerlbl.minimumScaleFactor = MINIMUM_FONT/MAXIMUM_FONT;
    else
        chatMessageCell.offerlbl.minimumScaleFactor = 1.0;
    
    chatMessageCell.contentView.userInteractionEnabled = YES;
    chatMessageCell.chatImageBubble.userInteractionEnabled = YES;
    chatMessageCell.userInteractionEnabled = YES;
    chatMessageCell.topLbl.userInteractionEnabled = YES;
    chatMessageCell.bottomLbl.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *touchy = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleTapToCopyText:)];
    [chatMessageCell.topLbl addGestureRecognizer:touchy];
    
    UITapGestureRecognizer *bottomtouchy = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(handleTapToCopyText:)];
    [chatMessageCell.bottomLbl addGestureRecognizer:bottomtouchy];
    
    chatMessageCell.lastSelectedIndexPath = indexPath;
    
    chatMessageCell.companyNamelbl.text = [chatMessageCell.cellPromo promo_message];
    NSString *headerMsg=[chatMessageCell.cellPromo promo_message];
    chatMessageCell.companyNamelbl.text = [chatMessageCell.cellPromo promo_message];
    
    

    return chatMessageCell;

}

#pragma mark - Get Bubble Image
- (void)saveImageInPhotoGallery: (UIGestureRecognizer*)recognizer
{
    IMMediaMessageCell* mediaCell = (IMMediaMessageCell*)[recognizer view];
    selectedMediaImageView = mediaCell.mediaImageView;
    //lastSelectedRow = mediaCell.tag;
    lastSelectedRowIndexPath = mediaCell.lastSelectedRowIndexPath;
    
    [self messagesFetchResultController];
    
    for(int i = 0; i < [[messagesFetchResultController sections] count]; ++i)
    {
        NSLog(@"saveImageInPhotoGallery %d", [[[messagesFetchResultController sections] objectAtIndex: i] numberOfObjects]);
    }
    
    CMMessageArchiving_Message_CoreDataObject *msgInfo = (CMMessageArchiving_Message_CoreDataObject *)[messagesFetchResultController objectAtIndexPath: lastSelectedRowIndexPath];
    if(!msgInfo.isOutgoing)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"Do you want to save it in photo album of your device?", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"NO", @"") otherButtonTitles:NSLocalizedString(@"YES", @""), nil];
        [alertView show];
    }
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error)
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == NO_CONTACT_ALERT)
    {
        if(buttonIndex == 1) {
            
            //[self performSegueWithIdentifier:@"IMAddNewContactViewController" sender:self];
            UIStoryboard *contactsStoryBoard = [[IMAppDelegate sharedDelegate] contactsStoryboard];
            IMAddNewContactViewController *contacts = [contactsStoryBoard instantiateViewControllerWithIdentifier:@"IMAddNewContactViewController"];
            
            NSRange range = [self.userJid rangeOfString: @"_"];
            if(range.location != NSNotFound) {
                contacts.imgrPhoneNumber = [self.userJid substringToIndex: range.location];
                contacts.chatVC = self;
                [self.navigationController pushViewController:contacts animated: YES];
            }
        }
        return;
    }
    if(buttonIndex == 0)
        return;
    
    CMMessageArchiving_Message_CoreDataObject *msgInfo = (CMMessageArchiving_Message_CoreDataObject *)[[self messagesFetchResultController] objectAtIndexPath: lastSelectedRowIndexPath];
    if(!msgInfo.isOutgoing)
    {
        UIImageWriteToSavedPhotosAlbum(selectedMediaImageView.image,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
    }
}

-(UIImage*)getChatBubbleImage: (CMMessageArchiving_Message_CoreDataObject*)message
{
    UIImage* bubbleImg = nil;
    NSString *companyName=[message.promo_Object promo_message];
    
    
    if(message.isOutgoing)
    {
        if(message.promo_Object) {
            if(companyName.length)
            {
                bubbleImg = [[UIImage imageNamed:bubbleoutGoingImageName_Promo] resizableImageWithCapInsets:UIEdgeInsetsMake(28, 30, 30, 30)];
            }else
            {
                bubbleImg = [[UIImage imageNamed:bubbleoutGoingImageName_NO_Promo] resizableImageWithCapInsets:UIEdgeInsetsMake(28, 30, 30, 30)];
            }
            
        }
        
        else
        {
            bubbleImg = [[UIImage imageNamed:bubbleoutGoingImageName_NO_Promo] resizableImageWithCapInsets:UIEdgeInsetsMake(28, 30, 30, 30)];
        }
    }
    else
    {
        if(message.promo_Object) {
            if ([(IMPromos *)message.promo_Object promo_link].length && [(IMPromos *)message.promo_Object promo_message].length) {
                
                bubbleImg = [[UIImage imageNamed:bubbleImageName_Promo] resizableImageWithCapInsets:UIEdgeInsetsMake(25, 30, 30, 30)];
            }
            else if([(IMPromos *)message.promo_Object promo_message].length)
            {
                 bubbleImg = [[UIImage imageNamed:bubbleImageName_Promo_NoLink] resizableImageWithCapInsets:UIEdgeInsetsMake(25, 30, 30, 30)];
            }
            else if([(IMPromos *)message.promo_Object promo_link].length)
            {
                
                bubbleImg = [[UIImage imageNamed:bubbleoutImageName_NO_PromoHeader] resizableImageWithCapInsets:UIEdgeInsetsMake(25, 30, 30, 30)];
  
            }
            else
            {
                bubbleImg = [[UIImage imageNamed:bubbleImageName_NO_Promo] resizableImageWithCapInsets:UIEdgeInsetsMake(25, 30, 30, 30)];
            }
            
        }
        else
        {
            bubbleImg = [[UIImage imageNamed:bubbleImageName_NO_Promo] resizableImageWithCapInsets:UIEdgeInsetsMake(25, 30, 30, 30)];
        }
    }
    return bubbleImg;
}

#pragma mark - table view delegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CMMessageArchiving_Message_CoreDataObject *msgInfo = (CMMessageArchiving_Message_CoreDataObject *)[[self messagesFetchResultController] objectAtIndexPath:indexPath];

    if (msgInfo.mediaThumbnailPath) {
        return 165;
    }
    
    CGFloat cellHeight = [self calculateHeightForCell:msgInfo indexPathName:indexPath.row];
    cellHeight += 10;//+ 10 for time label in the cell
    return cellHeight ;
   
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"hii");
//}


#pragma mark - tableviewcell height calculator

-(CGFloat)calculateHeightForCell: (CMMessageArchiving_Message_CoreDataObject*)message indexPathName: (NSInteger)index
{
    CGFloat height;

    IMPromos *promo = nil;
    if (!message.promo_Object) {
        promo = [IMDataHandler fetchPromoForPromoID:message.promo_id];
        message.promo_Object = promo;
    }
    else {
        promo = message.promo_Object;
    }
    
  if (promo)
  {
      CGSize size;
      
      CGSize headerSize;
      CGSize footerSize;
      CGSize stringSize;
    
      int imageWith,imageHeight,bubbleWidth,headerHeight;
      int marginMultiplier, heightMultiplier;
      
      NSString* decodedMessage = [message.body emoticonizedString];
      headerSize = [self findSizeChangeForString:promo.promo_name WithOriginalWidth:MAXIMUM_BUBBLE_WIDTH];
    
      
      stringSize = [self findSizeChangeForString:decodedMessage WithOriginalWidth:MAXIMUM_BUBBLE_WIDTH];

      if(message.isOutgoing) {
          size = headerSize;
          if(promo.promo_message.length)
             headerHeight = USER_IMAGE_Y_POS+(CHAT_IMAGE_BUBBLE_TOP_MARGIN*2);
          else
              headerHeight = USER_IMAGE_Y_POS_WITHOUT_HEADER+(CHAT_IMAGE_BUBBLE_TOP_MARGIN*2);
          
      }
      else {
          footerSize = [self findSizeChangeForString:promo.promo_message WithOriginalWidth:MAXIMUM_BUBBLE_WIDTH];
          size = (headerSize.width>footerSize.width)? headerSize: footerSize;
          
          if(promo.promo_link.length) {
              heightMultiplier = CHAT_IMAGE_BUBBLE_TOP_MARGIN;
              marginMultiplier = 2;
          }
          else {
              heightMultiplier = -CHAT_IMAGE_BUBBLE_TOP_MARGIN;
              marginMultiplier = 1;
          }
          if(promo.promo_message.length)
           headerHeight = ((USER_IMAGE_Y_POS-heightMultiplier)*marginMultiplier);
          else
              headerHeight = ((USER_IMAGE_Y_POS_WITHOUT_HEADER-heightMultiplier)*marginMultiplier);
          
      }
      
    if(stringSize.width>size.width)
        size = stringSize;
      
      NSString* promoImage = [[IMUtils getImageFolderPath] stringByAppendingPathComponent: [promo.promo_image lastPathComponent]];
      CGSize imageSize = [IMUtils imageSizeAtFilePath:promoImage];

      imageWith = MIN(imageSize.width, PROMO_MAX_EDGE_SIZE);
      imageHeight = MIN(imageSize.height, PROMO_MAX_EDGE_SIZE);
      
      if(imageHeight == 0)
          imageHeight = DUMMY_PROMO_IMAGE_SIZE;
      
    if(size.width<imageWith)
        size.width = imageWith;
    
    if([decodedMessage length] > 0)
    {
        CGRect rectFrame;
        
        bubbleWidth = size.width+padding+(imageWith*2);
        if(bubbleWidth > MAXIMUM_BUBBLE_WIDTH)
            bubbleWidth = MAXIMUM_BUBBLE_WIDTH;
        int labelWidth1 = bubbleWidth - (imageWith + padding) - 15;
        rectFrame = CGRectMake(0, 0, labelWidth1, imageHeight+font_size);
        
        dummyTopLbl.frame = CGRectIntegral(rectFrame);
        dummyTopLbl.numberOfLines = 0;
        dummyTopLbl.lineBreakMode = NSLineBreakByWordWrapping;
        dummyTopLbl.font = [IMUtils appFontWithSize:font_size];
        
        int len = [self getSplitIndexWithString:decodedMessage frame:rectFrame andFont:[IMUtils appFontWithSize:font_size]];
        NSString* str1 = [decodedMessage substringToIndex:len];
        
        if([decodedMessage length]>[str1 length])
        {
            dummyTopLbl.text = str1;
            [dummyTopLbl sizeToFit];
            
            NSString*  str2 = [decodedMessage substringFromIndex:len+1];
            CGSize size2 = [self findSizeChangeForString:str2 WithOriginalWidth:size.width];
            
            dummyBottomLbl.frame = CGRectIntegral(CGRectMake(0,50, bubbleWidth-30, size2.height));
            dummyBottomLbl.numberOfLines = 0;
            dummyBottomLbl.font = [IMUtils appFontWithSize:font_size];
            dummyBottomLbl.text = str2;
            [dummyBottomLbl sizeToFit];
            
            if(imageHeight>dummyTopLbl.frame.size.height)
                height = imageHeight + dummyBottomLbl.frame.size.height + headerHeight;
            else
                height = dummyTopLbl.frame.size.height + dummyBottomLbl.frame.size.height + headerHeight;
            
        }
        else
        {
            dummyTopLbl.text = str1;
            [dummyTopLbl sizeToFit];
            if ([(IMPromos *)message.promo_Object promo_link].length)
                headerHeight = headerHeight;
            else
                headerHeight = headerHeight+CHAT_IMAGE_BUBBLE_TOP_MARGIN;
            
            if(imageHeight>dummyTopLbl.frame.size.height)
                height = imageHeight + headerHeight;
            else
                height = dummyTopLbl.frame.size.height + headerHeight;
            
        }
    }
    else
    {
        height =imageHeight + headerHeight;
    }
      
   }
   else  // Not Promo
   {
       NSString* decodedMessage = [message.body emoticonizedString];
       CGSize noPromoStringSize = [self findSizeChangeForString:decodedMessage WithOriginalWidth:MAXIMUM_BUBBLE_WIDTH];
       
       //CGSize size2 = [self findSizeChangeForString:message.body WithOriginalWidth:noPromoStringSize.width];
      // int widht1 = noPromoStringSize.width+(padding*2);
       int bubbleWidth;
       
       bubbleWidth = noPromoStringSize.width+(padding*2);
       if(bubbleWidth > MAXIMUM_BUBBLE_WIDTH)
           bubbleWidth = MAXIMUM_BUBBLE_WIDTH;
       //else
       
       dummyBottomLbl.frame = CGRectIntegral(CGRectMake(0,50, bubbleWidth-30, noPromoStringSize.height));
       dummyBottomLbl.numberOfLines = 0;
       dummyBottomLbl.font = [IMUtils appFontWithSize:font_size];
       dummyBottomLbl.text = decodedMessage;
       [dummyBottomLbl sizeToFit];
       
       //headerHeight = USER_IMAGE_Y_POS+(CHAT_IMAGE_BUBBLE_TOP_MARGIN*2);
       height = dummyBottomLbl.frame.size.height+5 + padding/2;
   }
    return height+(CHAT_IMAGE_BUBBLE_TOP_MARGIN*2);
}

// Get size of a string
-(CGSize) findSizeChangeForString:(NSString *)message WithOriginalWidth:(CGFloat)width {

    if (message == nil) {
        message = @"";
    }
    CGSize  textSize = CGSizeMake(width,MAX_ROW_HEIGHT);
    
    if([IMUtils getOSVersion] >= 7.0)
    {
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:message attributes:@{NSFontAttributeName : [IMUtils appFontWithSize:font_size]}];
        CGRect rect = [attributedText boundingRectWithSize:(CGSize)textSize
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        
 //ss not in use       CGFloat lineHeight = font_size;
//ss not in use        NSUInteger linesInLabel = floor(rect.size.height/lineHeight);
        
        return CGSizeMake(ceilf(rect.size.width), ceilf(rect.size.height));
    }
    else
    {
    // for other os
        CGSize rect1 = [message sizeWithFont:[IMUtils appFontWithSize:font_size] constrainedToSize:textSize lineBreakMode:NSLineBreakByWordWrapping];
        return CGSizeMake(ceilf(rect1.width), ceilf(rect1.height));
    }
}

#pragma mark - Textfield Delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

   
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [messageTextField resignFirstResponder];
     keyboard=true;
     bonds=216;
     firstTime = false;
    return YES;
}

#pragma mark - UI Keyboard Notification

-(void)keyboardFrameWillChange:(NSNotification* )notify
{
   
    
    NSDictionary *keyboardInfo = [notify userInfo];
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    float keyboardHeight = keyboardFrameBeginRect.size.height;
     //NSLog(@"keyboardFrame rawframe=%@",NSStringFromCGRect(rawFrame));
   /* NSLog(@"keyboardFrame rawframe=%@",NSStringFromCGRect(rawFrame));
    CGRect keyboardFrame;
    [[notify.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardFrame];
    CGRect inputPanelFrame = self.inputPanel.frame;
    UIView *superView = [ self.inputPanel superview];
    inputPanelFrame.origin.y = (superView.frame.size.height -(keyboardFrame.size.height+self.inputPanel.frame.size.height));
    
    self.inputPanel.frame = inputPanelFrame;*/
}

- (void)showKeyboard:(NSNotification* )notify
{
    if (isPresentedSheet) {
        return;
    }
    CGRect keyboardBounds;
    NSDictionary *info = [notify userInfo];
    
    
    [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    NSLog(@"height1=%f origin=%f",keyboardBounds.size.height,keyboradFrame.origin.y);
    
    if (keyboardBounds.size.height == 224 || keyboardBounds.size.height == 225) {
       perdicative = false;
        
         NSLog(@"%@",NSStringFromCGRect(messageView.frame));
        CGRect keyboardBounds;
        NSDictionary *info = [notify userInfo];
        
        
        
        
        [[info objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
        NSLog(@"height1=%f origin=%f",keyboardBounds.size.height,keyboradFrame.origin.y);
        NSLog(@"%@",NSStringFromCGRect(messageView.frame));
        
        if (emoji) {
            if (messageView.frame.origin.y<= 5000){
                messageView.frame = CGRectMake(messageView.frame.origin.x,self.view.frame.size.height-(messageView.frame.size.height+keyboardBounds.size.height), messageView.frame.size.width, messageView.frame.size.height);
                chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width,  messageView.frame.origin.y-40);
            }
        }else if(firstTime){
        
        if (messageView.frame.origin.y<= 5000  ){
            messageView.frame = CGRectMake(messageView.frame.origin.x,self.view.frame.size.height-(messageView.frame.size.height+keyboardBounds.size.height), messageView.frame.size.width, messageView.frame.size.height);
            chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width, messageView.frame.origin.y-40);
        }
        }
        else if ( messageView.frame.origin.y==0){
            [messageView setFrame:CGRectMake(messageView.frame.origin.x,231, 320,47)];
            chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, 45, 320, 186);
            NSLog(@"message frame= %@",NSStringFromCGRect(messageView.frame));
            
        }
        else if (messageView.frame.origin.y <= 242){
            messageView.frame = CGRectMake(messageView.frame.origin.x,self.view.frame.size.height-(messageView.frame.size.height+keyboardBounds.size.height), messageView.frame.size.width, messageView.frame.size.height);
            chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width, messageView.frame.origin.y-40);
        }
        else if (messageView.frame.origin.y == 234){
            messageView.frame = CGRectMake(messageView.frame.origin.x,self.view.frame.size.height-(messageView.frame.size.height+keyboardBounds.size.height), messageView.frame.size.width, messageView.frame.size.height);
            chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width,messageView.frame.origin.y-40);
        }
        else if (messageView.frame.origin.y <= 227){
            messageView.frame = CGRectMake(messageView.frame.origin.x,messageView.frame.origin.y+29, messageView.frame.size.width, messageView.frame.size.height);
            chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width, messageView.frame.origin.y-40);
        }
        
        else{
            messageView.frame = CGRectMake(messageView.frame.origin.x,messageView.frame.origin.y-keyboardBounds.size.height, messageView.frame.size.width, messageView.frame.size.height);
            
            NSLog(@"%@",NSStringFromCGRect(messageView.frame));
            chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width, messageView.frame.origin.y-40);
            NSLog(@"%@",NSStringFromCGRect(chatTableVW.frame));
            bonds =keyboardBounds.size.height;
            
        }
        
        
        
    }
    else if(keyboardBounds.size.height == 253 || keyboardBounds.size.height == 260)
    {
        emoji = false;
        firstTime = true;
        perdicative =true;
        NSLog(@"%@",NSStringFromCGRect(messageView.frame));
        
        if (messageView.frame.origin.y<= 3000  ) {
            messageView.frame = CGRectMake(messageView.frame.origin.x,self.view.frame.size.height-(messageView.frame.size.height+keyboardBounds.size.height), messageView.frame.size.width, messageView.frame.size.height);
            chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width, messageView.frame.origin.y-40);
            NSLog(@"%@",NSStringFromCGRect(messageView.frame));
        }
        else if ( messageView.frame.origin.y==0){
            messageView.frame = CGRectMake(messageView.frame.origin.x,205, 320,47);
            chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, 45, 320, 161);
            NSLog(@"message frame=%@",NSStringFromCGRect(messageView.frame));
        }
        
        else{
            
            
           
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3];
          
            
            
            
            messageView.frame = CGRectMake(messageView.frame.origin.x,messageView.frame.origin.y-keyboardBounds.size.height, messageView.frame.size.width, messageView.frame.size.height);
            
            NSLog(@"%@",NSStringFromCGRect(messageView.frame));
            chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width, messageView.frame.origin.y-40);
            NSLog(@"%@",NSStringFromCGRect(chatTableVW.frame));
            [UIView commitAnimations];
            keyboard= false;
            bonds = keyboardBounds.size.height;
            
        }
    }
    
    else if(keyboardBounds.size.height == 216)
    {
        NSLog(@"%@",NSStringFromCGRect(messageView.frame));
        if (perdicative) {
            if (messageView.frame.origin.y<= 2050 ) {
                messageView.frame = CGRectMake(messageView.frame.origin.x,self.view.frame.size.height-(messageView.frame.size.height+keyboardBounds.size.height), messageView.frame.size.width, messageView.frame.size.height);
                chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width, messageView.frame.origin.y-40);
                NSLog(@"%@",NSStringFromCGRect(messageView.frame));
                emoji = false;
                firstTime = true;
            }
            else{
                
                /* NSDictionary* keyboardInfo = [notify userInfo];
                 NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
                 CGRect rawFrame      = [keyboardFrameBegin CGRectValue];
                 
                 NSLog(@"keyboardFrame rawframe=%@",NSStringFromCGRect(rawFrame));*/
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.3];
                //    CGFloat windowHeight = [UIApplication sharedApplication].keyWindow.frame.size.height;
                //    int height=windowHeight - messageView.frame.size.height - 66;
                //    int height2=messageView.frame.origin.y - IOS_KEYBOARD_HEIGHT;
                //    NSLog(@"height1=%d height2=%d",height,height2);
                
                
                
                messageView.frame = CGRectMake(messageView.frame.origin.x,messageView.frame.origin.y-keyboardBounds.size.height, messageView.frame.size.width, messageView.frame.size.height);
                
                NSLog(@"%@",NSStringFromCGRect(messageView.frame));
                chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width, messageView.frame.origin.y-40);
                NSLog(@"%@",NSStringFromCGRect(chatTableVW.frame));
                [UIView commitAnimations];
                keyboard= false;
                bonds = keyboardBounds.size.height;
                
            }
 
        }else if(!perdicative) {
            if (messageView.frame.origin.y<= 2050  ) {
                messageView.frame = CGRectMake(messageView.frame.origin.x,self.view.frame.size.height-(messageView.frame.size.height+keyboardBounds.size.height), messageView.frame.size.width, messageView.frame.size.height);
                chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width, messageView.frame.origin.y-40);
                NSLog(@"%@",NSStringFromCGRect(messageView.frame));
                emoji = true;
            }
            else{
                
                /* NSDictionary* keyboardInfo = [notify userInfo];
                 NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
                 CGRect rawFrame      = [keyboardFrameBegin CGRectValue];
                 
                 NSLog(@"keyboardFrame rawframe=%@",NSStringFromCGRect(rawFrame));*/
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.3];
                //    CGFloat windowHeight = [UIApplication sharedApplication].keyWindow.frame.size.height;
                //    int height=windowHeight - messageView.frame.size.height - 66;
                //    int height2=messageView.frame.origin.y - IOS_KEYBOARD_HEIGHT;
                //    NSLog(@"height1=%d height2=%d",height,height2);
                
                
                
                messageView.frame = CGRectMake(messageView.frame.origin.x,messageView.frame.origin.y-keyboardBounds.size.height, messageView.frame.size.width, messageView.frame.size.height);
                
                NSLog(@"%@",NSStringFromCGRect(messageView.frame));
                chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width, messageView.frame.origin.y-40);
                NSLog(@"%@",NSStringFromCGRect(chatTableVW.frame));
                [UIView commitAnimations];
                keyboard= false;
                bonds = keyboardBounds.size.height;
                
            }
        }
        
        
        
        
        
    }
    
    
}
- (void)hideKeyboard:(NSNotification* )notify
{
    if (isPresentedSheet) {
        return;
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    CGFloat windowHeight = [UIApplication sharedApplication].keyWindow.frame.size.height;
    messageView.frame = CGRectMake(messageView.frame.origin.x, windowHeight - messageView.frame.size.height - 66/*navigation + status bar*/ , messageView.frame.size.width, messageView.frame.size.height);
    chatTableVW.frame = CGRectMake(chatTableVW.frame.origin.x, chatTableVW.frame.origin.y, chatTableVW.frame.size.width, windowHeight - messageView.frame.size.height - chatTableVW.frame.origin.y - 66/*navigation + status bar*/);
    [UIView commitAnimations];
    keyboard=true;
    bonds=216;

}




-(void) adjustFrames:(UITextView*)textView
{
    //CGFloat diff = 0.0;
    
    int contentSize = 0;
    int limit = 0;
    int contentHeight = 0;
    int height = 0;
    int minimumTableHeight = 0;
    if([IMUtils getOSVersion] >= 7.0)
    {
        contentSize = 26;
        limit = 30;
        contentHeight = [self measureHeightOfUITextView: textView];
        height = 3;
        minimumTableHeight = 31;
    }
    else
    {
        contentSize = 31;
        limit = 20;
        contentHeight = textView.contentSize.height;
        height = 5;
        minimumTableHeight = 31;
    }
    
    CGRect textFrame = textView.frame;

    if (contentHeight == contentSize)
    {
        if (textFrame.size.height > limit)
        {
              // diff = contentHeight - textFrame.size.height - height;
            
               textFrame.size.height = contentHeight - height;
              CGFloat difference = textFrame.size.height - textView.frame.size.height;
               textView.frame = textFrame;
              CGRect messageFrame = messageView.frame;
              messageFrame.origin.y -= difference;
              messageFrame.size.height += difference;
              
              messageView.frame = messageFrame;
              CGRect chatFrame = chatTableVW.frame;
              chatFrame.size.height -= difference;
              chatTableVW.frame = chatFrame;
        }
    }
    else
    {
        textFrame.size.height = contentHeight - height;
        CGFloat difference = textFrame.size.height - textView.frame.size.height;

        CGRect messageFrame = messageView.frame;
        messageFrame.origin.y -= difference;
        messageFrame.size.height += difference;
//        if(messageFrame.origin.y <= 75) {
//            messageFrame.origin.y = 75;
//            
//            messageFrame.size.height = IS_IPHONE5 ? 215 : 120;
//            textFrame.size.height = IS_IPHONE5 ? 196 : 101;
//        }
        
        textView.frame = textFrame;

        messageView.frame = messageFrame;
          
        CGRect chatFrame = chatTableVW.frame;
        chatFrame.size.height -= difference;
        
        if(chatFrame.size.height <= minimumTableHeight)
            chatFrame.size.height = minimumTableHeight;
        
        chatTableVW.frame = chatFrame;
          
        if (textFrame.size.height > 34) {
            self.inputbackgroundImageView.image = [UIImage imageNamed:@"bg_input_chat.png"];
              textView.scrollEnabled = YES;
          }
          else
          {
              textView.scrollEnabled = NO;
              self.inputbackgroundImageView.image = [UIImage imageNamed:@"bg_input_message.png"];
          }
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
      //place holder......   
    
      if ([text isEqualToString:@"\n"]) {
        
        
          [textView resignFirstResponder];
          keyboard=true;
          bonds=216;
        
          return NO;
          }

      //[self adjustFrames:textView];
    
    return YES;
    
}

//- (void)scrollToCaretInTextView:(UITextView *)textView animated:(BOOL)animated
//{
//    CGRect rect = [textView caretRectForPosition:textView.selectedTextRange.end];
//    rect.size.height += textView.textContainerInset.bottom;
//    [textView scrollRectToVisible:rect animated:animated];
//}
//
//-(void)textViewDidChangeSelection:(UITextView *)textView {
//    [textView scrollRangeToVisible:textView.selectedRange];
//}

 - (void)textViewDidChange:(UITextView *)textView
{
    [self adjustFrames: textView];
}

- (CGRect)contentSizeRectForTextView:(UITextView *)textView
{
    [textView.layoutManager ensureLayoutForTextContainer:textView.textContainer];
    CGRect textBounds = [textView.layoutManager usedRectForTextContainer:textView.textContainer];
    CGFloat width = (CGFloat)ceil(textBounds.size.width + textView.textContainerInset.left + textView.textContainerInset.right);
    CGFloat height = (CGFloat)ceil(textBounds.size.height + textView.textContainerInset.top + textView.textContainerInset.bottom);
    return CGRectMake(0, 0, width, height);
}

- (CGFloat)measureHeightOfUITextView:(UITextView *)textView
{
    if ([textView respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)])
        {
              // This is the code for iOS 7. contentSize no longer returns the correct value, so
              // we have to calculate it.
              //
              // This is partly borrowed from HPGrowingTextView, but I've replaced the
              // magic fudge factors with the calculated values (having worked out where
              // they came from)
            
              CGRect frame = textView.bounds;
            
              // Take account of the padding added around the text.
            
              UIEdgeInsets textContainerInsets = textView.textContainerInset;
              UIEdgeInsets contentInsets = textView.contentInset;
            
              CGFloat leftRightPadding = textContainerInsets.left + textContainerInsets.right + textView.textContainer.lineFragmentPadding * 2 + contentInsets.left + contentInsets.right;
              CGFloat topBottomPadding = textContainerInsets.top + textContainerInsets.bottom + contentInsets.top + contentInsets.bottom;
            
            frame.size.width -= leftRightPadding;
            frame.size.height -= topBottomPadding;
            
            NSString *textToMeasure = textView.text;
            if ([textToMeasure hasSuffix:@"\n"])
                {
                    textToMeasure = [NSString stringWithFormat:@"%@-", textView.text];
                    }
            
            // NSString class method: boundingRectWithSize:options:attributes:context is
            // available only on ios7.0 sdk.
            
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
            
            NSDictionary *attributes = @{ NSFontAttributeName: textView.font, NSParagraphStyleAttributeName : paragraphStyle };
            
            CGRect size = [textToMeasure boundingRectWithSize:CGSizeMake(CGRectGetWidth(frame), MAXFLOAT)
                                  options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributes
                                  context:nil];
            
            CGFloat measuredHeight = ceilf(CGRectGetHeight(size) + topBottomPadding);
            return measuredHeight;
            }
    else
        {
            return textView.contentSize.height;
            }
}


#pragma mark Clipboard

-(void)handleTapToCopyText: (UIGestureRecognizer*) recognizer
{
    UILabel* view = (UILabel*)[recognizer view];
    
    IMChatMessageCell* messageCell = nil;
//    if([IMUtils getOSVersion] >= 7.0)
//        messageCell = (IMChatMessageCell*)[[[view superview] superview] superview];
//    else
        messageCell = (IMChatMessageCell*)[[view superview] superview];
    lastSelectedRowIndexPath = messageCell.lastSelectedIndexPath;

    IMChatMessageCell* cell = (IMChatMessageCell*)[chatTableVW cellForRowAtIndexPath:messageCell.lastSelectedIndexPath];
    [self becomeFirstResponder];
    UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setTargetRect:cell.bottomLbl.frame inView:cell];
    [menu setMenuVisible:YES animated:YES];
}

- (BOOL) canPerformAction: (SEL) action withSender: (id) sender
{
    return (action == @selector(copy:));
}

- (BOOL) canBecomeFirstResponder
{
    return YES;
}

- (void)copy:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    //IMChatMessageCell* cell = (IMChatMessageCell*)[chatTableVW cellForRowAtIndexPath:[NSIndexPath indexPathForRow:lastSelectedRow inSection:0]];
    IMChatMessageCell* cell = (IMChatMessageCell*)[chatTableVW cellForRowAtIndexPath: lastSelectedRowIndexPath];
    UILabel* topLbl = (UILabel*)cell.topLbl;//[self.view viewWithTag:lastSelectedRow];
    //UILabel* bottomLbl = (UILabel*)[self.view viewWithTag:lastSelectedRow+MESSAGE_TAG];
    UILabel* bottomLbl = (UILabel*)[self.view viewWithTag:lastSelectedRowIndexPath.row+MESSAGE_TAG];
    if(bottomLbl.hidden == YES)
        [pasteboard setString:topLbl.text];
    else
        [pasteboard setString:[NSString stringWithFormat:@"%@ %@",topLbl.text,bottomLbl.text]];
}

- (void)userDidTappedOnMessageFooter:(UITapGestureRecognizer *)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://google.com/"]];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"IMPromoChoiceViewController"]) {
        IMPromoChoiceViewController *choiceViewController = segue.destinationViewController;
        isSelectingPromo = YES;
        choiceViewController.selectionBlockObject = ^(IMPromos *selectedValue)
        {
            if (NO == [[NSUserDefaults standardUserDefaults] boolForKey:k_DEFAULTS_KEY_PROMOS]|| NO == self.userInfo.promo_enabled.boolValue) {
                [self.promoSettingsOffView setSelectedPromo:selectedValue];
            }
            else
            {
                [self.promoSettingsOnView setSelectedPromo:selectedValue];
            }
        };
    }
    else if([segue.destinationViewController isKindOfClass: [IMAddNewContactViewController class]])
    {
        
    }
}

- (void)forHost:(NSString *)hostName didProgressTransferMediaPercent:(float)percent withTag:(NSInteger)tag forJID:(NSString *)toJID
{
    dispatch_async(dispatch_get_main_queue(), ^{
        mediaTransferAlertView.message = [NSString stringWithFormat:@"Please wait sending image, completed %f Percent.",percent];
    });
}

/**
 * @details This Delegate is called when media transfer is completed but message is yet to be sent to receiver. Message is sent internally as soon as media transfer is completed. User doesn't need to send message.
 **/
- (void)forHost:(NSString *)hostName didCompleteTransferMedia:(NSString *)response withTag:(NSInteger)tag forJID:(NSString *)toJID
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [mediaTransferAlertView dismissWithClickedButtonIndex:0 animated:YES];
    });

}

/**
 * @details This Delegate is called when media transfer is failed and hence message is not sent to receiver.
 **/
- (void)forHost:(NSString *)hostName didFailTransferMedia:(NSError *)error withTag:(NSInteger)tag forJID:(NSString *)toJID
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [mediaTransferAlertView dismissWithClickedButtonIndex:0 animated:YES];
    });
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
