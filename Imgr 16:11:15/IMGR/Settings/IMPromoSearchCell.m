//
//  IMPromoSearchCell.m
//  IMGR
//
//  Created by Satendra Singh on 3/26/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMPromoSearchCell.h"

@implementation IMPromoSearchCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:[IMPromoSearchCell cellReusableIdentifier]];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (IMPromoSearchCell *)cellLoadedFromNibFile
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    for (NSObject *obj in objects)
    {
        if ([obj isKindOfClass:[self class]])
        {
            IMPromoSearchCell *object = (IMPromoSearchCell *)obj;
            return object;
            
            break;
        }
    }
    return nil;
}

+ (NSString *) cellReusableIdentifier
{
    return @"IMPromoSearchCell";
}

@end
