//
//  IMPromoSearchCell.h
//  IMGR
//
//  Created by Satendra Singh on 3/26/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMPromoSearchCell : UITableViewCell

+ (IMPromoSearchCell *)cellLoadedFromNibFile;

+ (NSString *) cellReusableIdentifier;

@end
