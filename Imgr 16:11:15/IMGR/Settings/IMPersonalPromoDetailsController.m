//
//  IMPersonalPromoDetailsController.m
//  IMGR
//
//  Created by Satendra Singh on 2/13/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMPersonalPromoDetailsController.h"
#import "IMAddPersonalPromoController.h"
#import "IMPromos.h"
#import "IMUtils.h"

@interface IMPersonalPromoDetailsController ()
{
    IBOutlet UILabel*       promoNameLabel;
    IBOutlet UILabel*       promoMessageLabel;
    IBOutlet UILabel*       promoLinkLabel;
    IBOutlet UILabel*       promoLinkTextLabel;
    
    IBOutlet UIButton*      promoIconButton;
    NSString*               promoImageName;
}

@property (weak, nonatomic) IBOutlet UIButton *testPromButton;
- (IBAction)userDidTappedtestPromo:(id)sender;

@end

@implementation IMPersonalPromoDetailsController
@synthesize promo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePromo) name:@"DELETE_PROMO" object:nil];
}

- (void)deletePromo
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DELETE_PROMO" object:nil];
    [self.navigationController popViewControllerAnimated: YES];
}

- (void)viewDidAppear:(BOOL)animated
{
//    promoNameLabel.text     = promo.promo_name;
//    promoMessageLabel.text  = promo.promo_message;
//    promoLinkLabel.text     = promo.promo_link;
//    promoLinkTextLabel.text = promo.promo_link_text;
//    
//    self.title = promo.promo_name;
//    NSString* filePath = [[IMUtils getImageFolderPath] stringByAppendingPathComponent: [promo.promo_image_path lastPathComponent]];
//    [promoIconButton setImage: [UIImage imageWithContentsOfFile: filePath] forState: UIControlStateNormal];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    promoNameLabel.text     = promo.promo_name;
    promoMessageLabel.text  = promo.promo_message;
    promoLinkLabel.text     = promo.promo_link;
    promoLinkTextLabel.text = promo.promo_link_text;
    
    self.title = promo.promo_name;
    NSString* filePath = [[IMUtils getImageFolderPath] stringByAppendingPathComponent: [promo.promo_image lastPathComponent]];
    BOOL isDirectory;
    BOOL fileExist = [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
    
    if (fileExist & ! isDirectory) {
        UIImage* image = [UIImage imageWithContentsOfFile: filePath];
        [promoIconButton setImage: image forState: UIControlStateNormal];
    }
    else{
        [promoIconButton setImage:[UIImage imageNamed: @"thumb_add_promo_img.png"]  forState: UIControlStateNormal];
    }
    [promoIconButton setNeedsLayout];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"IMAddPersonalPromoController"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        IMAddPersonalPromoController *controller = (IMAddPersonalPromoController *)[navigationController topViewController];
        controller.isupdatingPromo = YES;
        controller.promo = promo;
    }
}

- (IBAction)userDidTappedtestPromo:(id)sender {
    
    NSString *urlStr = self.promo.promo_link;
    if (urlStr.length) {
        if (NSNotFound == [urlStr rangeOfString:@"http"].location) {
            urlStr = [@"http://" stringByAppendingString:urlStr];
        }
        NSLog(@"open link = %@",urlStr);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    }
}
@end
