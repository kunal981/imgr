//
//  IMPersonalAdsViewController.m
//  IMGR
//
//  Created by Satendra Singh on 2/5/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMPersonalAdsViewController.h"
#import "IMPromoHeaderView.h"
#import "CMCoreDataHandler.h"
#import "IMDataHandler.h"
#import "IMPersonalPromoDetailsController.h"
#import "IMConstants.h"
#import "IMPromoSearchCell.h"

#define SELECTED_BG_VIEW_TAG 765

@interface IMPersonalAdsViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, IMDownloadPictureOperationDelegate>
{
    UITableViewController *_tableViewController;
    
    NSFetchedResultsController* fectchedResultsController;
    
    IMOperationQueue* queueOperation;
    
    __weak IBOutlet UIBarButtonItem *rightBarButton;
    NSIndexPath*                 selectedPromoIndex;
     BOOL isSelectDeselectPressesd;
}
@property (weak, nonatomic) IBOutlet UIButton *createNewButton;
@property (weak, nonatomic) IBOutlet UIButton *promoInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *selectAllButton;
@property (weak, nonatomic) IBOutlet UIView *createNewButtonCell;

- (IBAction)onEditTapped:(id)sender;
- (IBAction)onCreateNewPromoTapped:(id)sender;
- (IBAction)onSelectAllTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation IMPersonalAdsViewController
@synthesize fectchedResultsController = fectchedResultsController;
@synthesize promoSearchBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSFetchedResultsController*)fetchPersonalPromosResultController: (NSString*)searchString
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    if(fectchedResultsController == nil)
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMPromos"
                                                  inManagedObjectContext:moc];
        
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"index_character" ascending:YES];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"promo_name" ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, sd2, nil];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        if(searchString)
        {
            NSPredicate* predicate = [NSPredicate predicateWithFormat: @"promo_type == %d && promo_name contains[c] %@ && markedAsDeleted == %d", 1, searchString,0];
            [fetchRequest setPredicate: predicate];
        }
        else
        {
            NSPredicate* predicate = [NSPredicate predicateWithFormat: @"promo_type == %d && markedAsDeleted == %d", 1,0];
            [fetchRequest setPredicate: predicate];
        }
        fectchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:moc
                                                                          sectionNameKeyPath:@"index_character"
                                                                                   cacheName:nil];
        [fectchedResultsController setDelegate:self];
        
        NSError *error = nil;
        if (![fectchedResultsController performFetch:&error])
        {
            DDLogError(@"Error performing fetch: %@", error);
        }
    }
    return fectchedResultsController;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
     //[self.sponsoredAdsTableView reloadData];
    if (self.tableView.isEditing) {
        return;
    }
    [self performSelector:@selector(updateTable) withObject:nil afterDelay:.1];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    queueOperation = [[IMOperationQueue alloc] init];

    _tableView.contentOffset = CGPointMake(0, SEARCH_BAR_HEIGHT);

    _tableViewController = [[UITableViewController alloc]initWithStyle:UITableViewStylePlain];
    [self addChildViewController:_tableViewController];
    
    //_tableViewController.refreshControl = [UIRefreshControl new];
   // [_tableViewController.refreshControl addTarget:self action:@selector(loadStream) forControlEvents:UIControlEventValueChanged];
    
    _tableViewController.tableView = _tableView;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self didMoveToParentViewController: _tableViewController];
//    _tableView.tableHeaderView = self.createNewButton;
	// Do any additional setup after loading the view.
    
    if([IMUtils getOSVersion] >= 7.0) {
        self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        CGRect createButtonFrame = self.createNewButton.frame;
        createButtonFrame.size.width = 300;
        self.createNewButton.frame = createButtonFrame;
    }
    else {
        CGRect createButtonFrame = self.createNewButton.frame;
        createButtonFrame.size.width = 290;
        self.createNewButton.frame = createButtonFrame;
        
        CGRect promoInfoButtonFrame = self.promoInfoButton.frame;
        promoInfoButtonFrame.origin.x = 290;
        self.promoInfoButton.frame = promoInfoButtonFrame;
    }

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear: animated];
    isSelectDeselectPressesd=NO;
    _tableViewController.refreshControl = [UIRefreshControl new];
    [_tableViewController.refreshControl addTarget:self action:@selector(loadStream) forControlEvents:UIControlEventValueChanged];
    [self fetchPersonalPromosResultController: nil];
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    //[self.tableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    [moc rollback];
}

- (void)loadStream
{
    //    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:self.ascending];
    //    NSArray *sortDescriptors = @[sortDescriptor];
    //
    //    NSArray* _objects = [_objects sortedArrayUsingDescriptors:sortDescriptors];
    
//    _ascending = !_ascending;
    [self getPersonalAds];
    [self performSelector:@selector(updateTable) withObject:nil
               afterDelay:1];
}

- (void)updateTable
{
    
    [self.tableView reloadData];
    
    [_tableViewController.refreshControl endRefreshing];
}

#pragma mark - Webservice methods
- (void)getPersonalAds
{
    [[IMAppDelegate sharedDelegate] getPersonalAds];
}
-(void)setPersonalAds:(NSNumber *)promo_id is_Enable:(NSNumber *)is_Enable{
    [[IMAppDelegate sharedDelegate] setPersonalAds:promo_id is_Enable:is_Enable];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DownloadPictureoperation
-(void)didDownloadImage{
    
    
    DebugLog(@"didDownloadImage");
    
    //save path in core data
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [fectchedResultsController.sections count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (fectchedResultsController.fetchedObjects.count) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }

    // Return the number of rows in the section.
    if (section) {
        return [[fectchedResultsController.sections objectAtIndex: section-1] numberOfObjects];
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [IMPromoSearchCell cellLoadedFromNibFile];
    }
    
    if (cell.selectedBackgroundView.tag != SELECTED_BG_VIEW_TAG) {
        UIView *selectionColor = [[UIView alloc] initWithFrame:CGRectMake(0, 1, cell.frame.size.width, 42)];        selectionColor.tag = SELECTED_BG_VIEW_TAG;
        selectionColor.backgroundColor = [UIColor whiteColor];
        cell.selectedBackgroundView = selectionColor;
    }
    
    NSArray* array = nil;
    NSArray* sectionArray = fectchedResultsController.sections;
    if(sectionArray.count > indexPath.section - 1)
        array = [[sectionArray objectAtIndex: indexPath.section - 1] objects];
    
    IMPromos* promo = nil;
    if([array count] > indexPath.row)
        promo = (IMPromos*)[array objectAtIndex: indexPath.row];
    
    cell.tag = indexPath.row;
    cell.textLabel.text = promo.promo_name;
    // Configure the cell...
    if (tableView.isEditing) {
        cell.textLabel.textColor = [UIColor darkGrayColor];
        if (promo.is_enabled.boolValue) {
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition: UITableViewScrollPositionNone];
        }
        else
        {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
    else
    {
        if (promo.is_enabled.boolValue) {
            cell.textLabel.textColor = [UIColor darkGrayColor];
        }
        else {
            cell.textLabel.textColor = [UIColor redColor];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.tableView.editing)
    {
        IMPromos* promo = (IMPromos*)[[[fectchedResultsController.sections objectAtIndex: indexPath.section - 1] objects] objectAtIndex: indexPath.row];
        
        promo.is_enabled = [NSNumber numberWithBool: YES];
         [self setPersonalAds:promo.promo_id is_Enable:promo.is_enabled];
//        [[IMDataHandler sharedInstance] saveDatabase];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.tableView.editing)
    {
        IMPromos* promo = (IMPromos*)[[[fectchedResultsController.sections objectAtIndex: indexPath.section -1] objects] objectAtIndex: indexPath.row];
        
        promo.is_enabled = [NSNumber numberWithBool: NO];
        [self setPersonalAds:promo.promo_id is_Enable:promo.is_enabled];
//        [[IMDataHandler sharedInstance] saveDatabase];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section) {
        return 26;
    }
    else if(0==section)
    return 44;
    else
        return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section) {
        IMPromoHeaderView *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[IMPromoHeaderView cellReusableIdentifier]];
        if (nil == cell) {
            NSLog(@"[IMPromoHeaderView cellLoadedFromNibFile];");
            cell = [IMPromoHeaderView cellLoadedFromNibFile];
        }
        cell.headerLabel.text = [[fectchedResultsController.sections objectAtIndex: section-1] indexTitle];
        return cell;
    }
    else if(0==section)
        return _createNewButtonCell;
    return nil;
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSMutableArray* sectionList = [[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles] mutableCopy];
    id lastObject = [sectionList lastObject];
    [sectionList removeLastObject];
    [sectionList insertObject:lastObject atIndex: 0];
    return sectionList;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (title == UITableViewIndexSearch)
    {
        CGRect searchBarFrame = self.searchDisplayController.searchBar.frame;
        [tableView scrollRectToVisible:searchBarFrame animated:YES];
        
        return -1;
    }
    else {
        
        int count = 0;
        
        for(count=0; count < fectchedResultsController.sections.count; count++)
        {
            id <NSFetchedResultsSectionInfo> sectionInfo =
            [[fectchedResultsController sections] objectAtIndex:count];
            NSString *sectionHeaderNames = [sectionInfo name];
            NSLog(@"Input title = %@,index = %d,section: %@,count: %i ",title,index, sectionHeaderNames,count);
            
            if([title isEqualToString:sectionHeaderNames] || [[title lowercaseString] isEqualToString:sectionHeaderNames])
            {
                NSLog(@"return %d",count);
                
                return count;
                break;
            }
            else
            {
                if (title.length && sectionHeaderNames.length) {
                    unichar first = [title characterAtIndex:0];
                    if ((first >= 'a' && first <= 'z') || (first >='A' && first <= 'Z')) {
                        unichar fectchedChar = [[sectionHeaderNames uppercaseString] characterAtIndex:0];
                        if (first < fectchedChar) {
                            
                            NSLog(@"return %d",count);
                            
                            return count;
                            break;
                        }
                    }
                }
            }
        }
        NSLog(@"return %d",count);
        return count;

    }
}
/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */

- (IBAction)onEditTapped:(UIBarButtonItem *)sender {
    
    [self.tableView setEditing:!self.tableView.editing animated:YES];
    if (self.tableView.editing) {
        if(![[[IMDataHandler sharedInstance] disabledPromos: 1] count])
            [_selectAllButton setTitle: @"Deselect All" forState: UIControlStateNormal];
        else
            [_selectAllButton setTitle: @"Select All" forState: UIControlStateNormal];
        [sender setTitle:@"Done"];
        self.title = @"Enable/Disable Promo";
        self.selectAllButton.hidden = NO;
        CGRect tableFrame = self.tableView.frame;
        tableFrame.size.height -= self.selectAllButton.frame.size.height;
        self.tableView.frame = tableFrame;
    }
    else{
        
        [sender setTitle:@"Edit"];
        self.selectAllButton.hidden = YES;
        self.title = @"Personal Promos";

        CGRect tableFrame = self.tableView.frame;
        tableFrame.size.height += self.selectAllButton.frame.size.height;
        self.tableView.frame = tableFrame;
        [[IMDataHandler sharedInstance] saveDatabase];
        
        if(isSelectDeselectPressesd)
        {
            for(int i = 0; i < [fectchedResultsController.sections count]; ++i)
            {
                for(int j = 0; j < [[fectchedResultsController.sections objectAtIndex: i] numberOfObjects]; ++j)
                {
                    IMPromos* promo = (IMPromos*)[[[fectchedResultsController.sections objectAtIndex: i] objects] objectAtIndex: j];
                    [self setPersonalAds :promo.promo_id is_Enable:promo.is_enabled];
                    
                }
            }
            
        }

    }
    [self.tableView reloadData];
    
    
}

- (IBAction)onCreateNewPromoTapped:(id)sender {
    
    if ([[IMDataHandler sharedInstance] fetchPersonalPromos].count < 12) {
        [self performSegueWithIdentifier:@"CreateNewPromo" sender:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"You can not create more than 12 personal promos" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alertView show];
    }
}

- (IBAction)onSelectAllTapped:(id)sender {
    if (_selectAllButton.hidden) {
        return;
    }
    isSelectDeselectPressesd=YES;
    UIButton* button = (UIButton*)sender;
    
    if([button.titleLabel.text isEqualToString: @"Select All"])
    {
        for(int i = 0; i < [fectchedResultsController.sections count]; ++i)
        {
            for(int j = 0; j < [[fectchedResultsController.sections objectAtIndex: i] numberOfObjects]; ++j)
            {
                IMPromos* promo = (IMPromos*)[[[fectchedResultsController.sections objectAtIndex: i] objects] objectAtIndex: j];
                //            if([promo.is_enabled boolValue])
                NSIndexPath *path = [NSIndexPath indexPathForRow: j inSection: i + 1];
                NSLog(@"select at %@",path);
                [self.tableView selectRowAtIndexPath:path animated:YES scrollPosition: UITableViewScrollPositionTop];
                promo.is_enabled = [NSNumber numberWithBool:YES];
            }
        }
        [button setTitle: @"Deselect All" forState: UIControlStateNormal];
    }
    else
    {
        for(int i = 0; i < [fectchedResultsController.sections count]; ++i)
        {
            for(int j = 0; j < [[fectchedResultsController.sections objectAtIndex: i] numberOfObjects]; ++j)
            {
                IMPromos* promo = (IMPromos*)[[[fectchedResultsController.sections objectAtIndex: i] objects] objectAtIndex: j];
                //            if([promo.is_enabled boolValue])
                NSIndexPath *path = [NSIndexPath indexPathForRow: j inSection: i + 1];
                NSLog(@"select at %@",path);
                [self.tableView deselectRowAtIndexPath:path animated:YES];
                promo.is_enabled = [NSNumber numberWithBool:NO];
            }
        }
        [button setTitle: @"Select All" forState: UIControlStateNormal];
    }
}

- (IBAction)onPromoInfoButtonClick:(id)sender
{
    UIButton* button = (UIButton*)sender;
    
    UITableViewCell* cell = nil;
//    if([IMUtils getOSVersion] >= 7.0)
//        cell = (UITableViewCell*)[[[button superview] superview] superview];
//    else
        cell = (UITableViewCell*)[[button superview] superview];
    
    selectedPromoIndex = [self.tableView indexPathForCell: cell];
    [self performSegueWithIdentifier: @"IMPersonalPromoDetailsController" sender: self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    if ([segue.identifier isEqualToString:@"IMPersonalPromoDetailsController"]) {
//        UINavigationController *navigationController = segue.destinationViewController;
//        IMPersonalPromoDetailsController *controller = (IMPersonalPromoDetailsController *)[navigationController topViewController];
//        controller.isupdatingPromo = YES;
//    }
    
    if(selectedPromoIndex != nil) {
        IMPersonalPromoDetailsController* pvc = segue.destinationViewController;
        if([fectchedResultsController.sections count] > 0)
        {
            NSArray* array = [[fectchedResultsController.sections objectAtIndex: selectedPromoIndex.section-1] objects];
            pvc.promo = [array objectAtIndex: selectedPromoIndex.row];
        }
        selectedPromoIndex = nil;
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;                     // called when keyboard search button pressed
{
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    
    fectchedResultsController = [self fetchPersonalPromosResultController: searchBar.text];
    [self.tableView reloadData];
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    if([searchText isEqualToString: @""])
        fectchedResultsController = [self fetchPersonalPromosResultController: nil];
    else
        fectchedResultsController = [self fetchPersonalPromosResultController: searchText];
    [self.tableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    
    fectchedResultsController = [self fetchPersonalPromosResultController: nil];
    [self.tableView reloadData];
    
    //[nonImgrContactsTableView setContentOffset:CGPointMake(0, 44) animated:YES];
    [searchBar resignFirstResponder];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    fectchedResultsController.delegate = nil;
    fectchedResultsController = nil;
    
    //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    // NSArray* searchResults = [fetchedResultsController.fetchedObjects filteredArrayUsingPredicate:resultPredicate];
    if([searchText isEqualToString: @""])
        fectchedResultsController = [self fetchPersonalPromosResultController:nil];
    else
        fectchedResultsController = [self fetchPersonalPromosResultController: searchText];
    [self.tableView reloadData];
}

#pragma mark -
- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView
{
    tableView.editing = self.tableView.editing;
    tableView.allowsSelectionDuringEditing = self.tableView.allowsSelectionDuringEditing;
    tableView.allowsSelection = self.tableView.allowsSelection;
    tableView.allowsMultipleSelection = self.tableView.allowsMultipleSelection;
    tableView.allowsMultipleSelectionDuringEditing = self.tableView.allowsMultipleSelectionDuringEditing;
}
- (void)searchDisplayController:(UISearchDisplayController *)controller willUnloadSearchResultsTableView:(UITableView *)tableView;
{
    // search is done so get rid of the search FRC and reclaim memory
    //searchFetchedResultsController.delegate = nil;
    //searchFetchedResultsController = nil;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
