//
//  IMUserVerificationViewController.m
//  IMGR
//
//  Created by akram on 04/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMUserVerificationViewController.h"
#import "IMUserProfileViewController.h"
#import "IMUtils.h"
#import "CMNetManager.h"
#import "ColorC.h"
#import "MBProgressHUD.h"

@interface IMUserVerificationViewController ()<CMNetManagerDelegate>
{
    MBProgressHUD* progressView;
}
@end

@implementation IMUserVerificationViewController
@synthesize topConstraint;
@synthesize phoneNumber;
@synthesize countryCode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)adjustPositionOfButton: (int)xPos
{
//    CGFloat height = contactsTableView.contentSize.height;
//    CGFloat maxHeight = contactsTableView.superview.frame.size.height - contactsTableView.frame.origin.y;
//    
//    // if the height of the content is greater than the maxHeight of
//    // total space on the screen, limit the height to the size of the
//    // superview.
//    
//    if (height > maxHeight)
//        height = maxHeight;
//    
//    if(isIMGRViewSelected)
//        height -= 44;
    // now set the height constraint accordingly
    
    [UIView animateWithDuration:0.0 animations:^{
        //self.tableViewHeightConstraint.constant = height;
        xPosConstraint.constant = xPos;
        [self.view needsUpdateConstraints];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIView *paddingView         = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    paddingView.backgroundColor = [UIColor clearColor];
    pinCodeField.leftView       = paddingView;
    pinCodeField.leftViewMode   = UITextFieldViewModeAlways;
    
//    UIView *padView         = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
//    padView.backgroundColor = [UIColor clearColor];
//    countryCodeField.leftView   = padView;
//    countryCodeField.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *phonePaddingView    = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    phonePaddingView.backgroundColor = [UIColor clearColor];
    phoneNumberField.leftView   = phonePaddingView;
    phoneNumberField.leftViewMode = UITextFieldViewModeAlways;
    
    countryCodeField.hidden     = NO;
    countryCodeField.text       = countryCode;
    phoneNumberField.text       = phoneNumber;
    
    countryCodeField.backgroundColor = [UIColor clearColor];
    phoneNumberField.backgroundColor = [UIColor clearColor];
    
    CGSize size = [countryCode sizeWithFont:[UIFont systemFontOfSize: 17]
                           constrainedToSize:CGSizeMake(63, FLT_MAX)
                               lineBreakMode:NSLineBreakByTruncatingTail];
    
    [self adjustPositionOfButton: 16 + size.width + 10];
    
    //[pinCodeField becomeFirstResponder];
    
    if ([self respondsToSelector:@selector(topLayoutGuide)]) {
        [self.view removeConstraint:self.topConstraint];
        
        self.topConstraint =
        [NSLayoutConstraint constraintWithItem:verificationScrollView
                                     attribute:NSLayoutAttributeTop
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self.topLayoutGuide
                                     attribute:NSLayoutAttributeBottom
                                    multiplier:1
                                      constant:0];
        
        [self.view addConstraint:self.topConstraint];
        
        [self.view setNeedsUpdateConstraints];
        [self.view layoutIfNeeded];
    }
    
    progressView = nil;//SS we do not need it
    progressView = [[MBProgressHUD alloc] initWithView: self.view];
    progressView.labelText = @"Please wait...";
    
    [app.window addSubview: progressView];
}

- (void)viewWillAppear:(BOOL)animated
{
    
}

- (void)viewDidAppear:(BOOL)animated
{
   
}

- (void)verifyUser
{
    [progressView show: YES];
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
   
    NSString* udid = [[NSUserDefaults standardUserDefaults] objectForKey:K_DEFAULTS_DEVICE_UDID];
    NSAssert(nil != udid, @"Device UDID not expected to be nil.");
    [parameters setObject:udid forKey: @"deviceId"];
    DebugLog(@"Device ID = %@", udid);
    //NSString* countrycode = [countryCodeField.text stringByReplacingOccurrencesOfString:@"+" withString:@"00"];
    [parameters setObject:[IMUtils phonenumberFromFormattedNumber:phoneNumberField.text] forKey: @"phonenumber"];
    
    if(pinCodeField.text)
        [parameters setObject:pinCodeField.text forKey: @"activationcode"];
    
    DebugLog(@"Verification Parameters = %@", parameters);
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kVerifyUser] parameters:parameters queueIdentifier:"feedQueue" queueType:CONCURRENT httpMethod: POST];
    netManager.delegate = self;
    netManager.tag = E_VERIFICATION_SERVICE;
    [netManager startRequest];

}

#pragma mark - IBActions
- (IBAction)showRegistrationPopup:(id)sender
{
    if(pinCodeField && [pinCodeField.text isKindOfClass:[NSNull class]])
    {
        UIAlertView* registrationAlertView = [[UIAlertView alloc] initWithTitle: @"Registration Failed" message:@"Please enter valid activation code" delegate:nil cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
        [registrationAlertView show];
        return;
    }
    [self verifyUser];
    //[self displayUserProfileScreen];
}

#pragma mark - Webservice methods
- (void)registerUser
{
    [progressView show: YES];
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//     [defaults setBool:NO forKey:K_ALREADY_REGISTERED];
  //  [defaults synchronize];
    
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString* udid = [[NSUserDefaults standardUserDefaults] objectForKey:K_DEFAULTS_DEVICE_UDID];
    NSAssert(nil != udid, @"Device UDID not expected to be nil.");
    [parameters setObject:udid forKey: @"deviceId"];
    DebugLog(@"Device ID = %@", udid);
    
    NSString* deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:K_DEVICE_TOKEN];
    if(deviceToken != nil)
        [parameters setObject: deviceToken forKey: @"deviceToken"];
    DebugLog(@"Device Token = %@", deviceToken);
    
    NSString* countrycode = [countryCodeField.text stringByReplacingOccurrencesOfString:@"+" withString: @"00"];
    
    if(countryCode)
        [parameters setObject: countrycode forKey: @"countryCode"];
    
    if(phoneNumber)
        [parameters setObject:[IMUtils phonenumberFromFormattedNumber: phoneNumberField.text] forKey: @"phonenumber"];
    
    [parameters setObject:KDeviceModel forKey: @"deviceType"];
    [parameters setObject:OS forKey: @"os"];
    [parameters setObject:KAppVersion forKey: @"appVersion"];
    
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kRegisterUser] parameters:parameters queueIdentifier:"feedQueue" queueType:CONCURRENT httpMethod: POST];
    netManager.delegate = self;
    netManager.tag = E_REGISTER_SERVICE;
    [netManager startRequest];
}

#pragma mark - NetManager Delegate methods
//This delegate gets called on success
-(void)netManager:(CMNetManager*)netManager didReceiveResponse:(NSDictionary*)dictionary
{
    [progressView hide: YES];
    DebugLog(@"Response = %@", dictionary);
    
    if ([dictionary objectForKey:@"success"] && [[dictionary objectForKey:@"success"] integerValue] == 0)
    {
        switch (netManager.tag)
        {
            case E_VERIFICATION_SERVICE:
            {
                int code = [[[dictionary objectForKey: @"error"] objectForKey: @"errorCode"] intValue];
                switch (code) {
                    case 1001:
                    case 1016:
                    {
                        UIAlertView* registrationAlertView = [[UIAlertView alloc] initWithTitle: @"Registration Failed" message:@"Please enter valid activation code" delegate:nil cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
                        [registrationAlertView show];
                    }break;
                        
                    default:
                        break;
                }
            }break;
                
            default:
                break;
        }
    }
    else
    {
    switch (netManager.tag)
    {
        case E_REGISTER_SERVICE:
            phoneNumber = phoneNumberField.text;
            countryCode = countryCodeField.text;
            [editButton setTitle: @"Edit" forState: UIControlStateNormal];
            [pinCodeField becomeFirstResponder];
            //[self displayVerificationScreen];
            break;
            
        case E_VERIFICATION_SERVICE:
        {
            UIAlertView* registrationAlertView = [[UIAlertView alloc] initWithTitle: @"Registration Successful" message:[NSString stringWithFormat: @"%@\n%@",[NSString stringWithFormat: @"%@ %@",countryCodeField.text, phoneNumberField.text], @"This phone number is successfully registered with IMGR"] delegate:nil cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
            [registrationAlertView show];
             [self displayUserProfileScreen];
        }break;
            
        default:
            break;
    }
    }
}

-(void)netManager:(CMNetManager*)netManager didReceiveResponseString:(NSString*)response
{
    [progressView hide: YES];
}

//This delegate gets called on failure
-(void)netManager:(CMNetManager*)netManager didFailWithError:(NSError*)error
{
    [progressView hide: YES];
}

-(void)netManager:(CMNetManager *)netManager didGetSuccessToConnectWithServer:(NSError *)error
{
    
}

//This delegate gets called if internet connection is not available
-(void)netManager:(CMNetManager *)netManager didFailToConnectWithServer:(NSError *)error
{
    [progressView hide: YES];
    [IMUtils noInternetAvailable];
}

- (IBAction)onEditButtonClick:(id)sender
{
    UIButton* button = (UIButton*)sender;
    if([button.titleLabel.text isEqualToString: @"Edit"]) {
        [button setTitle: @"Done" forState: UIControlStateNormal];
        
        countryCodeField.backgroundColor = [UIColor colorFromRGBIntegers:242 green:242 blue:242 alpha:1.0];
        phoneNumberField.backgroundColor = [UIColor colorFromRGBIntegers:242 green:242 blue:242 alpha:1.0];
        
        countryCodeField.hidden = NO;
        phoneNumberField.text = phoneNumber;
        countryCodeField.text = countryCode;
        
        countryCodeField.userInteractionEnabled = YES;
        phoneNumberField.userInteractionEnabled = YES;
        
//        CGRect codeFrame = countryCodeField.frame;
//        codeFrame.origin.x = 20;
//        countryCodeField.frame = codeFrame;
        
//        CGRect rect = phoneNumberField.frame;
//        rect.origin.x = 73;
//        phoneNumberField.frame = rect;
        CGSize size = [countryCode sizeWithFont:[UIFont systemFontOfSize: 17]
                              constrainedToSize:CGSizeMake(63, FLT_MAX)
                                  lineBreakMode:NSLineBreakByTruncatingTail];
        [self adjustPositionOfButton: 16 + size.width + 15];
    }
    else {
        
        NSString* code = nil;
        if([countryCodeField.text isEqualToString: @""])
            code = @"";
        else
            code = countryCodeField.text;
        
        NSString* message = [NSString stringWithFormat: @"%@ %@\n Is this phone number correct?\n Your verification code will be sent to this number", code, phoneNumberField.text];
        
        UIAlertView* confirmationAlertView = [[UIAlertView alloc] initWithTitle: @"Number Confirmation" message: message delegate: self cancelButtonTitle:@"Change" otherButtonTitles:@"Yes", nil];
        confirmationAlertView.tag = 1000;
        [confirmationAlertView show];
        
            }
    [phoneNumberField becomeFirstResponder];
}

- (IBAction)onSendAgainButtonClick:(id)sender
{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"We have sent you an SMS with the code to enter(This may take some time):" delegate:nil cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
    [alertView show];
    if([editButton.titleLabel.text isEqualToString: @"Done"])
    {
        [editButton setTitle: @"Edit" forState: UIControlStateNormal];
        countryCodeField.text       = countryCode;
        phoneNumberField.text       = phoneNumber;
        
        CGSize size = [countryCode sizeWithFont:[UIFont systemFontOfSize: 17]
                              constrainedToSize:CGSizeMake(63, FLT_MAX)
                                  lineBreakMode:NSLineBreakByTruncatingTail];
        [self adjustPositionOfButton: 16 + size.width + 10];
        
        countryCodeField.userInteractionEnabled = NO;
        phoneNumberField.userInteractionEnabled = NO;
        
        countryCodeField.backgroundColor = [UIColor clearColor];
        phoneNumberField.backgroundColor = [UIColor clearColor];
        
        [countryCodeField resignFirstResponder];
        [phoneNumberField resignFirstResponder];
        
        [pinCodeField becomeFirstResponder];
    }
    else
    {
        [self registerUser];
    }
}

#pragma mark - UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1000) {
        switch (buttonIndex)
        {
            case 1:
            {
                [editButton setTitle: @"Edit" forState: UIControlStateNormal];
                countryCodeField.backgroundColor = [UIColor clearColor];
                phoneNumberField.backgroundColor = [UIColor clearColor];
                
                countryCodeField.userInteractionEnabled = NO;
                phoneNumberField.userInteractionEnabled = NO;
                
                CGSize size = [countryCodeField.text sizeWithFont:[UIFont systemFontOfSize: 17]
                                      constrainedToSize:CGSizeMake(63, FLT_MAX)
                                          lineBreakMode:NSLineBreakByTruncatingTail];
                [self adjustPositionOfButton:16 + size.width + 10];
                [self registerUser];
            }break;
                
            default:
                break;
        }
        
        return;
    }
    else
    {
        switch (buttonIndex)
        {
            case 0:
                //[self displayUserProfileScreen];
                break;
                
            case 1:
                
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - UITextFieldDelegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.3 animations:^{
        
        verificationScrollView.contentSize = CGSizeMake(verificationScrollView.frame.size.width, verificationScrollView.frame.size.height + 100);
        
    } completion:^(BOOL finished) {  }];
}

- (BOOL)backSpaceDetection: (NSString*)string fieldID: (int)index
{
    //Backspace Detection
    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char, "\b");
    
    if (isBackSpace == -8) {
        
        return YES;
    }
    return NO;
}

#pragma mark - helper methods

-(NSString*) formatPhoneString:(NSString*) preFormatted
{
    //delegate only allows numbers to be entered, so '-' is the only non-legal char.
    NSString* workingString = [preFormatted stringByReplacingOccurrencesOfString:@"(" withString:@""];
    //workingString = [preFormatted stringByReplacingOccurrencesOfString:@")" withString:@""];
    //workingString = [preFormatted stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    //insert first '('
    if(workingString.length > 0)
    {
        //workingString = [preFormatted stringByReplacingOccurrencesOfString:@"(" withString:@""];
        workingString = [workingString stringByReplacingCharactersInRange:NSMakeRange(0, 0) withString:@"("];
    }
    //insert second ')'
    if(workingString.length > 4)
    {
        workingString = [preFormatted stringByReplacingOccurrencesOfString:@")" withString:@""];
        workingString = [workingString stringByReplacingCharactersInRange:NSMakeRange(4, 0) withString:@")"];
    }
    
    //insert second '-'
    if(workingString.length > 8)
    {
        workingString = [workingString stringByReplacingOccurrencesOfString:@"-" withString:@""];
        workingString = [workingString stringByReplacingCharactersInRange:NSMakeRange(8, 0) withString:@"-"];
    }
    
    return workingString;
    
}

-(bool) range:(NSRange) range ContainsLocation:(NSInteger) location
{
    if(range.location <= location && range.location+range.length >= location)
    {
        return true;
    }
    
    return false;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Backspace Detection
    if([self backSpaceDetection: string fieldID: textField.tag])
        return YES;
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if(textField.tag == 2000)
    {
        int charCount = [newString length];
        NSCharacterSet *numSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789+"];
        if ([newString rangeOfCharacterFromSet:[numSet invertedSet]].location != NSNotFound
            || [string rangeOfString:@"+"].location != NSNotFound
            || charCount > 4) {
            return NO;
        }
        
        if (![string isEqualToString:@""])
        {
            if (charCount == 1)
            {
                newString = [NSString stringWithFormat:@"+%@", newString];
            }
        }
        textField.text = newString;
        textField.text = [textField.text substringToIndex: [textField.text length] - 1];
        return YES;
    }
    
    if(textField == pinCodeField)
    {
        return ([newString length] > 6) ? NO : YES;
    }
    
    //calculate new length
    NSInteger moddedLength = textField.text.length-(range.length-string.length);
    
    // max size.
    if (moddedLength >= MAX_PHONE_NUMBER) {
        return NO;
    }
    
    // Reject non-number characters
    if (range.length == 0 &&![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]]) {
        return NO;
    }
    
    // Auto-add hyphen before appending 1st, 4th or 8th digit
    if ([self range:range ContainsLocation:0] || [self range:range ContainsLocation:4] || [self range:range ContainsLocation:8]) {
        textField.text = [self formatPhoneString:[textField.text stringByReplacingCharactersInRange:range withString:string]];
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.3 animations:^{
        
        verificationScrollView.contentSize = CGSizeMake(verificationScrollView.frame.size.width, verificationScrollView.frame.size.height);
        
    } completion:^(BOOL finished) {  }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    IMUserProfileViewController *controller = segue.destinationViewController;
   // IMUserProfileViewController *controller = (IMUserProfileViewController *)[navigationController topViewController];
    controller.phoneNumber = phoneNumber;
    controller.countryCode = countryCode;
}

- (void)displayUserProfileScreen
{
    [self performSegueWithIdentifier:@"IMUserProfileViewController" sender:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
