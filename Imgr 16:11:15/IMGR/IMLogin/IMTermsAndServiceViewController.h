//
//  IMTermsAndServiceViewController.h
//  IMGR
//
//  Created by akram on 09/04/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMTermsAndServiceViewController : UIViewController
{
    IBOutlet UIWebView *termsAndServiceWebview;
}

@end
