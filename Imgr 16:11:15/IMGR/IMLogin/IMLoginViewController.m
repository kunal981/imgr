//
//  IMLoginViewController.m
//  IMGR
//
//  Created by akram on 04/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMLoginViewController.h"
#import "IMUserVerificationViewController.h"
#import "IMUtils.h"

#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "MBProgressHUD.h"

enum
{
    E_COUNTRY_CODE_FIELD_ID     = 1000,
    E_PHONE_NUMBER_FIELD_ID,
};

@interface IMLoginViewController ()
{
    NSString*   phoneNumber;
    CGFloat previousScrollViewYOffset;
    
    MBProgressHUD* progressView;
}
@end

@implementation IMLoginViewController
@synthesize topConstraint;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    phoneNumber = [[NSString alloc] init];
    
    [phoneNumberField becomeFirstResponder];
    if ([self respondsToSelector:@selector(topLayoutGuide)]) {
        [self.view removeConstraint:self.topConstraint];
        
        self.topConstraint =
        [NSLayoutConstraint constraintWithItem:registerScrollView
                                     attribute:NSLayoutAttributeTop
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self.topLayoutGuide
                                     attribute:NSLayoutAttributeBottom
                                    multiplier:1
                                      constant:0];
        
        [self.view addConstraint:self.topConstraint];
        
        [self.view setNeedsUpdateConstraints];
        [self.view layoutIfNeeded];
    }
    countryCodeField.text = @"+1";
    
    progressView = nil;//SS we do not need it
    progressView = [[MBProgressHUD alloc] initWithView: self.view];
    progressView.labelText = @"Please wait...";
    
    [app.window addSubview: progressView];
}

- (void)viewDidAppear:(BOOL)animated
{
    UIView *paddingView         = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    paddingView.backgroundColor = [UIColor clearColor];
    countryCodeField.leftView   = paddingView;
    countryCodeField.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *phonePaddingView    = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    phonePaddingView.backgroundColor = [UIColor clearColor];
    phoneNumberField.leftView   = phonePaddingView;
    phoneNumberField.leftViewMode = UITextFieldViewModeAlways;
    
    HMDiallingCode* dialingCode = [[HMDiallingCode alloc] initWithDelegate:self];
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    // Get carrier name
    NSString *carrierName = [carrier carrierName];
    if (carrierName != nil)
        DebugLog(@"Carrier: %@", carrierName);
    
    // Get mobile country code
    NSString *mcc = [carrier isoCountryCode];
    if (mcc != nil)
        DebugLog(@"Mobile Country Code (MCC): %@", mcc);
    
    // Get mobile network code
    NSString *mnc = [carrier mobileNetworkCode];
    
    if (mnc != nil)
        DebugLog(@"Mobile Network Code (MNC): %@", mnc);
    
    [dialingCode getDiallingCodeForCountry: mcc];
    
}

#pragma mark - HMDialingCode Delegate methods

- (void)didGetDiallingCode:(NSString *)diallingCode forCountry:(NSString *)countryCode
{
    DebugLog(@"Country Dialing Code: %@", diallingCode);
    
    countryCodeField.text = [@"+" stringByAppendingString: diallingCode];
}

- (void)didGetCountries:(NSArray *)countries forDiallingCode:(NSString *)diallingCode
{
    
}

- (void)failedToGetDiallingCode
{
    
}

#pragma mark - Webservice methods
- (void)registerUser
{
    [progressView show: YES];
    
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString* udid = [[NSUserDefaults standardUserDefaults] objectForKey:K_DEFAULTS_DEVICE_UDID];
    NSAssert(nil != udid, @"Device UDID not expected to be nil.");
    [parameters setObject:udid forKey: @"deviceId"];
    
    NSString* deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:K_DEVICE_TOKEN];
    if(deviceToken != nil)
        [parameters setObject: deviceToken forKey: @"deviceToken"];
    
    DebugLog(@"Device ID = %@", udid);
    NSString* countryCode = [countryCodeField.text stringByReplacingOccurrencesOfString:@"+" withString:@"00"];
    
    if(countryCode)
        [parameters setObject:countryCode forKey: @"countryCode"];
    
    
    if(phoneNumber)
        [parameters setObject: phoneNumber forKey: @"phonenumber"];
    
    [parameters setObject:KDeviceModel forKey: @"deviceType"];
    [parameters setObject:OS forKey: @"os"];
    [parameters setObject:KAppVersion forKey: @"appVersion"];
    
    DebugLog(@"Registration Parameters = %@", parameters);
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kRegisterUser] parameters:parameters queueIdentifier:"feedQueue" queueType:CONCURRENT httpMethod: POST];
    netManager.delegate = self;
    netManager.tag = E_REGISTER_SERVICE;
    [netManager startRequest];
}

#pragma mark - IBActions
- (IBAction)showConfirmationPopup:(id)sender
{
    phoneNumber = [IMUtils phonenumberFromFormattedNumber: phoneNumberField.text];
    if([phoneNumberField.text isEqualToString: @""] || [phoneNumber length] < 10)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: NSLocalizedString(@"CORRECT PHONE NUMBER", @"") delegate: nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        return;
    }
    if([countryCodeField.text isEqualToString: @""])
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: NSLocalizedString(@"CORRECT COUNTRY CODE", @"") delegate: nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        return;
    }
    NSString* code = nil;
    if([countryCodeField.text isEqualToString: @""])
        code = @"";
    else
        code = countryCodeField.text;
    NSString* message = [NSString stringWithFormat: @"%@ %@\n Is this phone number correct?\n Your verification code will be sent to this number", code, phoneNumberField.text];
    
    UIAlertView* confirmationAlertView = [[UIAlertView alloc] initWithTitle: @"Number Confirmation" message: message delegate: self cancelButtonTitle:@"Change" otherButtonTitles:@"Yes", nil];
    [confirmationAlertView show];
}

#pragma mark - UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 10000 && buttonIndex == 0)
    {
        NSString* phoneNo = [IMUtils phonenumberFromFormattedNumber:phoneNumber];
        
        [[NSUserDefaults standardUserDefaults] setObject: phoneNo forKey: k_DEFAULTS_KEY_USER_ID];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[IMAppDelegate sharedDelegate] login];
        [[IMAppDelegate sharedDelegate] getPromos];
        
        app.window.rootViewController = nil;
        
        UIStoryboard* messageSB = [UIStoryboard storyboardWithName: @"MessagesControllers" bundle: [NSBundle mainBundle]];
        
        UINavigationController* navigationController    = [messageSB instantiateInitialViewController];
        
        //app.window.rootViewController = navigationController;
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"REGISTRATION_COMPLETED" object: navigationController];
        return;
    }
    switch (buttonIndex)
    {
        case 0:
            
            break;
            
        case 1:
            [self performSelector:@selector(registerUser) withObject:nil afterDelay:.1];
            //[self displayVerificationScreen];
            break;
             
        default:
            break;
    }
}

#pragma mark - UITextFieldDelegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == countryCodeField) {
        countryCodeField.placeholder = @"";
        countryCodeField.text = @"";
    }
    [UIView animateWithDuration:0.3 animations:^{
        
        registerScrollView.contentSize = CGSizeMake(registerScrollView.frame.size.width, registerScrollView.frame.size.height + 100);
        
    } completion:^(BOOL finished) {  }];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //phoneNumber = textField.text;
    [UIView animateWithDuration:0.3 animations:^{
        
        registerScrollView.contentSize = CGSizeMake(registerScrollView.frame.size.width, registerScrollView.frame.size.height);
        
    } completion:^(BOOL finished) {  }];
}

- (BOOL)backSpaceDetection: (NSString*)string fieldID: (int)index
{
    //Backspace Detection
    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char, "\b");
    
    if (isBackSpace == -8) {
        
        return YES;
    }
    return NO;
}

#pragma mark - helper methods

-(NSString*) formatPhoneString:(NSString*) preFormatted
{
    //delegate only allows numbers to be entered, so '-' is the only non-legal char.
    NSString* workingString = [preFormatted stringByReplacingOccurrencesOfString:@"(" withString:@""];
    //workingString = [preFormatted stringByReplacingOccurrencesOfString:@")" withString:@""];
    //workingString = [preFormatted stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    //insert first '('
    if(workingString.length > 0)
    {
        //workingString = [preFormatted stringByReplacingOccurrencesOfString:@"(" withString:@""];
        workingString = [workingString stringByReplacingCharactersInRange:NSMakeRange(0, 0) withString:@"("];
    }
    //insert second ')'
    if(workingString.length > 4)
    {
        workingString = [preFormatted stringByReplacingOccurrencesOfString:@")" withString:@""];
        workingString = [workingString stringByReplacingCharactersInRange:NSMakeRange(4, 0) withString:@")"];
    }
    
    //insert second '-'
    if(workingString.length > 8)
    {
        workingString = [workingString stringByReplacingOccurrencesOfString:@"-" withString:@""];
        workingString = [workingString stringByReplacingCharactersInRange:NSMakeRange(8, 0) withString:@"-"];
    }
    
    return workingString;
    
}

-(bool) range:(NSRange) range ContainsLocation:(NSInteger) location
{
    if(range.location <= location && range.location+range.length >= location)
    {
        return true;
    }
    
    return false;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int charCount = [newString length];
    
    //Backspace Detection
    if([self backSpaceDetection: string fieldID: textField.tag])
        return YES;
    
    if(textField.tag == 1000)
    {
        NSCharacterSet *numSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789+"];
        if ([newString rangeOfCharacterFromSet:[numSet invertedSet]].location != NSNotFound
            || [string rangeOfString:@"+"].location != NSNotFound
            || charCount > 4) {
            return NO;
        }
        
        if (![string isEqualToString:@""])
        {
            if (charCount == 1)
            {
                newString = [NSString stringWithFormat:@"+%@", newString];
            }
        }
        textField.text = newString;
        textField.text = [textField.text substringToIndex: [textField.text length] - 1];
        return YES;
    }
    
    //calculate new length
    NSInteger moddedLength = textField.text.length-(range.length-string.length);
    
    // max size.
    if (moddedLength >= MAX_PHONE_NUMBER) {
        return NO;
    }
    
    // Reject non-number characters
    if (range.length == 0 &&![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]]) {
        return NO;
    }
    
    // Auto-add hyphen before appending 1st, 4th or 8th digit
    if ([self range:range ContainsLocation:0] || [self range:range ContainsLocation:4] || [self range:range ContainsLocation:8]) {
        textField.text = [self formatPhoneString:[textField.text stringByReplacingCharactersInRange:range withString:string]];
        return NO;
    }

    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    IMUserVerificationViewController *controller = segue.destinationViewController;
    //IMUserVerificationViewController *controller = (IMUserVerificationViewController *)[navigationController topViewController];
    controller.phoneNumber = phoneNumberField.text;
    if([countryCodeField.text isEqualToString: @""])
        controller.countryCode = countryCodeField.placeholder;
    else
        controller.countryCode = countryCodeField.text;
}

- (void)displayVerificationScreen
{
    [self performSegueWithIdentifier:@"IMUserVerificationViewController" sender:self];
}

#pragma mark UIScrollview Delegate methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    DebugLog(@"content offset y = %f", scrollView.contentOffset.y);
    if(scrollView.contentOffset.y > 20)
        return;
}

#pragma mark - NetManager Delegate methods
//This delegate gets called on success
-(void)netManager:(CMNetManager*)netManager didReceiveResponse:(NSDictionary*)dictionary
{
    [progressView hide: YES];
    DebugLog(@"Response = %@", dictionary);
    if ([dictionary objectForKey:@"success"] && [[dictionary objectForKey:@"success"] integerValue] == 0)
    {
        switch (netManager.tag)
        {
            case E_REGISTER_SERVICE:
            {
                int code = [[[dictionary objectForKey: @"error"] objectForKey: @"errorCode"] intValue];
                switch (code) {
                    case 1003:
                    {
                        UIAlertView* registrationAlertView = [[UIAlertView alloc] initWithTitle: @"" message:NSLocalizedString(@"ALREADY REGISTERED", @"") delegate:self cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
                        registrationAlertView.tag = 10000;
                        [registrationAlertView show];
                    }break;
                    
                    case 1015:
                    {
                        [self displayVerificationScreen];
                    }break;
                        
                    default:
                        break;
                }
            }break;
                
            default:
                break;
        }
    }
    else
    {
    switch (netManager.tag)
    {
        case E_REGISTER_SERVICE:
            [self displayVerificationScreen];
            break;
            
        default:
            break;
    }
    }
}

-(void)netManager:(CMNetManager*)netManager didReceiveResponseString:(NSString*)response
{
    
}

//This delegate gets called on failure
-(void)netManager:(CMNetManager*)netManager didFailWithError:(NSError*)error
{
    [progressView hide: YES];
}

-(void)netManager:(CMNetManager *)netManager didGetSuccessToConnectWithServer:(NSError *)error
{
    
}

//This delegate gets called if internet connection is not available
-(void)netManager:(CMNetManager *)netManager didFailToConnectWithServer:(NSError *)error
{
    [progressView hide: YES];
    [IMUtils noInternetAvailable];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
