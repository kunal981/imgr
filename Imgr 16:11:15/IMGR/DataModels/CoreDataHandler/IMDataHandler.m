//
//  IMDataHandler.m
//  IMGR
//
//  Created by akram on 17/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMDataHandler.h"
#import "CMCoreDataHandler.h"

@implementation IMDataHandler
@synthesize managedObjectContext;

static IMDataHandler *sharedInstance;

+ (IMDataHandler*)sharedInstance
{
    @synchronized(self)
    {
        if(!sharedInstance)
        {
            sharedInstance = [[IMDataHandler alloc] init];
            
        }
    }
    return sharedInstance;
}


+(id)alloc
{
    @synchronized(self)
    {
        NSAssert(sharedInstance == nil, @"Attempted to allocate a second instance of a singleton FAWDataController.");
        sharedInstance = [super alloc];
    }
    
    return sharedInstance;
}

- (NSFetchRequest *)getBasicRequestForEntityName: (NSString *)entityName
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext: self.managedObjectContext];
    [request setEntity:entity];
    
    return request;
}
- (NSFetchRequest *)getBasicRequestForContact: (NSString *)entityName
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext: [[CMCoreDataHandler sharedInstance] backgroundContext]];
    [request setEntity:entity];
    
    return request;
}

- (IMContacts*)getContactInfoStored: (NSString*)phoneNumber
{
//    self.managedObjectContext = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    
    NSFetchRequest *request = [self getBasicRequestForContact:@"IMContacts"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"phone_number_2 == %@", phoneNumber];
    [request setPredicate:predicate];
    
    NSError* error      = nil;
    NSArray* results    = [[[CMCoreDataHandler sharedInstance] backgroundContext] executeFetchRequest:request error:&error];
    IMContacts* contact = nil;
    if (!error && [results count] > 0) {
        
        contact = [results objectAtIndex:0];
    }
    if (error) {
        NSLog(@"fetch request error = %@", [error localizedDescription]);
    }
    NSLog(@"%@",contact);
    return contact;
}

- (IMPromos*)getPromoInfoStoredInBackground: (NSString*)promo_id
{
    self.managedObjectContext = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    NSManagedObjectContext *backgroundContext = [[CMCoreDataHandler sharedInstance] backgroundContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMPromos" inManagedObjectContext: backgroundContext];
    [request setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"promo_id == %@", promo_id];
    [request setPredicate:predicate];
    
    NSError* error      = nil;
    NSArray* results    = [backgroundContext executeFetchRequest:request error:&error];
    
    IMPromos* promo     = nil;
    if (!error && [results count] > 0) {
        
        promo = [results objectAtIndex:0];
    }
    if (error) {
        NSLog(@"fetch request error = %@", [error localizedDescription]);
    }
    
    return promo;
}

- (IMContacts*)parseContacts: (NSDictionary*)contactInfo
{
    IMContacts* contact = [self getContactInfoStored:[contactInfo objectForKey:@"phone_number_2"]];
    NSLog(@"%@",contact);
    
    if (!contact) {
        contact = [NSEntityDescription insertNewObjectForEntityForName:@"IMContacts" inManagedObjectContext: [CMCoreDataHandler sharedInstance].backgroundContext];
    }
    
    
    //store contact info
    if ([contactInfo objectForKey:@"phone_number"] && ![[contactInfo objectForKey:@"phone_number"] isKindOfClass:[NSNull class]]) {
        contact.phone_number        = [contactInfo objectForKey: @"phone_number"];
    }
    
    if ([contactInfo objectForKey:@"phone_number_2"] && ![[contactInfo objectForKey:@"phone_number_2"] isKindOfClass:[NSNull class]]) {
        contact.phone_number_2        = [contactInfo objectForKey: @"phone_number_2"];
    }
    
    if ([contactInfo objectForKey:@"first_name"] && ![[contactInfo objectForKey:@"first_name"] isKindOfClass:[NSNull class]]) {
        contact.first_name        = [contactInfo objectForKey: @"first_name"];
    }
    
    if ([contactInfo objectForKey:@"last_name"] && ![[contactInfo objectForKey:@"last_name"] isKindOfClass:[NSNull class]]) {
        contact.last_name        = [contactInfo objectForKey: @"last_name"];
    }
    
    NSString *sectionName = @"#";
    
    if ([contactInfo objectForKey:@"last_name"] && ![[contactInfo objectForKey:@"last_name"] isKindOfClass:[NSNull class]]) {
        
        if ([[contactInfo objectForKey: @"last_name"] length]) {
            unichar first = [[contactInfo objectForKey: @"last_name"] characterAtIndex:0];
            if ((first >= 'a' && first <= 'z') || (first >='A' && first <= 'Z')) {
                sectionName = [[NSString stringWithCharacters:&first length:1] lowercaseString] ;
            }
        }
    }


    if ([contactInfo objectForKey:@"first_name"] && ![[contactInfo objectForKey:@"first_name"] isKindOfClass:[NSNull class]]) {
        
        if ([[contactInfo objectForKey: @"first_name"] length]) {
            unichar first = [[contactInfo objectForKey: @"first_name"] characterAtIndex:0];
            if ((first >= 'a' && first <= 'z') || (first >='A' && first <= 'Z')) {
                sectionName = [[NSString stringWithCharacters:&first length:1] lowercaseString] ;
            }
        }
    }
    
    
    contact.index_character = sectionName;
    
    
    if ([contactInfo objectForKey:@"email"] && ![[contactInfo objectForKey:@"email"] isKindOfClass:[NSNull class]]) {
        contact.email        = [contactInfo objectForKey: @"email"];
    }
    
    if ([contactInfo objectForKey:@"is_blocked"] && ![[contactInfo objectForKey:@"is_blocked"] isKindOfClass:[NSNull class]]) {
        contact.is_blocked = [contactInfo objectForKey: @"is_blocked"];
    }
    
    if ([contactInfo objectForKey:@"is_imgr_user"] && ![[contactInfo objectForKey:@"is_imgr_user"] isKindOfClass:[NSNull class]]) {
        contact.is_imgr_user        = [contactInfo objectForKey: @"is_imgr_user"];
    }
    
    if ([contactInfo objectForKey:@"is_invited"] && ![[contactInfo objectForKey:@"is_invited"] isKindOfClass:[NSNull class]]) {
        contact.is_invited        = [contactInfo objectForKey: @"is_invited"];
    }
    
    if ([contactInfo objectForKey:@"user_id"] && ![[contactInfo objectForKey:@"user_id"] isKindOfClass:[NSNull class]]) {
        contact.user_id        = [contactInfo objectForKey: @"user_id"];
    }
    if ([contactInfo objectForKey:@"contactID"] && ![[contactInfo objectForKey:@"contactID"] isKindOfClass:[NSNull class]]) {
        contact.contact_id      = [contactInfo objectForKey: @"contactID"];
    }
    return contact;
}

- (void)deleteDeadContacts
{
    //self.managedObjectContext = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    
    NSFetchRequest *request = [self getBasicRequestForContact:@"IMContacts"];
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.isAvailabledInAddressBook == %d", NO];
    //[request setPredicate:predicate];
    
    NSError* error      = nil;
    NSArray* results    = [[[CMCoreDataHandler sharedInstance] backgroundContext] executeFetchRequest:request error:&error];
    IMContacts* contact = nil;
    
    for(contact in results)
    {
        if(![contact.isAvailabledInAddressBook boolValue])
            [[[CMCoreDataHandler sharedInstance] backgroundContext] deleteObject: contact];
        else
            contact.isAvailabledInAddressBook = [NSNumber numberWithBool: NO];
    }
}

- (void)parsePromos: (NSDictionary*)promoInfo promoType: (int)promoType
{
    IMPromos* promo = [self getPromoInfoStoredInBackground:[promoInfo objectForKey:@"promo_id"]];
    
    if (!promo) {
        promo = [NSEntityDescription insertNewObjectForEntityForName:@"IMPromos" inManagedObjectContext: [[CMCoreDataHandler sharedInstance] backgroundContext]];
        promo.is_enabled = [NSNumber numberWithBool:YES];
    }
    
    //store contact info
    if ([promoInfo objectForKey:@"promo_id"] && ![[promoInfo objectForKey:@"promo_id"] isKindOfClass:[NSNull class]]) {
        promo.promo_id        = [NSNumber numberWithInt: [[promoInfo objectForKey: @"promo_id"] intValue]];
    }
    
    if ([promoInfo objectForKey:@"promo_name"] && ![[promoInfo objectForKey:@"promo_name"] isKindOfClass:[NSNull class]]) {
        promo.promo_name        = [promoInfo objectForKey: @"promo_name"];
    }
    
    NSString *sectionName = @"#";
    if ([promoInfo objectForKey:@"promo_name"] && ![[promoInfo objectForKey:@"promo_name"] isKindOfClass:[NSNull class]]) {
        if ([[promoInfo objectForKey: @"promo_name"] length]) {
            unichar first = [[promoInfo objectForKey: @"promo_name"] characterAtIndex:0];
            if ((first >= 'a' && first <= 'z') || (first >='A' && first <= 'Z')) {
                sectionName = [[NSString stringWithCharacters:&first length:1] lowercaseString] ;
            }
        }
    }
    promo.index_character = sectionName;

    if ([promoInfo objectForKey:@"promo_message_header"] && ![[promoInfo objectForKey:@"promo_message_header"] isKindOfClass:[NSNull class]]) {
        promo.promo_message        = [promoInfo objectForKey: @"promo_message_header"];
    }
    
    if ([promoInfo objectForKey:@"promo_link"] && ![[promoInfo objectForKey:@"promo_link"] isKindOfClass:[NSNull class]]) {
        promo.promo_link        = [promoInfo objectForKey: @"promo_link"];
    }
    
    if ([promoInfo objectForKey:@"promo_link_text"] && ![[promoInfo objectForKey:@"promo_link_text"] isKindOfClass:[NSNull class]]) {
        promo.promo_link_text = [promoInfo objectForKey: @"promo_link_text"];
    }
    
    if ([promoInfo objectForKey:@"promo_image"] && ![[promoInfo objectForKey:@"promo_image"] isKindOfClass:[NSNull class]]) {
        promo.promo_image        = [promoInfo objectForKey: @"promo_image"];
    }
    
    if ([promoInfo objectForKey:@"promo_creation_date"] && ![[promoInfo objectForKey:@"promo_creation_date"] isKindOfClass:[NSNull class]]) {
        promo.promo_creation_date        = [promoInfo objectForKey: @"promo_creation_date"];
    }
    
    if ([promoInfo objectForKey:@"is_enabled"] && ![[promoInfo objectForKey:@"is_enabled"] isKindOfClass:[NSNull class]]) {
        promo.is_enabled        = [NSNumber numberWithInt: [[promoInfo objectForKey: @"is_enabled"] intValue]];
    }
    
    if ([promoInfo objectForKey:@"status"] && ![[promoInfo objectForKey:@"status"] isKindOfClass:[NSNull class]]) {
        promo.status        = [promoInfo objectForKey: @"status"];
    }
    
    if ([promoInfo objectForKey:@"promo_type"] && ![[promoInfo objectForKey:@"promo_type"] isKindOfClass:[NSNull class]]) {
        promo.promo_type        = [NSNumber numberWithInt:[[promoInfo objectForKey:@"promo_type"] intValue]];
    }

//    if ([promoInfo objectForKey:@"is_deleted"] && ![[promoInfo objectForKey:@"is_deleted"] isKindOfClass:[NSNull class]]) {
//        promo.markedAsDeleted        = [NSNumber numberWithInt:[[promoInfo objectForKey:@"is_deleted"] intValue] || ![[promoInfo objectForKey:@"is_enabled"] intValue] ];
//    }
    
    if ([promoInfo objectForKey:@"is_deleted"] && ![[promoInfo objectForKey:@"is_deleted"] isKindOfClass:[NSNull class]]) {
        promo.markedAsDeleted        = [NSNumber numberWithInt:[[promoInfo objectForKey:@"is_deleted"] intValue]];
    }


}

- (IMPromos*)fetchPromoWithId: (int)promoId
{
    self.managedObjectContext = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"IMPromos"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"promo_id == %i", promoId];
    [request setPredicate:predicate];
    
    NSError* error      = nil;
    NSArray* results    = [self.managedObjectContext executeFetchRequest:request error:&error];
    IMPromos* promo     = nil;
    if (!error && [results count] > 0) {
        
        promo = [results objectAtIndex:0];
    } else if (error) {
        NSLog(@"fetch request error = %@", [error localizedDescription]);
    }
    else
    {
        //[[IMAppDelegate sharedDelegate] downloadPromoWithId:promoId];
    }
    return promo;
}

- (IMContacts*)fetchContactWithUserJid: (NSString*)userJid
{
    self.managedObjectContext = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"IMContacts"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id == %@", userJid];
    [request setPredicate:predicate];
    
    NSError* error      = nil;
    NSArray* results    = [self.managedObjectContext executeFetchRequest:request error:&error];
    IMContacts* contact = nil;
    if (!error && [results count] > 0) {
        
        contact = [results objectAtIndex:0];
    }
    if (error) {
        NSLog(@"fetch request error = %@", [error localizedDescription]);
    }
    
    return contact;
}

- (NSArray*)fetchAllContacts
{
    self.managedObjectContext = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"IMContacts"];
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id == %@", userJid];
    //[request setPredicate:predicate];
    
    NSError* error      = nil;
    NSArray* results    = [self.managedObjectContext executeFetchRequest:request error:&error];
    return results;
}

- (IMContacts*)fetchContactWithPhoneNumber: (NSString*)phoneNumber
{
    self.managedObjectContext = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"IMContacts"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"phone_number_2 == %@", phoneNumber];
    [request setPredicate:predicate];
    
    NSError* error      = nil;
    NSArray* results    = [self.managedObjectContext executeFetchRequest:request error:&error];
    IMContacts* contact = nil;
    if (!error && [results count] > 0) {
        
        contact = [results objectAtIndex:0];
    }
    if (error) {
        NSLog(@"fetch request error = %@", [error localizedDescription]);
    }
    
    return contact;
}

- (IMContacts*)fetchContactInBackgroundWithPhoneNumber: (NSString*)phoneNumber
{
    
    NSFetchRequest *request = [self getBasicRequestForContact:@"IMContacts"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"phone_number_2 == %@", phoneNumber];
    [request setPredicate:predicate];
    
    NSError* error      = nil;
    NSArray* results    = [[CMCoreDataHandler sharedInstance].backgroundContext executeFetchRequest:request error:&error];
    IMContacts* contact = nil;
    if (!error && [results count] > 0) {
        
        contact = [results objectAtIndex:0];
    }
    if (error) {
        NSLog(@"fetch request error = %@", [error localizedDescription]);
    }
    
    return contact;
}

- (NSArray*)fetchIMGRContacts
{
    self.managedObjectContext = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"IMContacts"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"is_imgr_user == %d", 1];
    [request setPredicate:predicate];
    
    NSError* error      = nil;
    NSArray* results    = [self.managedObjectContext executeFetchRequest:request error:&error];
    return results;
}

- (BOOL)deletePromo: (IMPromos*)promo
{
    [self.managedObjectContext deleteObject: promo];
    NSError *error = nil;
    
    if (![self.managedObjectContext save:&error])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (BOOL)deleteContact: (IMContacts*)contact
{
    [self.managedObjectContext deleteObject: contact];
    NSError *error = nil;
    
    if (![self.managedObjectContext save:&error])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}
#pragma mark - Save Database
- (BOOL)saveDatabase
{
    [[CMCoreDataHandler sharedInstance] performAndSaveDataBaseTaskInBackground:^{
        
        
    }];
//    NSError *error = nil;
//    if(![self.managedObjectContext save: &error])
//    {
//        NSLog(@"Unresolved error save 2 post %@, %@", error, [error userInfo]);
//        return NO;
//    }
    return YES;
}

+ (IMPromos *) fetchPromoForPromoID:(NSString *)promo_ID;
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    
    if(!moc){
        return nil;
    }
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMPromos"
                                                  inManagedObjectContext:moc];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"promo_name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sd2, nil];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:sortDescriptors];
    NSPredicate *predicate = nil;
    predicate = [NSPredicate predicateWithFormat:@"promo_id == %@",promo_ID];
        
    [fetchRequest setPredicate:predicate];
    

    NSError *error = nil;
    NSArray *fetchedPromos = [moc executeFetchRequest:fetchRequest error:&error];
    if (nil != error)
    {
        DDLogError(@"Error performing fetch: %@", error);
    }
    else if (1 == fetchedPromos.count)
    {
        return [fetchedPromos lastObject];
    }
    else if (0 == fetchedPromos.count && promo_ID.integerValue)//Exclude 0 promo id
    {
        [[IMAppDelegate sharedDelegate] downloadPromoWithId:promo_ID.integerValue];
    }
    return nil;
}

- (NSArray *)fetchPersonalPromos
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMPromos"
                                                  inManagedObjectContext:moc];
        
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"index_character" ascending:YES];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"promo_name" ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, sd2, nil];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:sortDescriptors];
        

        NSPredicate* predicate = [NSPredicate predicateWithFormat: @"promo_type == %d&& markedAsDeleted == %d", 1,0];
        [fetchRequest setPredicate: predicate];
    return [moc executeFetchRequest:fetchRequest error:nil];
}

- (NSArray *)fetchThirdPartyPersonalPromos
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMPromos"
                                              inManagedObjectContext:moc];
    
    NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"index_character" ascending:YES];
    NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"promo_name" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, sd2, nil];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat: @"promo_type == %d", 99];
    [fetchRequest setPredicate: predicate];
    return [moc executeFetchRequest:fetchRequest error:nil];
}

- (NSArray*)disabledPromos: (int)promoType {
    
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMPromos"
                                              inManagedObjectContext:moc];
    
    NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"index_character" ascending:YES];
    NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"promo_name" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, sd2, nil];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat: @"is_enabled == %d && promo_type == %d", 0, promoType];
    [fetchRequest setPredicate: predicate];
    return [moc executeFetchRequest:fetchRequest error:nil];
}

@end
