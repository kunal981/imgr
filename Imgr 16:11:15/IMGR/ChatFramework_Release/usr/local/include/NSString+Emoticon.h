//
//  NSString+Emoticon.h
//  IMGR
//
//  Created by Satendra Singh on 4/1/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Emoticon)
-(NSString *)emoticonizedString;
+(NSString *)emoticonizedString:(NSString *)aString;
@end
