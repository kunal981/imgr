//
//  DownloadPictureOperation.h
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Operation.h"
#import "CMMessageArchiving_Message_CoreDataObject.h"

@class DownloadPictureOperation;
@protocol DownloadPictureOperationDelegate <NSObject>

@optional

-(void)downloadPicture:(DownloadPictureOperation*)operation didDownloadImageAtPath:(NSString *)filePath;

@end

@interface DownloadPictureOperation : Operation <DownloadPictureOperationDelegate> {
	id storedObject;
    
}
@property (nonatomic, retain) CMMessageArchiving_Message_CoreDataObject *mediaMessage;

@property (nonatomic, assign) id<DownloadPictureOperationDelegate> delegate;
- (NSString*)pictureURL;
- (id) initWithURL:(NSString*)url;
- (id) initWithURL:(NSString*)url message:(CMMessageArchiving_Message_CoreDataObject *)message;

@end

@interface UIImage (imageWithShadow)
- (UIImage *)imageWithShadow;
- (UIImage *)imageWithRoundedRect:(double)radius;

@end
