//
//  XMPPResourceCoreDataStorageObject.h
//  iPhoneXMPP
//
//  Created by Saurabh Verma on 20/12/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "XMPPResource.h"

@class XMPPStream;
@class XMPPPresence;
@class CMUserCoreDataStorageObject;

@interface CMResourceCoreDataStorageObject : NSManagedObject <XMPPResource>

@property (nonatomic, strong) XMPPJID *jid;
@property (nonatomic, strong) XMPPPresence *presence;

@property (nonatomic, assign) int priority;
@property (nonatomic, assign) int intShow;

@property (nonatomic, strong) NSString * jidStr;
@property (nonatomic, strong) NSString * presenceStr;

@property (nonatomic, retain) NSString * streamBareJidStr;

@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * show;
@property (nonatomic, strong) NSString * status;

@property (nonatomic, strong) NSDate * presenceDate;

@property (nonatomic, strong) NSNumber * priorityNum;
@property (nonatomic, strong) NSNumber * showNum;

@property (nonatomic, strong) CMUserCoreDataStorageObject *primaryResourceInverse;
@property (nonatomic, strong) CMUserCoreDataStorageObject *user;

+ (id)insertInManagedObjectContext:(NSManagedObjectContext *)moc
                      withPresence:(XMPPPresence *)presence
                  streamBareJidStr:(NSString *)streamBareJidStr;

- (void)updateWithPresence:(XMPPPresence *)presence;

- (NSComparisonResult)compare:(id <XMPPResource>)another;

@end
