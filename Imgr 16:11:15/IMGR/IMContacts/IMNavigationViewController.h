//
//  IMNavigationViewController.h
//  IMGR
//
//  Created by akram on 07/03/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMNavigationViewController : UINavigationController <UIBarPositioningDelegate>

@end
