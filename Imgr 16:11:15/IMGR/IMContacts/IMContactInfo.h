//
//  CMContactInfo.h
//  CMSmoothScrollDemo
//
//  Created by akram on 03/02/14.
//  Copyright (c) 2014 akram. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMContactInfo : NSObject
{
    NSString*       firstName;
    NSString*       lastName;
    NSString*       phoneNumber;
}
@property (nonatomic, retain)NSString* firstName;
@property (nonatomic, retain)NSString* lastName;
@property (nonatomic, retain)NSString* phoneNumber;

@end
