//
//  IMNewContactCell.h
//  IMGR
//
//  Created by akram on 11/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMNewContactCell : UITableViewCell


@property (strong, nonatomic)IBOutlet UILabel* contactNameLabel;
@property (strong, nonatomic)IBOutlet UITextField* editTextField;


+ (IMNewContactCell *)cellLoadedFromNibFile;
+ (NSString *)cellReusableIdentifier;

@end
