//
//  IMNonIMGRContactDetailViewController.m
//  IMGR
//
//  Created by akram on 05/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMNonIMGRContactDetailViewController.h"
#import "IMContacts.h"
#import "IMEditContactViewController.h"
#import "IMDataHandler.h"

@interface IMNonIMGRContactDetailViewController ()

@end

@implementation IMNonIMGRContactDetailViewController
@synthesize contact;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    if(contact.first_name.length && contact.last_name.length)
    {
        nameLabel.text = [NSString stringWithFormat: @"%@ %@", [contact.first_name capitalizedString], [contact.last_name capitalizedString]];

    }
    else if(contact.first_name.length){
        nameLabel.text = [contact.first_name capitalizedString];
    }
    else if(contact.last_name.length)
    {
        nameLabel.text = [contact.last_name capitalizedString];
    }
    else
        nameLabel.text = NSLocalizedString(@"NO NAME", @"");

    phoneLabel.text = [IMUtils phoneNumberForJid:contact.user_id];
    
    if(!contact.email.length)
        emailLabel.text = NSLocalizedString(@"NO EMAIL", @"");
    else
        emailLabel.text = contact.email;
    
    phoneLabel.text = contact.phone_number;
    
    if([contact.is_invited boolValue])
        [sendButton setTitle: NSLocalizedString(@"RESEND INVITE", @"") forState: UIControlStateNormal];
    else
        [sendButton setTitle: NSLocalizedString(@"INVITE IMGR", @"") forState: UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions methods
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    IMEditContactViewController* controller = segue.destinationViewController;
    controller.contact = contact;
    controller.isIMGRUser = NO;
}

- (IBAction)onEditButtonClick:(id)sender
{
    [self performSegueWithIdentifier:@"IMEditContactViewController" sender:self];
}

- (IBAction)onInviteToIMGRButtonClick:(id)sender
{
//    UIActionSheet* changePhotoSheet = [[UIActionSheet alloc] initWithTitle:@"Tell a friend about IMGR via" delegate:(id)self
//                                                         cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles: @"Mail", @"Message", nil];
//    
//    [changePhotoSheet showFromRect:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 0) inView:self.view animated:YES];
    if (contact.email.length) {
        [IMUtils displayActionSheet: self];
    }
    else
    {
        [self inviteViaMessage];
    }
}

- (void)inviteViaEmail
{
    [self sendInvitationToIMGR];
}

- (void)inviteViaMessage
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    
    if([MFMessageComposeViewController canSendText]) {
        
        NSString* bodyOfMessage = NSLocalizedString(@"MESSAGE BODY", @"");
        controller.body = bodyOfMessage;
        controller.recipients = [NSArray arrayWithObject: contact.phone_number];
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated: YES completion: ^{}];
        
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated: YES completion: ^{}];
    
    if (result == MessageComposeResultCancelled) {
        
        NSLog(@"Message cancelled");
        
    } else if (result == MessageComposeResultSent) {
        
        NSLog(@"Message sent");
        contact.is_invited = [NSNumber numberWithBool:YES];
        [sendButton setTitle: NSLocalizedString(@"RESEND INVITE", @"") forState: UIControlStateNormal];
        [[IMDataHandler sharedInstance] saveDatabase];

    }
    
}
- (void)sendInvitationToIMGR
{
    MFMailComposeViewController* mailCompose = [[MFMailComposeViewController alloc] init];
    if([MFMailComposeViewController canSendMail])
    {
        mailCompose.mailComposeDelegate = self;
        [mailCompose setSubject:NSLocalizedString(@"NEW MESSAGE", @"")];
        
        NSString* bodyStr = NSLocalizedString(@"MESSAGE BODY", @"");
        //bodyStr = [bodyStr stringByAppendingString: [NSString stringWithFormat: @"\n\nGet it now from\n%@", @"applinkfromweb.com"]];
        [mailCompose setToRecipients: [NSArray arrayWithObject: contact.email]];
        [mailCompose setMessageBody: bodyStr isHTML: NO];
        
        [self.navigationController presentViewController:mailCompose animated:YES completion: ^{}];

    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            
            break;
            
        case MFMailComposeResultSaved:
            break;
            
        case MFMailComposeResultSent:
        {
            contact.is_invited = [NSNumber numberWithBool:YES];
            [sendButton setTitle: NSLocalizedString(@"RESEND INVITE", @"") forState: UIControlStateNormal];
            [[IMDataHandler sharedInstance] saveDatabase];

        }break;
            
        case MFMailComposeResultFailed:
            break;
            
        default:
            break;
    }
    [controller dismissViewControllerAnimated: YES completion:^{}];
}

#pragma mark UIActionSheet Delegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self inviteViaEmail];
            break;
            
        case 1:
            [self inviteViaMessage];
            break;
            
        case 2:
            
            break;
            
        default:
            break;
    }
}

@end
