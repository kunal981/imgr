//
//  Operation.h
//  
//
//  Created by Admin on 31.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMNetManager.h"

#import "IMOperationQueue.h"

@interface IMOperation : NSOperation {
	BOOL _isExecuting;
    BOOL _isFinished;
	IMOperationQueue *ownerQueue;
}

@property (readonly) BOOL isExecuting;
@property (readonly) BOOL isFinished;

@property (assign) IMOperationQueue *ownerQueue;

- (void)cancelOperations:(NSString *)errorMessage;

- (void)finish;

- (void)createTask;

@end
