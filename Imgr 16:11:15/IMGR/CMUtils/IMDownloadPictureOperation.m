//
//  DownloadPictureOperation.m
//

#import "IMDownloadPictureOperation.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIImage (imageWithShadowOffset)

- (UIImage*)imageWithShadow {
    CGColorSpaceRef colourSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef shadowContext = CGBitmapContextCreate(NULL, self.size.width + 10, self.size.height + 10, CGImageGetBitsPerComponent(self.CGImage), 0, colourSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colourSpace);
	
    CGContextSetShadowWithColor(shadowContext, CGSizeMake(5, -5), 5, [UIColor grayColor].CGColor);
    CGContextDrawImage(shadowContext, CGRectMake(0, 10, self.size.width, self.size.height), self.CGImage);
	
    CGImageRef shadowedCGImage = CGBitmapContextCreateImage(shadowContext);
    CGContextRelease(shadowContext);
	
    UIImage * shadowedImage = [UIImage imageWithCGImage:shadowedCGImage];
    CGImageRelease(shadowedCGImage);
	
    return shadowedImage;
}

- (UIImage *)imageWithRoundedRect:(double)radius {
	return nil;
}

@end

@interface IMDownloadPictureOperation ()
{
    NSURLConnection* connection;
    UIViewContentMode mode;
    NSString* picURL;
    NSMutableData* responseData;
}
@end

@implementation IMDownloadPictureOperation

@synthesize storedObject, delegate;

- (id) initWithMode:(UIViewContentMode)contentMode picURL:(NSString*)url{
	mode = contentMode;
    picURL = [url retain];
	return [super init];
}
- (void)createTask {
	if (![self.ownerQueue didFailInQueueOperation]) {
        
        NSMutableURLRequest *request = [[self createRequest] retain];
		 connection  = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [connection start];
        _RELEASE(request);
	} else {
		[self finish];
	}
	
}

- (NSMutableURLRequest*) createRequest{
    
    NSMutableURLRequest *request= [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:picURL]];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:TIMEOUT_INTERVAL];
    
    return request;
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    DebugLog(@"Failed with error %@", [error localizedDescription]);
}

#pragma mark - NSURLConnectionDataDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    //DebugLog(@"Received Response");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(!responseData){
        responseData = [[NSMutableData alloc] initWithData:data];
    }
    else
        [responseData appendData:data];
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    

    if([self.storedObject objectForKey:@"imageView"]){
     
        // Set image
        if ([[self.storedObject objectForKey:@"imageView"] isKindOfClass:[UIImageView class]]) {
            
            UIImageView *imageView = (UIImageView *)[self.storedObject objectForKey:@"imageView"];
            UIImage *image = nil;
            image = [UIImage imageWithData:responseData];
            imageView.alpha = 1.0;
            imageView.image = nil;
            imageView.clipsToBounds = YES;
            imageView.image = image;
            imageView.contentMode = mode;
        }
        else if ([[self.storedObject objectForKey:@"imageView"] isKindOfClass:[UIButton class]]){
            
            UIButton *imageView = (UIButton *)[self.storedObject objectForKey:@"imageView"];
            UIImage *image = nil;
            image = [UIImage imageWithData:responseData];
            imageView.alpha = 1.0;
            [imageView setBackgroundImage:image forState:UIControlStateNormal];
        }
        
    }
    
    id model = [self.storedObject objectForKey:@"model"];
    NSString *imgKey = (NSString *)[self.storedObject objectForKey:@"imageKey"];
    
    if(model && imgKey){
        
        //save image in local directory
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *docsPath = [paths objectAtIndex:0];
        NSString* folderName = [NSString stringWithFormat:@"%@/Images", docsPath];
        
        NSFileManager *NSFm= [NSFileManager defaultManager];
        BOOL isDir=YES;
        
        NSMutableString *fileName = [self.storedObject objectForKey: @"imageName"];
        if(![NSFm fileExistsAtPath:folderName isDirectory:&isDir])
            if(![NSFm createDirectoryAtPath:folderName withIntermediateDirectories:YES attributes:nil error:nil])
                NSLog(@"Error: Create folder failed");
        
        //NSDate *dateForPictureName = [NSDate date];
        //NSTimeInterval timeInterval = [dateForPictureName timeIntervalSince1970];
        //[NSMutableString stringWithFormat:@"%f", timeInterval];
        //NSRange thePeriod = [fileName rangeOfString:@"."]; //Epoch returns with a period for some reason.
        //[fileName deleteCharactersInRange:thePeriod];
        NSString* filePath = [folderName stringByAppendingPathComponent:[fileName lastPathComponent]];
        if([responseData writeToFile:filePath atomically:YES]){
            
            // Set image url from directory
            //SS it is not required as we are already referring other path
//            [model setValue:[NSString stringWithFormat:@"%@", fileName] forKey:imgKey];
        }
        else{
            
        }
    }
    
    if (delegate && [delegate respondsToSelector:@selector(didDownloadImage)]) {
        [delegate didDownloadImage];
    }
    
    [self finish];
}
-(void)finish{
    
    self.delegate = nil;
    [super finish];
}
-(void)cancel{
    DebugLog(@"cancel operation");
    self.delegate = nil;
    [super cancel];
}
//- (void)netManagerDidFinishLoading:(NetManager *)netManager response:(NSMutableData *)responseData {
//	
//    UIImageView *arrayView;
//    
//    if([netManager.storedObject objectForKey:@"imageView"])
//        arrayView = (UIImageView *)[netManager.storedObject objectForKey:@"imageView"];
//    
//	
//	id model = [netManager.storedObject objectForKey:@"model"];
//	NSString *imgKey = (NSString *)[netManager.storedObject objectForKey:@"imageKey"];
//	if (netManager.error) {
////
//	} else {
//
//		UIImage *image = nil;
//		image = [UIImage imageWithData:responseData];
//		
//		arrayView.image = image;
//		arrayView.contentMode = self.mode;
//		
//		if (model && imgKey) {
//			[model setValue:responseData forKey:imgKey];
//        }
//		
//	}
//    NSError *error;
//    if (![ [(ModizoAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext] save:&error])
//	{
//		NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
//	}
//	[netManager release];
//	[self finish];
//}

- (void)dealloc {
    _RELEASE(picURL)
    _RELEASE(connection);
    _RELEASE(responseData);
    delegate = nil;
	[storedObject release];
    [super dealloc];
}

@end
