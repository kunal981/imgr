//
//  CMNetManager.h
//  
//
//  Created by Copper Mobile on 5/28/13.
//  Copyright (c) 2013 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <dispatch/dispatch.h>


#define TIMEOUT_INTERVAL 60

typedef enum {
	SERIAL = 0,
	CONCURRENT,
} QueueType;

typedef enum {
    POST = 10,
    GET,
} HttpMethod;

@class CMNetManager;

/***************	Basic protocols		***************/

@protocol CMNetManagerDelegate <NSObject>

@required
//This delegate gets called on success
-(void)netManager:(CMNetManager*)netManager didReceiveResponse:(NSDictionary*)dictionary;
-(void)netManager:(CMNetManager*)netManager didReceiveResponseString:(NSString*)response;

//This delegate gets called on failure
-(void)netManager:(CMNetManager*)netManager didFailWithError:(NSError*)error;

//This delegate gets called if internet connection is not available
-(void)netManager:(CMNetManager *)netManager didFailToConnectWithServer:(NSError *)error;

-(void)netManager:(CMNetManager *)netManager didGetSuccessToConnectWithServer:(NSError *)error;

@optional
- (void)netManager:(CMNetManager *)netManager didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite;
@end

/***********	Base class		***********/

@interface CMNetManager : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate, NSStreamDelegate>

@property (nonatomic) int tag;
@property (nonatomic, retain) NSString* attachmentName;
@property (nonatomic,assign) id<CMNetManagerDelegate> delegate;
@property (nonatomic,assign) id<CMNetManagerDelegate> failDelegate;
@property (nonatomic, retain) UIImage *thumbnailImage;
// Initializes CMNetManager object, takes port request paramaters, GCD queue identifier and queuType which could be Serial or Cuncurrent
-(id)initWithURL: (NSString*)postUrl parameters: (NSDictionary*)postParameters queueIdentifier:(const char*)thequeueIdentifier queueType: (QueueType)queueType httpMethod:(HttpMethod)postMethod;

// Call this method to start request. 
- (void) startRequest;

// Call this method to stop request.
- (void) stopRequest;

@end

